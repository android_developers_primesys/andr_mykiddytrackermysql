package com.primesys.mitra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.db.DBHelper;
import com.example.dto.UserScoreDTO;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FinalQuizScore extends Activity {
	Button ststic;
	ProgressBar levelprogress;
	TextView score,rank,name,ov_rank,ov_score;
	private CircleImageView boy_img;
	private String Result_req;
	int size,Levelscore;
	UserScoreDTO  userinfo=new UserScoreDTO();
Context context=FinalQuizScore.this;
private RequestQueue reuestQueue;
private StringRequest stringRequest;
private String TAG="REquest";
ArrayList<Integer> scorelist=new ArrayList<Integer>();
ArrayList<Integer> attempt=new ArrayList<Integer>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_final_quiz_score);
		findviewById();
		
		
		GetUsetData();

		Intent intent = getIntent();    

		Result_req =intent.getStringExtra("Result");
		size=intent.getIntExtra("Size",0);
		Levelscore=intent.getIntExtra("LevelScore",0);

		System.out.println("========="+Common.Level_marks);
		score.setText(Levelscore+"");
		SetLevelavgProgress();
	

		if(Common.connectionStatus)  
		{
			Calculate_result();
		}
		//report complaint 
		ststic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent feedIntent=new Intent(context,Res.class);
				feedIntent.putExtra("Scorelist",scorelist);
				startActivity(feedIntent);
			}
		});
		
		
	}
	
public void SetLevelavgProgress() {
		
		// Start lengthy operation in a background thread
		levelprogress.setMax(Common.maximum_marks*size);
		 Thread levelprogressTimeThread = new Thread() {
		    @Override
		    public void run() {
		        
		            if(Common.Level_marks<=Common.maximum_marks*size) {
		            	
		            	 levelprogress.setProgress(Levelscore);
		            	 
			             
		            }
		        }
		      };

		
			levelprogressTimeThread.start();
		}
		

	private void findviewById() {
		ststic=(Button) findViewById(R.id.btn_stat);
		score=(TextView) findViewById(R.id.overScore);
		rank=(TextView) findViewById(R.id.rank);
		levelprogress=(ProgressBar) findViewById(R.id.avg_progressbar);
		boy_img=(CircleImageView) findViewById(R.id.image_boy);
		name=(TextView) findViewById(R.id.labl_username);
		 Drawable draw=getResources().getDrawable(R.drawable.level_progresbar);
			// set the drawable as progress drawable
		 levelprogress.setProgressDrawable(draw);
		 ov_rank=(TextView) findViewById(R.id.ov_rank1);
		 ov_score=(TextView) findViewById(R.id.ov_score1);
	}
	
	 // make volley request 
 	void Calculate_result()
 	{
 		String url="http://192.168.1.110:8080/MyKiddyTrackerAPI/MyKiddy/ExamAPI/QuizzTransaction";

 		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
 		final ProgressDialog pDialog = new ProgressDialog(context);
 		pDialog.setTitle("Progress wait.......");
 		pDialog.setCancelable(false);
 		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
 		pDialog.show();
 		//JSon object request for reading the json data
 		stringRequest = new StringRequest(Method.POST,Common.MONGOURL+"ExamAPI/QuizzTransaction",new Response.Listener<String>() {

 			@Override
 			public void onResponse(String response) {


 				parseJSON(response);
 				pDialog.hide();

 			
 			}

 		},
 		new Response.ErrorListener() {


 			@Override
 			public void onErrorResponse(VolleyError error) {
 				pDialog.hide();
 				if(error.networkResponse != null && error.networkResponse.data != null){
 					VolleyError er = new VolleyError(new String(error.networkResponse.data));
 					error = er;
 					System.out.println(error.toString());
 					parseJSON(new String(error.networkResponse.data));
 				}
 			}
 		}) {

 			@Override
 			protected Map<String, String> getParams() {
 				Map<String, String> params = new HashMap<String, String>();

 				params.put("Result",Result_req);

 				System.out.println("REq----cal resulkt------"+Result_req);
 				return params;
 			}

 		};


 		stringRequest.setTag(TAG);
 		// Adding request to request queue
 		reuestQueue.add(stringRequest);
 	}
 	
 	
 	
    public void parseJSON(String result) {

		System.out.println(result);
		try {

			JSONObject jo=new JSONObject(result);
			String point=jo.getString("score");
			JSONArray score=new JSONArray(point);
			for (int i = 0; i < score.length(); i++) {

				scorelist.add(score.getInt(i));
			}
			//Common.showToast("REsult is "+result, context);
			System.out.println("Respo"+result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
 	
	private void GetUsetData() {
		String url="http://192.168.1.110:8080/MyKiddyTrackerAPI/MyKiddy/ExamAPI/UsersOverallScoreDetails";

		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.MONGOURL+"ExamAPI/UsersOverallScoreDetails",new Response.Listener<String>() {
			
			@Override
			public void onResponse(String response) {
			

				parseJSONUserdata(response);
				pDialog.hide();
				
				
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSONUserdata(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
			
				params.put("UserId",Common.userid);
				System.out.println("REq----Userdata------"+params);
				return params;
			}

		};


		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}

	protected void parseJSONUserdata(String response) {
		

		System.out.println("Respo Userdata"+ response);
		try {
			JSONObject jo=new JSONObject(response);
		//	CategoryDashbord cat=new CategoryDashbord();
			//cat.setImage();
			
			userinfo.setName(jo.getString("name"));
			userinfo.setPro_image(jo.getString("pro_image"));
			userinfo.setRank(jo.getString("rank"));
			userinfo.setScore(jo.getString("score"));
			
			setuserdata();
			
			
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
		}
	
	
	}

	
private void setuserdata() {
		
		Bitmap bmp=DBHelper.getInstance(context).getBitMap(Common.userid);
		if(bmp!=null)
			boy_img.setImageBitmap(bmp);
		else {

			try {
				DBHelper.getInstance(context).insertStudentPhoto(Common.bytebitmapconversion(userinfo.getPro_image()),Common.userid);
				bmp=DBHelper.getInstance(context).getBitMap(Common.userid);
				boy_img.setImageBitmap(bmp);

			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		
		name.setText(userinfo.getName());
		rank.setText(userinfo.getRank());		
		ov_rank.setText(userinfo.getRank());
		ov_score.setText(userinfo.getScore());
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.final_quiz_score, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
