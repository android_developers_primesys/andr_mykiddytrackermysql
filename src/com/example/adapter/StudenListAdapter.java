package com.example.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.example.dto.Student;
import com.primesys.mitra.Common;
import com.primesys.mitra.MainPrincipal;
import com.primesys.mitra.R;
import com.primesys.mitra.studentInformation;

public class StudenListAdapter extends ArrayAdapter<Student> {
	Context context;
	int layoutResourceId;
	public static ArrayList<Student> data;
	Intent Studeninfo;
	String flag;
	Animation animation = null;
	public static Boolean Status;
	public static String checkStudentId;
	public StudenListAdapter(Context context, int layoutResourceId,
			ArrayList<Student> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		StudenListAdapter.data=new ArrayList<Student>();
		StudenListAdapter.data = data;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		try{
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(layoutResourceId, parent, false);
			}
				Student s=data.get(position); 
				Button btnEdit = (Button) convertView.findViewById(R.id.studentame);
				btnEdit.setText(s.getFirstName()+" "+s.getLastName());
				final CheckBox chkStd=(CheckBox)convertView.findViewById(R.id.selection);
				chkStd.setChecked(s.getStatus());
				
				chkStd.setTag(position);
				animation = AnimationUtils.loadAnimation(context, R.anim.animlistview);
				chkStd.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						Student user1=data.get((Integer)chkStd.getTag());
						if(chkStd.isChecked()){
							checkStudentId=user1.getParentID();
							user1.setStatus(true);
						}
						else{
							studentInformation.rootSelection.setChecked(false);
							user1.setStatus(false);
						}
						boolean valid=true;
						for(Student su : data){
							if(!su.getStatus()){
								valid=false;
								break;
							}
						}
						if(valid)
							studentInformation.rootSelection.setChecked(true);
					}
				});
				btnEdit.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Student user1=data.get((Integer)chkStd.getTag());
						String photo=user1.getPhoto();
						photo=photo.replace("~", "");
						Common.to=photo;
						Intent intent=new Intent(context,MainPrincipal.class);
						intent.putExtra("to", user1.getParentID());
						intent.putExtra("student", user1.getStudentID());
						intent.putExtra("name",user1.getMiddleName());
						context.startActivity(intent);
						((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
					}
				});
			


		}catch(Exception e)
		{
		}
		animation.setDuration(500);
		convertView.startAnimation(animation);
		return convertView;

	}
}


