package com.primesys.mitra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dto.ResultDTO;
import com.google.gson.Gson;
import com.primesys.mitra.ExamActivity.getquestion;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
public class ResultActivity extends Activity {

	HashMap<String,String> answerhasmap=new HashMap<String, String>();
	Context context=ResultActivity.this;
	private RequestQueue reuestQueue;
	private StringRequest stringRequest;
	private String TAG="GETResult",Result_req="",Result;
	TextView result;
	ArrayList<Integer> scorelist=new ArrayList<Integer>();
	private String[] mMonth = new String[] {
			"1", "2" , "3", "4", "5", "6",
			"7", "8" , "9", "10", "11", "12"
	};
	GraphicalView mchartview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);

		Intent intent = getIntent();    

		Result_req =intent.getStringExtra("Result");


		if(Common.connectionStatus)  
		{
			Calculate_result();
		}

		/*result=(TextView)findViewById(R.id.textResult);
		for (int i = 0; i < FOCUSED_STATE_SET.length; i++) {
			int j = FOCUSED_STATE_SET[i];

		}
		result.setText(Result);*/
		/*//get rating bar object
		RatingBar bar=(RatingBar)findViewById(R.id.ratingBar1); 
		bar.setNumStars(5);
		bar.setStepSize(0.5f);
		//get text view
		TextView t=(TextView)findViewById(R.id.textResult);
		//get score
		Bundle b = getIntent().getExtras();
		int score= b.getInt("score");
		//display score
		bar.setRating(score);
		switch (score)
		{
		case 1:
		case 2: t.setText("Opps, try again bro, keep learning");
		break;
		case 3:
		case 4:t.setText("Hmmmm.. maybe you have been reading a lot of JasaProgrammer quiz");
		break;
		case 5:t.setText("Who are you? A student in JP???");
		break;
		}*/
	}



	// make volley request 
	void Calculate_result()
	{
		String url="http://192.168.1.110:8080/MyKiddyTrackerAPI/MyKiddy/ExamAPI/QuizzTransaction";

		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,url,new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {


				parseJSON(response);
				pDialog.hide();

				openChart();	
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();

				params.put("Result",Result_req);

				System.out.println("REq----------"+Result_req);
				return params;
			}

		};


		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}

	//Respo{"attempts":"[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]","score":"[10, 30, 70, 38, 0, 0, 38, 40, 38, 24, 30, 40, 52, 22, 68, 26, 36, 62, 26, 0, 30, 50, 40, 28, 40, 48, 22, 46, 46, 50, 46, 46, 26]","message":"Your Is playing Level 1 Again Time","error":"false"}


	protected void openChart() {
		int[] x = { 1,2,3,4,5,6,7,8 };
		int[] income = { 20,25,27,30,28,35,37,38};
		/*int[] expense = {2200, 2700, 2900, 2800, 2600, 3000, 3300, 3400 };
		 */
		// Creating an  XYSeries for Income
		XYSeries incomeSeries = new XYSeries("Attempts");
		// Creating an  XYSeries for Income
		/*XYSeries expenseSeries = new XYSeries("Score");*/
		// Adding data to Income and Expense Series
		for(int i=0;i<scorelist.size();i++){
			incomeSeries.add(i, scorelist.get(i));
			/*expenseSeries.add(x[i],expense[i]);*/
		}

		// Creating a dataset to hold each series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		// Adding Income Series to the dataset
		dataset.addSeries(incomeSeries);
		// Adding Expense Series to dataset
		/*	dataset.addSeries(expenseSeries);    	*/


		// Creating XYSeriesRenderer to customize incomeSeries
		XYSeriesRenderer incomeRenderer = new XYSeriesRenderer();
		incomeRenderer.setColor(Color.WHITE);
		incomeRenderer.setPointStyle(PointStyle.DIAMOND);
		incomeRenderer.setFillPoints(true);
		incomeRenderer.setLineWidth(5);
		incomeRenderer.setDisplayChartValues(true);
incomeRenderer.setChartValuesTextSize(10);
//incomeRenderer.setPointStyle(PointStyle.CIRCLE.getPointStyleForName(DISPLAY_SERVICE));

		// Creating a XYMultipleSeriesRenderer to customize the whole chart
		XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
		multiRenderer.setXLabels(0);
		multiRenderer.setChartTitle("Attempts vs Score Chart");
		multiRenderer.setXTitle("Attempt");
		multiRenderer.setYTitle("Score");
		multiRenderer.setChartValuesTextSize(10);

		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.parseColor("#CC6698"));
		multiRenderer.setZoomButtonsVisible(true);    	    	
		/*for(int i=0;i<scorelist.size();i++){
			multiRenderer.addXTextLabel(i+1, scorelist.get(i));    		
		}    */	

		// Adding incomeRenderer and expenseRenderer to multipleRenderer
		// Note: The order of adding dataseries to dataset and renderers to multipleRenderer
		// should be same
		multiRenderer.addSeriesRenderer(incomeRenderer);

		// Creating an intent to plot line chart using dataset and multipleRenderer
		/*Intent intent = ChartFactory.getLineChartIntent(getBaseContext(), dataset, multiRenderer);

		// Start Activity
		startActivity(intent);*/
		mchartview=ChartFactory.getLineChartView(context, dataset, multiRenderer);
		RelativeLayout.LayoutParams params =new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
		mchartview.setLayoutParams(params);

		RelativeLayout lay=(RelativeLayout)findViewById(R.id.lay);
		mchartview.setOnClickListener(new View.OnClickListener() {
     		@Override
     	    public void onClick(View v) {
     	    	
     			SeriesSelection seriesSelection = mchartview.getCurrentSeriesAndPoint();
Common.showToast(""+seriesSelection, context);
     			if (seriesSelection != null) {     				
     				int seriesIndex = seriesSelection.getSeriesIndex();
            	  	String selectedSeries="Income";
            	  	if(seriesIndex==0)
            	  		selectedSeries = "Income";
            	  	else
            	  		selectedSeries = "Expense";
            	  	// Getting the clicked Month
          			String month = mMonth[(int)seriesSelection.getXValue()];
          			// Getting the y value 
          			int amount = (int) seriesSelection.getValue();
          			Toast.makeText(
                	       getBaseContext(),
                	       selectedSeries + " in "  + month + " : " + amount ,
                	       Toast.LENGTH_SHORT).show();
     			}
     		}
  	
     	});
		
	
		/*mchartview.setDrawDataPoints(true);
		mchartview.setDataPointsRadius(15f);*/
		lay.addView(mchartview);

	}



	public void parseJSON(String result) {

		System.out.println(result);
		try {

			JSONObject jo=new JSONObject(result);
			String point=jo.getString("score");
			JSONArray score=new JSONArray(point);
			for (int i = 0; i < score.length(); i++) {

				scorelist.add(score.getInt(i));
			}
			//Common.showToast("REsult is "+result, context);
			System.out.println("Respo"+result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.

		return true;
	}

}
