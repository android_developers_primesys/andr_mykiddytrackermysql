package com.primesys.mitra;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adapter.CheckMessageTeacherAdapter;
import com.example.adapter.RecentMessagePrincipleAdapter;
import com.example.db.DBHelper;
import com.example.dto.LocationData;
import com.example.dto.LoginDetails;
import com.example.dto.MessageMain;
import com.example.dto.Parent;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.primesys.gpluslogin.SplashActivity;

@SuppressWarnings("deprecation")
public class LoginActivity extends Activity  implements
GoogleApiClient.ConnectionCallbacks,
GoogleApiClient.OnConnectionFailedListener   {
	static String[] permission=new String[] {"email"};
	private static final String[] PERMISSIONS = permission;

	CheckBox Dialogterms;
	Button btn_submit;
	EditText textUserid, textPassword;
	CheckBox checkAccept;
	static String user, password;
	Context loginContext = LoginActivity.this;
	ProgressDialog pDialog;
	ArrayList<String> arrayListSender;
	ArrayList<Parent> userArray = new ArrayList<Parent>();
	ArrayList<MessageMain> arrayList;
	static RecentMessagePrincipleAdapter RAdapter;
	static CheckMessageTeacherAdapter CAdapter;
	private GoogleApiClient mGoogleApiClient;
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
	private boolean mRequestingLocationUpdates = false;
	private LocationRequest mLocationRequest;
	private Location mLastLocation;
	public static Client mClient;
	static ClientTeacher mClientTech;
	static ClientPrincipal mClientPri;
	MessageMain msg = new MessageMain();
	String MessageSender = null;
	public static Boolean checkAllOrBulk = false;
	String emailid, mobilenumber;
	static String deviceid;
	Boolean execute = false;;
	public static HashMap<String, String> parent_info = new HashMap<String, String>();
	public static HashMap<String, String> chk_principle = new HashMap<String, String>();
	public static HashMap<String, String> msgrecv_time = new HashMap<String, String>();
	public static HashMap<String, String> student_Id = new HashMap<String, String>();
	CheckBox termAndCondition;
	public static ArrayList<MessageMain> arrayTeacher = new ArrayList<MessageMain>();
	public static ArrayList<String> arrayParent = new ArrayList<String>();
	public static String userIdforChat;
	Uri soundUri;
	int counter = 1;
	private SharedPreferences sharedPreferences,mPrefs;
	private Editor editor;
	boolean isFirst;
	public static String msg_Sender;
	Set<String> objSet;
	public static LoginDetails Ldetails = new LoginDetails();
	ArrayList<Parent> puserArray = new ArrayList<Parent>();
	Parent PList = new Parent();
	private final String key_IS = "IS_FIRST";
	private final String key_USER = "USER";
	private final String key_PASS = "PASS";
	TextView lblTerms,text_signup,text_change;
	int playSound;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG="LoginActivity";
	private final String schoolurl = Common.URL+"LoginServiceAPI.asmx/GetSchoolAndLogo";

	private final String loginurl = Common.URL + "LoginServiceAPI.asmx/GetLoginDetails";
	private final String newUrl=Common.URL + "ParentAPI.asmx/GetNewsAndEvent";
	TextView loginfacebook,logingoogle;
	Button btnRegister;
	private static String APP_ID = "1634968086732703";
	private Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;
	/*	public static LocationClient mLocationClient;*/
	private String phpurl="http://www.mykiddytracker.com/php/getAppId.php";
	String strname,strmessage,strcontact,stremail,strpassword,strusername;
	boolean isExist;   
	Context contxt_reg=LoginActivity.this;
	DBHelper helper = DBHelper.getInstance(loginContext);
	String key_Roll_id="roll_id";
	String key_User_id="User_Id";
	private String roll_id;


	@Override 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		deviceid = getDeviceID();
		emailid = getRegisteredID();
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		mobilenumber = tm.getSimSerialNumber();
		sharedPreferences = this.getSharedPreferences("userInfo",Context.MODE_PRIVATE);
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());

		//initiating google Plus thread
		// First we need to check availability of play services
		if (checkPlayServices()) {
			// Building the GoogleApi client
			buildGoogleApiClient();
			mGoogleApiClient.connect();
			getDeviceCordinates();
		}
		if(!sharedPreferences.contains(key_IS))
			isFirst = sharedPreferences.getBoolean(key_IS, true);

		if (!isFirst) {

			user = sharedPreferences.getString(key_USER, "");
			password = sharedPreferences.getString(key_PASS, "");
			System.err.println(Common.connectionStatus);


			if(Common.getConnectivityStatus(loginContext))
			{
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					new LoginTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
				} else {
					new LoginTask().execute();
				}
			}

		} else {

			setContentView(R.layout.activity_login2);
			// call the method findviewByID
			findViewByID();
			mAsyncRunner = new AsyncFacebookRunner(facebook);

			//login with facebook
			loginfacebook.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					try{

						loginToFacebook();
					}catch(Exception e)
					{
						Log.e("Exception", ""+e.getMessage());
					}

				}
			});
			//login with google plus
			logingoogle.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intentgplus=new Intent(getApplicationContext(),SplashActivity.class);
					startActivity(intentgplus);
				}
			});



			// on submit button click
			btn_submit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					user = textUserid.getText().toString().trim();
					password = textPassword.getText().toString().trim();
					if (user.length() == 0) {
						textUserid.setError("Please User ID!");
						textUserid.requestFocus();
					} else if (password.length() == 0) {
						textPassword.setError("Please Password!");
						textPassword.requestFocus();
					} else if (termAndCondition.isChecked()) {
						if (Common.connectionStatus) {
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
								new LoginTask().executeOnExecutor(
										AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
							} else {
								new LoginTask().execute();
							}
						}
					} else {
						Common.showToast("Please Accept Terms and Conditions",loginContext);
					}

				}
			});

			lblTerms.setOnClickListener(new OnClickListener() {

				@SuppressWarnings("deprecation")
				@Override
				public void onClick(View v) {
					final Dialog dialog = new Dialog(LoginActivity.this);
					dialog.setContentView(R.layout.maindialog);
					dialog.setTitle("Agreement & Terms");
					dialog.setCancelable(true);
					dialog.getWindow().setLayout(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT);

					// set up text
					TextView text = (TextView) dialog
							.findViewById(R.id.TextView01);
					Typeface custom_font = Typeface.createFromAsset(
							getAssets(), "fonts/Roboto-Regular.ttf");
					text.setTypeface(custom_font);
					text.setText(R.string.lots_of_text);
					text.setPadding(5, 5, 5, 5);
					Button buttoncancel = (Button) dialog
							.findViewById(R.id.button1);
					buttoncancel.setBackgroundColor(getResources().getColor(
							R.color.primary));
					buttoncancel.setTextColor(getResources().getColor(
							android.R.color.white));
					buttoncancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							dialog.dismiss();
						}
					});
					dialog.show();

				}
			});
			text_signup.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent sintent=new Intent(loginContext,SignUpActivity.class);
					startActivity(sintent);
				}
			});
			text_change.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent sintent=new Intent(loginContext,ForgetPassword.class);
					startActivity(sintent);
				}
			});
		}
	}



	@SuppressWarnings("deprecation")
	protected void loginToFacebook() {

		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);
		if (access_token != null) {
			facebook.setAccessToken(access_token);
			Log.e("FB Sessions", "" + facebook.isSessionValid());

		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}


		facebook.authorize(this, PERMISSIONS, Facebook.FORCE_DIALOG_AUTH,new DialogListener(){

			public void onCancel() {

			}

			public void onComplete(Bundle values) {
				// Function to handle complete event
				// Edit Preferences and update facebook acess_token

				SharedPreferences.Editor editor = mPrefs.edit();
				editor.putString("access_token",
						facebook.getAccessToken());
				editor.putLong("access_expires",
						facebook.getAccessExpires());
				editor.commit();
				Log.e("Fcaebook Login", "");

				GetProfileInformation(); //here you get profile information
			}


			public void onError(DialogError error) {
				// Function to handle error
				Common.showToast(""+error, loginContext);
			}


			public void onFacebookError(FacebookError fberror) {
				// Function to handle Facebook errors
				Common.showToast(""+fberror, loginContext);
			}

		});
		/*}*/


	}



	protected void GetProfileInformation() {
		mAsyncRunner.request("me", new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {

				//Get All data from Facebook
				try{
					//	Toast.makeText(loginContext, "pass getinfo"+response, Toast.LENGTH_SHORT).show();
					System.err.print("In Parse JSON value");
					System.err.print(response);
					Log.e("GetProfileInformation", ""+response);
					String json=response;
					JSONObject profile = new JSONObject(json);
					strname =""+profile.getString("first_name")+"\t"+profile.getString("last_name");
					stremail = profile.getString("email");
					strcontact="No Contact";
					strmessage="No Address";
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							try{
								signUpRequest();
							}catch(Exception e)
							{
								Log.e("Exception", ""+e);
							}

						}



					});

				}catch(Exception e)
				{
					Log.e("GetProfileInformation", ""+e.getMessage());
				}

			}

			@Override
			public void onIOException(IOException e, Object state) {


			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {


			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {


			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {


			}

		});

	}
	void signUpRequest() {

		reuestQueue=Volley.newRequestQueue(loginContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(loginContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.URL+"LoginServiceAPI.asmx/SaveDemoUser",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseJSONSign(response);
				pDialog.dismiss();
			}

		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSONSign(new String(error.networkResponse.data));
				}
			}
		}) {
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("name",strname);
				params.put("email",stremail);
				params.put("contact",strcontact);
				params.put("message",strmessage);
				params.put("registrationType", "Facebook");
				params.put("city", Common.user_regCity);

				return params;
			}

		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}

	void parseJSONSign(String result) {
		System.err.println(result);
		if(result.contains("error"))
		{
			Common.showToast("Check once again there is Error !", loginContext);
		}
		else{
			try
			{
				Log.e(TAG, result);

				JSONObject jo=new JSONObject(result);
				strusername=jo.getString("username");
				strpassword=jo.getString("password");
				isExist=jo.getBoolean("user_exist");
				/*if(isExist)*/
				///	Common.showToast("You are registered user. For latest password Click on Forgot password!", loginContext);
				if (Common.connectionStatus) {
					user=strusername;
					password=strpassword;

					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
						new LoginTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
					} else {
						new LoginTask().execute();
						/*Common.showToast("IN Login facebook 1", loginContext);*/

					}
				}

				/*else
						if (Common.connectionStatus) {
							user=strusername;
							password=strpassword;
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
								new LoginTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
							} else {
								new LoginTask().execute();
								Common.showToast("IN Login facebook 2", loginContext);

							}
						}*/
				//senEmailRequest();
			}
			catch(Exception e)
			{
				System.err.print(e.getMessage());
				//Common.showToast("IN Login facebook exception", loginContext);
			}
		}
	}
	//send email request
	public void senEmailRequest()
	{
		reuestQueue=Volley.newRequestQueue(loginContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(loginContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,phpurl,new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseEmail(response);
				pDialog.dismiss();
			}

		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseEmail(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("send_mail", "true");
				params.put("name",strname);
				params.put("username", strusername);
				params.put("email",stremail);
				params.put("password",strpassword);
				return params;
			}

		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	void parseEmail(String result)
	{
		Common.showToast("Check E-mail for Username and Password", loginContext);
		Intent loginIntent = new Intent(contxt_reg, LoginActivity.class);
		startActivity(loginIntent);
		finish();

	}

	// find View by id
	void findViewByID() {
		loginfacebook=(TextView)findViewById(R.id.login_fb);
		logingoogle=(TextView)findViewById(R.id.login_gplus);
		btn_submit = (Button) findViewById(R.id.btn_submit);
		textUserid = (EditText) findViewById(R.id.text_user);
		textPassword = (EditText) findViewById(R.id.text_pass);
		termAndCondition = (CheckBox) findViewById(R.id.check_accept);
		lblTerms = (TextView) findViewById(R.id.lbl_terms);
		text_signup=(TextView)findViewById(R.id.text_signup);
		text_change=(TextView)findViewById(R.id.text_change);
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onPause() {
		super.onPause();

	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	// get the registration id for android
	String getDeviceID() {
		return Secure.getString(loginContext.getContentResolver(),
				Secure.ANDROID_ID);
	}

	// read the registered gmail id of the device
	String getRegisteredID() {
		AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
		Account[] list = manager.getAccounts();
		String gmail = null;

		for (Account account : list) {
			if (account.type.equalsIgnoreCase("com.google")) {
				gmail = account.name;
				break;
			}
		}
		return gmail;
	}
	// Asynchronous logn task for the login
	public class LoginTask extends AsyncTask<Void, Void, String> {


		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(loginContext);
			pDialog.setMessage("Login  wait...");
			pDialog.setCancelable(false);
			pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String result = "User is not registered";

			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(loginurl);
			try {
				List<NameValuePair> postParameter = new ArrayList<NameValuePair>(
						4);
				postParameter.add(new BasicNameValuePair("UserName", user));
				Common.username = user;
				postParameter.add(new BasicNameValuePair("Password", password));
				postParameter.add(new BasicNameValuePair("App_Device_Id",
						deviceid));
				postParameter
				.add(new BasicNameValuePair("App_Emailid", emailid));
				System.out.println("Login----req "+postParameter);

				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));

				HttpResponse response = httpClient.execute(httpPost);
				result = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {
				result = e.getMessage();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			System.out.println("Login----respo "+result);

			pDialog.dismiss();
			if (result.contains("error")) {
				CustomDialog.displayDialog("Invalid Userid/Passward",loginContext);
				editor = sharedPreferences.edit();
				editor.clear();
				editor.commit();
				Intent intent=new Intent(loginContext,LoginActivity.class);
				startActivity(intent);
			} else {
				editor = sharedPreferences.edit();
				editor.putBoolean(key_IS, false);
				editor.putString(key_USER, user);
				editor.putString(key_PASS, password);
				editor.commit();
				parseJSON(result);
			}
		}
	}

	// parse the JSON response
	void parseJSON(String result) {
		try {
			
			JSONArray array = new JSONArray(result);
			JSONObject jo = array.getJSONObject(0);
			Common.roleid = jo.getString("Role_ID");
			Common.userid = jo.getString("UserID");
			Common.username = jo.getString("userName");
			Common.Student_Count = jo.getInt("Student_Count");
			editor = sharedPreferences.edit();
		
			editor.putString(key_Roll_id, Common.roleid);
			editor.putString(key_User_id, Common.userid);
			editor.commit();
			// setting data for join
			Ldetails.setClass_id(jo.getString("ClassName"));
			Common.classId = jo.getString("ClassName");
			Common.schoolId = jo.getString("school_id");
			Ldetails.setSchool_id(jo.getString("school_id"));
			Ldetails.setUserType(Common.roleid);
			Common.email=jo.getString("EmailID");
			Ldetails.setEmailId(jo.getString("EmailID"));
			Ldetails.setMobileNumber(jo.getString("MobileNo"));
			Common.markerImg=jo.getString("Photo");
			Toast.makeText(loginContext, Common.markerImg, Toast.LENGTH_LONG);
			System.out.println("Photo----"+Common.markerImg);
			Common.user_regType=jo.getString("RegistrationType");
			Log.e(TAG, Common.user_regType);



			//School Logo & name
			if (Common.connectionStatus) {
				getSchoolNameAndLogo();
			}///Added By Rupesh
			// news & events service
			if (Common.connectionStatus) {
				getNewsEventRequest();
			}///Added By Rupesh

			if (Common.connectionStatus) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					new chk_teacher().executeOnExecutor(
							AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
				} else {
					new chk_teacher().execute();
				}
			}
			// calling to get List All ParentName+techerName ,Id
			if (Common.connectionStatus) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					new Chk_principle().executeOnExecutor(
							AsyncTask.THREAD_POOL_EXECUTOR, null);
				} else {
					new Chk_principle().execute();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
			CustomDialog.displayDialog("Error While Establishing Connection To Server! Please Contact Admin",loginContext);
			editor = sharedPreferences.edit();
			editor.clear();
			editor.commit();
		}
	}

	private void getofflineNewsEventRequest() {
		// TODO Auto-generated method stub
		Log.d("Inside news offline", "NEws Off");
		//	Common.showToast("In Offline News& Event", loginContext);
		String response1 =helper.show_news();
		APIController cntrl = APIController.getInstance();
		cntrl.setNews(response1);
	}


	public class connectTask extends AsyncTask<String, String, Client> {
		@Override
		protected Client doInBackground(String... message) {
			mClient = new Client(new Client.OnMessageReceived() {
				@Override
				// here the messageReceived method is implemented
				public void messageReceived(String message) {
					publishProgress(message);
				}
				public void messageSend(MessageMain message) {
				}
				@Override
				public void messageSend(String message) {
				}
			});
			mClient.run();
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
			String comMsg = values[0];
			try {
				if (comMsg.contains(Common.EV_MSG_RECEIVED)) {

					JSONObject jo = new JSONObject(comMsg);
					JSONObject jData = jo.getJSONObject("data");
					MessageMain msg = new MessageMain();
					msg.setDate_time(jData.getLong("timestamp"));
					msg.setEvent(jo.getString("event"));
					msg.setType(jData.getString("type"));
					msg.setFrom(jData.getString("from"));
					msg.setMesageText(jData.getString("msg"));
					msg.setTo(jData.getString("to"));

					if(!jData.isNull("student_id"))
						helper.insertMessage(msg,jData.getString("student_id"));
					else
						helper.insertMessage(msg);
					try {
						if(APIController.getInstance().getRead()==0)
							NotifyParent("" + msg.getTo(), "" + msg.getMesageText()); 
					} catch (Exception e) {
					}
					if(DemoUserActivity.adapter!=null)
						DemoUserActivity.adapter.notifyDataSetChanged();
					if (MainActivity.arrayList != null&&MainActivity.mAdapter!=null) {
						MainActivity.arrayList.add(msg);
						MainActivity.mAdapter.notifyDataSetInvalidated();
						MainActivity.mAdapter.notifyDataSetChanged();
					} else {
						MainActivity.arrayList = new ArrayList<MessageMain>();
						MainActivity.arrayList.add(msg);
						if (MainActivity.mAdapter != null) {
							MainActivity.mAdapter.notifyDataSetInvalidated();
							MainActivity.mAdapter.notifyDataSetChanged();
						}
					}
				}
				System.out
				.println("LoginActivity.connectTask.onProgressUpdate()"+comMsg);

				if (comMsg.contains(Common.EV_CURR_LOC)) {

					/*ShowGMapC.changeLocation(comMsg);*/
					ShowGmapClient.changeLocation(comMsg);

				}
				else if (comMsg.contains(Common.EV_TOD_LOC)) {

					DBHelper helper = DBHelper.getInstance(loginContext);
					helper.insertLocation(parseLocation(comMsg));
				}
				else if (comMsg.contains(Common.EV_DEVICE_STATUS))
					HomeActivity.setDeviceData(comMsg);
				else if (comMsg.contains(Common.EV_GRP_CREAT)) 
					CreateGroup.parseJSON(comMsg);
				else if(comMsg.contains(Common.EV_ERROR))
					socketErrorMsg(comMsg);
				else if(comMsg.contains(Common.EV_GRPS_LIST))
					DemoUserActivity.parseJSON(comMsg);
				else if(comMsg.contains(Common.EV_MEM_LIST))

					MainActivity.parseJSON(comMsg);
				else if(comMsg.contains(Common.EV_FRND_SUCC)){
					AddUserActivity.parseJSON(comMsg);
					Common.showToast("Friend added successfully", loginContext);
				}
				else if(comMsg.contains(Common.EV_FRND_LIST)){
					if(Common.flag==0)
						DemoUserActivity.restParseJSON(comMsg);
					else
						AddParticpants.parseJSON(comMsg);
				}
				else if (comMsg.contains(Common.EV_GRP_ADD))
					Common.showToast("Member added successfully", loginContext);
				else if(comMsg.contains(Common.EV_MSG_ACK))
				{
					DBHelper helper = DBHelper.getInstance(loginContext);
					try
					{
						
						
						JSONObject jo=new JSONObject(comMsg);
						JSONObject jData=jo.getJSONObject("data");
						Log.e("DAta....", jData.toString());
						helper.UpdateMessage(jData.getLong("ref_id"),jData.getLong("timestamp"));
						if (MainActivity.mAdapter != null) {
							MainActivity.mAdapter.notifyDataSetInvalidated();
							MainActivity.mAdapter.notifyDataSetChanged();
						}
					}catch(Exception e)
					{
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();

			}
		}
	}
	void socketErrorMsg(String errmsg)
	{
		try{
			JSONObject jo=new JSONObject(errmsg);
			JSONObject jData=jo.getJSONObject("data");
			//	Common.showToast(jData.getString("error_msg"), loginContext);
		}catch(Exception e)
		{

		}
	}
	// asynctask for teacher
	public class connectTeacherTask extends
	AsyncTask<String, String, ClientTeacher> {
		@Override
		protected ClientTeacher doInBackground(String... message) {
			// we create a Client object and
			mClientTech = new ClientTeacher(
					new ClientTeacher.OnMessageReceived() {
						@Override
						// here the messageReceived method is implemented
						public void messageReceived(String message) {
							publishProgress(message);
						}

						public void messageSend(MessageMain message) {}

					});
			mClientTech.run();
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);

			try {
				
				String comMsg = values[0];
				System.out.println("LoginActivity.connectTeacherTask.onProgressUpdate()"+comMsg);
				if (comMsg.contains(Common.EV_MSG_RECEIVED)) {
					JSONObject jo = new JSONObject(comMsg);
					JSONObject jData = jo.getJSONObject("data");
					MessageMain msg = new MessageMain();
					msg.setDate_time(jData.getLong("timestamp"));
					msg.setEvent(jo.getString("event"));
					msg.setFrom(jData.getString("from"));
					msg.setMesageText(jData.getString("msg"));
					msg.setTo(jData.getString("to"));// Common.userid

					try {
						if(APIController.getInstance().getRead()==0)
							NotifyTeacher(
									"" + msg.getTo(),
									"" + msg.getMesageText()); // message
						// sending
						// Notification
					} catch (Exception e) {

					}
					if (!msg.getFrom().contains(Common.userid))
						if (!arrayListSender.contains(msg.getFrom())) {
							if (!arrayListSender.equals(Common.userid))
								arrayListSender.add(0, msg.getFrom());
						}
					DBHelper helper = DBHelper
							.getInstance(loginContext);
					helper.insertMessage(msg);
					msgrecv_time.put(msg.getFrom(), msg.getDate_time() + "");
					if (MainTeacher.arrayList != null) {
						MainTeacher.arrayList.add(msg);
						MainTeacher.mAdapter.notifyDataSetInvalidated();
						MainTeacher.mAdapter.notifyDataSetChanged();
					} else {
						MainTeacher.arrayList = new ArrayList<MessageMain>();
						MainTeacher.arrayList.add(msg);
						if (MainTeacher.mAdapter != null) {
							MainTeacher.mAdapter.notifyDataSetInvalidated();
							MainTeacher.mAdapter.notifyDataSetChanged();
						}
					}
				}
				else if(comMsg.contains(Common.EV_MSG_ACK))
				{
					DBHelper helper = DBHelper.getInstance(loginContext);
					try
					{
						JSONObject jo=new JSONObject(comMsg);
						JSONObject jData=jo.getJSONObject("data");
						helper.UpdateMessage(jData.getLong("ref_id"),jData.getLong("timestamp"));
						if (MainActivity.mAdapter != null) {
							MainActivity.mAdapter.notifyDataSetInvalidated();
							MainActivity.mAdapter.notifyDataSetChanged();
						}
					}catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();

			}

		}

	}
	public class Chk_principle extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			String result = "";
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(Common.URL
					+ "PrincipleAPI.asmx/GetMessageToAll");
			try {
				List<NameValuePair> postParameter = new ArrayList<NameValuePair>(
						3);
				postParameter.add(new BasicNameValuePair("Principal",
						Common.userid));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {
				result = e.getMessage();
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			parseJSON(result);
		}

		void parseJSON(String result) {
			try {
				JSONArray array = new JSONArray(result);
				for (int i = 0; i < array.length(); i++) {
					JSONObject jo = array.getJSONObject(i);
					chk_principle.put(jo.getString("ID"), jo.getString("name"));
				}
			} catch (JSONException e) {
			}
		}

	}
	// asynctask for principal
	public class connectPrincipalTask extends
	AsyncTask<String, String, ClientPrincipal> {
		@Override
		protected ClientPrincipal doInBackground(String... message) {
			// we create a Client object and
			// remove element from list

			mClientPri = new ClientPrincipal(
					new ClientPrincipal.OnMessageReceived() {
						@Override
						// here the messageReceived method is implemented
						public void messageReceived(String message) {
							publishProgress(message);
						}

						public void messageSend(MessageMain message) {
						}

						@Override
						public void messageSend(String message) {							
						}

					});

			mClientPri.run();
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
			try {
				String comMsg=values[0];
				if (comMsg.contains(Common.EV_MSG_RECEIVED)) {
					DBHelper helper = DBHelper.getInstance(loginContext);
					JSONObject jo = new JSONObject(comMsg);
					JSONObject jData = jo.getJSONObject("data");
					MessageMain msg = new MessageMain();
					msg.setDate_time(jData.getLong("timestamp"));
					msg.setEvent(jo.getString("event"));
					msg.setFrom(jData.getString("from"));
					msg.setMesageText(jData.getString("msg"));
					msg.setTo(jData.getString("to"));

					if(!jData.isNull("student_id"))
					{
						student_Id.put(jData.getString("from"), jData.getString("student_id"));
						helper.insertMessage(msg,jData.getString("student_id"));
					}
					else
						helper.insertMessage(msg);
					try {
						if(APIController.getInstance().getRead()==0)
							NotifyPrinciple("" + msg.getTo(),"" + msg.getMesageText()); 
					} catch (Exception e) {
					}
					if (!msg.getFrom().contains(Common.userid))
						if (!arrayListSender.contains(msg.getFrom())) {
							if (!arrayListSender.equals(Common.userid))
								arrayListSender.add(0, msg.getFrom());
						}

					if(!msgrecv_time.containsKey(msg.getFrom()))
						msgrecv_time.put(msg.getFrom(), msg.getDate_time() + "");

					if (MainPrincipal.arrayList != null&&MainPrincipal.mAdapter != null) {
						MainPrincipal.arrayList.add(msg);
						MainPrincipal.mAdapter.notifyDataSetInvalidated();
						MainPrincipal.mAdapter.notifyDataSetChanged();
					} else {
						MainPrincipal.arrayList = new ArrayList<MessageMain>();
						MainPrincipal.arrayList.add(msg);
						if (MainPrincipal.mAdapter != null) {
							MainPrincipal.mAdapter.notifyDataSetInvalidated();
							MainPrincipal.mAdapter.notifyDataSetChanged();
						}
					}
				}
				else if(comMsg.contains(Common.EV_MSG_ACK))
				{
					DBHelper helper = DBHelper.getInstance(loginContext);
					try
					{
						JSONObject jo=new JSONObject(comMsg);
						JSONObject jData=jo.getJSONObject("data");
						helper.UpdateMessage(jData.getLong("ref_id"),jData.getLong("timestamp"));
						if (MainActivity.mAdapter != null) {
							MainActivity.mAdapter.notifyDataSetInvalidated();
							MainActivity.mAdapter.notifyDataSetChanged();
						}
					}catch(Exception e)
					{
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			RAdapter.notifyDataSetChanged();
		}
	}

	private void NotifyTeacher(String notificationTitle,
			String notificationMessage) {
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.ic_launcher,getResources().getString(R.string.app_name),
				System.currentTimeMillis());
		notification.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Notification.FLAG_ONLY_ALERT_ONCE
				| Notification.FLAG_AUTO_CANCEL;
		if (playSound==0) {
			notification.sound = Uri
					.parse("android.resource://com.primesys.mitra/raw/beep1");  //added By amit 
			playSound++;
		}

		notification.defaults = Notification.DEFAULT_LIGHTS
				| Notification.DEFAULT_VIBRATE;

		Intent notificationIntent = new Intent(LoginActivity.this,
				HeadTeacherHome.class);
		notificationIntent.putExtra("to", msg.getTo());
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		notification.setLatestEventInfo(LoginActivity.this, notificationTitle,
				notificationMessage, pendingIntent);
		notificationManager.notify(9999, notification);
	}

	// Generating Notification for Parent

	private void NotifyParent(String notificationTitle,
			String notificationMessage) {
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.ic_launcher,
				getResources().getString(R.string.app_name),
				System.currentTimeMillis());
		notification.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
				|Notification.FLAG_ONLY_ALERT_ONCE
				| Notification.FLAG_AUTO_CANCEL;
		if (playSound==0) {
			notification.sound = Uri.parse("android.resource://com.primesys.mitra/raw/beep1"); //added by amit
			playSound++;
		}
		notification.defaults = Notification.DEFAULT_LIGHTS
				| Notification.DEFAULT_VIBRATE;
		Intent notificationIntent ;
		// redirecting to homepage
		if(Common.roleid.equals("5"))
			notificationIntent = new Intent(LoginActivity.this,
					DemoHomeActivity.class);
		else
			notificationIntent = new Intent(LoginActivity.this,
					HomeActivity.class);

		notificationIntent.putExtra("to", msg.getTo());
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		notification.setLatestEventInfo(LoginActivity.this, notificationTitle,
				notificationMessage, pendingIntent);
		notificationManager.notify(9999, notification);
	}

	// Generating Notification for Principle

	private void NotifyPrinciple(String notificationTitle,
			String notificationMessage) {
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.ic_launcher,
				getResources().getString(R.string.app_name),
				System.currentTimeMillis());
		notification.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Notification.FLAG_ONLY_ALERT_ONCE
				| Notification.FLAG_AUTO_CANCEL;
		if (playSound==0) {
			notification.sound = Uri
					.parse("android.resource://com.primesys.mitra/raw/beep1");
			playSound++;
		}
		notification.defaults = Notification.DEFAULT_LIGHTS
				| Notification.DEFAULT_VIBRATE;
		// redirecting to homePage of user
		Intent notificationIntent = new Intent(LoginActivity.this,
				PricipleHomeScreen.class);

		notificationIntent.putExtra("to", msg.getTo());
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		notification.setLatestEventInfo(LoginActivity.this, notificationTitle,
				notificationMessage, pendingIntent);
		notificationManager.notify(9999, notification);
	}// to get ParentId-parent name along with principle name principle id
	public class chk_teacher extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			String result = "";
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(Common.URL+"PrincipleAPI.asmx/GetParentAndPrincipalName?");
			try {
				List<NameValuePair> postParameter = new ArrayList<NameValuePair>(
						1);
				postParameter.add(new BasicNameValuePair("TeacherID",
						Common.userid));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {
				result = e.getMessage();

				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try {
				parseJSON(result);
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		void parseJSON(String result) throws JSONException {
			JSONArray array = new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo = array.getJSONObject(i);
				parent_info.put(jo.getString("ID"), jo.getString("Name"));

			}
		}
	}

	//get School Name & logo
	void getSchoolNameAndLogo()
	{
		reuestQueue=Volley.newRequestQueue(loginContext);
		pDialog = new ProgressDialog(loginContext);
		pDialog.setMessage("Progress wait.......");
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,schoolurl,new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseschoolInfo(response);
				pDialog.dismiss();
			}
		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.dismiss();
				try{
					if(error.networkResponse != null && error.networkResponse.data != null){
						VolleyError er = new VolleyError(new String(error.networkResponse.data));
						error = er;
						System.out.println(error.toString());
						parseschoolInfo(new String(error.networkResponse.data));
					}
				}catch(Exception e){
					System.err.print(e);
				}

			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("SchoolId",Common.schoolId);
				return params;
			}

		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	protected void parseschoolInfo(String response) {
		try{
			JSONArray joarray=new JSONArray(response);

			for (int i = 0; i < joarray.length(); i++) {
				JSONObject joObject=joarray.getJSONObject(i);
				APIController cntrl = APIController.getInstance();
				cntrl.setSchool_logo(joObject.getString("logo").replace("~",""));
				cntrl.setSchool_name(joObject.getString("school_name"));

			}

		}catch(Exception ex)
		{
			Log.e("Exception", ""+ex);
		}


	}

	//get the news and event data
	void getNewsEventRequest()
	{
		reuestQueue=Volley.newRequestQueue(loginContext);
		pDialog = new ProgressDialog(loginContext);
		pDialog.setMessage("Progress wait.......");
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,newUrl,new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseData(response);
				pDialog.hide();
			}
		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseData(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("ParentId",Common.userid);
				return params;
			}

		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	void parseData(String result) {
		String response = "";
		try {
			JSONArray array = new JSONArray(result);
			int j = 0;
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo = array.getJSONObject(i);
				response += " " + (j += 1) + ".";
				response += jo.getString("News") + "      ";
			}
			APIController cntrl = APIController.getInstance();
			cntrl.setNews(response);




			//Added BY Rupesh
			DBHelper helper = DBHelper.getInstance(loginContext);
			helper.insert_news(cntrl.getNews());
			helper.getLastTimeStamp();


		} catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			if (Common.roleid.equals("2")) {
				ClientTeacher.SERVERIP = Common.SERVERIP;
				arrayTeacher = new ArrayList<MessageMain>();
				arrayListSender = new ArrayList<String>();
				CAdapter = new CheckMessageTeacherAdapter(LoginActivity.this,
						arrayListSender);
				playSound=0; 
				new connectTeacherTask().execute();
				Intent intent = new Intent(loginContext, HeadTeacherHome.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			} else if (Common.roleid.equals("3")) {
				Client.SERVERIP = Common.SERVERIP;
				arrayTeacher = new ArrayList<MessageMain>();
				playSound=0; 
				new connectTask().execute();
				DBHelper.getInstance(loginContext).truncateTables("db_user");
				Intent intent = new Intent(loginContext, HomeActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			} else if (Common.roleid.equals("1")) {
				ClientPrincipal.SERVERIP = Common.SERVERIP;
				arrayListSender = new ArrayList<String>();
				RAdapter = new RecentMessagePrincipleAdapter(
						LoginActivity.this, arrayListSender);
				playSound=0; 
				new connectPrincipalTask().execute();
				Intent intent = new Intent(loginContext,
						PricipleHomeScreen.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			} else if (Common.roleid.equals("5")) {
				Client.SERVERIP = Common.SERVERIP;
				arrayTeacher = new ArrayList<MessageMain>();
				playSound=0; 
				new connectTask().execute();
				DBHelper.getInstance(loginContext).truncateTables("db_user");
				Intent intent = new Intent(loginContext, DemoHomeActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}else if(Common.roleid.equals("7"))
			{	Client.SERVERIP = Common.SERVERIP;
			arrayTeacher = new ArrayList<MessageMain>();
			playSound=0; 
			new connectTask().execute();
			DBHelper.getInstance(loginContext).truncateTables("db_user");
			Intent intent = new Intent(loginContext, TrackUserHomeActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		}
	}
	ArrayList<LocationData> parseLocation(String msg) {
		ArrayList<LocationData> list=new ArrayList<LocationData>();
		try {
			JSONObject jMain=new JSONObject(msg);
			JSONArray array=jMain.getJSONArray("data");
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo = array.getJSONObject(i);
				LocationData l = new LocationData();
				l.setLat(jo.getDouble("lat"));
				l.setLan(jo.getDouble("lan"));
				l.setSpeed(jo.getInt("speed"));
				l.setTimestamp(jo.getLong("timestamp"));
				list.add(l);
			//	Toast.makeText(loginContext, "dATAC ---->  "+msg,Toast.LENGTH_LONG).show();
				Log.e("Location", ""+msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Common.showToast(""+arg0, loginContext);

	}

	@Override
	public void onConnected(Bundle arg0) {
		getDeviceCordinates();

	}



	@Override
	public void onConnectionSuspended(int arg0) {
		Common.showToast(""+arg0, loginContext);

	}


	/**
	 * Creating google api client object
	 * */
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
		.addConnectionCallbacks(this)
		.addOnConnectionFailedListener(this)
		.addApi(LocationServices.API).build();
	}

	/**
	 * Method to verify google play services on the device
	 * */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();

			} else {
				Common.showToast("Your Location Cann't Traced", loginContext);
				finish();
			}
			return false;
		}
		return true;
	}



	private void getDeviceCordinates() {
		mLastLocation = LocationServices.FusedLocationApi
				.getLastLocation(mGoogleApiClient);
		try{
			if (mLastLocation != null) {
				double latitude = mLastLocation.getLatitude();
				double longitude = mLastLocation.getLongitude();
				Log.e("#################COrdinated Data #############", ""+mLastLocation);
				ReverseGeoCoding geoCode=new ReverseGeoCoding(loginContext);
				geoCode.Execute(mLastLocation);

			} 
		}catch(Exception ex){
			ex.printStackTrace();
		}


	}



}


