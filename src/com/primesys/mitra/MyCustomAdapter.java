package com.primesys.mitra;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.db.DBHelper;
import com.example.dto.GroupInfo;
import com.example.dto.MessageMain;
import com.primesys.emojilibrary.EmojiconTextView;

public class MyCustomAdapter extends BaseAdapter {
	private ArrayList<MessageMain> mListItems;
	private LayoutInflater mLayoutInflater;
	Context currentContext;
	String name;
	ImageLoader.ImageCache imageFrom ;
	ImageLoader.ImageCache imageTO ;
	ImageLoader imageLFrom ;
	ImageLoader imageLTo ;
	public MyCustomAdapter(Context context, ArrayList<MessageMain> arrayList,String name){
		mListItems = arrayList;
		currentContext=context;
		this.name=name;
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageFrom = new LruBitmapCache();
		imageTO = new LruBitmapCache();
		imageLFrom = new ImageLoader(Volley.newRequestQueue(currentContext), imageFrom);
		imageLTo = new ImageLoader(Volley.newRequestQueue(currentContext), imageTO);
	}
	@Override
	public int getCount() {
		return mListItems.size();
	}
	@Override
	public Object getItem(int i) {
		return null;
	}
	@Override
	public long getItemId(int i) {
		return 0;
	}
	@Override
	public View getView(int position, View view, ViewGroup viewGroup) {
		MessageMain stringItem = mListItems.get(position);
		ViewHolderLeft left;
		ViewHolderRight right;

		if (stringItem != null) 
			if (view == null) {
				view = mLayoutInflater.inflate(R.layout.new_list_item, null);
			}

		try{
			String tmp=Common.to;

			if(stringItem.getTo().contains(tmp)|stringItem.getFrom().contains(tmp)){
				if(stringItem.getType().equalsIgnoreCase("s")||stringItem.getType().equalsIgnoreCase("b"))
				{
					if (stringItem != null) {
						view = mLayoutInflater.inflate(R.layout.items_rightmessage, null);

						if (stringItem.getFrom().contains(tmp)) 
						{
							EmojiconTextView messages=(EmojiconTextView)view.findViewById(R.id.message_text);
							TextView text_date=(TextView)view.findViewById(R.id.time_text);
							TextView chat_company_reply_author=(TextView)view.findViewById(R.id.chat_company_reply_author);

							CircularNetworkImageView im = (CircularNetworkImageView) view.findViewById(R.id.chat_image);
							chat_company_reply_author.setText(name);
							messages.setText(stringItem.getMesageText());
							text_date.setText(Common.getDateCurrentTimeZone(stringItem.getDate_time()));
							try {

								(im).setImageUrl(Common.relativeurl+Common.to, imageLFrom);
								((CircularNetworkImageView) im)
								.setDefaultImageResId(R.drawable.student);
								((CircularNetworkImageView) im)
								.setErrorImageResId(R.drawable.student);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						else {
							view = mLayoutInflater.inflate(R.layout.item_leftmessage, null);
							CircularNetworkImageView im = (CircularNetworkImageView) view.findViewById(R.id.chat_image);
						
							Bitmap bitmap=DBHelper.getInstance(currentContext).getBitMap(Common.studID);
							if(bitmap!=null)
								im.setImageBitmap(bitmap);
							EmojiconTextView messages=(EmojiconTextView)view.findViewById(R.id.message_text);
							messages.setText(stringItem.getMesageText());
							TextView text_date=(TextView)view.findViewById(R.id.time_text);
							text_date.setText(Common.getDateCurrentTimeZone(stringItem.getDate_time()));
							TextView chat_company_reply_author=(TextView)view.findViewById(R.id.chat_company_reply_author);
							chat_company_reply_author.setText(Common.username);
						}
					}
				}//end of type if
				else if(stringItem.getType().equalsIgnoreCase("g"))
				{

					
					tmp=Common.userid;
					if (stringItem != null) {
						view = mLayoutInflater.inflate(R.layout.items_rightmessage, null);

						if (!stringItem.getFrom().contains(tmp)) 
						{
							EmojiconTextView messages=(EmojiconTextView)view.findViewById(R.id.message_text);
							TextView text_date=(TextView)view.findViewById(R.id.time_text);
							TextView chat_company_reply_author=(TextView)view.findViewById(R.id.chat_company_reply_author);
							CircularNetworkImageView im = (CircularNetworkImageView) view.findViewById(R.id.chat_image);
							chat_company_reply_author.setText(getName(stringItem.getFrom()));
							messages.setText(stringItem.getMesageText());
							text_date.setText(Common.getDateCurrentTimeZone(stringItem.getDate_time()));
							im.setImageResource(R.drawable.student);
						/*	try {

								(im).setImageUrl(Common.relativeurl+Common.to, imageLFrom);
								((CircularNetworkImageView) im)
								.setDefaultImageResId(R.drawable.student);
								((CircularNetworkImageView) im)
								.setErrorImageResId(R.drawable.student);
							} catch (Exception e) {
								Log.e("setdata", "" + e);
							}*/
						}
						else {
							view = mLayoutInflater.inflate(R.layout.item_leftmessage, null);
							CircularNetworkImageView im = (CircularNetworkImageView) view.findViewById(R.id.chat_image);
							im.setImageResource(R.drawable.student);
						/*	try {

								(im).setImageUrl(Common.photo, imageLTo);
								((CircularNetworkImageView) im)
								.setDefaultImageResId(R.drawable.student);
								((CircularNetworkImageView) im)
								.setErrorImageResId(R.drawable.student);
							} catch (Exception e) {
								e.printStackTrace();
							}*/
							EmojiconTextView messages=(EmojiconTextView)view.findViewById(R.id.message_text);
							messages.setText(stringItem.getMesageText());
							TextView text_date=(TextView)view.findViewById(R.id.time_text);
							text_date.setText(Common.getDateCurrentTimeZone(stringItem.getDate_time()));
							TextView chat_company_reply_author=(TextView)view.findViewById(R.id.chat_company_reply_author);
							chat_company_reply_author.setText(getName(stringItem.getFrom()));
						}
					}
				}

			}
		}catch(Exception e)
		{
			Log.d("Mycustome", ""+e.getCause());
			e.printStackTrace();
		}
		//this method must return the view corresponding to the data at the specified position.
		return view;
	}
	//get name from id
	String getName(String id)
	{
		String name="";
		for (GroupInfo g : MainActivity.listGrp) {
			if(g.getParticipantId().equals(id))
			{
				name=g.getParticipantName();
				return name;
			}
		}
		return name;
	}
	
	
	//get Imagepath With Id
	
	static class ViewHolderLeft
	{
		EmojiconTextView messages;
		TextView text_date;
		TextView chat_company_reply_author;
		CircularNetworkImageView im;
	}
	static class ViewHolderRight
	{
		EmojiconTextView messages;
		TextView text_date;
		TextView chat_company_reply_author;
		CircularNetworkImageView im;	
	}
}
