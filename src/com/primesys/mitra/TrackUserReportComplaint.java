package com.primesys.mitra;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

public class TrackUserReportComplaint extends Activity {
	Button btnsend,btnClear;
	EditText textTo,textFrom,textSubject,text_msg;
	String to,from,subject,emailid,message,issue;
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	Context context=TrackUserReportComplaint.this;
	Spinner dropDownList;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_track_complaint);
		findViewById();


		emailid=getRegisteredID();

		textFrom.setText(Common.email);
		dropDownList.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				String issues[]=getResources().getStringArray(R.array.complaint_msg_track);
				textSubject.setText(Common.userid +" - "+issues[position]);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		btnClear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				reset();	
			}
		});

		//send mail from android mobile
		btnsend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				to=textTo.getText().toString();
				from=textFrom.getText().toString();
				subject=textSubject.getText().toString();
				message=text_msg.getText().toString();
				if(from.length()==0)
				{
					textFrom.setError("Enter Email ID");
					textFrom.requestFocus();

				}
				else if(to.length()==0)
				{
					textTo.setError("Enter Email Id ");
					textTo.requestFocus();
				}
				else if(subject.length()==0)
				{
					textSubject.setError("Enter Subject");
					textSubject.requestFocus();
				}
				else
				{
					//send email code
					Intent emailIntent = new Intent(Intent.ACTION_SEND);
					emailIntent.setType("message/rfc822");
					emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] {to});
					emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
					emailIntent.putExtra(Intent.EXTRA_TEXT,message);
					try {
						startActivity(Intent.createChooser(emailIntent, "Send mail..."));
						finish();
						Common.showToast("Email Sent successfully",TrackUserReportComplaint.this);
					} catch (android.content.ActivityNotFoundException ex) {
						Common.showToast("There is no email client installed.", TrackUserReportComplaint.this);
					}
				}

			}
		});

		//on text change lisenter 
		textTo.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				textTo.setError(null);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		textFrom.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				textFrom.setError(null);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		textSubject.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				textSubject.setError(null);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

	}

	private void findViewById() {
		btnClear=(Button)findViewById(R.id.btn_clear);
		btnsend=(Button)findViewById(R.id.btn_send);
		textTo=(EditText)findViewById(R.id.text_to);
		textFrom=(EditText)findViewById(R.id.text_from);
		textSubject=(EditText)findViewById(R.id.text_subject);
		text_msg=(EditText)findViewById(R.id.text_msg);
		dropDownList=(Spinner)findViewById(R.id.sp_issue);
	}
	String getRegisteredID()
	{
		AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
		Account[] list = manager.getAccounts();
		String gmail = null;

		for(Account account: list)
		{
			if(account.type.equalsIgnoreCase("com.google"))
			{
				gmail = account.name;
				break;
			}
		}
		return gmail;
	}
	//reset button
	void reset()
	{
		textTo.setText("");
		textTo.setError(null);
		textFrom.setText("");
		textFrom.setError(null);
		textSubject.setText("");
		textSubject.setError(null);
		textFrom.requestFocus();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
}
