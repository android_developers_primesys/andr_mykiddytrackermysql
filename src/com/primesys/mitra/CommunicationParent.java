package com.primesys.mitra;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.dto.Child;

public class CommunicationParent extends Activity {
	Context homeContext=CommunicationParent.this;
	static List<Child> childlist=new ArrayList<Child>();
	CircleImageView btnPrinciple,btnTeacher;
	TextView text_teacname,text_priname;
	String PrincipleName,TeacherName,PrincipleID,TeacherID;
	Context commContext=CommunicationParent.this;
	String StudentID;
	boolean valid=true;
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	Child child=new Child();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_communicate);

		StudentID=getIntent().getStringExtra("StudentID");
		Common.freindflag=false;
		FindViewById();
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);




		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		/*textNews=(TextView)findViewById(R.id.text_news);

				textNews.setTypeface();
			textNews.setText(APIController.getInstance().getNews());
		 */
		if (Common.connectionStatus) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				new GetTeacherPrincipalInfo().executeOnExecutor(
						AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			} else {
				new GetTeacherPrincipalInfo().execute();
			}
		}
		try{
			//chat with priciple
			btnPrinciple.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if(valid)
					{
						Intent intent=new Intent(commContext,MainActivity.class);
						intent.putExtra("to",PrincipleID);
						intent.putExtra("name",PrincipleName);
						intent.putExtra("student",StudentID);
						startActivity(intent);
						overridePendingTransition(R.anim.right_in, R.anim.left_out);
					}
					else{
						Common.showToast("Invalid Data Contact Admin",commContext);
					}

				}
			});


			btnTeacher.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if(valid)
					{
						Intent intent=new Intent(commContext,MainActivity.class);

						intent.putExtra("to",TeacherID);
						intent.putExtra("name",TeacherName);
						startActivity(intent);
						overridePendingTransition(R.anim.right_in, R.anim.left_out);
					}else
					{
						Common.showToast("Invalid Data Contact Admin",commContext);
					}

				}
			});


		}catch(Exception e)
		{
			Log.d("btnPrinciple", ""+e);
		}
	}

	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);

			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(commContext)&& APIController.getInstance().getSchool_logo()!=null) {
				imageLoader = new ImageLoader(
						Volley.newRequestQueue(commContext), imageCache);
				(img_school).setImageUrl(
						Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
				((CircularNetworkImageView) img_school)
				.setDefaultImageResId(R.drawable.student);
				((CircularNetworkImageView) img_school)
				.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(commContext)&& APIController.getInstance().getSchool_logo()!=null) {
				imageLoader = new ImageLoader(
						Volley.newRequestQueue(commContext), imageCache);
				(img_school).setImageUrl(
						Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
				((CircularNetworkImageView) img_school)
				.setDefaultImageResId(R.drawable.student);
				((CircularNetworkImageView) img_school)
				.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}


	}

	private void FindViewById() {
		btnPrinciple=(CircleImageView)findViewById(R.id.image_principle);
		btnTeacher=(CircleImageView)findViewById(R.id.image_teacher);
		text_priname=(TextView)findViewById(R.id.text_priname);
		text_teacname=(TextView)findViewById(R.id.text_tname);
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(commContext)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(commContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	class GetTeacherPrincipalInfo extends AsyncTask<Void, Void, String>
	{
		String url=Common.URL+"ParentAPI.asmx/GetTeacherPrincipalInfo";


		@Override
		protected String doInBackground(Void... params) {
			String result="User is not registered";

			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(url);
			try
			{
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("StudentID",StudentID));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));

				Log.d("GetTeacherPrincipalInfo---------", ""+postParameter);

				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	
			}

			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			Log.e("Communication with school", ""+result);

			if(result.contains("error"))
			{
				/*CustomDialog.displayDialog("No Child",homeContext);*/
			}
			else{
				parseJSON(result);
			}
		}
	}
	//parse the Result which is coming from the JSon
	void parseJSON(String result)
	{
		try {
			JSONArray array=new JSONArray(result);

			if(array.length()<2|array.length()>2)
			{
				valid=false;
			}else{
				JSONObject jo=array.getJSONObject(0);
				PrincipleID  =jo.getString("TeacherID");
				PrincipleName=jo.getString("FirstName");
				JSONObject jot=array.getJSONObject(1);
				TeacherID= jot.getString("TeacherID");
				TeacherName=jot.getString("FirstName");

				Log.d("---------", ""+PrincipleID);

				setData();
			}

		} catch (JSONException e) {
			CustomDialog.displayDialog(e.getMessage(),homeContext);
		}
	}

	private void setData() {
		text_priname.setText(PrincipleName);
		text_teacname.setText(TeacherName);
	}


}
