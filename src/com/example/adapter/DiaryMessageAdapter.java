package com.example.adapter;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.IconTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dto.DiaryDetails;
import com.primesys.mitra.Common;
import com.primesys.mitra.DiaryDetailsActivity;
import com.primesys.mitra.R;

public class DiaryMessageAdapter extends BaseAdapter {

	ArrayList<DiaryDetails> listDiaryDetails;
	Context context;
	Animation animation = null;
	String type,studentID;
	ProgressDialog pDialog;
	File dFile;
	String path="";

	public DiaryMessageAdapter(Context context,ArrayList<DiaryDetails> list,String type,String studentID) {
		this.context=context;
		this.listDiaryDetails=list;
		this.type=type;
		this.studentID=studentID;
	}

	@Override
	public int getCount() {
		return listDiaryDetails.size();
	}

	@Override
	public DiaryDetails getItem(int position) {
		return listDiaryDetails.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DiaryDetails d=getItem(position);
		DiaryHolder diaryholder;
		
		if(convertView==null)
		{
			LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView=inflater.inflate(R.layout.diary_row, parent,false);
			diaryholder=new DiaryHolder();
			diaryholder.icon_status=(IconTextView)convertView.findViewById(R.id.icon_status);
			diaryholder.icon_fromto=(IconTextView)convertView.findViewById(R.id.icon_fromto);
			diaryholder.text_name=(TextView)convertView.findViewById(R.id.text_name);
			diaryholder.imageArrow=(ImageView)convertView.findViewById(R.id.image_arrow);
			diaryholder.icon_attach=(ImageView)convertView.findViewById(R.id.icon_attach);
			diaryholder.text_date=(TextView)convertView.findViewById(R.id.text_date);
			convertView.setTag(diaryholder);
		}
		else{
			diaryholder=(DiaryHolder)convertView.getTag();
		}
		animation = AnimationUtils.loadAnimation(context, R.anim.animlistview);
		final TextView text_message=(TextView)convertView.findViewById(R.id.text_message);
		text_message.setText(d.getMessage());
		text_message.setTag(position);
		if(type.equals("P"))
		{
			if(d.getFrom_id()==Integer.parseInt(studentID))
			{
				diaryholder.text_name.setText(d.getStudentName());
				diaryholder.icon_fromto.setText("{fa-arrow-up}");
				diaryholder.icon_fromto.setTextColor(context.getResources().getColor(R.color.green));
			}
			else{
				diaryholder.text_name.setText(d.getTeacherName());
				diaryholder.icon_fromto.setText("{fa-arrow-down}");
				diaryholder.icon_fromto.setTextColor(context.getResources().getColor(R.color.blue));
			}
		}
		else if(type.equals("T"))
		{
			if(d.getFrom_id()==Integer.parseInt(Common.userid))
			{
				diaryholder.text_name.setText(d.getTeacherName());
				diaryholder.icon_fromto.setText("{fa-arrow-up}");
				diaryholder.icon_fromto.setTextColor(context.getResources().getColor(R.color.green));
			}
			else{
				diaryholder.text_name.setText(d.getStudentName());
				diaryholder.icon_fromto.setText("{fa-arrow-down}");
				diaryholder.icon_fromto.setTextColor(context.getResources().getColor(R.color.blue));
			}

		}
		Animation anim = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
		anim.setDuration(500); // duration - half a second
		anim.setInterpolator(new LinearInterpolator()); // do not alter animation rate
		anim.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
		anim.setRepeatMode(Animation.REVERSE);

		if(d.getStatus().equals("R"))
		{
			diaryholder.icon_status.setText("{fa-check-square-o}");
			diaryholder.icon_status.setTextColor(context.getResources().getColor(R.color.primary));
		}
		else
		{
			diaryholder.icon_status.setText("{fa-star}");
			diaryholder.icon_status.setAnimation(anim);
		}
		diaryholder.imageArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(context,DiaryDetailsActivity.class);
				intent.putExtra("diary", listDiaryDetails.get((Integer)text_message.getTag()));
				intent.putExtra("StudentID", studentID);
				intent.putExtra("Type", type);
				((Activity)context).startActivity(intent);
				((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
			if(listDiaryDetails.get((Integer)text_message.getTag()).getDocuments().length()==0||listDiaryDetails.get((Integer)text_message.getTag()).getDocuments()==null)
				diaryholder.icon_attach.setVisibility(View.GONE);
		else
			diaryholder.icon_attach.setVisibility(View.VISIBLE);
			diaryholder.icon_attach.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String url=listDiaryDetails.get((Integer)text_message.getTag()).getDocuments();
				if(url.length()==0||url==null)
					Common.showToast("There is no attachment !", context);
				else
				{
					url=url.replace("~", "");
					url=Common.relativeurl+url;
					if (Common.connectionStatus) {
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
							new DownloadFileFromURL().executeOnExecutor(
									AsyncTask.THREAD_POOL_EXECUTOR, url);
						} else {
							new DownloadFileFromURL().execute(url);
						}
					}
				}
			}
		});
			diaryholder.text_date.setText(convertDate(d.getMsg_record_dt_time()));

		animation.setDuration(500);
		convertView.startAnimation(animation);
		return convertView;
	}
	// create progress dialog for downloading file
	Dialog createDialog()
	{
		pDialog=new ProgressDialog(context);
		pDialog.setMessage("Downloading file. Please wait......");
		pDialog.setIndeterminate(false);
		pDialog.setMax(100);
		pDialog.setCancelable(true); 
		pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pDialog.show();
		return pDialog;
	}

	String convertDate(String date) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
			Date d = format.parse(date);
			SimpleDateFormat serverFormat = new SimpleDateFormat("EEEE, MMM dd");
			return serverFormat.format(d);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			createDialog();
		}
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				path=Environment.getExternalStorageDirectory().toString()+"/"+ URLUtil.guessFileName(f_url[0], null, null);
				conection.connect();
				int lenghtOfFile = conection.getContentLength();

				// download the file
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);
				// Output stream
				OutputStream output = new FileOutputStream(path);
				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					publishProgress(""+(int)((total*100)/lenghtOfFile));
					// writing data to file
					output.write(data, 0, count);
				}
				// flushing output
				output.flush();

				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}
		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			Common.showToast("Downloading Completed", context);
			dFile=new File(path);
			Intent intent = new Intent();
			intent.setAction(android.content.Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(dFile),getMimeType(dFile.getAbsolutePath()));
			 if (null != intent.resolveActivity( ((Activity)context).getPackageManager())) {
				 ((Activity)context).startActivity(intent);
			    }
			 
		}

		private String getMimeType(String url)
		{
			String parts[]=url.split("\\.");
			String extension=parts[parts.length-1];
			String type = null;
			if (extension != null) {
				MimeTypeMap mime = MimeTypeMap.getSingleton();
				type = mime.getMimeTypeFromExtension(extension);
			}
			return type;
		}
	}
	// view holder for diary 
	static class DiaryHolder
	{
		IconTextView icon_status;
		IconTextView icon_fromto;
		TextView text_name;
	 	ImageView imageArrow;
	 	ImageView icon_attach;
	 	TextView text_date;
	}
	
}
