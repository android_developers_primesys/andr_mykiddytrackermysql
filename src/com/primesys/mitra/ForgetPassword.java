package com.primesys.mitra;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class ForgetPassword extends Activity {

	Button btnSave,btnClear;
	EditText txtUserId;
	TextView txtnews;
	String struserId;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	String TAG="forget Passwors";
	Context forgetContext=ForgetPassword.this;
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private String url=Common.URL+"LoginServiceAPI.asmx/ForgetPassward";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forget_password);
		FindViewByID();


		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (Validation()) {
					txtUserId.setError(null);
					//CALL API Here Using Volley
					try{
						postForgetPasswordREQ();
						setClear();

					}catch(Exception ex)
					{

					}
				}
			}
		});

		btnClear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setClear();

			}
		});

	}

	void setClear() {
		txtUserId.setError(null);
		txtUserId.setText("");

	}

	boolean Validation() {
		Boolean valid=true;
		struserId=txtUserId.getText().toString().trim();
		if (struserId.length()==0) {
			txtUserId.setError("Enter valid Email-ID");
			txtUserId.requestFocus();
			valid=false;
		}else
			if (!struserId.equalsIgnoreCase("")) {
				Pattern pattern = Pattern.compile(EMAIL_PATTERN);
				Matcher matcher = pattern.matcher(struserId);
				valid=matcher.matches();
				if (valid==false) {
					txtUserId.setError("Enter Email-ID ");
					txtUserId.requestFocus();
					valid=false;
				}
			}
		return valid;
	}

	void FindViewByID() {
		btnSave=(Button)findViewById(R.id.btn_submit);
		btnClear=(Button)findViewById(R.id.btn_Cancel);
		txtUserId=(EditText)findViewById(R.id.text_ID);
		txtnews=(TextView)findViewById(R.id.text_news);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater=getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);


	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			Intent redirecLogin=new Intent(getApplicationContext(),LoginActivity.class);
			startActivity(redirecLogin);
			finish();
			break;


		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	//Use Volley
	// make volley request
	void postForgetPasswordREQ()
	{
		reuestQueue=Volley.newRequestQueue(forgetContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(forgetContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,url,new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				try{
					System.out.println(response);
					parseJSON(response);
					pDialog.dismiss();	
				}catch(Exception ex){

				}

			}

		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				try{
					if(error.networkResponse != null && error.networkResponse.data != null){
						VolleyError er = new VolleyError(new String(error.networkResponse.data));
						error = er;
						System.out.println(error.toString());
						parseJSON(new String(error.networkResponse.data));
					}	
				}catch(Exception ex){

				}

			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("Email_Id", struserId);
				return params;
			}

		};
		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	//Parsing Response Coming From  server
	void parseJSON(String response) {
		try
		{
			JSONObject jo=new JSONObject(response);
			if (jo.getString("msg").contains("Your credentials send to your email Id.")) {
				Intent loginIntent = new Intent(forgetContext, LoginActivity.class);
				startActivity(loginIntent);
				finish();
				Common.showToast("Your credentials send to your email Id",forgetContext);
			}else if(jo.getString("msg").contains("Wrong Email Id")){
				Common.showToast(getResources().getString(R.string.Wrong_email), forgetContext);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
