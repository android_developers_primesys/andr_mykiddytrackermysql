package com.primesys.mitra;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AddChildActivity extends Activity {


	Button btnAdd;
	EditText textUsername,textPassword;
	Context addContext=AddChildActivity.this;
	private String username,password;
	ProgressDialog pDialog;
	TextView textNews;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_child);		
		findViewBYID();
		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		textNews.setTypeface(custom_font);
		textNews.setText(APIController.getInstance().getNews());
		btnAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(validation())
				{
					if(Common.connectionStatus)  
					{
						if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
							new AddChildTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
						}
						else{
							new AddChildTask().execute();
						}
					}
					Clear();
				}
			}
		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	boolean  validation()
	{
		boolean valid=true;
		username=textUsername.getText().toString().trim();
		password=textPassword.getText().toString().trim();
		if(username.length()==0||username==null)
		{
			Common.showToast("Please enter the username !",addContext);
			valid=false;
		}
		else if(password.length()==0||password==null)
		{
			Common.showToast("Please enter the password !",addContext);
			valid=false;
		} 
		return valid;
	}
	

	// find View By ID
	void findViewBYID()
	{
		btnAdd=(Button)findViewById(R.id.btn_add);
		textUsername=(EditText)findViewById(R.id.text_user);
		textPassword=(EditText)findViewById(R.id.text_pass);
		textNews=(TextView)findViewById(R.id.text_news);
	}

	void Clear()
	{
		textUsername.setText("");
		textPassword.setText("");
	}
	
	// send the async request to add the children into the current user
	//Asynchronous logn task for the login
	class AddChildTask extends AsyncTask<Void, Void, String>
	{
		String url=Common.URL+"ParentAPI.ASMX/AddNewChild";
		@Override
		protected void onPreExecute() {
			pDialog=new ProgressDialog(addContext);
			pDialog.setMessage("Progress  wait...");
			pDialog.setCancelable(false);
			pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String result="User is not registered";

			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(url);
			try
			{
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(3);
				postParameter.add(new BasicNameValuePair("UserId",Common.userid));
				postParameter.add(new BasicNameValuePair("UserName",textUsername.getText().toString().trim()));
				postParameter.add(new BasicNameValuePair("Password",textPassword.getText().toString().trim()));


				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));

				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			parseJSON(result);
		}
	}

	//parse the JSON response
	void parseJSON(String result)
	{
		System.out.println(result);
		try
		{
			JSONArray array=new JSONArray(result);
			JSONObject jo=array.getJSONObject(0);
			CustomDialog.displayDialog(jo.getString("msg"),addContext);
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
		}
	}
}
