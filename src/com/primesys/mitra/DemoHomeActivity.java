package com.primesys.mitra;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;










import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.dto.MyKiddyTrackerContact;
import com.example.dto.UserContact;
import com.google.gson.Gson;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;
import com.nemezis.sample.contacts.AccountGeneral;
import com.nemezis.sample.contacts.ContactsManager;
import com.primesys.friendtrack.Map_service;
import com.primesys.friendtrack.Track_User;

public class DemoHomeActivity extends Activity {

	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	Button btnChatMsg,btnchatFrnd,btnfrndTrack,btndiary,btnQuiz,btnfrdtrack;
	Context dHomeContext=DemoHomeActivity.this;
	MenuItem username;
	Bitmap G_image;
	ByteArrayOutputStream stream;
	String TAG="DemoHomeActivity";
	String  UPLOAD_FILE_PATH="/sdcard/Download/images.jpg";
	public static String contactJson=null;
	Gson gson=new Gson();
	String contact;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_demo_home);
		findViewbyID();
		///##################################################################################
		//Add MyKiddytracker Account Here for contact Synchronization
		if (Common.getConnectivityStatus(dHomeContext)) {
			addNewAccount(AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS);
			try{
				if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
					new GetUserContact().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
				}
				else{
					new GetUserContact().execute();
				}		
			}catch(Exception ex){
				System.err.print(ex);

			}

		}

		//################################################################################


		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);


		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);

		GPSEnableSetting gps=new GPSEnableSetting();
		gps.GPSDialog(dHomeContext);
		btnChatMsg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(dHomeContext,CommunicationNew.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});
		//send messages to all
		btnchatFrnd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(dHomeContext,DemoUserActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});
		//create diary
		btnfrndTrack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(dHomeContext,ShowGmapClient.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});
		
	
		//report complaint 
		/*btnMail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intentemail=new Intent(dHomeContext,SendMailActivity.class);
				startActivity(intentemail);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});
		*/
		btndiary.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intentemail=new Intent(dHomeContext,DigitalDiaryActivity.class);
				startActivity(intentemail);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});
		
		//report complaint 
		btnQuiz.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						Intent feedIntent=new Intent(DemoHomeActivity.this,QuizGridDashBord.class);
						startActivity(feedIntent);
					}
				});
		
		
		btnfrdtrack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent feedIntent=new Intent(DemoHomeActivity.this,Track_User.class);
				startActivity(feedIntent);
			}
		});
	}

	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);



			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(dHomeContext)&& APIController.getInstance().getSchool_logo()!=null) {
				imageLoader = new ImageLoader(
						Volley.newRequestQueue(dHomeContext), imageCache);
				if (Common.getConnectivityStatus(dHomeContext)) {


					(img_school).setImageUrl(
							Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
					((CircularNetworkImageView) img_school)
					.setDefaultImageResId(R.drawable.student);
					((CircularNetworkImageView) img_school)
					.setErrorImageResId(R.drawable.student);
				}
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(dHomeContext)&& APIController.getInstance().getSchool_logo()!=null) {
				imageLoader = new ImageLoader(
						Volley.newRequestQueue(dHomeContext), imageCache);
				if (Common.getConnectivityStatus(dHomeContext)) {

					(img_school).setImageUrl(
							Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);}
				((CircularNetworkImageView) img_school)
				.setDefaultImageResId(R.drawable.student);
				((CircularNetworkImageView) img_school)
				.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}


	}

	void findViewbyID()
	{
		textNews=(TextView)findViewById(R.id.text_news);
		btnChatMsg=(Button)findViewById(R.id.image_com);
		btnchatFrnd=(Button)findViewById(R.id.image_frndcom);
		btnfrndTrack=(Button)findViewById(R.id.image_track);
		btndiary=(Button)findViewById(R.id.image_diary);
		textNews=(TextView)findViewById(R.id.text_news);
		btnfrdtrack=(Button)findViewById(R.id.image_frd_track);

		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		btnQuiz=(Button) findViewById(R.id.image_exam);
		if (Common.getConnectivityStatus(dHomeContext)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(dHomeContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
		 //Satrt Service
	    if (Common.getConnectivityStatus(dHomeContext)) {
	    	  try {
	    		  Intent startServiceIntent =new Intent(getBaseContext(), Map_service.class);
	    		  startService(startServiceIntent);

	    		//  Getlocation();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
		startActivity(intent);
		finish();
		System.exit(0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_logout, menu);
		return super.onCreateOptionsMenu(menu);

	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		try{
			username=menu.findItem(R.id.username);
			username.setTitle(Common.username);
			username.setIcon(new IconDrawable(this, IconValue.fa_user).colorRes(R.color.primary).actionBarSize());
			menu.findItem(R.id.share).setIcon(new IconDrawable(this, IconValue.fa_share_alt).colorRes(R.color.primary).actionBarSize());
			menu.findItem(R.id.add).setIcon(new IconDrawable(this, IconValue.fa_map_marker).colorRes(R.color.primary).actionBarSize());
			menu.findItem(R.id.renew).setIcon(new IconDrawable(this, IconValue.fa_refresh).colorRes(R.color.primary).actionBarSize());
			menu.findItem(R.id.changepass).setIcon(new IconDrawable(this, IconValue.fa_pencil).colorRes(R.color.primary).actionBarSize());
			menu.findItem(R.id.reportcomplaint).setIcon(new IconDrawable(this, IconValue.fa_pencil).colorRes(R.color.primary).actionBarSize());

		}catch(Exception ex){

		}
		return super.onPrepareOptionsMenu(menu);
	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.username:
			Intent intentProfile=new Intent(dHomeContext,UserProfileActivity.class);
			startActivity(intentProfile);
			/*Common.showToast(getResources().getString(R.string.welcome)+Common.username, dHomeContext);*/
			return true;
		case R.id.add:
			Common.showToast(getResources().getString(R.string.demo_msg), dHomeContext);
			return true;
		case R.id.share:
			shareIt();
			return true;
		case R.id.renew:
			Common.showToast(getResources().getString(R.string.demo_msg), dHomeContext);
			return true;
		case R.id.changepass:

			if (Common.user_regType.equalsIgnoreCase("Google")||Common.user_regType.equalsIgnoreCase("Facebook")) {


			}else{
				Intent chIntent=new Intent(DemoHomeActivity.this,ChanagePassword.class);
				startActivity(chIntent);
			}


			return true;
		case R.id.reportcomplaint:
			
			if (Common.getConnectivityStatus(dHomeContext)) {
				Intent intentemail=new Intent(dHomeContext,SendMailActivity.class);
				startActivity(intentemail);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);

			}
			

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	// share the application
	private void shareIt() {
		//sharing implementation here
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "MyKiddyTrack");
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.sharemessaage));
		startActivity(sharingIntent);
	}
	/*//upload photo or video and send using socket in binary format
	//convert file to byte[]
	byte[] convertFile()
	{
		byte[] data=null;
		System.out.println("Inside the method");
		RandomAccessFile f =null;
		try {
			f = new RandomAccessFile("/sdcard/Download/images.jpg", "r");
			// Get and check length
			long longlength = f.length();
			int length = (int) longlength;
			if (length != longlength)
				throw new IOException("File size >= 2 GB");
			// Read file and return data
			data = new byte[length];
			f.readFully(data);
			//return data;
		} catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			try
			{
				f.close();
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		display(data);
		return data;
	}
	//display the byte array
	void display(byte[] data)
	{
		for (int i = 0; i < data.length; i++) {
			System.out.println(data[i]);
		}
	}*/
	/*// make ne wevent to upload image from app
	String uploadImage(byte[] data)
	{
		String trackSTring="{}";
		try{
			JSONObject jo=new JSONObject();
			jo.put("event","multimedia_msg");
			jo.put("data",new String(data));
			trackSTring=jo.toString();
		}
		catch(Exception e)
		{
			System.err.println("check in JSOn");
         e.printStackTrace();
		}
		System.err.println(trackSTring);
		return trackSTring;
	}*/
	//compressing Image 
	class BitMapAsync extends AsyncTask<Void, String, String>{

		@Override
		protected String doInBackground(Void... params) {
			String result="Success";
			try{
				G_image=getRoundedShape(BitmapFactory.decodeStream((InputStream)new URL(Common.relativeurl+Common.markerImg.replaceAll("~", "").trim()).getContent()));
				stream = new ByteArrayOutputStream();
				G_image.compress(Bitmap.CompressFormat.PNG, 100, stream);
			}catch(Exception e){
				G_image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_boy);
			}
			return result;
		}

	}
	private Bitmap getRoundedShape(Bitmap bitmap) {
		int targetWidth = 100;
		int targetHeight = 100;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, 
				targetHeight,Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), 
						((float) targetHeight)) / 2),
						Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = bitmap;
		canvas.drawBitmap(sourceBitmap, 
				new Rect(0, 0, sourceBitmap.getWidth(),
						sourceBitmap.getHeight()), 
						new Rect(0, 0, targetWidth, targetHeight), null);
		return targetBitmap;

	}
	class TrackInfrmation extends AsyncTask<Void, String, String>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(Void... params) {
			String result="";
			try{
				HttpClient httpclient=new DefaultHttpClient();
				HttpPost httpost=new HttpPost(Common.URL+"ParentAPI.asmx/GetTrackInfo");
				List<NameValuePair> param=new ArrayList<NameValuePair>(1);
				param.add(new BasicNameValuePair("ParentId", Common.userid));
				httpost.setEntity(new UrlEncodedFormEntity(param));
				HttpResponse response = httpclient.execute(httpost);
				result=EntityUtils.toString(response.getEntity());
				Log.e("response", ""+result);
			}catch(Exception e){
				result=e.getMessage();
			}

			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			parsingTrackInfo(result);

		}
	}
	public void parsingTrackInfo(String result) {
		try{
			JSONArray joArray=new JSONArray(result);
			for (int i = 0; i < joArray.length(); i++) {
				JSONObject joObject =joArray.getJSONObject(i);

				if (i<=0) {
					Common.TrackUser=joObject.getString("Name");
				}

			}


		}catch(Exception e){
			Log.e("Exception", ""+e);

		}
	}
	private void addNewAccount(String accountType, String authTokenType) {
		final AccountManagerFuture<Bundle> future = AccountManager.get(this).addAccount(accountType, authTokenType, null, null, this, new AccountManagerCallback<Bundle>() {
			@Override
			public void run(AccountManagerFuture<Bundle> future) {
				try {
					Bundle bnd = future.getResult();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, null);
	}
	public class GetUserContact extends AsyncTask <Void, Void, String> {

		@Override
		protected void onPreExecute() {
		}
		@Override
		protected String doInBackground(Void... params) {
			String result = null;
			try
			{
				readContact();

			}catch(Exception e)
			{

				e.printStackTrace();
			}

			return result;
		}
	}
	private void readContact() {

		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
				null, null, null, null);
		ArrayList<UserContact> list=new ArrayList<UserContact>();
		//set to Contact Class
		UserContact  contact = null;

		if (cur.getCount() > 0) {

			while (cur.moveToNext()) {
				String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
					System.out.println("name : " + name + ", ID : " + id);


					// get the phone number
					Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
							new String[]{id}, null);
					while (pCur.moveToNext()) {
						String phone = pCur.getString(
								pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						System.out.println("phone" + phone);
						contact=new UserContact();
						contact.setName(name);
						contact.setPhoneno(phone);
					}
					pCur.close();


					// get email and <span id="IL_AD7" class="IL_AD">type</span>
					Cursor emailCur = cr.query(
							ContactsContract.CommonDataKinds.Email.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
							new String[]{id}, null);
					if (emailCur.getCount()>0) {
						while (emailCur.moveToNext()) {
							// This would allow you get several email addresses
							// if the email addresses were stored in an array
							String email = emailCur.getString(
									emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
							String emailType = emailCur.getString(
									emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));System.out.println("Email " + email + " Email Type : " + emailType);contact.setEmail(email);
						}
					}else {
						contact.setEmail("");
					}

					emailCur.close();
					list.add(contact);

				}
			}
		}


		//send Information to Server	
		contactJson=gson.toJson(list);

		try{
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				new PushContactServer().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			}
			else{
				new PushContactServer().execute();
			}		
		}catch(Exception ex){
			System.err.print(ex);

		}

	}
	//Contact verify from server
	class PushContactServer extends AsyncTask <Void, Void, String> {

		@Override
		protected void onPreExecute() {

		}
		@Override
		protected String doInBackground(Void... params) {
			String result="";

			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(Common.URL+"LoginServiceAPI.asmx/GetContactDetails");
			try
			{


				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("ContactMobileNoString",contactJson));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	
				e.printStackTrace();
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			Log.e("PushContactServer", ""+result);
			ParseContactInformation(result);

		}
	}

	//Parsing Contact from Server
	public void ParseContactInformation(String result) {

		try{
			Log.e("MYkIddyTracker Contact###############################################", ""+result);

			ArrayList<MyKiddyTrackerContact> list=new ArrayList<MyKiddyTrackerContact>();

			JSONArray joarray=new JSONArray(result);
			for (int i = 0; i < joarray.length(); i++) {

				MyKiddyTrackerContact mykidd=new MyKiddyTrackerContact();
				JSONObject jobject=joarray.getJSONObject(i);
				mykidd.setEmailId(jobject.getString("EmailID"));
				mykidd.setName(jobject.getString("Name"));
				mykidd.setContactno(jobject.getString("ContactNo"));
				mykidd.setID(jobject.getString("Id"));

				/*	//adding  contact
							ContactsManager.addContact(context, mykidd);*/

				//updating  contact
				contact=jobject.getString("ContactNo");
				list.add(mykidd);
				/*	ContactsManager.updateMyContact(dHomeContext, mykidd);*/
				/*if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
						new AddUserContact().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
					}
					else{
						new AddUserContact().execute();
					}	*/
				LoginActivity.mClient.sendMessage(addFriendForChat(jobject.getString("Name"), jobject.getString("Id")));

			}

			ContactsManager.addContact(dHomeContext, list);

		}catch(Exception ex)
		{
			Log.e("Exception ", ""+ex);
		}

	}
	class AddUserContact extends AsyncTask<Void, String, String>{
		String result="";
		HttpClient client=new DefaultHttpClient();
		HttpPost post=new HttpPost(Common.URL+"ParentAPI.asmx/SearchFriendList");

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}
		@Override
		protected String doInBackground(Void... params) {
			try{
				List<NameValuePair> list=new ArrayList<NameValuePair>(3);
				list.add(new BasicNameValuePair("UserId", Common.userid));
				list.add(new BasicNameValuePair("Entity", contact));
				list.add(new BasicNameValuePair("Flag", "1"));
				post.setEntity(new UrlEncodedFormEntity(list));
				HttpResponse response=client.execute(post);
				result=EntityUtils.toString(response.getEntity());

			}catch(Exception ex){
				result=ex.getMessage();	
			}


			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			parseJSON(result);


		}
	}
	public void parseJSON(String result) {
		try {
			JSONArray array=new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo=array.getJSONObject(i);
				LoginActivity.mClient.sendMessage(addFriendForChat(jo.getString("Name"), jo.getString("Id")));
			}
		} catch (Exception e) {
			Log.e("ParsonJSON", ""+e);
		}

	}


	//add Friend
	public static String addFriendForChat(String friendName,String friendId)
	{
		String addSTring="{}";
		try{

			System.out.print("##Sending DATA ###");
			JSONObject jo=new JSONObject();
			jo.put("event","add_friend");
			jo.put("user_id",Common.userid);
			jo.put("user_name",Common.username);
			jo.put("friend_id",friendId);
			jo.put("friend_name",friendName);
			addSTring=jo.toString();
		}
		catch(Exception e)
		{
			Log.e("addFriendForChat", ""+e);
		}
		return addSTring; 
	}

}
