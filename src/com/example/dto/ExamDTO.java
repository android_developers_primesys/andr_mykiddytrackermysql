package com.example.dto;

import java.io.Serializable;
import java.util.ArrayList;

import com.example.dto.OptionDTO;

public class ExamDTO implements Serializable {

	String QuestionId,QuestionLabel,OptionId,OptionLabel,Answer,ExamMasterId,AnswerOptionId,AnsType;
 public String getAnswerOptionId() {
		return AnswerOptionId;
	}

	public void setAnswerOptionId(String answerOptionId) {
		AnswerOptionId = answerOptionId;
	}

	public String getAnsType() {
		return AnsType;
	}

	public void setAnsType(String ansType) {
		AnsType = ansType;
	}

public String getExamMasterId() {
		return ExamMasterId;
	}

	public void setExamMasterId(String examMasterId) {
		ExamMasterId = examMasterId;
	}

ArrayList<OptionDTO> list;
	public ArrayList<OptionDTO> getList() {
	return list;
}

public void setList(ArrayList<OptionDTO> olist) {
	this.list = olist;
}

	public String getQuestionId() {
		return QuestionId;
	}

	public void setQuestionId(String questionId) {
		QuestionId = questionId;
	}

	public String getQuestionLabel() {
		return QuestionLabel;
	}

	public void setQuestionLabel(String questionLabel) {
		QuestionLabel = questionLabel;
	}

	public String getOptionId() {
		return OptionId;
	}

	public void setOptionId(String optionId) {
		OptionId = optionId;
	}

	public String getOptionLabel() {
		return OptionLabel;
	}

	public void setOptionLabel(String optionLabel) {
		OptionLabel = optionLabel;
	}

	public String getAnswer() {
		return Answer;
	}

	public void setAnswer(String answer) {
		Answer = answer;
	}
}
