package com.example.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adapter.QuizLevelAdpter.RecordHolder;
import com.example.dto.QuizLevelDTO;
import com.primesys.mitra.QuizzLevelActivity;
import com.primesys.mitra.R;

public class leveladpter extends ArrayAdapter<String> {

	Context context;
	int layoutResourceId;
	ArrayList<String> data = new ArrayList<String>();

	public leveladpter(Context context, int layoutResourceId,
			ArrayList<String> allLevel) {
		super(context, layoutResourceId);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = allLevel;
	}

	/*public QuizLevelAdpter(QuizzLevelActivity quizzLevelActivity,
			int quizLevelRow, ArrayList<String> allLevel) {
		// TODO Auto-generated constructor stub
	}*/
	
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;
System.out.println("Inside Lvel adopter"+data.toString());
		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			
			row.setTag(position);

		} else {
		//	holder = (RecordHolder) row.getTag();
		}
		
	//	CategoryDashbord item = data.get(position);
	
		
		final TextView level_label=(TextView)row.findViewById(R.id.lbl_level);
		level_label.setText(data.get(position));
		level_label.setTag(position);
	/*	level_label = (ImageView) row.findViewById(R.id.lbl_level);
		imageItem.setImageBitmap(item.getImage());*/

		
	

		row.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	Intent intent=new Intent(context,QuizzLevelActivity.class);
            //	CategoryDashbord Cat=data.get((Integer) level_label.getTag());
        /*    	System.out.println(Cat.getTitle());
    			intent.putExtra(CATEGORY_KEY,Cat.getTitle());
    			intent.putExtra(QUIZID_KEY,Cat.getQuiz_id());   */
    			context.startActivity(intent);
    			((Activity) context).overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
            }
        });

		return row;
		
		
	
		
		
		

	}

	static class RecordHolder {
		TextView lbl_level,score,rank,status;
		ImageView imageItem;

	}
	
}
