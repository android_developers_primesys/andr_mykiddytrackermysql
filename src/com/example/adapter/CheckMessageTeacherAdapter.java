package com.example.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.IconTextView;
import android.widget.TextView;

import com.primesys.mitra.Common;
import com.primesys.mitra.LoginActivity;
import com.primesys.mitra.MainTeacher;
import com.primesys.mitra.R;

public class CheckMessageTeacherAdapter extends BaseAdapter {
	private ArrayList<String> mListItems;
	private LayoutInflater mLayoutInflater;
	Context recentContext;
	public static String  MsgSender;
	public HashMap<String, String> sendername=LoginActivity.parent_info;
	public HashMap<String, String> recv_time=LoginActivity.msgrecv_time;
	String pass_name;
	int flag=0;

	public CheckMessageTeacherAdapter(Context context, ArrayList<String> arrayList){
		mListItems = arrayList;
		recentContext=context;
		//get the layout inflater
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	@Override
	public int getCount() {
		//getCount() represents how many items are in the list
		return mListItems.size();
	}
	@Override
	public Object getItem(int i) {
		return null;
	}
	@Override
	//get the position id of the item from the list
	public long getItemId(int i) {
		return 0;
	}
	@Override
	public View getView(final int position, View view, ViewGroup viewGroup) {
		//check to see if the reused view is null or not, if is not null then reuse it
		if (view == null) {
			view = mLayoutInflater.inflate(R.layout.activity_recent, null);
		}
		//get the string item from the position "position" from array list to put it on the TextView
		final Button itemName = (Button) view.findViewById(R.id.recent);
		TextView recvtime = (TextView) view.findViewById(R.id.time);
		IconTextView icon_status=(IconTextView)view.findViewById(R.id.icon_status);

		Animation anim = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
		anim.setDuration(500); // duration - half a second
		anim.setInterpolator(new LinearInterpolator()); // do not alter animation rate
		anim.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
		anim.setRepeatMode(Animation.REVERSE);
		icon_status.setAnimation(anim);
		String MsgSender = mListItems.get(position);
		if (MsgSender != null) {

			//for setting name in recent Message
			for(Map.Entry entry: sendername.entrySet()){
				if(entry.getKey().equals(MsgSender)){
					itemName.setText(entry.getValue().toString());
					pass_name=entry.getValue().toString();
				}
			} 
			//for setting time in recent messages
			for(Map.Entry entry: recv_time.entrySet()){
				if(entry.getKey().equals(MsgSender)){
					recvtime.setText(Common.getDateCurrentTimeZone(Long.parseLong(entry.getValue().toString())));
				}
			}  
		}
		itemName.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try{

					String MsgSender = mListItems.get(position); 
					Intent intent=new Intent(recentContext,MainTeacher.class);
					intent.putExtra("to",MsgSender);

					for(Map.Entry entry: sendername.entrySet()){
						if(entry.getKey().equals(MsgSender)){
							itemName.setText(entry.getValue().toString());
							intent.putExtra("name",entry.getValue().toString());
						}
					} 
					recentContext.startActivity(intent);
					((Activity)recentContext).overridePendingTransition(R.anim.right_in, R.anim.left_out);
				}catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		});

		return view;
	}

}
