package com.primesys.friendtrack;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.commonsware.cwac.locpoll.LocationPoller;
import com.commonsware.cwac.locpoll.LocationPollerParameter;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;






public class Map_service extends IntentService {
	 Context context = this;
	 private static final int PERIOD=900000; 	// 15 minutes==900000 , 10min==600000
		private PendingIntent pi=null;
		private AlarmManager mgr=null;
		
		
    public Map_service() {
       super("MyTestService");
       
    }

	@Override
	protected void onHandleIntent(Intent intent) {
		
		try {
			System.err.println("Inside service");

			mgr=(AlarmManager)getSystemService(ALARM_SERVICE);
			
			Intent locintent=new Intent(this, LocationPoller.class);
			
			Bundle bundle = new Bundle();
			LocationPollerParameter parameter = new LocationPollerParameter(bundle);
			parameter.setIntentToBroadcastOnCompletion(new Intent(this, LocationReceiver.class));
			// try GPS and fall back to NETWORK_PROVIDER
			parameter.setProviders(new String[] {LocationManager.GPS_PROVIDER, LocationManager.NETWORK_PROVIDER});
			parameter.setTimeout(60000);	
			System.out.println(bundle.toString());
			locintent.putExtras(bundle);
			
			pi=PendingIntent.getBroadcast(this, 0,locintent, 0);
	        String TAG="sgg";

			 // Cancel alarms
		    try {
		    	mgr.cancel(pi);
				Log.e(TAG, "AlarmManager update was  canceled. ");

		    } catch (Exception e) {
				Log.e(TAG, "AlarmManager update was not canceled. " + e.toString());
		    }
		    
		    
			mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,SystemClock.elapsedRealtime(),PERIOD,pi);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  
	
	
	}

	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		boolean enabled=true;
		// TODO Auto-generated method stub
		setIntentRedelivery(enabled);
		return super.onStartCommand(intent, flags, startId);
	}
	/*@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		return START_STICKY;
	}*/


}




