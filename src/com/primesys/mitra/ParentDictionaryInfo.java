package com.primesys.mitra;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.adapter.ParentDitinaryListAdapter;
import com.example.dto.Student;

public class ParentDictionaryInfo extends Activity {


	ListView userList;
	ParentDitinaryListAdapter userAdapter;
	ArrayList<Student> userArray = new ArrayList<Student>();
	String SelectedClassID;
	Context context=ParentDictionaryInfo.this;
	ProgressDialog pDialog;
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_parentdicionary_list);

		findViewById();
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);



		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		if(Common.connectionStatus)  
		{
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				new GetClassInformation().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			}
			else{
				new GetClassInformation().execute();
			}
		}
	}

	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(context), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(context), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}



	}

	private void findViewById() {
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {

		imageLoader = new ImageLoader(
				Volley.newRequestQueue(context), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	public class GetClassInformation  extends  AsyncTask <Void, Void, String>{

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(ParentDictionaryInfo.this);
			pDialog.setMessage("Progress wait....");
			pDialog.setCancelable(false);
			pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String result="";
			HttpClient httpClient=new DefaultHttpClient();

			HttpPost httpPost=new HttpPost(Common.URL+"PrincipleAPI.asmx/GetStudentForDigitalDairy");
			try
			{
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("teacher_id",Common.userid));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	

				e.printStackTrace();
			}
			Log.d("getParents:=>>", ""+result);
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			parseJSON(result);
		}

		void parseJSON(String result)
		{
			try {
				JSONArray array=new JSONArray(result);
				for (int i = 0; i < array.length(); i++) {
					JSONObject jo=array.getJSONObject(i);
					Student PList=new Student();
					PList.setStudentID(jo.getString("StudentID")); 
					PList.setFirstName(jo.getString("FirstName"));
					PList.setMiddleName(jo.getString("MiddleName"));
					PList.setLastName(jo.getString("LastName"));
					String photo=jo.getString("Photo");
					photo=photo.replace("~", "");
					PList.setPhoto(photo);
					userArray.add(PList);


				}
				userList=(ListView)findViewById(R.id.listview_student);
				userAdapter = new ParentDitinaryListAdapter(context, R.layout.activity_student_row, userArray);
				userList.setAdapter(userAdapter);


			} catch (JSONException e) {
				Log.d("ParsonJSON:", ""+e);
			}
		}

	}
}
