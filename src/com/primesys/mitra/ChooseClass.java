package com.primesys.mitra;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.adapter.ClassListAdapter;
import com.example.db.DBHelper;
import com.example.dto.ClassList;
import com.example.dto.MessageMain;
import com.example.dto.Student;
import com.primesys.emojilibrary.EmojiconEditText;
import com.primesys.emojilibrary.EmojiconGridView.OnEmojiconClickedListener;
import com.primesys.emojilibrary.EmojiconsPopup;
import com.primesys.emojilibrary.EmojiconsPopup.OnEmojiconBackspaceClickedListener;
import com.primesys.emojilibrary.EmojiconsPopup.OnSoftKeyboardOpenCloseListener;
import com.primesys.emojilibrary.emoji.Emojicon;

public class ChooseClass extends Activity {
	ListView userList;
	ClassListAdapter userAdapter;
	public static CheckBox selection;
	String Selected_ClassID;
	static String to;
	static Date date=new Date();
	static String formattedDate = new SimpleDateFormat("dd MMM yyyy hh:mm:ss").format(date);
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	Context context=ChooseClass.this;
	ArrayList<ClassList> userArray = new ArrayList<ClassList>();
	ArrayList<Student> userStudent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_classes);
		FindViewById();
		final EmojiconEditText editText = (EmojiconEditText) findViewById(R.id.editText);
		ImageView send = (ImageView)findViewById(R.id.send_button);
		final View rootView = findViewById(R.id.root_view);
		final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);
		final EmojiconsPopup popup = new EmojiconsPopup(rootView,this);
		//Will automatically set size according to the soft keyboard size
		popup.setSizeForSoftKeyboard();

		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);
		//img_school.setVisibility(View.INVISIBLE);
		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		if(Common.connectionStatus)  
		{
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				new GetClassLisAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			}
			else{
				new GetClassLisAsync().execute();
			}
		}
		send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String message = editText.getText().toString();
				boolean valid=false;
				if (message.equalsIgnoreCase("")) {
					editText.setError("Enter Message");
				}
				else{
					for (ClassList su:userArray) {
						if (su.getStatus()) {
							valid=true;
							break;
						}
					}
					if(valid){
						message = editText.getText().toString();
						for(ClassList su:userArray){
							if (su.getStatus()) {
								if(su.getParentid()!=null)
								{
									for(int j=0;j<su.getParentid().length;j++)
									{
										MessageMain m=new MessageMain();
										m.setMesageText(message);
										m.setDate_time(Common.convertToLong(formattedDate));
										m.setTo(su.getParentid()[j]);
										m.setFrom(Common.userid);
										m.setEvent("msg");
										m.setApp_id(Common.app_id);
										m.setType("s");
										m.setRef_id(Common.getRefNo());
										DBHelper helper=DBHelper.getInstance(getApplicationContext());
										helper.insertMessage(m,su.getStudentId()[j]);
										LoginActivity.mClientPri.messageSend(schoolMessage(m,su.getStudentId()[j]));
									}
								}
							}
						}
						Common.showToast("Message Sent.",ChooseClass.this);
						editText.setText("");
					}
					else{
						Common.showToast("Please Select Class",ChooseClass.this);
					}
				}
			}
		});
		selection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (selection.isChecked())
				{
					for(ClassList c : userArray)
						c.setStatus(true);
				}
				else if (!selection.isChecked())
				{
					for(ClassList c : userArray)
						c.setStatus(false);
				}
				userAdapter.notifyDataSetChanged();
				userList.setAdapter(userAdapter);

			}
		});

		popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {
			@Override
			public void onEmojiconClicked(Emojicon emojicon) {
				editText.append(emojicon.getEmoji());
			}
		});
		//Set on backspace click listener
		popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {
			@Override
			public void onEmojiconBackspaceClicked(View v) {
				KeyEvent event = new KeyEvent(
						0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
				editText.dispatchKeyEvent(event);
			}
		});
		//If the emoji popup is dismissed, change emojiButton to smiley icon
		popup.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() {
				changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
			}
		});
		//If the text keyboard closes, also dismiss the emoji popup
		popup.setOnSoftKeyboardOpenCloseListener(new OnSoftKeyboardOpenCloseListener() {
			@Override
			public void onKeyboardOpen(int keyBoardHeight) {
			}
			@Override
			public void onKeyboardClose() {
				if(popup.isShowing())
					popup.dismiss();
			}
		});
		//On emoji clicked, add it to edittext
		popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {
			@Override
			public void onEmojiconClicked(Emojicon emojicon) {
				editText.append(emojicon.getEmoji());
			}
		});
		//On backspace clicked, emulate the KEYCODE_DEL key event
		popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {
			@Override
			public void onEmojiconBackspaceClicked(View v) {
				KeyEvent event = new KeyEvent(
						0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
				editText.dispatchKeyEvent(event);
			}
		});
		// To toggle between text keyboard and emoji keyboard keyboard(Popup)
		emojiButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//If popup is not showing => emoji keyboard is not visible, we need to show it

				if(!popup.isShowing()){
					//If keyboard is visible, simply show the emoji popup
					if(popup.isKeyBoardOpen()){
						popup.showAtBottom();
						changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
					}
					//else, open the text keyboard first and immediately after that show the emoji popup
					else{
						editText.setFocusableInTouchMode(true);
						editText.requestFocus();
						popup.showAtBottomPending();
						final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
						changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
					}
				}
				//If popup is showing, simply dismiss it to show the undelying text keyboard
				else{
					popup.dismiss();
				}
			}
		}); 

	}
	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(context), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(context), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}

	}
	private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId){
		iconToBeChanged.setImageResource(drawableResourceId);
	}
	public void FindViewById() {
		userList=(ListView)findViewById(R.id.listview_classes);
		selection=(CheckBox)findViewById(R.id.rootselection);
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {
		imageLoader = new ImageLoader(
				Volley.newRequestQueue(context), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	//async task for class
	public class GetClassLisAsync extends AsyncTask <Void, Void, String> {
		@Override
		protected String doInBackground(Void... params) {
			String result="";
			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(Common.URL+"PrincipleAPI.asmx/GetClassesAndParent");
			try
			{
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("PrincipalID",Common.userid));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	
				e.printStackTrace();
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			parseJSON(result);
		}
		void parseJSON(String result)
		{
			try {
				JSONArray array=new JSONArray(result);
				for (int i = 0; i < array.length(); i++) {
					JSONObject jo=array.getJSONObject(i);
					ClassList cList=new ClassList();
					cList.setClassID(jo.getString("ClassID"));
					cList.setClassName(jo.getString("ClassName"));
					JSONArray parentArray=jo.getJSONArray("ParentID");
					JSONArray parentName=jo.getJSONArray("ParentName");
					JSONArray studId=jo.getJSONArray("StudentID");
					if(parentArray!=null)
					{
						String[] name=new String[parentArray.length()];
						String[] id=new String[parentArray.length()];
						String[] stduentId=new String[parentArray.length()];
						for(int j=0;j<parentArray.length();j++)
						{
							id[j]=parentArray.getString(j);
							name[j]=parentName.getString(j);
							stduentId[j]=studId.getString(j);
						}
						cList.setParentid(id);
						cList.setParentname(name);
						cList.setStudentId(stduentId);
					}
					userArray.add(cList);
				}
				userAdapter = new ClassListAdapter(ChooseClass.this, R.layout.acitivity_row, userArray);
				userList.setAdapter(userAdapter);
				userList.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> av, final View view, final int i, long i2) {
						Animation anim = AnimationUtils.loadAnimation(ChooseClass.this, R.anim.fade_in);
						view.startAnimation(anim);
					}
				});  
			} catch (JSONException e) {
				Log.e("ParsonJSON= ", ""+e);
			}
		}
	}
	//school messages
	String schoolMessage(MessageMain msg,String studentId)
	{
		JSONObject jmsg = new JSONObject();
		try
		{
			jmsg.put("event", "school_msg");
			jmsg.put("app_id",Common.app_id);
			jmsg.put("from",Common.userid);
			jmsg.put("msg",msg.getMesageText());
			jmsg.put("to", msg.getTo());
			jmsg.put("student_id",studentId);
			jmsg.put("ref_id",msg.getRef_id());
			jmsg.put("type", "s");
		}catch(Exception e){}
		return jmsg.toString();
	}
}
