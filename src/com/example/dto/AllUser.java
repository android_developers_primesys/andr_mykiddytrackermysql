package com.example.dto;

public class AllUser {

	String Username,EmailId,MobileNo,Gender,UserID,Tag;
	boolean status;
	
	public  AllUser(	String Username,String EmailId,String MobileNo,String Gender,String UserID,boolean status,String Tag)
	{
		this.Username=Username;
		this.EmailId=EmailId;
		this.MobileNo=MobileNo;
		this.Gender=Gender;
		this.UserID=UserID;
		this.status=status;
		this.Tag=Tag;
	}
    public AllUser() {}
	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getTag() {
		return Tag;
	}
	public void setTag(String tag) {
		Tag = tag;
	}
	@Override
	public String toString() {
		return UserID;
	}
}
