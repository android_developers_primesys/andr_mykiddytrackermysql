package com.example.dto;

public class OptionDTO {

	String OptionId,OptionLabel,Answer;

	public String getOptionId() {
		return OptionId;
	}

	public void setOptionId(String optionId) {
		OptionId = optionId;
	}

	public String getOptionLabel() {
		return OptionLabel;
	}

	public void setOptionLabel(String optionLabel) {
		OptionLabel = optionLabel;
	}

	public String getAnswer() {
		return Answer;
	}

	public void setAnswer(String answer) {
		Answer = answer;
	}
}
