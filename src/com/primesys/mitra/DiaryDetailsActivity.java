package com.primesys.mitra;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dto.DiaryDetails;

public class DiaryDetailsActivity extends Activity {

	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	EditText text_message,text_date,text_from;
	ImageView image_sign;
	DiaryDetails dd;
	LinearLayout mContent;
	signature mSignature;
	public static String tempDir;
	public int count = 1;
	public String current = null,studentId="0",signPath;
	private Bitmap mBitmap;
	View mView;
	File mypath;
	byte[] imageArray;
	private String uniqueId;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG="DiaryDetailsActivity";
	Context addContext=DiaryDetailsActivity.this;
	Button btnUpdate;
	String type="";
	ProgressDialog pDialog;
	File dFile;
	String path;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_diarydetails);
		FindViewBYID();
		textNews=(TextView)findViewById(R.id.text_news);
		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		textNews.setTypeface(custom_font);
		textNews.setText(APIController.getInstance().getNews());
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);

		 

		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		btnUpdate=(Button)findViewById(R.id.btn_update);
		text_message=(EditText)findViewById(R.id.text_message);
		text_from=(EditText)findViewById(R.id.text_from);
		text_date=(EditText)findViewById(R.id.text_date);
		image_sign=(ImageView)findViewById(R.id.image_sign);
		dd=(DiaryDetails)getIntent().getSerializableExtra("diary");
		type=getIntent().getStringExtra("Type");
		setData();
		studentId=getIntent().getStringExtra("StudentID");
		mContent = (LinearLayout) findViewById(R.id.userSignature);
		mSignature = new signature(this, null);
		mSignature.setBackgroundColor(Color.WHITE);
		mContent.addView(mSignature, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		mView = mContent;
		tempDir = Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.external_dir) + "/";
		ContextWrapper cw = new ContextWrapper(addContext);
		File directory = cw.getDir(getResources().getString(R.string.external_dir), Context.MODE_PRIVATE);
		mView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				image_sign.setVisibility(View.GONE);
				return true;
			}
		});
		prepareDirectory();
		uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + Math.random();
		current = uniqueId + ".png";
		mypath= new File(directory,current);
		mSignature = new signature(this, null);
		mSignature.setBackgroundColor(Color.WHITE);
		mContent.addView(mSignature, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		if(dd.getStatus().equals("R"))
		{
			btnUpdate.setVisibility(View.GONE);
			mContent.setVisibility(View.GONE);
		}
		if(type.equals("P")&&dd.getFrom_id()==Integer.parseInt(studentId))
		{
			btnUpdate.setVisibility(View.GONE);
			mContent.setVisibility(View.GONE);
		}
		if(type.equals("T")&&dd.getFrom_id()==Integer.parseInt(Common.userid))
		{
			btnUpdate.setVisibility(View.GONE);
			mContent.setVisibility(View.GONE);
		}
		btnUpdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.v("log_tag", "Panel Saved");
				mView.setDrawingCacheEnabled(true);
				mSignature.save(mView);
				signPath=mypath.getAbsolutePath();
				if(Common.connectionStatus)
				{
					postDiaryRequest();
				}
			}
		});
	}
	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(addContext)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(addContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(addContext)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(addContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}

	}
	private void FindViewBYID() {
		textNews=(TextView)findViewById(R.id.text_news);
		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		textNews.setTypeface(custom_font);
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(addContext)&& APIController.getInstance().getSchool_logo()!=null) {
		imageLoader = new ImageLoader(
				Volley.newRequestQueue(addContext), imageCache);
		(img_school).setImageUrl(
			Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
	}
	void setData()
	{
		text_message.setText(dd.getMessage());
		text_date.setText(dd.getMsg_record_dt_time());
		if(type.equals("T"))
		{
			image_sign.setImageBitmap(Base64ToBitmap(dd.getSign_by_parent()));
		}
		else
		{
			image_sign.setImageBitmap(Base64ToBitmap(dd.getSign_by_teacher()));
		}
		// set the from data 
		if(dd.getFrom_id()==Integer.parseInt(Common.userid))
		{
			text_from.setText(dd.getStudentName());
		}
		else
			text_from.setText(dd.getTeacherName());
	}
	@Override
	protected void onDestroy() {
		Log.w("GetSignature", "onDestory");
		super.onDestroy();
	}
	// clear all the fields
	void clearAll()
	{
		btnUpdate.setEnabled(false);
	}
	private String getTodaysDate() { 

		final Calendar c = Calendar.getInstance();
		int todaysDate =     (c.get(Calendar.YEAR) * 10000) + 
				((c.get(Calendar.MONTH) + 1) * 100) + 
				(c.get(Calendar.DAY_OF_MONTH));
		return(String.valueOf(todaysDate));

	}

	private String getCurrentTime() {

		final Calendar c = Calendar.getInstance();
		int currentTime =     (c.get(Calendar.HOUR_OF_DAY) * 10000) + 
				(c.get(Calendar.MINUTE) * 100) + 
				(c.get(Calendar.SECOND));
		return(String.valueOf(currentTime));

	}


	private boolean prepareDirectory() 
	{
		try
		{
			if (makedirs()) 
			{
				return true;
			} else {
				return false;
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
			Common.showToast("Could not initiate File System.. Is Sdcard mounted properly?",DiaryDetailsActivity.this);
			return false;
		}
	}

	private boolean makedirs() 
	{
		File tempdir = new File(tempDir);
		if (!tempdir.exists())
			tempdir.mkdirs();

		if (tempdir.isDirectory()) 
		{
			File[] files = tempdir.listFiles();
			for (File file : files) 
			{
				if (!file.delete()) 
				{
					System.out.println("Failed to delete " + file);
				}
			}
		}
		return (tempdir.isDirectory());
	}
	//convert the given Base64 to Bitmap image
	Bitmap Base64ToBitmap(String myImageData)
	{
		byte[] imageAsBytes = Base64.decode(myImageData.getBytes(),Base64.DEFAULT);
		return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
	}
	//convert image to Base64
	String fileToBase64(String docPath)
	{
		String UploadFile="";
		try
		{
			File Uploadfilepath = new File(docPath);
			byte[]  bFileConversion = new byte[(int) Uploadfilepath.length()];
			//convert file into array of bytes
			FileInputStream fileInputStream = new FileInputStream(Uploadfilepath);
			fileInputStream.read(bFileConversion);
			UploadFile=Base64.encodeToString(bFileConversion, Base64.DEFAULT);
		}catch(Exception e)
		{

		}
		return UploadFile;
	}
	Dialog createDialog()
	{
		pDialog=new ProgressDialog(DiaryDetailsActivity.this);
		pDialog.setMessage("Downloading file. Please wait......");
		pDialog.setIndeterminate(false);
		pDialog.setMax(100);
		pDialog.setCancelable(true); 
		pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pDialog.show();
		return pDialog;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_dd, menu);
		return super.onCreateOptionsMenu(menu);

	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		menu.findItem(R.id.add).setIcon(getResources().getDrawable(R.drawable.ic_attach));
		return super.onPrepareOptionsMenu(menu);
	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
			return true;
		case R.id.add:
			String url=dd.getDocuments();
			if(url.length()==0||url==null)
				Common.showToast("There is no attachment !", DiaryDetailsActivity.this);
			else
			{
				url=url.replace("~", "");
				url=Common.relativeurl+url;
				if (Common.connectionStatus) {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
						new DownloadFileFromURL().executeOnExecutor(
								AsyncTask.THREAD_POOL_EXECUTOR, url);
					} else {
						new DownloadFileFromURL().execute(url);
					}
				}
			}
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	public class signature extends View 
	{
		private static final float STROKE_WIDTH = 5f;
		private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
		private Paint paint = new Paint();
		private Path path = new Path();
		private float lastTouchX;
		private float lastTouchY;
		private final RectF dirtyRect = new RectF();

		public signature(Context context, AttributeSet attrs) 
		{
			super(context, attrs);
			paint.setAntiAlias(true);
			paint.setColor(Color.BLACK);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStrokeWidth(STROKE_WIDTH);
		}

		public void save(View v) 
		{
			if(mBitmap == null)
			{
				mBitmap =  Bitmap.createBitmap (mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
			}
			Canvas canvas = new Canvas(mBitmap);
			try
			{
				FileOutputStream mFileOutStream = new FileOutputStream(mypath);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				v.draw(canvas); 
				mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, mFileOutStream);
				imageArray=stream.toByteArray();

				//save the signature into the DB

				mFileOutStream.flush();
				mFileOutStream.close();
				/*String url = Images.Media.insertImage(getContentResolver(), mBitmap, "title", null);
					Log.v("log_tag","url: " + url);*/
			}
			catch(Exception e) 
			{ 
				Log.v("log_tag", e.toString()); 
			} 
		}

		public void clear() 
		{
			path.reset();
			invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) 
		{
			getParent().requestDisallowInterceptTouchEvent(true);
			canvas.drawPath(path, paint);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) 
		{
			float eventX = event.getX();
			float eventY = event.getY();
			btnUpdate.setEnabled(true);

			switch (event.getAction()) 
			{
			case MotionEvent.ACTION_DOWN:
				path.moveTo(eventX, eventY);
				lastTouchX = eventX;
				lastTouchY = eventY;
				return true;

			case MotionEvent.ACTION_MOVE:

			case MotionEvent.ACTION_UP:

				resetDirtyRect(eventX, eventY);
				int historySize = event.getHistorySize();
				for (int i = 0; i < historySize; i++) 
				{
					float historicalX = event.getHistoricalX(i);
					float historicalY = event.getHistoricalY(i);
					expandDirtyRect(historicalX, historicalY);
					path.lineTo(historicalX, historicalY);
				}
				path.lineTo(eventX, eventY);
				break;

			default:
				getParent().requestDisallowInterceptTouchEvent(true);
				debug("Ignored touch event: " + event.toString());
				return false;
			}

			invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
					(int) (dirtyRect.top - HALF_STROKE_WIDTH),
					(int) (dirtyRect.right + HALF_STROKE_WIDTH),
					(int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

			lastTouchX = eventX;
			lastTouchY = eventY;

			return true;
		}

		private void debug(String string){
		}

		private void expandDirtyRect(float historicalX, float historicalY) 
		{
			if (historicalX < dirtyRect.left) 
			{
				dirtyRect.left = historicalX;
			} 
			else if (historicalX > dirtyRect.right) 
			{
				dirtyRect.right = historicalX;
			}

			if (historicalY < dirtyRect.top) 
			{
				dirtyRect.top = historicalY;
			} 
			else if (historicalY > dirtyRect.bottom) 
			{
				dirtyRect.bottom = historicalY;
			}
		}

		private void resetDirtyRect(float eventX, float eventY) 
		{
			dirtyRect.left = Math.min(lastTouchX, eventX);
			dirtyRect.right = Math.max(lastTouchX, eventX);
			dirtyRect.top = Math.min(lastTouchY, eventY);
			dirtyRect.bottom = Math.max(lastTouchY, eventY);
		}
	}

	// update service to make the status change
	void postDiaryRequest()
	{
		reuestQueue=Volley.newRequestQueue(addContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(addContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.URL+"ParentAPI.asmx/UpdateDigitalDairy",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseJSON(response);
				pDialog.hide();
			}

		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("id",dd.getId()+"");
				params.put("sign", fileToBase64(signPath));
				params.put("type", type);
				return params;
			}

		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	void parseJSON(String result)
	{
		if(result.contains("false"))
		{
			Common.showToast("Diary created successfully", addContext);
			finish();
		}
		else
			Common.showToast("Check once again there is Error !", addContext);
	}
	class DownloadFileFromURL extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Bar Dialog
		 * */
		@Override
		protected void onPreExecute() {
			createDialog();
		}

		/**
		 * Downloading file in background thread
		 * */
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				URL url = new URL(f_url[0]);
				System.out.println(f_url[0]);
				URLConnection conection = url.openConnection();
				path=Environment.getExternalStorageDirectory().toString()+"/"+ URLUtil.guessFileName(f_url[0], null, null);
				conection.connect();

				int lenghtOfFile = conection.getContentLength();

				// download the file
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);

				// Output stream
				OutputStream output = new FileOutputStream(path);
				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					publishProgress(""+(int)((total*100)/lenghtOfFile));

					output.write(data, 0, count);
				}

				// flushing output
				output.flush();

				// closing streams
				output.close();
				input.close();

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		/**
		 * Updating progress bar
		 * */
		protected void onProgressUpdate(String... progress) {
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		@Override
		protected void onPostExecute(String file_url) {
			pDialog.hide();
			Common.showToast("Downloading Completed", DiaryDetailsActivity.this);
			dFile=new File(path);
			Intent intent = new Intent();
			intent.setAction(android.content.Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(dFile),getMimeType(dFile.getAbsolutePath()));
			if (null != intent.resolveActivity(getPackageManager())) {
				startActivity(intent); 
			}
		}
		private String getMimeType(String url)
		{
			String parts[]=url.split("\\.");
			String extension=parts[parts.length-1];
			String type = null;
			if (extension != null) {
				MimeTypeMap mime = MimeTypeMap.getSingleton();
				type = mime.getMimeTypeFromExtension(extension);
			}
			return type;
		}

	}
}
