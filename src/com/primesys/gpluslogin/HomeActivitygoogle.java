package com.primesys.gpluslogin;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.primesys.mitra.Common;
import com.primesys.mitra.LoginActivity;
import com.primesys.mitra.R;



/**
 * @author Amit
 * 
 */
public class HomeActivitygoogle extends Activity {
	EditText txtdob,txtphone_number;
	ImageView imageProfile;
	boolean isExist;
	Button save;
	int year,month,day;
	Context homecontext=HomeActivitygoogle.this;
	DatePicker datePPicker;
	TextView textViewName, textViewEmail, textViewGender, textViewBirthday;
	String textName, textEmail, textGender, textBirthday="0000-00-00", userImageUrl,textcontact;
	Context loginContext=HomeActivitygoogle.this;
	Calendar calendar;
	String prefcount ;
	TextView lbl_register;
	ProgressDialog progress;
	String strname,strmessage,strcontact,stremail,password,username;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG="SignUpActivity"; 
	Context signupContext=HomeActivitygoogle.this;
	private String phpurl="http://www.mykiddytracker.com/php/getAppId.php";
	public Editor editor;
	public final String key_IS = "IS_FIRST";
	public final String key_USER = "USER";
	public final String key_PASS = "PASS";
	LoginActivity login=null;
	public SharedPreferences sharedPreferences;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_googleplus_login);
		sharedPreferences = this.getSharedPreferences("userInfo",
				Context.MODE_PRIVATE);

		Intent intent = getIntent();
		textEmail = intent.getStringExtra("email_id");
		// lbl_register=(TextView)findViewById(R.id.textView1);
		/**
		 * get user data from google account
		 */

		try {

			System.out.println("On Home Page***"
					+ AbstractGetNameTask.GOOGLE_USER_DATA);
			JSONObject profileData = new JSONObject(
					AbstractGetNameTask.GOOGLE_USER_DATA);
			Log.e("profileData", ""+profileData);
			if (profileData.has("picture")) {
				userImageUrl = profileData.getString("picture");
				new GetImageFromUrl().execute(userImageUrl);
			}
			if (profileData.has("name")) {
				strname = profileData.getString("name");

			}
			/*if (profileData.has("email")) {
				stremail = profileData.getString("email");

			}*/
			stremail=SplashActivity.Email;
			strcontact="";
			strmessage="Registration with google plus";
		/*	lbl_register.setText("Registation in Progress");*/
			//execute for Registration

			signUpRequest();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 0) {
			return new DatePickerDialog(this, myDateListener, year, month, day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener myDateListener
	= new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
			showDate(arg1, arg2+1, arg3);
		}
	};

	//show date method
	void showDate(int year,int month,int day)
	{/*
		txtdob.setText(new StringBuilder().append(year).append("-").append(month).append("-").append(day));
		txtdob.setError(null);
		textBirthday=txtdob.getText().toString();*/
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	public class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
		@Override
		protected Bitmap doInBackground(String... urls) {
			Bitmap map = null;
			for (String url : urls) {
				map = downloadImage(url);
			}
			return map;
		}

		// Sets the Bitmap returned by doInBackground
		@Override
		protected void onPostExecute(Bitmap result) {
			/*imageProfile.setImageBitmap(result);*/
		}

		// Creates Bitmap from InputStream and returns it
		private Bitmap downloadImage(String url) {
			Bitmap bitmap = null;
			InputStream stream = null;
			BitmapFactory.Options bmOptions = new BitmapFactory.Options();
			bmOptions.inSampleSize = 1;

			try {
				stream = getHttpConnection(url);
				bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);
				stream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return bitmap;
		}

		// Makes HttpURLConnection and returns InputStream
		private InputStream getHttpConnection(String urlString)
				throws IOException {
			InputStream stream = null;
			URL url = new URL(urlString);
			URLConnection connection = url.openConnection();

			try {
				HttpURLConnection httpConnection = (HttpURLConnection) connection;
				httpConnection.setRequestMethod("GET");
				httpConnection.connect();

				if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					stream = httpConnection.getInputStream();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return stream;
		}
	}
	// volley request for posting the parameter
	public void signUpRequest()
	{
		reuestQueue=Volley.newRequestQueue(signupContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(signupContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setMessage("Please  Wait Sigin in google+ ");
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.URL+"LoginServiceAPI.asmx/SaveDemoUser",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseJSON(response);
				pDialog.dismiss();
			}

		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("name",strname);
				params.put("email",stremail);
				params.put("contact",strcontact);
				params.put("message",strmessage);
				params.put("registrationType", "Google");
				params.put("city", Common.user_regCity);
				return params;
			}

		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	void parseJSON(String result)
	{
		if(result.contains("error"))
		{
			Common.showToast("Check once again there is Error !", signupContext);
		}
		else{

			try
			{
				sharedPreferences = this.getSharedPreferences("userInfo",
						Context.MODE_PRIVATE);   
				SharedPreferences.Editor editor = sharedPreferences.edit(); 
				JSONObject jo=new JSONObject(result);
				username=jo.getString("username");
				password=jo.getString("password");
				isExist=jo.getBoolean("user_exist");
				//to invoke logintask in LoginActivity
				if(isExist){

					editor.putBoolean(key_IS, true);
					editor.putString(key_USER, username);
					editor.putString(key_PASS, password);
					editor.commit();
					Intent ilogitask=new Intent(signupContext,LoginActivity.class);
					startActivity(ilogitask);
					//finish();
				}

				else{

					editor.putBoolean(key_IS, true);
					editor.putString(key_USER, username);
					editor.putString(key_PASS, password);
					editor.commit();  
					Intent ilogitask=new Intent(signupContext,LoginActivity.class);
					startActivity(ilogitask);
					finish();

				}
			}

			catch(Exception e)
			{
				System.out.print(e);
			}
		}
	}


	//send email request
	public void senEmailRequest()
	{
		reuestQueue=Volley.newRequestQueue(signupContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(signupContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,phpurl,new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseEmail(response);
				pDialog.dismiss();
			}

		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.dismiss();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseEmail(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("send_mail", "true");
				params.put("name",strname);
				params.put("username", username);
				params.put("email",stremail);
				params.put("password",password);
				return params;
			}

		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	void parseEmail(String result)
	{
		Common.showToast("Check E-mail for Username and Password", signupContext);
		Intent loginIntent = new Intent(signupContext, LoginActivity.class);
		startActivity(loginIntent);
		finish();
	}



}