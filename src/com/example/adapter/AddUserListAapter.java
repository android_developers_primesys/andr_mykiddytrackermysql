package com.example.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dto.AddUserDTO;
import com.primesys.mitra.AddUserActivity;
import com.primesys.mitra.LoginActivity;
import com.primesys.mitra.R;

public class AddUserListAapter extends ArrayAdapter<AddUserDTO>{
	Context context;
	int layoutResourceId;
	public static ArrayList<AddUserDTO> data;
	Animation animation = null;
	public AddUserListAapter(Context context, int layoutResourceId,
			ArrayList<AddUserDTO> data) {
		super(context, layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		AddViewHolder holder;
		try{
			final AddUserDTO s=data.get(position); 
			if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder=new AddViewHolder();
			holder.btnAddUser=(TextView)convertView.findViewById(R.id.studentame);
			holder.city=(TextView)convertView.findViewById(R.id.city);
			convertView.setTag(holder);
			}
			else
                 holder=(AddViewHolder)convertView.getTag();
			
			holder.btnAddUser.setText(s.getUserName());
			holder.city.setText(s.getUserCity());
			holder.btnAddUser.setTag(position); 
			holder.btnAddUser.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try{
						AddUserDTO addusers=data.get((Integer)v.getTag());
						LoginActivity.mClient.sendMessage(AddUserActivity.addFriendForChat(addusers.getUserName(),addusers.getUserId()));	

					}catch(Exception e)
					{
						Log.e("Exception", ""+e);
					}
				}
			});

		}catch(Exception e)
		{
			Log.e("Exception", ""+e);
		}
		return convertView;
	}
	//view holder for add user adapter
	static class AddViewHolder
	{
		TextView btnAddUser;
		TextView city;
	}
}
