
package com.example.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.example.dto.ClassList;
import com.primesys.mitra.ChooseClass;
import com.primesys.mitra.R;
import com.primesys.mitra.studentInformation;

public class ClassListAdapter extends ArrayAdapter<ClassList> {

	Context context;
	int layoutResourceId;
	ArrayList<ClassList> data;
	Intent Studeninfo;
	public static Boolean Class_List_Status=false;
	Animation animation = null;


	public ClassListAdapter(Context context, int layoutResourceId,
			ArrayList<ClassList> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		ClassList user =  data.get(position);
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);
		} 
		Button	btnEdit = (Button) convertView.findViewById(R.id.Classname);
		btnEdit.setText(""+user.getClassName());
		btnEdit.setTag(position); 

		final CheckBox selection=(CheckBox)convertView.findViewById(R.id.selection);
		selection.setChecked(user.getStatus());
		selection.setTag(position);
		selection.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

				if (selection.isChecked()) {
					data.get((Integer)selection.getTag()).setStatus(true);
				}
				else {
					ChooseClass.selection.setChecked(false);
					data.get((Integer)selection.getTag()).setStatus(false);
				}
				boolean valid=true;
				for(ClassList su : data){
					if(!su.getStatus()){
						valid=false;
						break;
					}
				}

				if(valid)
					ChooseClass.selection.setChecked(true);
			}
		});
		btnEdit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ClassList user1=data.get((Integer)v.getTag());
				Intent studeninfo=new Intent(context,studentInformation.class);
				studeninfo.putExtra("ClassID", user1.getClassID());
				context.startActivity(studeninfo);
				((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
		return convertView;
	}
	
}


