package com.primesys.mitra;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

import android.provider.SyncStateContract.Helpers;
import android.util.Log;

import com.example.dto.LoginDetails;
import com.example.dto.MessageMain;

public class ClientTeacher {
	private String serverMessage;
	static String SERVERIP = Common.SERVERIP;
	private static OnMessageReceived mMessageListener = null;
	private boolean mRun = false;
	static PrintWriter out;
	private Socket socket;
	private BufferedReader in;
	boolean valid = true;
	static MessageMain m=new MessageMain();
	static Date date=new Date();
	static String formattedDate = new SimpleDateFormat("dd MMM yyyy hh:mm:ss").format(date);
	public ClientTeacher(OnMessageReceived listener) {
		mMessageListener = listener;

	}
	public ClientTeacher() {
		// TODO Auto-generated constructor stub
	}
	public static void sendMessage(MessageMain m) {
		if (out != null && !out.checkError()) {
			try {

				mMessageListener.messageSend(m);
				out.println(m.toString());
				out.flush();
			} catch (Exception e) {
				Log.e("write the message error",
						e.getMessage() + " " + e.getCause());
			}
		}
	}
	public static void principleReply(MessageMain message) {
		if (out != null && !out.checkError()) {

			try {

				mMessageListener.messageSend(message);
				out.println(message);
				out.flush();
			} catch (Exception e) {
				Log.e("write the message error",
						e.getMessage() + " " + e.getCause());
			}
		}
	}
	//teacher to parent communication
	public static void sendMessageParent(MessageMain message) {
		if (out != null && !out.checkError()) {
			try {
				mMessageListener.messageSend(message);
				out.println(message);
				out.flush();

			} catch (Exception e) {
				Log.e("write the message error",
						e.getMessage() + " " + e.getCause());
			}
		}

	}

	public void stopClient() {
		mRun = false;
	}

	public void run() {
		mRun = true;
		try {
			// here you must put your computer's IP address.
			InetAddress serverAddr = InetAddress.getByName(SERVERIP);
			Log.e("serverAddr", serverAddr.toString());
			Log.e("TCP Client", "C: Connecting...");
			// create a socket to make the connection with the server
			socket = new Socket(serverAddr, Common.PORT);
			if (valid) {
				
				//added bY amit 17.02.2015
				LoginDetails L_Details=LoginActivity.Ldetails;
				PrintWriter obj = new PrintWriter(socket.getOutputStream());
				JSONObject jo = new JSONObject();
				JSONObject joData = new JSONObject();
				joData.put("class",""+L_Details.getClass_id());  //send class Id 
				joData.put("school",""+L_Details.getSchool_id());//send school Id
				joData.put("user_type",""+L_Details.getUserType());//send userType
				joData.put("mobile_number",""+L_Details.getMobileNumber());
				joData.put("email_id",""+L_Details.getEmailId());
				jo.put("event", "join");
				jo.put("name", Common.userid);
				jo.put("app_id", Common.app_id);
				jo.put("timestamp", Common.timstamp);
				jo.put("data",joData);  //optional parameter
				obj.write(jo.toString());
				Log.d("joTeacher", ""+jo);
				obj.flush();

				valid = false;
			}
			try {
				out = new PrintWriter(new BufferedWriter(
						new OutputStreamWriter(socket.getOutputStream())), true);
				Log.e("TCP Client", "C: Sent.");
				Log.e("TCP Client", "C: Done.");

				in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));

				while (mRun) {
					serverMessage = in.readLine();
					if (serverMessage != null && mMessageListener != null) {
						mMessageListener.messageReceived(serverMessage);
					}
					serverMessage = null;
				}
			} catch (Exception e) {
				Log.e("TCP", "S: Error", e);
			} finally {
				socket.close();
				stopClient();
			}
		} catch (Exception e) {
			Log.e("TCP", "C: Error", e);
		}
	}

	public interface OnMessageReceived {
		public void messageReceived(String message);
		public void messageSend(MessageMain message);
	}



}