package com.example.dto;

import java.io.Serializable;

public class ParentStudent implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String StudentID,studentNmae,Gender,Prim_ContactNo,Prim_EmailID,ParentID,ParentName;
    boolean SelectionStatus;
	public ParentStudent(String StudentID,String studentNmae,String Gender,String Prim_ContactNo,String Prim_EmailID,String ParentID,String ParentName, boolean SelectionStatus) {
		this.StudentID=StudentID;
		this.studentNmae=studentNmae;
		this.Gender=Gender;
		this.Prim_ContactNo=Prim_ContactNo;
		this.Prim_EmailID=Prim_EmailID;
		this.ParentID=ParentID;
		this.ParentName=ParentName;
		this.SelectionStatus=SelectionStatus;
		
	}
	public ParentStudent() {
		// TODO Auto-generated constructor stub
	}
	public String getStudentID() {
		return StudentID;
	}
	public void setStudentID(String studentID) {
		StudentID = studentID;
	}
	public String getStudentNmae() {
		return studentNmae;
	}
	public void setStudentNmae(String studentNmae) {
		this.studentNmae = studentNmae;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public String getPrim_ContactNo() {
		return Prim_ContactNo;
	}
	public void setPrim_ContactNo(String prim_ContactNo) {
		Prim_ContactNo = prim_ContactNo;
	}
	public String getPrim_EmailID() {
		return Prim_EmailID;
	}
	public void setPrim_EmailID(String prim_EmailID) {
		Prim_EmailID = prim_EmailID;
	}
	public String getParentID() {
		return ParentID;
	}
	public void setParentID(String parentID) {
		ParentID = parentID;
	}
	public String getParentName() {
		return ParentName;
	}
	public void setParentName(String parentName) {
		ParentName = parentName;
	}
	public void setSelectionStatus(boolean b) {
		// TODO Auto-generated method stub
		
	}
	public boolean getSelectionStatus() {
		return SelectionStatus;
	}

	@Override
	public String toString() {
		return ParentID;
	}
}
