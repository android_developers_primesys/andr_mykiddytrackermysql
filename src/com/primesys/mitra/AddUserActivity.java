package com.primesys.mitra;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.IconTextView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adapter.AddUserListAapter;
import com.example.dto.AddUserDTO;


public class AddUserActivity extends Activity {

	static EditText text_username;
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	Button btnAdd;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG=AddUserActivity.class.getSimpleName();
	private String userName;
	Context addContext=AddUserActivity.this;
	ProgressDialog pDialog;
	ArrayList<AddUserDTO> userArray = new ArrayList<AddUserDTO>();
	ListView userList;
	AddUserListAapter userAdapter;
	RadioGroup adduserBy;
	RadioButton byName,bymobile,byemail;
	String strEntity,flag;
	IconTextView search;
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_user);
		
		
		FindViewBYID();

		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);
	


		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		flag="1";
		search=(IconTextView)findViewById(R.id.search);
		search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				strEntity=text_username.getText().toString();

				if (Validation()) {
					text_username.setError(null);
					if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
						new GetAddUserList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
					}
					else{
						new GetAddUserList().execute();
					}			
				}

			}
		});
		text_username.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				strEntity=text_username.getText().toString();

				if (Validation()&&strEntity.length()>=3&&text_username.isFocused()) {
					text_username.setError(null);
					if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
						new GetAddUserList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
					}
					else{
						new GetAddUserList().execute();
					}			
				}
			}
		});
		//GetType User
		adduserBy.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				userList.setAdapter(null);
				if (checkedId==R.id.radioname) {
					flag="1";  
					text_username.setHint("Enter Name");
				}
				if (checkedId==R.id.radiomobile) {
					flag="2";  
					text_username.setHint("Enter Mobile Number");
				}
				if (checkedId==R.id.radioemail) {
					flag="3";  
					text_username.setHint("Enter Email");
				}

			}
		});
	}
	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(addContext)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(addContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(addContext)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(addContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
			
		}


		
	}
	//Name Validation
	boolean Validation() {
		Boolean valid=true;
		strEntity=text_username.getText().toString();

		if (strEntity.length()==0) {
			text_username.setError("Enter Name");
			text_username.requestFocus();
			valid=false;
		}
		if (flag.equals("3")) {
			if (strEntity.length()==0) {
				text_username.setError("Enter  email ");
				text_username.requestFocus();
				valid=false;
			}else{
				Pattern pattern = Pattern.compile(EMAIL_PATTERN);
				Matcher matcher = pattern.matcher(strEntity);
				valid=matcher.matches();
				if (valid==false) {
					text_username.setError("Enter valid email ");
					text_username.requestFocus();
					valid=false;
				}
			}
		}
		if (flag.equals("2")) {
			if (strEntity.length()<10) {
				text_username.setError("Enter 10 Digit Number");
				text_username.requestFocus();
				valid=false;
			}

		}
		return valid;
	}

	void FindViewBYID() {
		search=(IconTextView)findViewById(R.id.search);
		text_username=(EditText)findViewById(R.id.text_username);
		userList = (ListView) findViewById(R.id.list_data); 
		adduserBy=(RadioGroup)findViewById(R.id.rgb_choose);
		textNews=(TextView)findViewById(R.id.text_news);
		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		textNews.setTypeface(custom_font);
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(addContext)&& APIController.getInstance().getSchool_logo()!=null) {
		imageLoader = new ImageLoader(
				Volley.newRequestQueue(addContext), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
		
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			/*Intent friendlist=new Intent(addContext, DemoUserActivity.class);
			startActivity(friendlist);*/
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	static void parseJSON(String result)
	{
		try
		{
			JSONObject jo=new JSONObject(result);
			JSONObject jData=jo.getJSONObject("data");
			int id=jData.getInt("friend_id");
			String name=jData.getString("friend_name");	
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			text_username.setText("");
		}	
	}
	

	//Call Service To Get matched user ID's
	//asynv task for class
	public class GetAddUserList extends AsyncTask <Void, Void, String> {

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(addContext);
			pDialog.setMessage("Processing  wait...");
			pDialog.setCancelable(false);
			pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
			pDialog.show();
		}
		@Override
		protected String doInBackground(Void... params) {
			String result="";

			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(Common.URL+"ParentAPI.asmx/SearchFriendList");
			try
			{
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(3);
				postParameter.add(new BasicNameValuePair("UserId",Common.userid));
				postParameter.add(new BasicNameValuePair("Entity",strEntity));
				postParameter.add(new BasicNameValuePair("Flag",flag));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	
				e.printStackTrace();
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			if (result.contains("error")) {
				Common.showToast("No User Found", addContext);
			}
			parseJSON(result);
		}
		void parseJSON(String result)
		{
			try {
				userArray = new ArrayList<AddUserDTO>();
				JSONArray array=new JSONArray(result);
				for (int i = 0; i < array.length(); i++) {
					JSONObject jo=array.getJSONObject(i);
					AddUserDTO adduser=new AddUserDTO();
					adduser.setUserName(jo.getString("Name"));
					adduser.setUserId(jo.getString("Id"));
					adduser.setUserCity(jo.getString("City"));
					userArray.add(adduser);
				}
			} catch (Exception e) {
				Log.e("ParsonJSON", ""+e);
			}
			userAdapter = new AddUserListAapter(AddUserActivity.this, R.layout.activity_adduserinfo, userArray);
			userList = (ListView) findViewById(R.id.list_data); 
			userList.setAdapter(userAdapter);
		}
	}
	public static String addFriendForChat(String friendName,String friendId)
	{
		String addSTring="{}";
		try{
			JSONObject jo=new JSONObject();
			jo.put("event","add_friend");
			jo.put("user_id",Common.userid);
			jo.put("user_name",Common.username);
			jo.put("friend_id",friendId);
			jo.put("friend_name",friendName);
			addSTring=jo.toString();
		}
		catch(Exception e)
		{
			Log.e("addFriendForChat", ""+e);
		}
		return addSTring; 
	}

}
