package com.primesys.mitra;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

import android.util.Log;

import com.example.dto.LoginDetails;
import com.example.dto.MessageMain;

public class ClientPrincipal {

	private String serverMessage;
	public static String SERVERIP = Common.SERVERIP; 
	private Socket socket;
	static private OnMessageReceived mMessageListener = null;
	private boolean mRun = false;
	private PrintWriter out;
	private BufferedReader in;
	boolean valid = true;
	static MessageMain m=new MessageMain();
	static Date date=new Date();
	static String formattedDate = new SimpleDateFormat("dd MMM yyyy hh:mm:ss").format(date);
	public ClientPrincipal(OnMessageReceived listener) {
		mMessageListener = listener;

	}
	public ClientPrincipal() {
	}



	public void sendMessage(MessageMain message) {
		if (out != null && !out.checkError()) {
			try {
				mMessageListener.messageSend(message);
				out.println(message);
				out.flush();
			} catch (Exception e) {
				Log.e("write the message error",
						e.getMessage() + " " + e.getCause());
			}
		}

	}
	public void messageSend(String string)  {
		if (out != null && !out.checkError()) {
			try {
				mMessageListener.messageSend(string);
				out.println(string);
				out.flush();
			} catch (Exception e) {
				Log.e("write the message error",
						e.getMessage() + " " + e.getCause());
			}
		}

	}

	

	public void stopClient() {
		mRun = false;
	}

	public void run() {
		mRun = true;
		try {
			// here you must put your computer's IP address.
			InetAddress serverAddr = InetAddress.getByName(SERVERIP);
			Log.e("serverAddr", serverAddr.toString());
			Log.e("TCP Client", "C: Connecting...");
			// create a socket to make the connection with the server
			socket = new Socket(serverAddr, Common.PORT);
			if (valid) {
				

				//added By amit 17.02.2015
				LoginDetails L_Details=LoginActivity.Ldetails;
				PrintWriter obj = new PrintWriter(socket.getOutputStream());
				JSONObject jo = new JSONObject();
				JSONObject joData = new JSONObject();
				joData.put("class",""+L_Details.getClass_id());  //send classid 
				joData.put("school",""+L_Details.getSchool_id());//send school Id
				joData.put("user_type",""+L_Details.getUserType());//send userType
				joData.put("mobile_number",""+L_Details.getMobileNumber());
				joData.put("email_id",""+L_Details.getEmailId());
				jo.put("event", "join");
				jo.put("name", Common.userid);
				jo.put("app_id", Common.app_id);
				jo.put("timestamp", Common.timstamp);
				jo.put("data", joData);  //optional parameter
				obj.write(jo.toString());
				Log.d("joPrinc", ""+jo);
				obj.flush();

				valid = false;
			}
			try {
				out = new PrintWriter(new BufferedWriter(
						new OutputStreamWriter(socket.getOutputStream())), true);
				Log.e("TCP Client", "C: Sent.");
				Log.e("TCP Client", "C: Done.");

				in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));

				while (mRun) {
					serverMessage = in.readLine();
					if (serverMessage != null && mMessageListener != null) {
						mMessageListener.messageReceived(serverMessage);
					}
					serverMessage = null;
				}
			} catch (Exception e) {
				Log.e("TCP", "S: Error", e);
			} finally {
				socket.close();
				stopClient();
			}
		} catch (Exception e) {
			Log.e("TCP", "C: Error", e);
		}
	}

	public interface OnMessageReceived {
		public void messageReceived(String message);
		public void messageSend(MessageMain ms);
		public void messageSend(String message);

	}

	

}