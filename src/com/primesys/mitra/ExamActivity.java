package com.primesys.mitra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.example.db.DBHelper;
import com.example.dto.ExamDTO;
import com.example.dto.OptionDTO;
import com.example.dto.Question;
import com.example.dto.ResultDTO;
import com.google.gson.Gson;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

public class ExamActivity extends Activity implements OnClickListener {
	List<Question> quesList;
	int score=0,checkid;
	String qid="0";
	ExamDTO currentQ;
	TextView txtQuestion,txttime;
	Button rda, rdb, rdc ,rdd;
	Button butNext;	
	Context context=ExamActivity.this;
	DBHelper helper = DBHelper.getInstance(context);
	ArrayList<ExamDTO>responselist=new ArrayList<ExamDTO>();
	HashMap<String,String> answer=new HashMap<String, String>();
	private int progress = 0,mProgressStatus=0;
	private final int progressBarMax = 10,levelprogressBarMax=400;
	ProgressBar progressBar;
	int Timeto_solve=0,Beforemarks=0,currentmarks=0;
	RadioGroup grp;
	String CATEGORY_KEY="Category",category_label,level_no,LEVEL_NO="Levelno";
	Boolean Status=false;
//	Thread time=new Thread();
	private ProgressBar avg_progressBar;
	protected int correctansno=0;
	private Thread progressBarTimeThread,avg_progressBarTimeThread;
	private TextView txtscore,txtlevel;
	private TextView txtcategory;
	private ImageView ic_rda,ic_rdc,ic_rdb,ic_rdd,stud_image;
	String QUIZID_KEY="Quiz_Id";
	private String Quiz_id;
	private TextView name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.avtivity_quiz_question);
		
		Intent intent=getIntent();
		category_label=intent.getStringExtra(CATEGORY_KEY);
		Quiz_id=intent.getStringExtra(QUIZID_KEY);
		level_no=intent.getStringExtra(LEVEL_NO);		

		findviewByID();

		if(Common.connectionStatus)  
		{
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				new getquestion().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			}
			else{
				new getquestion().execute();
			}
			
		}


	}
	
	private void setcorrect_answer(String answerOptionId) {
		
		System.out.println( "ansselct:   "+ answerOptionId);
		
		
		if(rda.getText().equals(answerOptionId)){
			rda.setBackground(getResources().getDrawable(R.drawable.radio_on_white));
				rda.setTextColor(R.color.selectrd_answer);
				ic_rda.setBackgroundResource(R.drawable.ic_right_ans);
		}else if (rdb.getText().equals(answerOptionId)) {
			rdb.setBackground(getResources().getDrawable(R.drawable.radio_on_white));
			rdb.setTextColor(R.color.selectrd_answer);
			ic_rdb.setBackgroundResource(R.drawable.ic_right_ans);

		}else if (rdc.getText().equals(answerOptionId)) {
			rdc.setBackground(getResources().getDrawable(R.drawable.radio_on_white));
			rdc.setTextColor(R.color.selectrd_answer);
			ic_rdc.setBackgroundResource(R.drawable.ic_right_ans);

		}else if (rdd.getText().equals(answerOptionId)) {
			rdd.setBackground(getResources().getDrawable(R.drawable.radio_on_white));
			rdd.setTextColor(R.color.selectrd_answer);
			ic_rdd.setBackgroundResource(R.drawable.ic_right_ans);

		}
		
	}

	public void SetLevelavgProgress() {
		
		// Start lengthy operation in a background thread
		avg_progressBar.setMax(Common.maximum_marks*responselist.size());
		 avg_progressBarTimeThread = new Thread() {
		    @Override
		    public void run() {
		        
		            if(Common.Level_marks<=Common.maximum_marks*responselist.size()) {
		            	
		            	 avg_progressBar.setProgress(Common.Level_marks);

			    //         System.out.println("jhsghhjshjjf"+Common.Level_marks);
		                
		            }
		        }
		      };

		if (!avg_progressBarTimeThread.isAlive()) {
			avg_progressBarTimeThread.start();
		}
		
		
	
	}

	

	private void findviewByID() {
		txtQuestion=(TextView)findViewById(R.id.Question);
		rda=(Button)findViewById(R.id.radio0);
		rdb=(Button)findViewById(R.id.radio1);
		rdc=(Button)findViewById(R.id.radio2);
		rdd=(Button)findViewById(R.id.radio3);
		name=(TextView) findViewById(R.id.name);
		name.setText(Common.quiz_name);
		rda.setVisibility(View.INVISIBLE);
        rdb.setVisibility(View.INVISIBLE);
        rdc.setVisibility(View.INVISIBLE);
        rdd.setVisibility(View.INVISIBLE);
	    progressBar = (ProgressBar)findViewById (R.id.circularProgressBar);
		txttime=(TextView) findViewById(R.id.time_texview);
		txtscore=(TextView) findViewById(R.id.levelscore);
		txtlevel=(TextView) findViewById(R.id.Level);
		txtcategory=(TextView) findViewById(R.id.category);
		grp=(RadioGroup)findViewById(R.id.radioGroup);
		 avg_progressBar=(ProgressBar)findViewById (R.id.avg_progressbar);	 
		
		 Drawable draw=getResources().getDrawable(R.drawable.avg_progresbar);
		// set the drawable as progress drawable
		 avg_progressBar.setProgressDrawable(draw);

		
		txtlevel.setText("Level No: "+level_no);
		txtcategory.setText(category_label);
		ic_rda=(ImageView)findViewById(R.id.im_radio0);
		ic_rdb=(ImageView)findViewById(R.id.im_radio1);
		ic_rdc=(ImageView)findViewById(R.id.im_radio2);
		ic_rdd=(ImageView)findViewById(R.id.im_radio3);
		stud_image=(ImageView) findViewById(R.id.image_boy);
		Bitmap bmp=DBHelper.getInstance(context).getBitMap(Common.userid);
		if(bmp!=null)
			stud_image.setImageBitmap(bmp);
	}

	
	void SetTimerProgresBar(){
		// change it programmatically
		progress=0;
				progressBar.setMax(Common.bonus_time_interval);
				  progressBarTimeThread = new Thread() {
				    @Override
				    public void run() {
				        try {
				            while(progress<=Common.bonus_time_interval) {
				            	 progressBar.setProgress(progress);
				                 settime(Common.bonus_time_interval-progress);
					          //   System.out.println(progress);
				                sleep(1000);
				                ++progress; 
				            }
				        }
				        catch(InterruptedException e) {
				        }
				    }
				};

			progressBarTimeThread.start();
				
			}
	
	
	protected void settime(final int progress2) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				   txttime.setText(progress2+"");

			}
		});
     
	}


	class getquestion extends AsyncTask<Void, Void, String>
	{
		//String url="http://192.168.1.110:8080/MyKiddyTrackerAPI/MyKiddy/ExamAPI/GetQuestion";
		private ProgressDialog pDialog;
		@Override
		protected void onPreExecute() {
			pDialog=new ProgressDialog(context);
			pDialog.setMessage("Progress wait....");
			pDialog.setCancelable(false);
			pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String result="User is not registered";

			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(Common.SQLURL+"SQLQuiz/GetQuestions");
			try
			{
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(1);
				/*postParameter.add(new BasicNameValuePair("ClassId",Common.classId));
				postParameter.add(new BasicNameValuePair("UserId",Common.userid));
				postParameter.add(new BasicNameValuePair("Category",category_label));
				postParameter.add(new BasicNameValuePair("LevelNo",level_no));
				postParameter.add(new BasicNameValuePair("SchoolId",Common.schoolId));*/

			//	postParameter.add(new BasicNameValuePair("QuizId",Quiz_id));
				postParameter.add(new BasicNameValuePair("QuizId","4"));

				postParameter.add(new BasicNameValuePair("UserId",Common.userid));
				postParameter.add(new BasicNameValuePair("Category",category_label));
				postParameter.add(new BasicNameValuePair("LevelNo",level_no));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));

				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
				System.out.println("Req Question"+postParameter);
			}catch(Exception e)
			{
				
				
				result=e.getMessage();	
			}

			return result;
		}
		@Override
		protected void onPostExecute(String result) {
		pDialog.dismiss(); 
			
			if(result.contains("error"))
			{
				/*CustomDialog.displayDialog("No Child",homeContext);*/
			}
			else{
				parseJSON(result);
			//	SetTimerProgresBar();

			}
			
		}
		


	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.review, menu);
		return true;
	}
	public void parseJSON(String result) {
		try {

			JSONArray array = new JSONArray(result);
			System.err.println("Result Question="+result+"----------" +array.length());
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo=array.getJSONObject(i);
				ExamDTO examres=new ExamDTO();
				

				examres.setQuestionId(jo.getString("QuestionId"));
				examres.setQuestionLabel(jo.getString("QuestionLabel"));
				examres.setAnswerOptionId(jo.getString("AnswerOptionId"));
				examres.setAnsType(jo.getString("AnsType"));

				ArrayList<OptionDTO> olist=new ArrayList<OptionDTO>();
				JSONArray oparray =new JSONArray(jo.getString("list"));
				for (int j = 0; j< oparray.length(); j++) {
					JSONObject op=oparray.getJSONObject(j);
					OptionDTO option=new OptionDTO();
					//option.setOptionId(op.getString("OptionId"));
					option.setOptionLabel(op.getString("OptionLabel"));
					//option.setAnswer(op.getString("Answer"));
					olist.add(option);
					examres.setList(olist);
					
				}
				
			
						
			responselist.add(examres);
			
		//	System.out.println(responselist);	
			}
			System.err.println("responsize"+responselist.size());

			setQuestionView(responselist);

			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void setQuestionView(ArrayList<ExamDTO> responselist)
	{
		rda.setBackground(getResources().getDrawable(R.drawable.radio_on_background));
		rdb.setBackground(getResources().getDrawable(R.drawable.radio_on_background));
		rdc.setBackground(getResources().getDrawable(R.drawable.radio_on_background));
		rdd.setBackground(getResources().getDrawable(R.drawable.radio_on_background));
		Timeto_solve=0;
		progress = 0;
		ic_rda.setBackgroundResource(0);
		ic_rdb.setBackgroundResource(0);
		ic_rdc.setBackgroundResource(0);
		ic_rdd.setBackgroundResource(0);
		rda.setTextColor(Color.WHITE);
		rdb.setTextColor(Color.WHITE);
		rdc.setTextColor(Color.WHITE);
		rdd.setTextColor(Color.WHITE);
 		txtscore.setText(Common.Level_marks+"");

	


		if(Common.question_count<responselist.size())
		{
			/*if(Common.question_count == 0){
				System.out.println(Common.question_count+"------------11111");
				ExamDTO currentQ=new ExamDTO();
				currentQ=responselist.get(Common.question_count);
				txtQuestion.setText(currentQ.getQuestionLabel());
				rda.setText(currentQ.getList().get(0).getOptionLabel());
				rdb.setText(currentQ.getList().get(1).getOptionLabel());
				rdc.setText(currentQ.getList().get(2).getOptionLabel());
				rdd.setText(currentQ.getList().get(3).getOptionLabel());
				Common.question_count++;
			}
			else{*/
				System.out.println(Common.question_count+"------------222222");

		
			final ExamDTO current_question=responselist.get(Common.question_count);

				txtQuestion.setText(current_question.getQuestionLabel());
				Runnable myRun = new Runnable(){
				    public void run(){
				            synchronized(this){
				                try {
									wait(Common.showanw_time_interval*1000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} //wait 30 seconds before changing text
				            }
				            //to change the textView you must run code on UI Thread so:
				            runOnUiThread(new Runnable(){
				                public void run(){
				            		grp.setVisibility(View.VISIBLE);

				                	rda.setText(current_question.getList().get(0).getOptionLabel());
				    				rdb.setText(current_question.getList().get(1).getOptionLabel());
				    				rdc.setText(current_question.getList().get(2).getOptionLabel());
				    				rdd.setText(current_question.getList().get(3).getOptionLabel());
				    				rda.setVisibility(View.VISIBLE);
				    				rdb.setVisibility(View.VISIBLE);
				    				rdc.setVisibility(View.VISIBLE);
				    				rdd.setVisibility(View.VISIBLE);
				    				Common.question_count++;
				    				Status=true;
				                }
				            });
				            
						
				        	SetTimerProgresBar();
							
							
				        
				    }
				};
				Thread T = new Thread(myRun);
				T.start();
			
				
		    			//SetTimerProgresBar();
		    		
					
			
			/*}*/
			
		}
		else {
			Gson gson = new Gson();
			ResultDTO result=new ResultDTO();
			result.setQuestionAnsMap(answer);
			//result.setMapstring(answer.toString().replaceAll("\\\\u003",":"));
			result.setCategory(category_label);
			result.setLevel(level_no);
			result.setQuizId("");
			result.setScore(Common.Level_marks+"");
			result.setSubCategory("");
			result.setTime_Taken("time");
			result.setUserId(Common.userid);
			result.setEvaluation("");
			result.setCurrent_Timestamp("46565");
		//	Common.showToast("Thank You !!!!!!!!!!!", context);
			Intent intent = new Intent(ExamActivity.this, FinalQuizScore.class);
			intent.putExtra("LevelScore",Common.Level_marks);
			String Resultreq = gson.toJson(result);
			intent.putExtra("Result",Resultreq);
			intent.putExtra("Size",responselist.size());
			startActivity(intent);
			Common.question_count=0;
			Common.Level_marks=0;
			finish();
		}
		

	

		

	//	qid++;
	}



	@Override
	public void onClick(View v) { 
		
		switch(v.getId()){
    
    
    case R.id.radio0:
	
    	
						try {
							progressBarTimeThread.interrupt();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} 
	                    	checkid=0;
	        				Timeto_solve=Integer.parseInt(txttime.getText().toString());
	        			//	System.out.println("Remain Time=="+Timeto_solve);
	        				ExamDTO CurrentAnswer=new ExamDTO();
	        			
	        				if (Common.question_count <=responselist.size()) {
	        					CurrentAnswer=responselist.get(Common.question_count-1);

	        				if(!rda.getText().equals(CurrentAnswer.getOptionId())){
	        					rda.setBackground(getResources().getDrawable(R.drawable.radio_on_white));
	        					rda.setTextColor(R.color.selectrd_answer);
	        					ic_rda.setBackgroundResource(R.drawable.ic_wrong_ans);

	        					}
	        					setcorrect_answer(CurrentAnswer.getAnswerOptionId());
	        			
	        					Runnable myRun = new Runnable(){
	        					    public void run(){
	        					            synchronized(this){
	        					                try {
	        										wait(Common.showanw_time_interval*1000);
	        									} catch (InterruptedException e) {
	        										// TODO Auto-generated catch block
	        										e.printStackTrace();
	        									} //wait 30 seconds before changing text
	        					            }
	        					            //to change the textView you must run code on UI Thread so:
	        					            runOnUiThread(new Runnable(){
	        					                public void run(){
	        					                rda.setVisibility(View.INVISIBLE);
	        					                rdb.setVisibility(View.INVISIBLE);
	        					                rdc.setVisibility(View.INVISIBLE);
	        					               rdd.setVisibility(View.INVISIBLE);
	        					                	setQuestionView(responselist);
	        					                	
	        					                }
	        					            });

	        					        
	        					    }
	        					};
	        					Thread New_question = new Thread(myRun);
	        					New_question.start();
	        					Beforemarks=Common.Level_marks;
	        					if(CurrentAnswer.getAnswerOptionId().equals(rda.getText())){
	        						currentmarks=Common.minimum_marks_question+(Timeto_solve*Common.per_second_bonus_marks);
	        						Common.Level_marks=Common.Level_marks+currentmarks;
	        						txtscore.setText(Beforemarks+"+"+currentmarks+"");
	        						SetLevelavgProgress();
	        					}else txtscore.setText(Beforemarks+"+"+0+"");
	        			
	        				    answer.put(CurrentAnswer.getQuestionId(),rda.getText().toString());
	        				    System.out.println("check"+CurrentAnswer.getQuestionId()+"   "+rda.getText());
	        				}
	        				
		                    break;


    case R.id.radio1:
    	
									    	try {
												progressBarTimeThread.interrupt();
											} catch (Exception e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											} 
									
									    	Timeto_solve=Integer.parseInt(txttime.getText().toString());
											System.out.println("Remain Time=="+Timeto_solve);
									
									    	checkid=1;
									    	if (Common.question_count <=responselist.size()) {
												CurrentAnswer=responselist.get(Common.question_count-1);
									
												String Qustion_Id=CurrentAnswer.getQuestionId();
											//	System.out.println("Inside BTNNEXT --"+Common.question_count+"--"+Qustion_Id);
									
												String Selected_OptionId=CurrentAnswer.getList().get(checkid).getOptionId();
												if(!rdb.getText().equals(CurrentAnswer.getOptionId())){
													rdb.setBackground(getResources().getDrawable(R.drawable.radio_on_white));
													rdb.setTextColor(R.color.selectrd_answer);
													ic_rdb.setBackgroundResource(R.drawable.ic_wrong_ans);
									
												}
												setcorrect_answer(CurrentAnswer.getAnswerOptionId());
												Runnable myRun = new Runnable(){
												    public void run(){
												            synchronized(this){
												                try {
																	wait(Common.showanw_time_interval*1000);
																} catch (InterruptedException e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																} //wait 30 seconds before changing text
												            }
												            //to change the textView you must run code on UI Thread so:
												            runOnUiThread(new Runnable(){
												                public void run(){  rda.setVisibility(View.INVISIBLE);
					        					                rdb.setVisibility(View.INVISIBLE);
					        					                rdc.setVisibility(View.INVISIBLE);
					        					               rdd.setVisibility(View.INVISIBLE);
												                	setQuestionView(responselist);
												                }
												            });
									
												        
												    }
												};
												Thread New_question = new Thread(myRun);
												New_question.start();
												Beforemarks=Common.Level_marks;
					        					if(CurrentAnswer.getAnswerOptionId().equals(rdb.getText())){
					        						currentmarks=Common.minimum_marks_question+(Timeto_solve*Common.per_second_bonus_marks);
					        						Common.Level_marks=Common.Level_marks+currentmarks;
					        						
					        						txtscore.setText(Beforemarks+"+"+currentmarks+"");

					        						SetLevelavgProgress();
					        					}else txtscore.setText(Beforemarks+"+"+0+"");
												 answer.put(Qustion_Id,rdb.getText().toString());
												    System.out.println("check"+Qustion_Id+"   "+rdb.getText());
											}
									    	
									    	
									        break;

    case R.id.radio2:
    	
									    	try {
												progressBarTimeThread.interrupt();
											} catch (Exception e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											} 
									
									    	Timeto_solve=Integer.parseInt(txttime.getText().toString());
											System.out.println("Remain Time=="+Timeto_solve);
									
									    	checkid=2;
									    	if (Common.question_count <=responselist.size()) {
												CurrentAnswer=responselist.get(Common.question_count-1);
									
												String Qustion_Id=CurrentAnswer.getQuestionId();
												//System.out.println("Inside BTNNEXT --"+Common.question_count+"--"+Qustion_Id);
									
												if(!rdc.getText().equals(CurrentAnswer.getOptionId())){
													rdc.setBackground(getResources().getDrawable(R.drawable.radio_on_white));
													rdc.setTextColor(R.color.selectrd_answer);
													ic_rdc.setBackgroundResource(R.drawable.ic_wrong_ans);
									
												}
												setcorrect_answer(CurrentAnswer.getAnswerOptionId());
												Runnable myRun = new Runnable(){
												    public void run(){
												            synchronized(this){
												                try {
																	wait(Common.showanw_time_interval*1000);
																} catch (InterruptedException e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																} //wait 30 seconds before changing text
												            }
												            //to change the textView you must run code on UI Thread so:
												            runOnUiThread(new Runnable(){
												                public void run(){  rda.setVisibility(View.INVISIBLE);
					        					                rdb.setVisibility(View.INVISIBLE);
					        					                rdc.setVisibility(View.INVISIBLE);
					        					               rdd.setVisibility(View.INVISIBLE);
												                	setQuestionView(responselist);
												                }
												            });
									
												        
												    }
												};
												Thread New_question = new Thread(myRun);
												New_question.start();
												Beforemarks=Common.Level_marks;
					        					if(CurrentAnswer.getAnswerOptionId().equals(rdc.getText())){
					        						currentmarks=Common.minimum_marks_question+(Timeto_solve*Common.per_second_bonus_marks);
					        						Common.Level_marks=Common.Level_marks+currentmarks;
					        						
					        						txtscore.setText(Beforemarks+"+"+currentmarks+"");

					        						SetLevelavgProgress();
					        					}else txtscore.setText(Beforemarks+"+"+0+"");
												/*if(CurrentAnswer.getAnswerOptionId().equals(Selected_OptionId)){
													rdc.setTextColor(R.color.anstrue);
													//rdc.setBackgroundColor(R.color.anstrue);
													Common.Level_marks=20+(Timeto_solve*2);
												//	SetLevelavgProgress(Common.Level_marks);
												}else{
												//	rdc.setTextColor(R.color.ansfalse);
													
												}*/
												 answer.put(Qustion_Id,rdc.getText().toString());
												    System.out.println("check"+Qustion_Id+"   "+rdc.getText());
											  
											}
									    	
									        break;

    case R.id.radio3:
									    	
									    	try {
												progressBarTimeThread.interrupt();
											} catch (Exception e1) {
												// TODO Auto-generated catch block
												e1.printStackTrace();
											} 
									
									    	Timeto_solve=Integer.parseInt(txttime.getText().toString());
											System.out.println("Remain Time=="+Timeto_solve);
									
									    	checkid=3;
									    	if (Common.question_count <=responselist.size()) {
												CurrentAnswer=responselist.get(Common.question_count-1);
									
												String Qustion_Id=CurrentAnswer.getQuestionId();
									
												String Selected_OptionId=CurrentAnswer.getList().get(checkid).getOptionId();
												if(!rdd.getText().equals(CurrentAnswer.getOptionId())){
													rdd.setBackground(getResources().getDrawable(R.drawable.radio_on_white));
													rdd.setTextColor(R.color.selectrd_answer);
													ic_rdd.setBackgroundResource(R.drawable.ic_wrong_ans);
									
												}
												setcorrect_answer(CurrentAnswer.getAnswerOptionId());
												
												Runnable myRun = new Runnable(){
												    public void run(){
												            synchronized(this){
												                try {
																	wait(Common.showanw_time_interval*1000);
																} catch (InterruptedException e) {
																	// TODO Auto-generated catch block
																	e.printStackTrace();
																} //wait 30 seconds before changing text
												            }
												            //to change the textView you must run code on UI Thread so:
												            runOnUiThread(new Runnable(){
												                public void run(){  rda.setVisibility(View.INVISIBLE);
					        					                rdb.setVisibility(View.INVISIBLE);
					        					                rdc.setVisibility(View.INVISIBLE);
					        					               rdd.setVisibility(View.INVISIBLE);
												                	setQuestionView(responselist);
												                }
												            });
									
												        
												    }
												};
												Thread New_question = new Thread(myRun);
												New_question.start();
												
												Beforemarks=Common.Level_marks;
					        					if(CurrentAnswer.getAnswerOptionId().equals(rdd.getText())){
					        						currentmarks=Common.minimum_marks_question+(Timeto_solve*Common.per_second_bonus_marks);
					        						Common.Level_marks=Common.Level_marks+currentmarks;
					        						
					        						txtscore.setText(Beforemarks+"+"+currentmarks+"");

					        						SetLevelavgProgress();
					        					}else txtscore.setText(Beforemarks+"+"+0+"");
												/*if(CurrentAnswer.getAnswerOptionId().equals(Selected_OptionId)){
													rdd.setTextColor(R.color.anstrue);
													//rdd.setBackgroundColor(R.color.anstrue);
													Common.Level_marks=20+(Timeto_solve*2);
												//	SetLevelavgProgress(Common.Level_marks);
												}else{
												//	rdd.setTextColor(R.color.ansfalse);
													
												}*/
												 answer.put(Qustion_Id,rdd.getText().toString());
												    System.out.println("check"+Qustion_Id+"   "+rdd.getText());
											}
									    	
									        break;
}}

	
}
