package com.example.db;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract.CommonDataKinds;
import android.sax.StartElementListener;
import android.util.Log;

import com.example.dto.Child;
import com.example.dto.DiaryDetails;
import com.example.dto.LocationData;
import com.example.dto.MessageMain;
import com.primesys.mitra.APIController;
import com.primesys.mitra.Common;
import com.primesys.mitra.DateTimeActivity;
import com.primesys.mitra.HistoryActivity;
import com.primesys.mitra.ShowGMap;
import com.primesys.mitra.ShowGmapClient;

public class DBHelper extends SQLiteOpenHelper {

	static final String DATABASE_NAME = "tracking.db";
	private static final int DATABASE_VERSION = 8;
	private static final String TABLE_PARENT="db_parent";
	private static final String TABLE_TEACHER="db_teacher";
	private static final String TABLE_PRINCIPLE="db_principle";
	private static final String TABLE_HISTORY="db_history";
	private static final String TABLE_USER="db_user";
	private static final String TABLE_TEACHER_DIARY="db_teach_diary";
	private static final String TABLE_NEWS="db_news";
	private static final String TABLE_PRODATA="db_prodata";

	private static final String create_table_parent="create table "+TABLE_PARENT+" (id INTEGER PRIMARY KEY AUTOINCREMENT,message TEXT,sentdate DATETIME ,from_msg VARCHAR(50),to_msg VARCHAR(50),group_id VARCHAR(50),type varchar(1),ref_id long,student_id varchar(20),status INTEGER)";
	private static final String create_table_teacher="create table "+TABLE_TEACHER+" (id INTEGER PRIMARY KEY AUTOINCREMENT,message TEXT,sentdate DATETIME,from_msg VARCHAR(50),to_msg VARCHAR(50),group_id VARCHAR(50),type varchar(1),ref_id long,status INTEGER)";
	private static final String create_table_principal="create table "+TABLE_PRINCIPLE+" (id INTEGER PRIMARY KEY AUTOINCREMENT,message TEXT,sentdate DATETIME,from_msg VARCHAR(50),to_msg VARCHAR(50),group_id VARCHAR(50),type varchar(1),ref_id long,student_id varchar(20),status INTEGER)";
	private static final String create_table_history="create table "+TABLE_HISTORY+" (id INTEGER PRIMARY KEY AUTOINCREMENT,lat DOUBLE,lan DOUBLE,timestamp INTEGER,speed INTEGER)";
	private static final String create_table_user="Create table "+TABLE_USER+"(userid String PRIMARY KEY  not null,Photo blob)";
	private static final String create_table_teacher_diary="Create table "+TABLE_TEACHER_DIARY+"(id INTEGER PRIMARY KEY ,diary_id INTEGER,from_id INTEGER,to_id INTEGER,message TEXT,sign_by_parent TEXT,sign_by_teacher TEXT,document VARCHAR(50),"
			+ "sentdate varchar(20),status varchar(3),teacher_name varchar(50),student_name varchar(50))";
	private static final String create_table_news="Create table "+TABLE_NEWS+"(id INTEGER PRIMARY KEY AUTOINCREMENT,news TEXT)";
	private static final String create_table_prodata="Create table "+TABLE_PRODATA+"(id INTEGER PRIMARY KEY,name TEXT,stud_id INTEGER)";


	private static DBHelper helper;
	SQLiteDatabase database;
	DBHelper context=DBHelper.this;
	public static DBHelper getInstance(Context context) {
		if (helper == null) {
			helper  = new DBHelper(context,DATABASE_NAME,DATABASE_VERSION);
		}
		return helper;
	}
	public DBHelper(Context context, String name,int version) {
		super(context, name,null, version);
	}
	//create  tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		try
		{	db.execSQL(create_table_news);     
		db.execSQL(create_table_principal);                 
		db.execSQL(create_table_teacher); 
		db.execSQL(create_table_parent);
		db.execSQL(create_table_history);
		db.execSQL(create_table_user);  
		db.execSQL(create_table_teacher_diary);
		db.execSQL(create_table_prodata);
		}catch(Exception e)
		{
			Log.e("Exception in create table",e.getMessage()+" "+e.getCause());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//on upgrade drop older tables
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRINCIPLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEACHER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_USER);  
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_TEACHER_DIARY); 
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_NEWS); 
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_PRODATA); 
		// create new tables
		onCreate(db);
	}

	//insert the message into the table
	public String  insertMessage(MessageMain m)
	{
		String result="success";
		String tablename="";
		if(Common.roleid.equals("1"))
			tablename=TABLE_PRINCIPLE;    
		else if(Common.roleid.equals("2"))
			tablename=TABLE_TEACHER; 
		else
			if(Common.roleid.equals("3")||Common.roleid.equals("5"))
				tablename=TABLE_PARENT;

		database=this.getWritableDatabase();
		try
		{
			String query="select * from "+tablename;
			Cursor cusr=database.rawQuery(query,null);
		//	System.err.println("@@@@@@@@@@@@@@@@@"+cusr.getCount());
			if(cusr.getCount()>=500)
			{
				truncateTables(tablename);
			}
			else
			{
				String sql="insert into "+tablename+"(message,sentdate,from_msg,to_msg,group_id,type,ref_id,status) values(?,?,?,?,?,?,?,?)";
				SQLiteStatement statement = database.compileStatement(sql);
				database.beginTransaction();
				statement.bindString(1, m.getMesageText());
				statement.bindString(2,m.getDate_time()+"");
				statement.bindString(3, m.getFrom());
				statement.bindString(4, m.getTo());
				statement.bindString(5, m.getGroup_id());
				statement.bindString(6, m.getType());
				statement.bindLong(7, m.getRef_id());
				statement.bindLong(8, APIController.getInstance().getRead());
				statement.execute();

				database.setTransactionSuccessful();	
				database.endTransaction();
			}
		}catch(Exception e)
		{
			result="failure";
		}
		finally
		{
			database.close();
		}
		return result;
	}
	// insert the message plus student id
	public String  insertMessage(MessageMain m,String studentID)
	{
		String result="success";
		String tablename="";
		if(Common.roleid.equals("1"))
			tablename=TABLE_PRINCIPLE;    
		else
			if(Common.roleid.equals("3")||Common.roleid.equals("5"))
				tablename=TABLE_PARENT;

		database=this.getWritableDatabase();
		try
		{
			String query="select * from "+tablename;
			Cursor cusr=database.rawQuery(query,null);
			if(cusr.getCount()>=500)
			{
				truncateTables(tablename);
			}
			else
			{
				String sql="insert into "+tablename+"(message,sentdate,from_msg,to_msg,group_id,type,ref_id,student_id,status) values(?,?,?,?,?,?,?,?,?)";
				SQLiteStatement statement = database.compileStatement(sql);
				database.beginTransaction();
				statement.bindString(1, m.getMesageText());
				statement.bindString(2,m.getDate_time()+"");
				statement.bindString(3, m.getFrom());
				statement.bindString(4, m.getTo());
				statement.bindString(5, m.getGroup_id());
				statement.bindString(6, m.getType());
				statement.bindLong(7, m.getRef_id());
				statement.bindString(8, studentID);
				statement.bindLong(9, APIController.getInstance().getRead());
				statement.execute();

				database.setTransactionSuccessful();	
				database.endTransaction();
			}
		}catch(Exception e)
		{
			result="failure";
		}
		finally
		{
			database.close();
		}
		return result;
	}
	//update the dateTime from the acknoweldgement 
	public void  UpdateMessage(long refId,long timestamp )
	{
		//System.out.println("DBHelper.UpdateMessage()"+refId+"    "+timestamp);
		String tablename="";
		if(Common.roleid.equals("1"))
			tablename=TABLE_PRINCIPLE;    
		else if(Common.roleid.equals("2"))
			tablename=TABLE_TEACHER; 
		else
			if(Common.roleid.equals("3")||Common.roleid.equals("5"))
				tablename=TABLE_PARENT;
		database=this.getWritableDatabase();
		try
		{
			ContentValues values=new ContentValues();
			values.put("sentdate", timestamp);
			if(refId!=0)
				database.update(tablename, values, "ref_id="+refId, null);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}
	}

	//select all the messages from the data table
	public ArrayList<MessageMain> getAllMessages(String toMsg,String fromMsg,boolean isGroup,String studentID)
	{
		String tablename="",sql="";
		if(Common.roleid.equals("1"))
			tablename=TABLE_PRINCIPLE;    
		else if(Common.roleid.equals("2"))
			tablename=TABLE_TEACHER; 
		else
			if(Common.roleid.equals("3")||Common.roleid.equals("5"))
				tablename=TABLE_PARENT;
		ArrayList<MessageMain> list=new ArrayList<MessageMain>();
		database=this.getReadableDatabase();
		if(isGroup)
			sql="select * from "+tablename+" where (from_msg ='"+fromMsg+"')or(to_msg = '"+fromMsg+"') order by sentdate";	
		else if(!studentID.equals("A"))
			sql="select * from "+tablename+" where (to_msg = '"+toMsg+"' and from_msg ='"+fromMsg+"')or(to_msg = '"+fromMsg+"' and from_msg = '"+toMsg+"') and student_id= '"+studentID+"'"+" order by sentdate";
		else
			sql="select * from "+tablename+" where (to_msg = '"+toMsg+"' and from_msg ='"+fromMsg+"')or(to_msg = '"+fromMsg+"' and from_msg = '"+toMsg+"') order by sentdate";

		try
		{
			Cursor cursor=database.rawQuery(sql, null);
			while(cursor.moveToNext())
			{
				MessageMain m=new MessageMain();
				m.setMesageText(cursor.getString(1));
				m.setDate_time(Long.parseLong(cursor.getString(2)));
				m.setFrom(cursor.getString(3));
				m.setTo(cursor.getString(4));
				m.setGroup_id(cursor.getString(5));
				m.setType(cursor.getString(6));
				list.add(m);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally
		{
			database.close();
		}

		return list;
	}
	// get the last message from the data table
	public MessageMain getLastMessage()
	{
		String tablename="";
		if(Common.roleid.equals("1"))
			tablename=TABLE_PRINCIPLE;    
		else if(Common.roleid.equals("2"))
			tablename=TABLE_TEACHER; 
		else
			if(Common.roleid.equals("3")||Common.roleid.equals("5"))
				tablename=TABLE_PARENT;
		database=this.getReadableDatabase();
		MessageMain m=new MessageMain();
		String sql="select * from "+tablename+" ORDER BY sentdate DESC LIMIT 1 ";
		try
		{
			Cursor cursor=database.rawQuery(sql, null);
			if(cursor.moveToFirst())
			{
				m.setMesageText(cursor.getString(1));
				m.setDate_time(Long.parseLong(cursor.getString(2)));
				m.setFrom(cursor.getString(3));
				m.setTo(cursor.getString(4));
			}
		}catch(Exception e)
		{

		}finally
		{
			database.close();
		}
		return m;
	}
	//delete all the records from the table
	public void truncateTables(String tablename)
	{
		try
		{
			database=this.getWritableDatabase();
			String query ="DELETE FROM "+tablename;
			database.execSQL(query);
		}catch(Exception e)
		{
			Log.e("Exception in truncate",e.getMessage());
		}
		finally{
			database.close();
		}
	}

	//get last timestamp value from the DB
	public void getLastTimeStamp()
	{
		String tablename="";
		String timestamp="00000";
		if(Common.roleid.equals("1"))
			tablename=TABLE_PRINCIPLE;    
		else if(Common.roleid.equals("2"))
			tablename=TABLE_TEACHER; 
		else
			if(Common.roleid.equals("3")||Common.roleid.equals("5"))
				tablename=TABLE_PARENT;
		database=this.getReadableDatabase();
		String sql="select  sentdate from "+tablename+" ORDER BY sentdate DESC LIMIT 1 ";
		try
		{
			Cursor cursor=database.rawQuery(sql, null);
			if(cursor.moveToFirst())
			{
				timestamp=cursor.getString(0);
			}
		}catch(Exception e)
		{

		}finally
		{
			database.close();
		}
		Common.timstamp= timestamp;
	}
	//convert date into unix timestamp
	long convertToLong(String date)
	{
		long time=0;
		try
		{
			DateFormat dfm = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
			time = dfm.parse(date).getTime();  
		}catch(Exception e)
		{
			Log.e("date conversation error",e.getMessage());
			e.printStackTrace();
			time=Long.parseLong(date);
		}
		return time;
	}
	// insert the data into the history table
	public String insertLocation(ArrayList<LocationData> list)
	{
		String result="success";
		database=this.getWritableDatabase();
		try
		{
			String sql="insert or replace into "+TABLE_HISTORY+"(lat,lan,timestamp,speed) values(?,?,?,?)";
			SQLiteStatement statement=database.compileStatement(sql);
			database.beginTransaction();
			for(LocationData l : list)
			{

				statement.bindDouble(1, l.getLat());
				statement.bindDouble(2, l.getLan());
				statement.bindLong(3, l.getTimestamp());
				statement.bindLong(4, l.getSpeed()); 
				statement.execute();


			}

			database.setTransactionSuccessful();
			database.endTransaction();
		}catch(Exception e)
		{
			result="failure";
			System.err.print(e);
			e.printStackTrace();
		}
		finally
		{
			
			if (DateTimeActivity.selStatus) {
				ShowGmapClient.startMethod();
				database.close();
			}

		}
		return result;
	}
	//select all the history detail from the table
	public ArrayList<LocationData> showDetails()
	{
		ArrayList<LocationData> list=new ArrayList<LocationData>();
		try
		{
			String query="Select * from "+TABLE_HISTORY;
			database=this.getReadableDatabase();
			Cursor cursor=database.rawQuery(query, null);
			while(cursor.moveToNext())
			{

				LocationData l=new LocationData();
				
				l.setLat(cursor.getDouble(1));
				l.setLan(cursor.getDouble(2));
				l.setTimestamp(cursor.getLong(3));
				l.setSpeed(cursor.getInt(4));
				list.add(l);

			}


		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{

			database.close();
		}
		return list;
	}

	//insert the Photo and the userid 
	public long insertPhoto(Bitmap bmp,String userid)
	{
		long c=0;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
		byte[] buffer=out.toByteArray();
		database=this.getWritableDatabase();
		database.beginTransaction();
		ContentValues values;
		try
		{
			values = new ContentValues();
			values.put("photo", buffer);
			values.put("userid",userid);
			c= database.replace(TABLE_USER, null, values);
			database.setTransactionSuccessful();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.endTransaction();
			database.close();
		}
		return c;
	}
	// get the bitmap object based on the id
	public Bitmap getBitMap(String id)
	{
		Bitmap bmp=null;
		database=this.getReadableDatabase();
		database.beginTransaction();
		try
		{
			String sql="select Photo from "+TABLE_USER+" where userid = "+id;
			Cursor cursor=database.rawQuery(sql, null);
			if(cursor.getCount()>0){
				if (cursor.moveToNext()) {
					byte[] blob = cursor.getBlob(cursor.getColumnIndex("Photo"));
					// Convert the byte array to Bitmap
					bmp=BitmapFactory.decodeByteArray(blob, 0, blob.length);
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			database.endTransaction();
			database.close();
		}
		return bmp;
	}
	//get the count of unread messages
	public int getCount(String userid,boolean isGroup)
	{
		String sql;
		int cnt=0;
		if(!isGroup)
			sql="select message from "+TABLE_PARENT+" where status =0 and from_msg="+userid+" and to_msg="+Common.userid;
		else
			sql="select message from "+TABLE_PARENT+" where status =0 and to_msg="+userid+" and from_msg !="+Common.userid;

		database=this.getReadableDatabase();
		Cursor cur=database.rawQuery(sql, null);
		cnt=cur.getCount();
		database.close();
		return cnt;
	}
	//update the status of all the messages whose id match
	public int updateMessages(String userid,boolean isGroup)
	{
		int cnt=0;
		String sql;
		if(!isGroup)
			sql="update "+TABLE_PARENT+" set status = 1 where status =0 and from_msg="+userid+" and to_msg="+Common.userid;
		else
			sql="update "+TABLE_PARENT+" set status = 1  where status =0 and to_msg="+userid+" and from_msg !="+Common.userid;

		database=this.getWritableDatabase();
		Cursor cur=database.rawQuery(sql, null);
		cnt=cur.getCount();
		database.close();
		return cnt;         
	}
	public String insert_teacher_diary(ArrayList<DiaryDetails> listDiaryDetails) {
		// TODO Auto-generated method stub
		if(Common.connectionStatus){
			truncateTables(TABLE_TEACHER_DIARY);
		}
		String result="success";
		database=this.getWritableDatabase();
		try
		{

			String sql1="insert or replace into "+TABLE_TEACHER_DIARY+"(diary_id ,from_id,to_id,message,sign_by_parent,sign_by_teacher,document,sentdate,status,teacher_name,student_name) values(?,?,?,?,?,?,?,?,?,?,?)";
			SQLiteStatement statement=database.compileStatement(sql1);
			database.beginTransaction();
			for(DiaryDetails l : listDiaryDetails)
			{ 
				statement.bindLong(1,l.getDairy_id());

				statement.bindLong(2, l.getFrom_id());
				statement.bindLong(3, l.getTo_id());
				statement.bindString(4, l.getMessage());
				statement.bindString(5, l.getSign_by_parent());
				statement.bindString(6,l.getSign_by_teacher());
				statement.bindString(7, l.getDocuments());
				statement.bindString(8,l.getMsg_record_dt_time());
				statement.bindString(9, l.getStatus());
				statement.bindString(10, l.getStudentName());
				statement.bindString(11, l.getTeacherName());



				statement.execute();
			}
			database.setTransactionSuccessful();
			database.endTransaction();
		}catch(Exception e)
		{
			result="failure";
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}
		return result;

	}
	public String insert_teacher_diaryoffline(ArrayList<DiaryDetails> listDiaryDetails) {
		// TODO Auto-generated method stub
		String result="success";
		database=this.getWritableDatabase();
		try
		{
			String sql1="insert or replace into "+TABLE_TEACHER_DIARY+"(diary_id ,from_id,to_id,message,sign_by_parent,sign_by_teacher,document,sentdate,status,teacher_name,student_name) values(?,?,?,?,?,?,?,?,?,?,?)";
			SQLiteStatement statement=database.compileStatement(sql1);
			database.beginTransaction();
			for(DiaryDetails l : listDiaryDetails)
			{ 
				statement.bindLong(1,l.getDairy_id());

				statement.bindLong(2, l.getFrom_id());
				statement.bindLong(3, l.getTo_id());
				statement.bindString(4, l.getMessage());
				statement.bindBlob(5, l.getSign_by_parent().getBytes());
				statement.bindBlob(6,l.getSign_by_teacher().getBytes() );
				statement.bindString(7, l.getDocuments());
				statement.bindString(8,l.getMsg_record_dt_time());
				statement.bindString(9, l.getStatus());
				statement.bindString(10, l.getStudentName());
				statement.bindString(11, l.getTeacherName());

				statement.execute();
			}
			database.setTransactionSuccessful();
			database.endTransaction();
		}catch(Exception e)
		{
			result="failure";
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}
		return result;

	}

	// GEt Last Time_stamp Of teacher Diary	
	public String getlast_diary(){
		database=this.getReadableDatabase();
		Cursor cursor=null;
		String result = "";
		try
		{
			String sql="select sentdate from "+TABLE_TEACHER_DIARY+" where id = "+"(select MAX(id) from" +TABLE_TEACHER_DIARY+")";
			cursor=database.rawQuery(sql, null);
			if (cursor.moveToNext()) {
				if(!cursor.isNull(0))
					result=cursor.getString(0);
				else
					result="";
			}

		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}
		return result;
	}


	//select all the TEacher_Diary detail from the table
	public ArrayList<DiaryDetails> show_Diary_Details()
	{
		ArrayList<DiaryDetails> list=new ArrayList<DiaryDetails>();
		try
		{
			String query="Select * from "+TABLE_TEACHER_DIARY;
			database=this.getReadableDatabase();
			Cursor cursor=database.rawQuery(query, null);
			while(cursor.moveToNext())
			{
				DiaryDetails l=new DiaryDetails();

				l.setDairy_id(cursor.getInt(1));
				l.setFrom_id(cursor.getInt(2));
				l.setTo_id(cursor.getInt(3));
				l.setMessage(cursor.getString(4));
				l.setSign_by_parent(cursor.getBlob(5).toString());
				l.setSign_by_teacher(cursor.getBlob(6).toString());					
				l.setDocuments(cursor.getString(7));
				l.setMsg_record_dt_time(cursor.getString(8));
				l.setStatus(cursor.getString(9));
				l.setStudentName(cursor.getString(10));
				l.setTeacherName(cursor.getString(11));


				list.add(l);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			database.close();
		}
		return list;
	}
	public void insert_news(String response) {
		// TODO Auto-generated method stub
		truncateTables(TABLE_NEWS);
		String result="success";
		database=this.getWritableDatabase();
		try
		{
			String sql1="insert into "+TABLE_NEWS+" (news) values (?)";
			SQLiteStatement statement=database.compileStatement(sql1);
			database.beginTransaction();
			APIController cntrl = APIController.getInstance();
			statement.bindString(1,cntrl.getNews());


			statement.execute();

			database.setTransactionSuccessful();
			database.endTransaction();
		}catch(Exception e)
		{
			result="failure";
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}


	}
	public String show_news() {
		String response =" ";

		try
		{
			String query="Select * from "+TABLE_NEWS;
			database=this.getReadableDatabase();
			Cursor cursor=database.rawQuery(query, null);
			APIController cntrl = APIController.getInstance();


			while(cursor.moveToNext())
			{

				response=cursor.getString(1);

			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			database.close();
		}
		return response;
	}
	public void insert_prodata(ArrayList<Child> childlist) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		if(Common.connectionStatus){
			truncateTables(TABLE_PRODATA);
		}
		String result="success";
		database=this.getWritableDatabase();
		try
		{

			String sql1="insert or replace into "+TABLE_PRODATA+"(name,stud_id) values(?,?)";
			SQLiteStatement statement=database.compileStatement(sql1);
			database.beginTransaction();
			for(Child s : childlist)
			{
				statement.bindString(1,s.getFullname());
				statement.bindLong(2,s.getStudentId());
				statement.execute();
			}
			database.setTransactionSuccessful();
			database.endTransaction();
		}catch(Exception e)
		{
			result="failure";
			e.printStackTrace();
		}
		finally
		{
			database.close();
		}

	}


	public ArrayList<Child> showprodata() {

		ArrayList<Child> list=new ArrayList<Child>();
		try
		{
			String query="Select * from "+TABLE_PRODATA;
			database=this.getReadableDatabase();
			Cursor cursor=database.rawQuery(query, null);
			APIController cntrl = APIController.getInstance();


			while(cursor.moveToNext())
			{
				Child l=new Child();
				l.setStudentId(cursor.getInt(2));
				l.setFullname(cursor.getString(1));

				list.add(l);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			database.close();
		}
		return list;
	}

	public long insertStudentPhoto(Bitmap bmp,String userid)
	{
		long c=0;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
		byte[] buffer=out.toByteArray();
		database=this.getWritableDatabase();
		database.beginTransaction();
		ContentValues values;
		try
		{
			values = new ContentValues();
			values.put("photo", buffer);
			values.put("userid",userid);
			c= database.replace(TABLE_USER, null, values);
			database.setTransactionSuccessful();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			database.endTransaction();
			database.close();
		}
		return c;
	}
}
