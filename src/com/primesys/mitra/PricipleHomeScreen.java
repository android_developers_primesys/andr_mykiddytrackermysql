package com.primesys.mitra;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.dto.MyKiddyTrackerContact;
import com.example.dto.UserContact;
import com.google.gson.Gson;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;
import com.nemezis.sample.contacts.AccountGeneral;
import com.nemezis.sample.contacts.ContactsManager;
import com.primesys.mitra.HomeActivity.AddUserContact;
import com.primesys.mitra.HomeActivity.GetUserContact;
import com.primesys.mitra.HomeActivity.PushContactServer;

import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class PricipleHomeScreen extends Activity {

	Button btnRecent,btnChoose,btnBulk;
	Context principleContext=PricipleHomeScreen.this;
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	Bitmap G_image;
	ByteArrayOutputStream stream;
	String TAG="DemoHomeActivity";
	String  UPLOAD_FILE_PATH="/sdcard/Download/images.jpg";
	public static String contactJson=null;
	Gson gson=new Gson();
	String contact;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principle_home);
		findViewByID();
		///##################################################################################
		//Add MyKiddytracker Account Here for contact Synchronization
		if (Common.getConnectivityStatus(principleContext)) {
			addNewAccount(AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS);
			try{
				if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
					new GetUserContact().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
				}
				else{
					new GetUserContact().execute();
				}		
			}catch(Exception ex){
				System.err.print(ex);

			}

		}
		
		//################################################################################


		//Implement Flipper
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);
		



		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		btnRecent.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(principleContext,RM_senderListActivity.class);
				intent.putExtra("to", "Principle");
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});

		//choose button click
		btnChoose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(principleContext,ChooseClass.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});

		//bulk message send to all 
		btnBulk.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(principleContext,ParentStudentInformation.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});

	}

	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			if (Common.getConnectivityStatus(principleContext)) {
				
			
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(principleContext)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(principleContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);}
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			if (Common.getConnectivityStatus(principleContext)) {
				
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(principleContext)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(principleContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);}
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_change_password, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.changepass).setIcon(new IconDrawable(this, IconValue.fa_pencil).colorRes(R.color.primary).actionBarSize());
		return super.onPrepareOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.changepass:
			Intent chIntent=new Intent(principleContext,ChanagePassword.class);
			startActivity(chIntent);
			return true;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);
        }
	//findView by id
	void findViewByID()
	{
		btnRecent=(Button)findViewById(R.id.image_recent);
		btnChoose=(Button)findViewById(R.id.image_choose);
		btnBulk=(Button)findViewById(R.id.image_bulk);
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		if (Common.getConnectivityStatus(principleContext)&& APIController.getInstance().getSchool_logo()!=null) {

		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		imageLoader = new ImageLoader(
				Volley.newRequestQueue(principleContext), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);}


	}
	
	private void addNewAccount(String accountType, String authTokenType) {
		final AccountManagerFuture<Bundle> future = AccountManager.get(this).addAccount(accountType, authTokenType, null, null, this, new AccountManagerCallback<Bundle>() {
			@Override
			public void run(AccountManagerFuture<Bundle> future) {
				try {
					Bundle bnd = future.getResult();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, null);
	}
	public class GetUserContact extends AsyncTask <Void, Void, String> {

		@Override
		protected void onPreExecute() {
		}
		@Override
		protected String doInBackground(Void... params) {
			String result = null;
			try
			{
				readContact();

			}catch(Exception e)
			{

				e.printStackTrace();
			}

			return result;
		}
	}
	private void readContact() {

		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
				null, null, null, null);
		ArrayList<UserContact> list=new ArrayList<UserContact>();
		//set to Contact Class
		UserContact  contact = null;

		if (cur.getCount() > 0) {

			while (cur.moveToNext()) {
				String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
					System.out.println("name : " + name + ", ID : " + id);


					// get the phone number
					Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
							new String[]{id}, null);
					while (pCur.moveToNext()) {
						String phone = pCur.getString(
								pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						System.out.println("phone" + phone);
						contact=new UserContact();
						contact.setName(name);
						contact.setPhoneno(phone);
					}
					pCur.close();


					// get email and <span id="IL_AD7" class="IL_AD">type</span>
					Cursor emailCur = cr.query(
							ContactsContract.CommonDataKinds.Email.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
							new String[]{id}, null);
					if (emailCur.getCount()>0) {
						while (emailCur.moveToNext()) {
							// This would allow you get several email addresses
							// if the email addresses were stored in an array
							String email = emailCur.getString(
									emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
							String emailType = emailCur.getString(
									emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));System.out.println("Email " + email + " Email Type : " + emailType);contact.setEmail(email);
						}
					}else {
						contact.setEmail("");
					}

					emailCur.close();
					list.add(contact);

				}
			}
		}


		//send Information to Server	
		contactJson=gson.toJson(list);

		try{
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				new PushContactServer().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			}
			else{
				new PushContactServer().execute();
			}		
		}catch(Exception ex){
			System.err.print(ex);

		}

	}
	//Contact verify from server
	class PushContactServer extends AsyncTask <Void, Void, String> {

		@Override
		protected void onPreExecute() {

		}
		@Override
		protected String doInBackground(Void... params) {
			String result="";

			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(Common.URL+"LoginServiceAPI.asmx/GetContactDetails");
			try
			{
				
				
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("ContactMobileNoString",contactJson));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	
				e.printStackTrace();
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			Log.e("PushContactServer", ""+result);
			ParseContactInformation(result);

		}
	}

	//Parsing Contact from Server
	public void ParseContactInformation(String result) {

		try{
			Log.e("MYkIddyTracker Contact###############################################", ""+result);

			ArrayList<MyKiddyTrackerContact> list=new ArrayList<MyKiddyTrackerContact>();

			JSONArray joarray=new JSONArray(result);
			for (int i = 0; i < joarray.length(); i++) {

				MyKiddyTrackerContact mykidd=new MyKiddyTrackerContact();
				JSONObject jobject=joarray.getJSONObject(i);
				mykidd.setEmailId(jobject.getString("EmailID"));
				mykidd.setName(jobject.getString("Name"));
				mykidd.setContactno(jobject.getString("ContactNo"));
				mykidd.setID(jobject.getString("Id"));

				/*	//adding  contact
						ContactsManager.addContact(context, mykidd);*/

				//updating  contact
				contact=jobject.getString("ContactNo");
				list.add(mykidd);
				/*	ContactsManager.updateMyContact(dHomeContext, mykidd);*/
			/*	if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
					new AddUserContact().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
				}
				else{
					new AddUserContact().execute();
				}*/	
			LoginActivity.mClient.sendMessage(addFriendForChat(jobject.getString("Name"), jobject.getString("Id")));

			}

			ContactsManager.addContact(principleContext, list);

		}catch(Exception ex)
		{
			Log.e("Exception ", ""+ex);
		}

	}
	class AddUserContact extends AsyncTask<Void, String, String>{
		String result="";
		HttpClient client=new DefaultHttpClient();
		HttpPost post=new HttpPost(Common.URL+"ParentAPI.asmx/SearchFriendList");

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}
		@Override
		protected String doInBackground(Void... params) {
			try{
				List<NameValuePair> list=new ArrayList<NameValuePair>(3);
				list.add(new BasicNameValuePair("UserId", Common.userid));
				list.add(new BasicNameValuePair("Entity", contact));
				list.add(new BasicNameValuePair("Flag", "1"));
				post.setEntity(new UrlEncodedFormEntity(list));
				HttpResponse response=client.execute(post);
				result=EntityUtils.toString(response.getEntity());

			}catch(Exception ex){
				result=ex.getMessage();	
			}


			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			parseJSONcontact(result);


		}
	}
	public void parseJSONcontact(String result) {
		try {
			JSONArray array=new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo=array.getJSONObject(i);
				LoginActivity.mClient.sendMessage(addFriendForChat(jo.getString("Name"), jo.getString("Id")));
			}
		} catch (Exception e) {
			Log.e("ParsonJSON", ""+e);
		}

	}


	//add Friend
	public static String addFriendForChat(String friendName,String friendId)
	{
		String addSTring="{}";
		try{

			System.out.print("##Sending DATA ###");
			JSONObject jo=new JSONObject();
			jo.put("event","add_friend");
			jo.put("user_id",Common.userid);
			jo.put("user_name",Common.username);
			jo.put("friend_id",friendId);
			jo.put("friend_name",friendName);
			addSTring=jo.toString();
		}
		catch(Exception e)
		{
			Log.e("addFriendForChat", ""+e);
		}
		return addSTring; 
	}
}
