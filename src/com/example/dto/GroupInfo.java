package com.example.dto;

import java.io.Serializable;

public class GroupInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	int id;
	String GroupName,ParticipantId,ParticipantName,Type;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGroupName() {
		return GroupName;
	}
	public void setGroupName(String groupName) {
		GroupName = groupName;
	}
	public String getParticipantId() {
		return ParticipantId;
	}
	public void setParticipantId(String participantId) {
		ParticipantId = participantId;
	}
	public String getParticipantName() {
		return ParticipantName;
	}
	public void setParticipantName(String participantName) {
		ParticipantName = participantName;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	
	
}
