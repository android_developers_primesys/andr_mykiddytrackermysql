
package com.example.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.primesys.mitra.Common;
import com.primesys.mitra.LoginActivity;
import com.primesys.mitra.MainPrincipal;
import com.primesys.mitra.R;

public class RecentMessagePrincipleAdapter extends BaseAdapter {
	private ArrayList<String> mListItems;
	private LayoutInflater mLayoutInflater;
	Context recentContext;
	String  MsgSender;
	 HashMap<String, String> sendername=LoginActivity.chk_principle;
	 HashMap<String, String> recv_time=LoginActivity.msgrecv_time;
	 HashMap<String, String> student_Id=LoginActivity.student_Id;
	public RecentMessagePrincipleAdapter(Context context, ArrayList<String> arrayList){
		mListItems = arrayList;
		recentContext=context;
		mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	@Override
	public int getCount() {
		//getCount() represents how many items are in the list
		return mListItems.size();
	}
	@Override
	//get the data of an item from a specific position
	//i represents the position of the item in the list
	public Object getItem(int i) {
		return null;
	}
	@Override
	//get the position id of the item from the list
	public long getItemId(int i) {
		return 0;
	}
	@Override
	public View getView(final int position, View view, ViewGroup viewGroup) {
		//check to see if the reused view is null or not, if is not null then reuse it
		if (view == null) {
			view = mLayoutInflater.inflate(R.layout.activity_recent, null);
		}
		//get the string item from the position "position" from array list to put it on the TextView
		Button itemName = (Button) view.findViewById(R.id.recent);
		TextView recvtime = (TextView) view.findViewById(R.id.time);
		String MsgSender = mListItems.get(position);
		
		//setting up sender name
		for(Map.Entry entry: sendername.entrySet()){
			if(entry.getKey().equals(MsgSender)){
				itemName.setText(entry.getValue().toString());
			}
		}   
		//for setting time in recent messages
		for(Map.Entry entry: recv_time.entrySet()){
			if(entry.getKey().equals(MsgSender)){
				recvtime.setText(Common.getDateCurrentTimeZone(Long.parseLong(entry.getValue().toString())));
			}

		}  
		itemName.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try{

					String MsgSender = mListItems.get(position); 
					Intent intent=new Intent(recentContext,MainPrincipal.class);
					intent.putExtra("to",MsgSender);
					for(Map.Entry entry: sendername.entrySet()){
						if(entry.getKey().equals(MsgSender)){
					       intent.putExtra("name",entry.getValue().toString());
					       intent.putExtra("student", student_Id.get(MsgSender));
						}
					}   
					recentContext.startActivity(intent);
					((Activity)recentContext).overridePendingTransition(R.anim.right_in, R.anim.left_out);
				}catch(Exception e)
				{
					Log.d("recentContext", ""+e);
				}
			}
		});

		return view;
	}
}
