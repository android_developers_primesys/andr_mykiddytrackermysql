package com.primesys.mitra;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Common {
	//public static String URL="http://www.mykidtracker.in:81/API/";
//	For Mysql 
	public static String URL="http://192.168.1.23:80/API/";
	public static String MONGOURL="http://192.168.1.103/TrackingApplication/API/";
	public static String SQLURL="http://192.168.1.110:8080/TrackingAppDB/TrackingAPP/";
	public static String TrackURL="http://192.168.1.110:8080/TrackingAppDB/TrackingAPP/";

	static public String relativeurl="http://www.mykidtracker.in:81";
	public static Boolean freindflag=false;
	//	static public String relativeurl="http://192.168.1.23:80/TrackingApplication";
	public static String roleid,email,markerImg,quiz_name;
	public static int question_count=0;

	public static String userid="mykiddytracker",username;
	protected static String status;
	public static String to="";
	public static int toid=R.drawable.ic_boy,Student_Count;
	public static String photo="";
	static int port=5555;     
	static String SERVERIP ="www.mykiddytracker.com";//"192.168.1.101";
	static int PORT = 5555;


	public static String currTrack;
	public static boolean connectionStatus=true;
	static String app_id="54c3627b3595081f8adbe2c8",classId,schoolId;
	public static String timstamp;
	static String EV_MSG_RECEIVED="msg_received";
	static String EV_CURR_LOC="current_location";
	static String EV_TOD_LOC="todays_loc";
	static String EV_DEVICE_STATUS="device_status";
	static String EV_GRP_CREAT="group_created";
	static String EV_ERROR="error";
	static String TrackUser;
	static String EV_GRPS_LIST="groups_listing";
	static String EV_MEM_LIST="group_members_listing";
	static String EV_FRND_SUCC="friend_added_successfully";
	static String EV_FRND_LIST="friend_list";
	static String EV_GRP_ADD="group_member_added";
	static String EV_MSG_ACK="msg_ack";
	public static String studID="",stuName="",teacher_name="";
	static int flag=0;
	public static String user_regType,user_regCity="";
	
	static String SCHOOLNAME="";
	static int Level_marks=0;
	//variable foe Quiz Metadata
	static int showanw_time_interval=0;
	static int    shownext_question_interval=0;
	static int    maximum_marks=0;
	static int   bonus_time_interval=0;
	static int  minimum_marks_question=0;
	static int  per_second_bonus_marks=0;
	public static int passing_marks=0;
	
	//convert date to timestamp
	public static long convertToLong(String date)
	{

		long time=0;
		try
		{
			Calendar cal=Calendar.getInstance();
			cal.setTimeZone(TimeZone.getTimeZone("GMT"));
			time=(int)((cal.getTimeInMillis()+cal.getTimeZone().getOffset(cal.getTimeInMillis()))/1000L);
		}catch(Exception e)
		{
			e.printStackTrace();
			time=Long.parseLong(date);
		}
		return time;
	}

	public static long getGMTTimeStampFromDate(String datetime) {
		long timeStamp = 0;
		Date localTime = new Date();

		String format = "dd-MM-yyyy hh:mm aa";
		SimpleDateFormat sdfLocalFormat = new SimpleDateFormat(format);
		sdfLocalFormat.setTimeZone(TimeZone.getDefault());

		try {

			localTime = (Date) sdfLocalFormat.parse(datetime); 

			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"),
					Locale.getDefault());
			@SuppressWarnings("unused")
			TimeZone tz = cal.getTimeZone();

			cal.setTime(localTime);

			timeStamp = (localTime.getTime()/1000L);
			Log.d("GMT TimeStamp: ", " Date TimegmtTime: " + datetime
					+ ", GMT TimeStamp : " + localTime.getTime());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return timeStamp;

	}
	public static String getDateCurrentTimeZone(long timestamp) {
		long dv = Long.valueOf(timestamp)*1000;
		Date df = new java.util.Date(dv);
		String vv = new SimpleDateFormat("dd-MMM-yy hh:mma").format(df);
		return vv;
	}

	public static String getTimeCurrentTimeZone(long timestamp) {
		long dv = Long.valueOf(timestamp)*1000;
		Date df = new java.util.Date(dv);
		String vv = new SimpleDateFormat("hh:mma").format(df);
		return vv;
	}
	// common to the show the toast
	public static void showToast(String message,Context context)
	{
		LayoutInflater inflate=((Activity)context).getLayoutInflater();
		View layout=inflate.inflate(R.layout.custom_toast, (ViewGroup)((Activity)context).findViewById(R.id.layout));
		TextView text = (TextView) layout.findViewById(R.id.text);
		text.setText(message);
		Toast toast=Toast.makeText(context, message, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setView(layout);
		toast.show();
	}




	public static boolean getConnectivityStatus(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (null != activeNetwork) {
			if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
				return true;

			if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
				return true;
		} 
		return false;
	}
	//convert to unicode
	static String getALLUnicodes(String msg)
	{
		StringBuilder s=new StringBuilder();
		for (int i = 0; i <msg.length(); i++) {
			s.append(String.format ("\\u%04x", (int)msg.charAt(i)));
		}
		return s.toString();
	}
	//genrate the random number
	public static long getRefNo()
	{
		return (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
	}
	//added by amit
			public static Bitmap bytebitmapconversion(String bitmap){
				Bitmap bmp=null;

				byte[] imgbytes = Base64.decode(bitmap, Base64.DEFAULT);
				bmp = BitmapFactory.decodeByteArray(imgbytes, 0,imgbytes.length);
				return bmp;


			}
}
