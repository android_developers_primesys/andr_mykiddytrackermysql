package com.example.dto;

import android.graphics.Bitmap;

public class CategoryDashbord {
	Bitmap image;
	String title;
	String quiz_id;
	
	public CategoryDashbord(Bitmap homeIcon, String string, String quizid) {
		
		this.image = homeIcon;
		this.title = string;
		this.quiz_id=quizid;
	
	}
	public String getQuiz_id() {
		return quiz_id;
	}
	public void setQuiz_id(String quiz_id) {
		this.quiz_id = quiz_id;
	}
	public Bitmap getImage() {
		return image;
	}
	public void setImage(Bitmap image) {
		this.image = image;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	

}
