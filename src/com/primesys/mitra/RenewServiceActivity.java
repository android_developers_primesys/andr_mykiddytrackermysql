package com.primesys.mitra;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.IconTextView;
import android.widget.RadioGroup;
import android.widget.TextView;

public class RenewServiceActivity extends Activity {

	Button btnRenew;
	Context renewContext=RenewServiceActivity.this;	
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();

	TextView text_name,text_username,text_date,text_status;
	RadioGroup ragpayment;
	IconTextView textprice_month,textprice_quarterly,textprice_half,textprice_yearly;
	ProgressDialog pDialog;
	String rupee=" {fa-inr}";
	double month,quat,half,year;
	// paypal code
	private static final String TAG = "paymentExample";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_renew);
		findViewByID();
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);
	
		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		if(Common.connectionStatus)
		{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				new PaymentInfoTask().executeOnExecutor(
						AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			} else {
				new PaymentInfoTask().execute();
			}
		}

	}
	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(renewContext)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(renewContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(renewContext)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(renewContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}


	}
	void findViewByID()
	{
		btnRenew=(Button)findViewById(R.id.btn_renew);

		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(renewContext)&& APIController.getInstance().getSchool_logo()!=null) {

		imageLoader = new ImageLoader(
				Volley.newRequestQueue(renewContext), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
		text_username=(TextView)findViewById(R.id.text_username);
		text_date=(TextView)findViewById(R.id.text_date);
		ragpayment=(RadioGroup)findViewById(R.id.rbt_services);
		textprice_month=(IconTextView)findViewById(R.id.textprice_month);
		textprice_quarterly=(IconTextView)findViewById(R.id.textprice_quarterly);
		textprice_half=(IconTextView)findViewById(R.id.textprice_half);
		textprice_yearly=(IconTextView)findViewById(R.id.textprice_yearly);

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}


	// make the request for taking the values from the DB
	class PaymentInfoTask extends AsyncTask<Void, Void, String>
	{
		String url=Common.URL+"ParentAPI.asmx/GetPaymentDetails";
		@Override
		protected void onPreExecute() {
			pDialog=new ProgressDialog(renewContext);
			pDialog.setMessage("Fetching Data...");
			pDialog.setCancelable(false);
			pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
			pDialog.show();
		}
		@Override
		protected String doInBackground(Void... params) {
			String result="Data Not Found";

			HttpClient httpclient=new DefaultHttpClient();
			HttpPost post=new HttpPost(url);
			try {
				List<NameValuePair> postParameter = new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("ParentId",Common.userid));
				post.setEntity(new UrlEncodedFormEntity(postParameter));

				HttpResponse response = httpclient.execute(post);
				result = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {
				result = e.getMessage();
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			if (result.contains("error")) {
				CustomDialog.displayDialog("Invalid User.Contact Admin",renewContext);
			} else 
				parseData(result);
		}
	}
	void parseData(String result)
	{
		try
		{
			JSONArray array=new JSONArray(result);
			JSONObject jo=array.getJSONObject(0);	 
			text_name.setText(jo.getString("Name"));
			text_username.setText(jo.getString("UserName"));
			month=Double.parseDouble(jo.getString("OneMonth"));
			textprice_month.setText(jo.getString("OneMonth")+rupee);
			quat=Double.parseDouble(jo.getString("Quarterly"));
			textprice_quarterly.setText(jo.getString("Quarterly")+rupee);
			half=Double.parseDouble(jo.getString("HalfYear"));
			textprice_half.setText(jo.getString("HalfYear")+rupee);
			year=Double.parseDouble(jo.getString("OneYear"));
			textprice_yearly.setText(jo.getString("OneYear")+rupee);
			System.out.println(jo.getString(jo.getString("expiry_date")));
			text_date.setText(jo.getString(jo.getString("expiry_date")));
			text_status.setText(checkDate(jo.getString("expiry_date")));
		}catch(Exception e)
		{
			System.err.print(e);
		}
	}
	// check the current date is greater than the expiray date
	String checkDate(String exDate)
	{
		String status="Activated";
		Date ex=new Date(exDate);
		Date currentDate=new Date();

		if(!(ex==max(ex, currentDate)))
			status="Expired";

		return status;
	}
	public  Date max(Date d1, Date d2) {
		if (d1 == null && d2 == null) return null;
		if (d1 == null) return d2;
		if (d2 == null) return d1;
		return (d1.after(d2)) ? d1 : d2;
	}
}
