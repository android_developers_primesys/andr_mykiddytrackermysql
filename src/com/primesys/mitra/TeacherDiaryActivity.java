package com.primesys.mitra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adapter.DiaryMessageAdapter;
import com.example.dto.DiaryDetails;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;

public class TeacherDiaryActivity extends Activity {

	ListView listDiary;
	String studentID;

	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	Context teacher_context=TeacherDiaryActivity.this;
	int showtime=15000;
	final Handler handler = new Handler();	Context context=TeacherDiaryActivity.this;
	final String TAG="TeacherActivity";
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	ArrayList<DiaryDetails> listDiaryDetails=new ArrayList<DiaryDetails>();
	DiaryMessageAdapter adapter;
    String type;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_diary_details);
		listDiary=(ListView)findViewById(R.id.list_diary);
		studentID=getIntent().getStringExtra("StudentID");
		textNews=(TextView)findViewById(R.id.text_news);
		FindViewBYID();
		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);



		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		type=getIntent().getStringExtra("Type");  
		
		if(Common.connectionStatus)
		{
			postDiaryRequest();
		}
	}
	@Override
	protected void onRestart() {
		super.onRestart();
		if(Common.connectionStatus)
		{
			listDiaryDetails.removeAll(listDiaryDetails);
			postDiaryRequest();
			if(adapter!=null)
			adapter.notifyDataSetChanged();
		}
	}
	
	
	
	
	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(teacher_context), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(teacher_context), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_dd, menu);
		return super.onCreateOptionsMenu(menu);

	}
	private void FindViewBYID() {
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);

		textschool.setText(APIController.getInstance().getSchool_name());
		if(Common.getConnectivityStatus(teacher_context))
		{ ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {

		imageLoader = new ImageLoader(
				Volley.newRequestQueue(teacher_context), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);}
		}

	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		menu.findItem(R.id.add).setIcon(new IconDrawable(this, IconValue.fa_pencil_square_o).colorRes(R.color.primary).actionBarSize());
		return super.onPrepareOptionsMenu(menu);
	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
			return true;
		case R.id.add:
			Intent intent=new Intent(TeacherDiaryActivity.this,AddDiaryActivity.class);
			intent.putExtra("StudentID",studentID);
			intent.putExtra("Type", type);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	// get the Diary details record from the DB
	// make volley request 
	void postDiaryRequest()
	{
		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.URL+"ParentAPI.asmx/GetDigitalDairy",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				System.out.println("Diary Get Repo---"+response);
				parseJSON(response);
				pDialog.hide();
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("UserId", studentID);
				params.put("Flag", "0");

				return params;
			}
		};
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, 0));
		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	void parseJSON(String result)
	{
		if(result.contains("true"))
		{
			DiaryDetails d=new DiaryDetails();
			d.setMessage("Data Not Found ");
			listDiaryDetails.add(d);
		}
		try
		{
			JSONArray array=new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo=array.getJSONObject(i);
				DiaryDetails d=new DiaryDetails();
				d.setId(jo.getInt("id"));
				d.setDairy_id(jo.getInt("dairy_id"));
				d.setFrom_id(jo.getInt("from_id"));
				d.setTo_id(jo.getInt("to_id"));
				d.setMessage(jo.getString("message"));
				d.setDocuments(jo.getString("documents"));
				d.setSign_by_parent(jo.getString("sign_by_parent"));
				d.setSign_by_teacher(jo.getString("sign_by_teacher"));
				d.setMsg_record_dt_time(jo.getString("msg_record_dt_time"));
				d.setStatus(jo.getString("status"));
				d.setStudentName(jo.getString("StudentName"));
				d.setTeacherName(jo.getString("TeacherName"));
				listDiaryDetails.add(d);
				
			}
			adapter=new DiaryMessageAdapter(context,listDiaryDetails,type,studentID);
			listDiary.setAdapter(adapter);
		}
		catch(Exception e)
		{
            System.err.println(e.getMessage());
		}
	}
}
