package com.example.dto;

import java.io.Serializable;

public class DiaryDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int id,dairy_id,from_id,to_id;
	String message;
	String documents;
	String sign_by_parent,sign_by_teacher;
	String msg_record_dt_time,status;
	String TeacherName,StudentName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDairy_id() {
		return dairy_id;
	}
	public void setDairy_id(int dairy_id) {
		this.dairy_id = dairy_id;
	}
	public int getFrom_id() {
		return from_id;
	}
	public void setFrom_id(int from_id) {
		this.from_id = from_id;
	}
	public int getTo_id() {
		return to_id;
	}
	public void setTo_id(int to_id) {
		this.to_id = to_id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDocuments() {
		return documents;
	}
	public void setDocuments(String documents) {
		this.documents = documents;
	}
	public String getSign_by_parent() {
		return sign_by_parent;
	}
	public void setSign_by_parent(String sign_by_parent) {
		this.sign_by_parent = sign_by_parent;
	}
	public String getSign_by_teacher() {
		return sign_by_teacher;
	}
	public void setSign_by_teacher(String sign_by_teacher) {
		this.sign_by_teacher = sign_by_teacher;
	}
	public String getMsg_record_dt_time() {
		return msg_record_dt_time;
	}
	public void setMsg_record_dt_time(String msg_record_dt_time) {
		this.msg_record_dt_time = msg_record_dt_time;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTeacherName() {
		return TeacherName;
	}
	public void setTeacherName(String teacherName) {
		TeacherName = teacherName;
	}
	public String getStudentName() {
		return StudentName;
	}
	public void setStudentName(String studentName) {
		StudentName = studentName;
	}
	
	
}
