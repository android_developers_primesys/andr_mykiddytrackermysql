package com.primesys.mitra;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.db.DBHelper;
import com.example.dto.MessageMain;
import com.example.dto.ParentStudent;
import com.primesys.emojilibrary.EmojiconEditText;
import com.primesys.emojilibrary.EmojiconGridView.OnEmojiconClickedListener;
import com.primesys.emojilibrary.EmojiconsPopup;
import com.primesys.emojilibrary.EmojiconsPopup.OnEmojiconBackspaceClickedListener;
import com.primesys.emojilibrary.EmojiconsPopup.OnSoftKeyboardOpenCloseListener;
import com.primesys.emojilibrary.emoji.Emojicon;

public class ParentStudentInformation extends Activity {
	ListView userList;
	ArrayList<ParentStudent> userArray = new ArrayList<ParentStudent>();
	ArrayList<String> messagesList = new ArrayList<String>();
	String SelectedClassID;
	public static Boolean selected = false;
	public static CheckBox selection;
	Button send;
	private ClientPrincipal mClient = LoginActivity.mClientPri;
	public static String message;
	EditText messagetext;
	Button SendMessage;

	ParentStudent ps = new ParentStudent();
	static Date date = new Date();
	public static String senderId = null;
	ArrayAdapter<String> adapter ;
	String Finalresult="";
	static String formattedDate = new SimpleDateFormat("dd MMM yyyy hh:mm:ss")
	.format(date);
	Context ctx=ParentStudentInformation.this;
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acvtivity_allparentuser);
		findViewById();
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);
		

		userList = (ListView) findViewById(R.id.list);



		final EmojiconEditText editText = (EmojiconEditText) findViewById(R.id.editText);
		ImageView send = (ImageView)findViewById(R.id.send_button);
		final View rootView = findViewById(R.id.root_view);
		final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);
		final EmojiconsPopup popup = new EmojiconsPopup(rootView,this);
		popup.setSizeForSoftKeyboard();
		if (Common.connectionStatus) {
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				new GetBroadcastMessage().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			}
			else{
				new GetBroadcastMessage().execute();
			}

		}


	handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		/*if (Common.connectionStatus) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				new GetBulkMessage().executeOnExecutor(
						AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			} else {
				new GetBulkMessage().execute();
			}
		}*/

		adapter = new ArrayAdapter<String>(
				getApplicationContext(), R.layout.simple_list,
				messagesList);
		userList.setAdapter(adapter);
		send.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				message = editText.getText().toString();
				boolean valid = true;
				if (message.equalsIgnoreCase("")) {
					editText.setError("Enter Message");
				}
				else{   
					/*if (userArray.size() == 0) {
						valid = false;
					}*/
					if (valid) {
						messagesList.add(message);
						/*for (ParentStudent al : userArray) {
							MessageMain m = new MessageMain();
							m.setMesageText(message);
							m.setDate_time(Common.convertToLong(formattedDate));
							m.setTo(al.getParentID());
							m.setFrom(Common.userid);
							m.setEvent("msg");
							m.setApp_id(Common.app_id);
							m.setType("b");
							if (message != null)
								if (mClient != null) {
									ClientPrincipal.principleReply(m);
								}
						}// refresh the list
						 */
						if (message != null)
							if (mClient != null) {
								LoginActivity.mClientPri.messageSend((getBulkMSG(message)));
							}
						if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
							new SaveBroadcastMessage().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{Common.getALLUnicodes(message)});
						}
						else{
							new SaveBroadcastMessage().execute(new String[]{Common.getALLUnicodes(message)});
						}
						editText.setText("");
						adapter.notifyDataSetChanged();
						Common.showToast("Message Sent.",ctx);
					} else {
						Common.showToast("Users Not Found", ctx);
					}
				}
			}

		});

		popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {
			@Override
			public void onEmojiconClicked(Emojicon emojicon) {
				editText.append(emojicon.getEmoji());
			}
		});
		//Set on backspace click listener
		popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {
			@Override
			public void onEmojiconBackspaceClicked(View v) {
				KeyEvent event = new KeyEvent(
						0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
				editText.dispatchKeyEvent(event);
			}
		});
		//If the emoji popup is dismissed, change emojiButton to smiley icon
		popup.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() {
				changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
			}
		});
		//If the text keyboard closes, also dismiss the emoji popup
		popup.setOnSoftKeyboardOpenCloseListener(new OnSoftKeyboardOpenCloseListener() {
			@Override
			public void onKeyboardOpen(int keyBoardHeight) {
			}
			@Override
			public void onKeyboardClose() {
				if(popup.isShowing())
					popup.dismiss();
			}
		});
		//On emoji clicked, add it to edittext
		popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {
			@Override
			public void onEmojiconClicked(Emojicon emojicon) {
				editText.append(emojicon.getEmoji());
			}
		});
		//On backspace clicked, emulate the KEYCODE_DEL key event
		popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {
			@Override
			public void onEmojiconBackspaceClicked(View v) {
				KeyEvent event = new KeyEvent(
						0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
				editText.dispatchKeyEvent(event);
			}
		});
		// To toggle between text keyboard and emoji keyboard keyboard(Popup)
		emojiButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//If popup is not showing => emoji keyboard is not visible, we need to show it

				if(!popup.isShowing()){
					//If keyboard is visible, simply show the emoji popup
					if(popup.isKeyBoardOpen()){
						popup.showAtBottom();
						changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
					}
					//else, open the text keyboard first and immediately after that show the emoji popup
					else{
						editText.setFocusableInTouchMode(true);
						editText.requestFocus();
						popup.showAtBottomPending();
						final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
						changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
					}
				}
				//If popup is showing, simply dismiss it to show the undelying text keyboard
				else{
					popup.dismiss();
				}
			}
		}); 
	}
	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(ctx)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(ctx), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(ctx)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(ctx), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}


	}
	private void findViewById() {
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(ctx)&& APIController.getInstance().getSchool_logo()!=null) {

		imageLoader = new ImageLoader(
				Volley.newRequestQueue(ctx), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
	}
	// bulk message event
	String getBulkMSG(String msg)
	{
		String result="";
		try
		{
			JSONObject jo=new JSONObject();
			jo.put("event", "bulk_msg");
			jo.put("app_id",Common.app_id);
			jo.put("from", Common.userid);
			jo.put("msg", msg);
			jo.put("type", "b");
			JSONObject data=new JSONObject();
			data.put("user_type", "3");
			jo.put("data", data);
			result=jo.toString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

	private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId){
		iconToBeChanged.setImageResource(drawableResourceId);
	}
	public class GetBulkMessage extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			String result = "";
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(Common.URL
					+ "PrincipleAPI.asmx/BullkMessage");
			try {
				List<NameValuePair> postParameter = new ArrayList<NameValuePair>(
						1);
				postParameter.add(new BasicNameValuePair("Principal",
						Common.userid));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {
				result = e.getMessage();

				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			parseJSON(result);
		}

		void parseJSON(String result) {
			try {
				JSONArray array = new JSONArray(result);
				for (int i = 0; i < array.length(); i++) {
					JSONObject jo = array.getJSONObject(i);
					ParentStudent ps = new ParentStudent();
					ps.setStudentNmae(jo.getString("studentNmae"));
					ps.setStudentID(jo.getString("StudentID"));
					ps.setGender(jo.getString("Gender"));
					ps.setParentID(jo.getString("ParentID"));
					ps.setPrim_EmailID(jo.getString("Prim_EmailID"));
					ps.setPrim_ContactNo(jo.getString("Prim_ContactNo"));
					userArray.add(ps);
				}

			} catch (JSONException e) {
				Log.d("ParsonJSON:", "" + e);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}

	// onclick menu item
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	// insert the message into the database
	void insertMessage(MessageMain m) {
		DBHelper helper = DBHelper.getInstance(getApplicationContext());
		helper.insertMessage(m);
	}

	void parseJsonResult(String result) {
		try {
			System.err.println("Bulk="+result);
			JSONArray array = new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo=array.getJSONObject(i);
				messagesList.add(jo.getString("message"));	
			}
			adapter.notifyDataSetChanged();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//asynctask for SaveBroadcastMessage  string date, int user_id, string message
	class SaveBroadcastMessage extends AsyncTask<String, Void, String>
	{
		@Override
		protected String doInBackground(String... params) {
			String result="";
			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(Common.URL+"PrincipleAPI.asmx/SaveBroadcastMessage");
			try
			{
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(3);
				postParameter.add(new BasicNameValuePair("date",formattedDate));
				postParameter.add(new BasicNameValuePair("user_id",Common.userid));
				postParameter.add(new BasicNameValuePair("message",params[0]));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	

				e.printStackTrace();
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			try
			{
				if(result!=null)
				{
					JSONObject jo=new JSONObject(result);
					Finalresult=jo.getString("error");
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	//asynctask for GetBroadcastMessage int user_id
	class GetBroadcastMessage extends AsyncTask<Void, Void, String>
	{
		@Override
		protected String doInBackground(Void... params) {
			String result="";
			HttpClient httpClient=new DefaultHttpClient();

			HttpPost httpPost=new HttpPost(Common.URL+"PrincipleAPI.asmx/GetBroadcastMessage");
			try
			{
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("user_id",Common.userid));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	

				e.printStackTrace();
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			parseJsonResult(result);
		}
	}
}