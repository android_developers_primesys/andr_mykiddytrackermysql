package com.example.dto;

import java.io.Serializable;

public class ClassList implements  Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String ClassID,ClassName,Section,CreatedBy,UpdatedDate;
	int Position;
	boolean status;
    String parentid[];
    String parentname[];
    String studentId[];
	
	public ClassList(String ClassID,String ClassName) {
		this.ClassID=ClassID;
		this.ClassName=ClassName;
	}

	public ClassList() {
	}
   
	
	public String[] getStudentId() {
		return studentId;
	}

	public void setStudentId(String[] studentId) {
		this.studentId = studentId;
	}

	public String[] getParentid() {
		return parentid;
	}

	public void setParentid(String[] parentid) {
		this.parentid = parentid;
	}

	public String[] getParentname() {
		return parentname;
	}

	public void setParentname(String[] parentname) {
		this.parentname = parentname;
	}

	public int getPosition() {
		return Position;
	}

	 
	public void setPosition(int position) {
		Position = position;
	}

	public String getClassID() {
		return ClassID;
	}

	public void setClassID(String classID) {
		ClassID = classID;
	}

	public String getClassName() {
		return ClassName;
	}

	public void setClassName(String className) {
		ClassName = className;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	



}
