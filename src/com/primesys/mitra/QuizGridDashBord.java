package com.primesys.mitra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adapter.QuizGridAdapter;



import com.example.db.DBHelper;
import com.example.dto.CategoryDashbord;
import com.example.dto.UserScoreDTO;
import com.google.gson.JsonObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class QuizGridDashBord extends Activity {

	private GridView gridView;
	private QuizGridAdapter QuizGridAdapter;
	private RequestQueue reuestQueue;
	private StringRequest stringRequest;
Context context=QuizGridDashBord.this;
CircleImageView boy_img;
DBHelper helper = DBHelper.getInstance(context);

private ArrayList<CategoryDashbord> gridArray=new  ArrayList<CategoryDashbord>();
private String TAG="Rquest";
UserScoreDTO  userinfo=new UserScoreDTO();
private TextView score;
private TextView rank;
private TextView name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quiz_grid_dash_bord);
		findviewById();
			try {
						
						if (Common.getConnectivityStatus(context)) {
							try {
							//	GetUsetData();
								GetCategory();

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			
			
		

		gridView = (GridView) findViewById(R.id.gridView1);

		if (Common.getConnectivityStatus(context)) {
		try {
			GetMetadta();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	
		
		
	}

	private void findviewById() {
		score=(TextView) findViewById(R.id.overScore);
		rank=(TextView) findViewById(R.id.rank);
		boy_img=(CircleImageView) findViewById(R.id.image_boy);
		name=(TextView) findViewById(R.id.labl_username);
	}

	private void GetUsetData() {
		String url="http://192.168.1.110:8080/MyKiddyTrackerAPI/MyKiddy/ExamAPI/UsersOverallScoreDetails";

		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.MONGOURL+"ExamAPI/UsersOverallScoreDetails",new Response.Listener<String>() {
			
			@Override
			public void onResponse(String response) {
			

				parseJSONUserdata(response);
				pDialog.hide();
				
				
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
			
				params.put("UserId",Common.userid);
				System.out.println("REq----------"+params);
				return params;
			}

		};


		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}

	protected void parseJSONUserdata(String response) {
		

		System.out.println("Respo userdartta "+ response);
		try {
			JSONObject jo=new JSONObject(response);
		//	CategoryDashbord cat=new CategoryDashbord();
			//cat.setImage();
			
			userinfo.setName(jo.getString("name"));
			Common.quiz_name=jo.getString("name");
			userinfo.setPro_image(jo.getString("pro_image"));
			userinfo.setRank(jo.getString("rank"));
			userinfo.setScore(jo.getString("score"));
			
			setuserdata();
			
			
		
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				GetCategory();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
	
		
	
	
	
	}

	private void setuserdata() {
		
		Bitmap bmp=DBHelper.getInstance(context).getBitMap(Common.userid);
		if(bmp!=null)
			boy_img.setImageBitmap(bmp);
		else {

			try {
				DBHelper.getInstance(context).insertStudentPhoto(Common.bytebitmapconversion(userinfo.getPro_image()),Common.userid);
				bmp=DBHelper.getInstance(context).getBitMap(Common.userid);
				boy_img.setImageBitmap(bmp);

			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		
		name.setText(userinfo.getName());
		score.setText(userinfo.getScore());
		rank.setText(userinfo.getRank());		
	}

	private void GetCategory() {
		//String url="http://192.168.1.110:8080/MyKiddyTrackerAPI/MyKiddy/ExamAPI/CategoryDetails";

		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.SQLURL+"SQLQuiz/GetCategories",new Response.Listener<String>() {
			
			@Override
			public void onResponse(String response) {
			

				parseJSON(response);
				pDialog.hide();
				
				
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
			
				params.put("UserId",Common.userid);
				params.put("School_id",Common.schoolId);
				System.out.println("REq----------"+params);
				return params;
			}

		};


		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}

	protected void parseJSON(String response) {
		System.out.println("Respo "+ response);
		try {
		
			JSONArray category=new JSONArray(response);
			for (int i = 0; i < category.length(); i++) {
				JSONObject jo=category.getJSONObject(i);
			
				CategoryDashbord cat=new CategoryDashbord(Common.bytebitmapconversion(jo.getString("Category_Image")),jo.getString("Category"),jo.getString("Quiz_Id"));
			gridArray.add(cat);
		
			}
			
			QuizGridAdapter = new QuizGridAdapter(this, R.layout.category_row,gridArray);
			gridView.setAdapter(QuizGridAdapter);
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
		}
		
	}

	
	
	// make volley request 
			void GetMetadta()
			{
				String url="http://192.168.1.110:8080/MyKiddyTrackerAPI/MyKiddy/ExamAPI/QuizMetadata";

				reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
				final ProgressDialog pDialog = new ProgressDialog(context);
				pDialog.setTitle("Progress wait.......");
				pDialog.setCancelable(false);
				pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
				pDialog.show();
				//JSon object request for reading the json data
				stringRequest = new StringRequest(Method.POST,Common.SQLURL+"SQLQuiz/GetMetaData",new Response.Listener<String>() {
					
					@Override
					public void onResponse(String response) {
					
						System.out.println("Respo Meta"+response);

						parseJSONMeta(response);
						pDialog.hide();
					}

				},
				new Response.ErrorListener() {


					@Override
					public void onErrorResponse(VolleyError error) {
						pDialog.hide();
						if(error.networkResponse != null && error.networkResponse.data != null){
							VolleyError er = new VolleyError(new String(error.networkResponse.data));
							error = er;
							System.out.println(error.toString());
							parseJSONMeta(new String(error.networkResponse.data));
						}
					}
				}) {

					@Override
					protected Map<String, String> getParams() {
						Map<String, String> params = new HashMap<String, String>();
					
						params.put("school_id",Common.schoolId);
						
						System.out.println("REq----------"+params);
						return params;
					}

				};


				stringRequest.setTag(TAG);
				// Adding request to request queue
				reuestQueue.add(stringRequest);
			}
			


			public void parseJSONMeta(String result) {
				
				try {
					JSONObject jo=new JSONObject(result);
					Common.showanw_time_interval=jo.getInt("showanw_time_interval");
					Common.shownext_question_interval=jo.getInt("shownext_question_interval");;
					Common.maximum_marks=jo.getInt("maximum_marks");;
					Common.bonus_time_interval=jo.getInt("bonus_time_interval");;
					Common.minimum_marks_question=jo.getInt("minimum_marks_question");
					Common.per_second_bonus_marks=jo.getInt("per_second_bonus_marks");
					Common.passing_marks=jo.getInt("passing_marks");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.quiz_grid_dash_bord, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
