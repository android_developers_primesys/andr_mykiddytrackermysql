package com.primesys.mitra;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Base64;
import android.util.Log;



public class FileUploadServer {
	FileInputStream fileInputStream;
	String UploadFile;
	File Uploadfilepath;
	String Upload_filename;
	String studentId,ParentId;
	String testFile_path="";
	String testFile_name="";
	byte[] bFileConversion; 
	public static String Uploadresponse;
	public void FileToByteArrayConversion(String studentId,String docPath, String filename) throws IOException
	{
		System.out.println(studentId+"  "+docPath+"  "+filename);
		this.studentId=studentId;
		Upload_filename=filename;
		Uploadfilepath = new File(docPath);
		bFileConversion = new byte[(int) Uploadfilepath.length()];
		//convert file into array of bytes
		fileInputStream = new FileInputStream(Uploadfilepath);
		fileInputStream.read(bFileConversion);
		UploadFile=Base64.encodeToString(bFileConversion, Base64.DEFAULT);

		if (null!=UploadFile) {
			if (Common.connectionStatus) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					new FileUploadAsync().executeOnExecutor(
							AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
				} else {
					new FileUploadAsync().execute();
				}
			}

			Log.d("FileUploadServer", "3");
		}
	}

	//for Parent-Photo  Upload
	public void FileToByteArrayConversionForParent(String ParentId,String docPath, String filename) throws IOException
	{

		this.ParentId=ParentId;
		Upload_filename=filename;
		Uploadfilepath = new File(docPath);
		bFileConversion = new byte[(int) Uploadfilepath.length()];
		//convert file into array of bytes
		fileInputStream = new FileInputStream(Uploadfilepath);
		fileInputStream.read(bFileConversion);
		UploadFile=Base64.encodeToString(bFileConversion, Base64.DEFAULT);

		if (null!=UploadFile) {
			if (Common.connectionStatus) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					new ParentPhotoUpload().executeOnExecutor(
							AsyncTask.THREAD_POOL_EXECUTOR, null);
				} else {
					new ParentPhotoUpload().execute();
				}
			}

			Log.e("ParentId", ParentId);
			Log.e("FileName", Upload_filename);
			Log.e("ByteArray", UploadFile);

		}
	}
	public class FileUploadAsync  extends AsyncTask<Void, Void, String>
	{
		String result="Data Not Found";
		String url=Common.URL + "ParentAPI.asmx/UploadPhoto";

		@Override
		protected void onPreExecute() {

			super.onPreExecute();

		}
		@Override
		protected String doInBackground(Void... params) {
			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httppost=new HttpPost(url); 

			try {
				List<NameValuePair> postparam=new ArrayList<NameValuePair>(3);
				postparam.add(new BasicNameValuePair("StudentID", studentId));
				postparam.add(new BasicNameValuePair("ImageName",Upload_filename));
				postparam.add(new BasicNameValuePair("Photo",UploadFile));
				httppost.setEntity(new UrlEncodedFormEntity(postparam));
				HttpResponse response=httpClient.execute(httppost);
				result=EntityUtils.toString(response.getEntity());
			} catch (Exception e) {

				result=e.getMessage();
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			System.err.println(result);
		}		
	}
	public class ParentPhotoUpload extends AsyncTask<Void, Void, String>{
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(Void... params) {
			String result="";
			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(Common.URL+"ParentAPI.asmx/UploadParentPhoto");
			try{

				List<NameValuePair> parame=new ArrayList<NameValuePair>(3);
				parame.add(new BasicNameValuePair("UserID", ParentId));
				parame.add(new BasicNameValuePair("Photo", UploadFile));
				parame.add(new BasicNameValuePair("ImageName", Upload_filename));
				httpPost.setEntity(new UrlEncodedFormEntity(parame));
				HttpResponse response=httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception ex)
			{
				Log.e("ParentphotoUpload", ""+ex);
			}

			return result;
		}


		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			System.err.println("88888888888889"+result);
			Uploadresponse=result;
		}
	}
}
