package com.example.dto;

public class TeacherPrincipal {
String Teacher_Id,Teacher_name,Principal_Id,Principal_name;

public TeacherPrincipal(String Teacher_Id,String Teacher_name,String Principal_Id,String Principal_name) {
	this.Teacher_Id=Teacher_Id;
	this.Principal_name=Teacher_name;
	this.Principal_Id=Principal_Id;
	this.Principal_name=Principal_name;
}

public String getTeacher_Id() {
	return Teacher_Id;
}

public void setTeacher_Id(String teacher_Id) {
	Teacher_Id = teacher_Id;
}

public String getTeacher_name() {
	return Teacher_name;
}

public void setTeacher_name(String teacher_name) {
	Teacher_name = teacher_name;
}

public String getPrincipal_Id() {
	return Principal_Id;
}

public void setPrincipal_Id(String principal_Id) {
	Principal_Id = principal_Id;
}

public String getPrincipal_name() {
	return Principal_name;
}

public void setPrincipal_name(String principal_name) {
	Principal_name = principal_name;
}


}
