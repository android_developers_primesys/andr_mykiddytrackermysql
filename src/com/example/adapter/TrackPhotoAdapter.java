package com.example.adapter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.Volley;
import com.example.db.DBHelper;
import com.example.dto.Child;
import com.example.dto.TrackCordinates;
import com.primesys.mitra.CircularNetworkImageView;
import com.primesys.mitra.Common;
import com.primesys.mitra.LruBitmapCache;
import com.primesys.mitra.R;
import com.primesys.mitra.ShowGMap;
import com.primesys.mitra.ShowGmapClient;

public class TrackPhotoAdapter  extends ArrayAdapter<Child> {

	Context context;
	ArrayList<Child> data;
	ImageLoader imageLoader;
	final static int RESULT_LOAD_IMAGE=100;
	String picturePath,filename;
	Bitmap bitmap;
	String DeviceID;
	int pos;
	List<TrackCordinates> cordinates = new ArrayList<TrackCordinates>();
	Child child = new Child();
	TrackCordinates Tc = new TrackCordinates();
	Bitmap G_image;
	public static String lang, log;
	ImageLoader.ImageCache imageCache = new LruBitmapCache();

	public TrackPhotoAdapter(Context context, int layoutResourceId,
			ArrayList<Child> data) {
		super(context, layoutResourceId, data);
		this.context = context;
		this.data=data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		try
		{
			final Child s=data.get(position); 
			if(convertView==null)
			{
				LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView=inflater.inflate(R.layout.list_photo, parent,false);
			}
			convertView.findViewById(R.id.image_edit).setVisibility(View.GONE);
			TextView text_name=(TextView)convertView.findViewById(R.id.text_name);
			text_name.setText(s.getFirstname()+" "+s.getMiddlename()+" "+s.getLastname());
			final CircularNetworkImageView	pro_pic=(CircularNetworkImageView)convertView.findViewById(R.id.pro_pic);
			pro_pic.setTag(position);
			bitmap=DBHelper.getInstance(context).getBitMap(s.getStudentId()+"");


			if(bitmap!=null)
				pro_pic.setImageBitmap(bitmap);
			else{
				try {
					ImageLoader.ImageCache imageCache = new LruBitmapCache();
					imageLoader = new ImageLoader(Volley.newRequestQueue(context), imageCache);
					(pro_pic).setImageUrl(Common.relativeurl+ s.getPhoto(), imageLoader);
					/*	((CircularNetworkImageView) pro_pic)
					.setDefaultImageResId(R.drawable.student);*/
					((CircularNetworkImageView) pro_pic)
					.setErrorImageResId(R.drawable.student);
					imageLoader.get(Common.relativeurl+ s.getPhoto(), new ImageLoader.ImageListener() {
						public void onErrorResponse(VolleyError arg0) {
							(pro_pic).setImageResource(R.drawable.student);
						}
						public void onResponse(ImageContainer response, boolean arg1) {
							if (response.getBitmap() != null) {
								DBHelper.getInstance(context).insertPhoto(response.getBitmap(),s.getStudentId()+"");
							} 
						}
					});

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			pro_pic.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try{
						pos=(Integer)v.getTag();
						Intent intent = new Intent(context, ShowGmapClient.class);
						BitmapDrawable bitmap_draw=(BitmapDrawable)pro_pic.getDrawable();
						G_image=bitmap_draw.getBitmap();
						Bitmap bmp = null;
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						G_image.compress(Bitmap.CompressFormat.PNG, 100, stream);
						byte[] byteArray = stream.toByteArray();
						
						intent.putExtra("G_image", byteArray);
						intent.putExtra("deviceId",data.get(pos).getDeviceid());
						intent.putExtra("StudentId",data.get(pos).getStudentId());
						((Activity)context).startActivity(intent);
					}catch(Exception ex)
					{
						Log.e("Exception ", ""+ex);
					}
				}
			});
		}catch(Exception e)
		{
			e.printStackTrace();
		}

		return convertView;
	}
}
