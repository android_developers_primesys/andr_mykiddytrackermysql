package com.primesys.mitra;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.dto.Child;
import com.example.dto.MyKiddyTrackerContact;
import com.example.dto.UserContact;
import com.google.gson.Gson;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;
import com.nemezis.sample.contacts.AccountGeneral;
import com.nemezis.sample.contacts.ContactsManager;
import com.primesys.mitra.DemoHomeActivity.AddUserContact;
import com.primesys.mitra.DemoHomeActivity.GetUserContact;
import com.primesys.mitra.DemoHomeActivity.PushContactServer;

public class HomeActivity extends Activity  {

	Button btnTrack,btnCommunication,btnCall,btnMail,btnDiary,image_frndchat;
	static Context homeContext;
	static List<Child> childlist=new ArrayList<Child>();
	ProgressDialog pDialog;
	String GetMob_StudentID,Mobile_no;
	static String StudentID;
	Child child=new Child();
	TextView textNews,lbltext;
	TextView textschool;
	Context context=HomeActivity.this;;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();

	RelativeLayout deviceOne,deviceTwo;
	static TextView device_data1,device_data2;
	static Button btnonoff1,btnonoff2,btn_battry1,btn_battry2,signal1,signal2;
	MenuItem username;
	Bitmap G_image;
	ByteArrayOutputStream stream;
	String TAG="DemoHomeActivity";
	String  UPLOAD_FILE_PATH="/sdcard/Download/images.jpg";
	public static String contactJson=null;
	Gson gson=new Gson();
	String contact;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_test);
		homeContext=HomeActivity.this;
		//find view by id
		findViewByID();
		///##################################################################################
				//Add MyKiddytracker Account Here for contact Synchronization
				if (Common.getConnectivityStatus(homeContext)) {
					addNewAccount(AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS);
					try{
						if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
							new GetUserContact().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
						}
						else{
							new GetUserContact().execute();
						}		
					}catch(Exception ex){
						System.err.print(ex);

					}

				}
				
				//################################################################################



		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);




		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);

			if(Common.Student_Count==1)
		{
			deviceTwo.setVisibility(View.GONE);
		}

		//GPS Enale Alert
		GPSEnableSetting gps=new GPSEnableSetting();
		gps.GPSDialog(homeContext);
		if(Common.connectionStatus)  
		{
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				new Parenttask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			}
			else{
				new Parenttask().execute();
			}
		}


		//track button onclick
		btnTrack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(homeContext,TrackNewActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});
		//call button on click
		btnCall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (Mobile_no!=null) {
					try{
						Intent callIntent = new Intent(Intent.ACTION_DIAL);
						callIntent.setData(Uri.parse("tel:"+Mobile_no+""));
						startActivity(callIntent);
						//overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
					}catch(Exception e)
					{
						e.printStackTrace();
					}

				}else{
					Common.showToast("Phone No. Not Found", homeContext);
				}
			}
		});
		//email button on click

		btnMail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intentemail=new Intent(homeContext,SendMailActivity.class);
				startActivity(intentemail);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});
		//commmunication
		btnCommunication.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(Common.connectionStatus)  
				{
					if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
						new Parenttask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
					}
					else{
						new Parenttask().execute();
					}
				}
				Intent intentcom=new Intent(homeContext,CommunicationNew.class);
				startActivity(intentcom);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});
		// digital diary communication
		btnDiary.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(homeContext,DigitalDiaryActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});

		image_frndchat.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(homeContext,DemoUserActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
			}
		});
	}

	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {

				imageLoader = new ImageLoader(
						Volley.newRequestQueue(context), imageCache);
				(img_school).setImageUrl(
						Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);}
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);

			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);

			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {
				ImageLoader.ImageCache imageCache = new LruBitmapCache();
				imageLoader = new ImageLoader(
						Volley.newRequestQueue(context), imageCache);
				(img_school).setImageUrl(
						Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);}
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);

			Updatestatus=0;
			showtime=6000;
		}

	}

	//find view by id
	void findViewByID()
	{
		try{
			btnTrack=(Button)findViewById(R.id.image_track);
			btnCommunication=(Button)findViewById(R.id.image_com);
			btnCall=(Button)findViewById(R.id.image_call);
			btnMail=(Button)findViewById(R.id.image_email);
			textNews=(TextView)findViewById(R.id.text_news);
			textNews.setText(APIController.getInstance().getNews());
			lbltext=(TextView)findViewById(R.id.lbltext);
				textschool=(TextView)findViewById(R.id.txt_school);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();

			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {				imageLoader = new ImageLoader(
						Volley.newRequestQueue(context), imageCache);
				(img_school).setImageUrl(
						Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			}

			btnDiary=(Button)findViewById(R.id.image_diary);
			image_frndchat=(Button)findViewById(R.id.image_frndchat);
			deviceOne=(RelativeLayout)findViewById(R.id.device_one);
			deviceTwo=(RelativeLayout)findViewById(R.id.device_two);



			device_data1=(TextView)findViewById(R.id.device_data1);
			device_data2=(TextView)findViewById(R.id.device_data2);
			signal1=(Button)findViewById(R.id.signal1);
			signal2=(Button)findViewById(R.id.signal2);
			btnonoff1=(Button)findViewById(R.id.btnonoff1);
			btnonoff2=(Button)findViewById(R.id.btnonoff2);
			btn_battry2=(Button)findViewById(R.id.btn_battry2);
			btn_battry1=(Button)findViewById(R.id.btn_battry1);
			btn_battry1.setVisibility(View.GONE);
			btn_battry2.setVisibility(View.GONE);
			btnonoff1.setVisibility(View.GONE);
			btnonoff2.setVisibility(View.GONE);	
			signal1.setVisibility(View.GONE);
			signal2.setVisibility(View.GONE);
			device_data1.setVisibility(View.GONE);
			device_data2.setVisibility(View.GONE);
		}catch(Exception e){

		}

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);
        }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_logout, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		username=menu.findItem(R.id.username);
		username.setTitle(Common.username);
		username.setIcon(new IconDrawable(this, IconValue.fa_user).colorRes(R.color.primary).actionBarSize());
		menu.findItem(R.id.share).setIcon(new IconDrawable(this, IconValue.fa_share_alt).colorRes(R.color.primary).actionBarSize());
		menu.findItem(R.id.add).setIcon(new IconDrawable(this, IconValue.fa_map_marker).colorRes(R.color.primary).actionBarSize());
		menu.findItem(R.id.renew).setIcon(new IconDrawable(this, IconValue.fa_refresh).colorRes(R.color.primary).actionBarSize());
		menu.findItem(R.id.changepass).setIcon(new IconDrawable(this, IconValue.fa_pencil).colorRes(R.color.primary).actionBarSize());
		return super.onPrepareOptionsMenu(menu);
	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {	
		case R.id.username:
			Intent intentProfile=new Intent(homeContext,UserProfileActivity.class);
			startActivity(intentProfile);
			/*Common.showToast(getResources().getString(R.string.welcome)+Common.username, homeContext);*/
			return true;
		case R.id.add:
			Intent intent =new Intent(homeContext,AddChildActivity.class);
			startActivity(intent);
			return true;
		case R.id.share:
			shareIt();
			return true;
		case R.id.renew:
			Intent reIntent=new Intent(homeContext,RenewServiceActivity.class);
			startActivity(reIntent);
			return true;
		case R.id.changepass:
			if (!Common.user_regType.equals("Google")||Common.user_regType.equals("Facebook")) {
				Intent chIntent=new Intent(HomeActivity.this,ChanagePassword.class);
				startActivity(chIntent);
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	//READ  the child information from the service
	class Parenttask extends AsyncTask<Void, Void, String>
	{
		String url=Common.URL+"ParentAPI.asmx/GetChildInfo";
		@Override
		protected void onPreExecute() {
			pDialog=new ProgressDialog(homeContext);
			pDialog.setMessage("Progress wait....");
			pDialog.setCancelable(false);
			pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String result="User is not registered";

			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(url);
			try
			{
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("ParentId",Common.userid));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));

				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	
			}

			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss(); 
			if(result.contains("error"))
			{
				/*CustomDialog.displayDialog("No Child",homeContext);*/
			}
			else{
				parseJSON(result);
			}
		}
	}
	//parse the Result which is coming from the JSon
	void parseJSON(String result)
	{
		try {
			JSONArray array=new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo=array.getJSONObject(i);
				child.setStudentId(jo.getInt("StudentID"));
				child.setFirstname(jo.getString("FirstName"));
				child.setMiddlename(jo.getString("MiddleName"));
				child.setLastname(jo.getString("LastName"));
				child.setGender(jo.getString("Gender"));
				child.setAdmissiondate(jo.getString("AdmissionDate"));
				child.setClassid(jo.getInt("ClassID"));
				child.setDeviceid(jo.getString("DeviceID"));
				child.setDob(jo.getString("dob"));
				child.setPrim_ContactNo(jo.getString("Prim_ContactNo"));
				child.setPrim_EmailID(jo.getString("Prim_EmailID"));
				child.setAddress(jo.getString("Address"));
				child.setCity(jo.getString("City"));
				child.setState(jo.getString("State"));
				child.setPhoto((jo.getString("Photo")));
				child.setActiveStatus(jo.getString("ActiveStatus"));
				child.setCreatedBy(jo.getString("CreatedBy"));
				child.setUpdatedDate(jo.getString("UpdatedDate"));
				childlist.add(child);
				GetMob_StudentID=""+child.getStudentId();
				//to get mobile no
				if(Common.connectionStatus)  
				{
					if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
						new GetMobMail().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
					}
					else{
						new GetMobMail().execute();
					}
				}

			}


		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	//to get school Mobile Number
	class GetMobMail extends AsyncTask<Void, Void, String>
	{
		String url=Common.URL+"ParentAPI.asmx/GetMobMail";
		@Override
		protected String doInBackground(Void... params) {
			String result="User is not registered";

			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(url);
			try
			{
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("StudentID",GetMob_StudentID));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));

				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());

			}catch(Exception e)
			{
				result=e.getMessage();	
			}

			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			if(result.contains("error"))
			{
				/*CustomDialog.displayDialog("No Child",homeContext);*/
			}
			else{
				parseJSONMobile(result);
			}
		}
	}
	//parse the Result which is coming from the JSon
	void parseJSONMobile(String result)
	{
		try {
			JSONArray array=new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo=array.getJSONObject(i);
				Mobile_no=jo.getString("phone_no");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	// invite friends logic goes here
	private void shareIt() {
		//sharing implementation here
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "MyKiddyTrack");
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.sharemessaage));
		startActivity(sharingIntent);
	}
	// set the data to device
	static void setDeviceData(String result)
	{
		try
		{
			JSONObject jo=new JSONObject(result);
			JSONObject jData=jo.getJSONObject("data");
			JSONObject jTerminal=jData.getJSONObject("terminal_info");
			device_data1.setText(jo.getString("student_id"));

			// set the battry data
			switch (jData.getInt("voltage_level")) {
			case 0:
			case 1:
				btn_battry1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.pb_empty));
				break;
			case 2:
				btn_battry1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.pb_tfpb));
				break;
			case 3:
				btn_battry1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.pb_fpb));
				break;
			case 4:
				btn_battry1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.pb_sfb));
				break;
			case 5:
				btn_battry1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.pb_full));
				break;

			default:
				break;
			}
			// set the gsm signal
			// set the battry data
			switch (jData.getInt("gsm_signal_strength")) {
			case 0:
				signal1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.pb_hempty));
				break;
			case 1:
				signal1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.pb_tfp));
				break;
			case 2:
				signal1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.pb_fp));
				break;
			case 3:
				signal1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.pb_sfp));
				break;
			case 4:
				signal1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.pb_hfull));
				break;	
			default:
				break;
			}
			if(jTerminal.getInt("status")==1)
				btnonoff1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.ic_green));
			else
				btnonoff1.setBackgroundDrawable(homeContext.getResources().getDrawable(R.drawable.ic_red));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/////////////****************************************
	private void addNewAccount(String accountType, String authTokenType) {
		final AccountManagerFuture<Bundle> future = AccountManager.get(this).addAccount(accountType, authTokenType, null, null, this, new AccountManagerCallback<Bundle>() {
			@Override
			public void run(AccountManagerFuture<Bundle> future) {
				try {
					Bundle bnd = future.getResult();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, null);
	}
	public class GetUserContact extends AsyncTask <Void, Void, String> {

		@Override
		protected void onPreExecute() {
		}
		@Override
		protected String doInBackground(Void... params) {
			String result = null;
			try
			{
				readContact();

			}catch(Exception e)
			{

				e.printStackTrace();
			}

			return result;
		}
	}
	private void readContact() {

		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
				null, null, null, null);
		ArrayList<UserContact> list=new ArrayList<UserContact>();
		//set to Contact Class
		UserContact  contact = null;

		if (cur.getCount() > 0) {

			while (cur.moveToNext()) {
				String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
					System.out.println("name : " + name + ", ID : " + id);


					// get the phone number
					Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
							new String[]{id}, null);
					while (pCur.moveToNext()) {
						String phone = pCur.getString(
								pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						System.out.println("phone" + phone);
						contact=new UserContact();
						contact.setName(name);
						contact.setPhoneno(phone);
					}
					pCur.close();


					// get email and <span id="IL_AD7" class="IL_AD">type</span>
					Cursor emailCur = cr.query(
							ContactsContract.CommonDataKinds.Email.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
							new String[]{id}, null);
					if (emailCur.getCount()>0) {
						while (emailCur.moveToNext()) {
							// This would allow you get several email addresses
							// if the email addresses were stored in an array
							String email = emailCur.getString(
									emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
							String emailType = emailCur.getString(
									emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));System.out.println("Email " + email + " Email Type : " + emailType);contact.setEmail(email);
						}
					}else {
						contact.setEmail("");
					}

					emailCur.close();
					list.add(contact);

				}
			}
		}


		//send Information to Server	
		contactJson=gson.toJson(list);

		try{
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				new PushContactServer().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			}
			else{
				new PushContactServer().execute();
			}		
		}catch(Exception ex){
			System.err.print(ex);

		}

	}
	//Contact verify from server
	class PushContactServer extends AsyncTask <Void, Void, String> {

		@Override
		protected void onPreExecute() {

		}
		@Override
		protected String doInBackground(Void... params) {
			String result="";

			HttpClient httpClient=new DefaultHttpClient();
			HttpPost httpPost=new HttpPost(Common.URL+"LoginServiceAPI.asmx/GetContactDetails");
			try
			{
				
				
				List<NameValuePair> postParameter=new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("ContactMobileNoString",contactJson));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result=EntityUtils.toString(response.getEntity());
			}catch(Exception e)
			{
				result=e.getMessage();	
				e.printStackTrace();
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			Log.e("PushContactServer", ""+result);
			ParseContactInformation(result);

		}
	}

	//Parsing Contact from Server
	public void ParseContactInformation(String result) {

		try{
			Log.e("MYkIddyTracker Contact###############################################", ""+result);

			ArrayList<MyKiddyTrackerContact> list=new ArrayList<MyKiddyTrackerContact>();

			JSONArray joarray=new JSONArray(result);
			for (int i = 0; i < joarray.length(); i++) {

				MyKiddyTrackerContact mykidd=new MyKiddyTrackerContact();
				JSONObject jobject=joarray.getJSONObject(i);
				mykidd.setEmailId(jobject.getString("EmailID"));
				mykidd.setName(jobject.getString("Name"));
				mykidd.setContactno(jobject.getString("ContactNo"));
				mykidd.setID(jobject.getString("Id"));

				/*	//adding  contact
						ContactsManager.addContact(context, mykidd);*/

				//updating  contact
				contact=jobject.getString("ContactNo");
				list.add(mykidd);
				/*	ContactsManager.updateMyContact(dHomeContext, mykidd);*/
			/*	if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
					new AddUserContact().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
				}
				else{
					new AddUserContact().execute();
				}	*/
			LoginActivity.mClient.sendMessage(addFriendForChat(jobject.getString("Name"), jobject.getString("Id")));

			}

			ContactsManager.addContact(homeContext, list);

		}catch(Exception ex)
		{
			Log.e("Exception ", ""+ex);
		}

	}
	class AddUserContact extends AsyncTask<Void, String, String>{
		String result="";
		HttpClient client=new DefaultHttpClient();
		HttpPost post=new HttpPost(Common.URL+"ParentAPI.asmx/SearchFriendList");

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}
		@Override
		protected String doInBackground(Void... params) {
			try{
				List<NameValuePair> list=new ArrayList<NameValuePair>(3);
				list.add(new BasicNameValuePair("UserId", Common.userid));
				list.add(new BasicNameValuePair("Entity", contact));
				list.add(new BasicNameValuePair("Flag", "1"));
				post.setEntity(new UrlEncodedFormEntity(list));
				HttpResponse response=client.execute(post);
				result=EntityUtils.toString(response.getEntity());

			}catch(Exception ex){
				result=ex.getMessage();	
			}


			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			parseJSONcontact(result);


		}
	}
	public void parseJSONcontact(String result) {
		try {
			JSONArray array=new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo=array.getJSONObject(i);
				LoginActivity.mClient.sendMessage(addFriendForChat(jo.getString("Name"), jo.getString("Id")));
			}
		} catch (Exception e) {
			Log.e("ParsonJSON", ""+e);
		}

	}


	//add Friend
	public static String addFriendForChat(String friendName,String friendId)
	{
		String addSTring="{}";
		try{

			System.out.print("##Sending DATA ###");
			JSONObject jo=new JSONObject();
			jo.put("event","add_friend");
			jo.put("user_id",Common.userid);
			jo.put("user_name",Common.username);
			jo.put("friend_id",friendId);
			jo.put("friend_name",friendName);
			addSTring=jo.toString();
		}
		catch(Exception e)
		{
			Log.e("addFriendForChat", ""+e);
		}
		return addSTring; 
	}

}
