package com.primesys.friendtrack;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;



import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adapter.Track_ListAdpter;
import com.example.dto.Sales_person;
import com.primesys.mitra.Common;
import com.primesys.mitra.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class Invitation_all extends Activity {
	
	ListView personlist;
	Context context=Invitation_all.this;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG="REquest";
	ArrayList<Sales_person> tracklist=new ArrayList<Sales_person>();
	Track_ListAdpter padpter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_track__user);
		findviewbyid();
		if(Common.getConnectivityStatus(context)){
			GetAllTrackperson();
		}
		
	}

	private void GetAllTrackperson() {


		reuestQueue = Volley.newRequestQueue(context);
		final ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setTitle("Please wait.......");
		pDialog.setCancelable(true);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.TrackURL+"AdminWebServices/GetAllTrackperson",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseData(response);
				System.err.println("Track Data"+response);
				pDialog.hide();
			}
		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseData(new String(error.networkResponse.data));
				}
			}
		}) {



		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	
	
	}

	protected void parseData(String result) {
		
		// TODO Auto-generated method stub

		String response = "";
		try {
			Sales_person p;
			JSONArray array = new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				p=new Sales_person();
				JSONObject jo = array.getJSONObject(i);
				p.setName(jo.getString("name"));
				p.setContactNumber(jo.getString("cont_no"));
				p.setRoleID(jo.getString("roleid"));
				p.setID(jo.getString("id"));
				p.setPartyName(jo.getString("party_group"));
				if(jo.has("photo"))
				p.setPhoto(jo.getString("photo"));
				else p.setPhoto(null);
					tracklist.add(p);

			

			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			System.err.println("personlist"+tracklist);
			padpter=new Track_ListAdpter(context, 0, tracklist);

			personlist.setAdapter(padpter);
		}
	
		
	
	}

	private void findviewbyid() {
		
	personlist=(ListView) findViewById(R.id.person_list);
	ActionBar ab = getActionBar(); 
	ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#20b0b0"));     
	ab.setBackgroundDrawable(colorDrawable);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_back, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.back) {
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
