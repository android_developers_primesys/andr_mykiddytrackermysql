package com.example.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.example.dto.Student;
import com.primesys.mitra.Common;
import com.primesys.mitra.ParentInformation;
import com.primesys.mitra.R;
import com.primesys.mitra.TeacherDiaryActivity;

public class ParentDitinaryListAdapter extends ArrayAdapter<Student> {
	Context context;
	int layoutResourceId;
	public static ArrayList<Student> data;
	Intent Studeninfo;
	CheckBox selection;
	Animation animation = null;
	public static String selectedparent;
	public ParentDitinaryListAdapter(Context context, int layoutResourceId,
			ArrayList<Student> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		ParentDitinaryListAdapter.data=new ArrayList<Student>();
		ParentDitinaryListAdapter.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);
		}
		Student s = data.get(position);
			// Log.e("student",s.getFirstName());
			final CheckBox selection=(CheckBox)convertView.findViewById(R.id.selection);

			selection.setChecked(s.getStatus());
			if(selection.isChecked())

				Common.to=s.getParentID();
			selection.setTag(position);
			selection.setVisibility(View.GONE);
            
			Button btnEdit = (Button) convertView.findViewById(R.id.studentame);
			btnEdit.setText("\t\t"+""+"\t" + s.getFirstName()+" "+s.getLastName());
			animation = AnimationUtils.loadAnimation(context, R.anim.animlistview);
			selection.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					Student user1=data.get((Integer)selection.getTag());
					if(selection.isChecked()){
						Common.to=user1.getParentID();
						user1.setStatus(true);
					}
					else{
						user1.setStatus(false);
						ParentInformation.selection.setChecked(false);
					}
					boolean valid=true;
					for(Student su : data){
						if(!su.getStatus()){
							valid=false;
							break;
						}
					}
					if(valid)
						ParentInformation.selection.setChecked(true);
				}
			});

			//to initiate one to one chat..
			btnEdit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Student user1=data.get((Integer)selection.getTag());
					Intent initiat_chat=new Intent(context,TeacherDiaryActivity.class);
					initiat_chat.putExtra("StudentID", ""+user1.getStudentID());
					initiat_chat.putExtra("Type", "T");
					context.startActivity(initiat_chat);
					((Activity)context).overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out); 
				}
			});
		animation.setDuration(500);
		convertView.startAnimation(animation);
		/*  animation = null;*/
		return convertView;

	}
}
