package com.example.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.IconTextView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dto.FrindsDTO;
import com.primesys.mitra.Common;
import com.primesys.mitra.GroupDetails;
import com.primesys.mitra.LoginActivity;
import com.primesys.mitra.R;

public class ParticipantAdapter extends BaseAdapter {

	Context currentContext;
	ArrayList<FrindsDTO> listFrnd;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG=GroupDetails.class.getSimpleName();
	private String grpId;

	// constructor
	public ParticipantAdapter(Context context,ArrayList<FrindsDTO> list,String grpID) {
		this.currentContext=context;
		this.listFrnd=list;
		this.grpId=grpID;
	}

	@Override
	public int getCount() {
		return listFrnd.size();
	}

	@Override
	public FrindsDTO getItem(int position) {
		return listFrnd.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FrindsDTO frnd=getItem(position);
		if(convertView==null)
		{
			LayoutInflater inflater = ((Activity) currentContext).getLayoutInflater();
			convertView=inflater.inflate(R.layout.list_friends, null);
		}
		final TextView member_name=(TextView)convertView.findViewById(R.id.member_name);
		member_name.setText(frnd.getMemberName());
		member_name.setTag(position);
		IconTextView userIcon=(IconTextView)convertView.findViewById(R.id.icon_user);
		userIcon.setVisibility(View.GONE);

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// call the add participant video
				LoginActivity.mClient.sendMessage(addMember(listFrnd.get((Integer) member_name.getTag()).getMemberId()));
			}
		});
		return convertView;
	}

	void  addParticiapnt(final int pid)
	{
		reuestQueue=Volley.newRequestQueue(currentContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(currentContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.URL+"ParentAPI.asmx/AddGroupParticipant",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseJSON(response);
				pDialog.hide();
			}

		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("GroupId",grpId);
				params.put("ParticipantId",pid+"");
				return params;
			}

		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	String addMember(final int pid)
	{
		String result="";
		try
		{
			JSONObject jo=new JSONObject();
			jo.put("event", "add_group_member");
			jo.put("group_id", grpId);
			jo.put("user_id",pid+"");
			result=jo.toString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

	void parseJSON(String result)
	{
		try
		{
			    JSONObject jo=new JSONObject(result);
				Common.showToast(jo.getString("msg"), currentContext);
				((Activity) currentContext).finish();
		}catch(Exception e)
		{}

	}
}
