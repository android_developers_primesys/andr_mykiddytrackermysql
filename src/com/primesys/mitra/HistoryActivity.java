package com.primesys.mitra;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.adapter.HistoryMapAdapter;
import com.example.db.DBHelper;
import com.example.dto.GmapDetais;
import com.example.dto.LocationData;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;

public class HistoryActivity extends Activity{

	static Context historyContext;
	GoogleMap googleMap;
	ArrayList<LocationData> list;
	ArrayList<LatLng> arrayPoints=new ArrayList<LatLng>(); 
	private Marker selectedMarker;
	private PolylineOptions polylineOptions = new PolylineOptions();
	Handler handler = new Handler();
	Random random = new Random();
	FragmentManager manager;
	private static final int REQ_DATETIME=1;
	private List<Marker> markers = new ArrayList<Marker>();
	int totalCount;
	int  StudentId;
	ListView gmapList;
	String defaultImage;
	public static Boolean Updatestatus=false;
	ArrayList<GmapDetais> arr=new ArrayList<GmapDetais>();
	int cnt=0;
	ImageLoader imageLoader;
	static RequestQueue RecordSyncQueue;
	HistoryMapAdapter myAdapter;
	Handler handle =new Handler();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		historyContext=HistoryActivity.this;
		if (!isGooglePlayServicesAvailable()) {
			Common.showToast("Google Play services not available !", historyContext);
			finish();
		}
		StudentId=getIntent().getIntExtra("StudentId",0);
		RecordSyncQueue = Volley.newRequestQueue(historyContext);

		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		
		imageLoader = new ImageLoader(
				RecordSyncQueue, imageCache);
		imageLoader = new ImageLoader(
				Volley.newRequestQueue(historyContext), imageCache);
		setContentView(R.layout.activity_currentlocation);

		initilizeMap();
		addDefaultLocations();
		if(list.size()!=0)
			startAnimation();
		//Show Track Menu There
		final int abTitleId = getResources().getIdentifier("action_bar_title", "id", "android");
		gmapList=(ListView)findViewById(R.id.list);
		findViewById(abTitleId).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try{

					gmapList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
					if (cnt<=0) {
						gmapList.setVisibility(View.VISIBLE);
						myAdapter=new HistoryMapAdapter(HistoryActivity.this, R.layout.fragment_mapsidebar, arr,imageLoader);
						gmapList.setAdapter(myAdapter);
						overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
						gmapList.setSelection(0);
						cnt++;
					}else{
						gmapList.setVisibility(View.GONE);
						cnt=0;
					}

				}catch(Exception ex){
					Log.e("Exception", ""+ex);
				}

			}
		});

		//call Service

		try{
			if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				new TrackInfrmation().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			}
			else{
				new TrackInfrmation().execute();
			}
		}catch(Exception ex)
		{
			//Common.showToast("No user Information ", contextMap);
		}
	}





	//get Set

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.gmap)).getMap();
			// check if map is created successfully or not
			if (googleMap == null) {
				Common.showToast("Sorry! unable to create maps", historyContext);
			}
		}
	}
	private boolean isGooglePlayServicesAvailable() {
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (ConnectionResult.SUCCESS == status) {
			return true;
		} else {
			GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
			return false;
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_map, menu);
		return super.onCreateOptionsMenu(menu);

	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.pick).setIcon(new IconDrawable(this, IconValue.fa_calendar).colorRes(R.color.primary).actionBarSize());
		return super.onPrepareOptionsMenu(menu);
	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
			finish();
			return super.onOptionsItemSelected(item);
		case R.id.pick:
			Intent i=new Intent(historyContext,DateTimeActivity.class);
			i.putExtra("StudentId",ShowGmapClient.StudentId);
			startActivityForResult(i,REQ_DATETIME);
			return super.onOptionsItemSelected(item);
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(data!=null&&requestCode==REQ_DATETIME&&resultCode==RESULT_OK)
		{

		}
	}
	//add Default Location
	public  void addDefaultLocations() {
		clearMarkers();
		list=new ArrayList<LocationData>();
		DBHelper dbH=DBHelper.getInstance(historyContext);
		list=dbH.showDetails();
		totalCount=list.size();
		if(totalCount==0)
			Common.showToast(getResources().getString(R.string.history_msg), historyContext);
		for (int i = 0; i < totalCount; i++) {
			addMarkerToMap(list.get(i),i);
		}
	}


	private void startAnimation() {

		googleMap.animateCamera(
				CameraUpdateFactory.newLatLngZoom(markers.get(0).getPosition(), 18), 
				5000,
				MyCancelableCallback);      
		currentPt = 0-1;	
	}
	int currentPt;
	CancelableCallback MyCancelableCallback =
			new CancelableCallback(){

		@Override
		public void onCancel() {
		}

		@Override
		public void onFinish() {

			if(++currentPt < markers.size()){

				float targetBearing = bearingBetweenLatLngs( googleMap.getCameraPosition().target, markers.get(currentPt).getPosition());
				LatLng targetLatLng = markers.get(currentPt).getPosition();
				CameraPosition cameraPosition =
						new CameraPosition.Builder()
				.target(targetLatLng)
				.tilt(currentPt<markers.size()-1 ? 90 : 0)
				.bearing(targetBearing)
				.zoom(googleMap.getCameraPosition().zoom)
				.build();
				googleMap.animateCamera(
						CameraUpdateFactory.newCameraPosition(cameraPosition), 
						3000,
						currentPt==markers.size()-1 ? FinalCancelableCallback : MyCancelableCallback);
				highLightMarker(currentPt);
			}
		}
	};
	CancelableCallback FinalCancelableCallback = new CancelableCallback() {

		@Override
		public void onFinish() {
			googleMap.animateCamera( CameraUpdateFactory.zoomTo( 17.0f ) ); 
		}

		@Override
		public void onCancel() {

		}
	};
	private Location convertLatLngToLocation(LatLng latLng) {
		Location loc = new Location("someLoc");
		loc.setLatitude(latLng.latitude);
		loc.setLongitude(latLng.longitude);
		return loc;
	}

	private float bearingBetweenLatLngs(LatLng begin,LatLng end) {
		Location beginL= convertLatLngToLocation(begin);
		Location endL= convertLatLngToLocation(end);
		return beginL.bearingTo(endL);
	}

	public void toggleStyle() {
		if (GoogleMap.MAP_TYPE_NORMAL == googleMap.getMapType()) {
			googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);		
		} else {
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
	/**
	 * Adds a marker to the map.
	 */
	public void addMarkerToMap(LocationData data,int pos) {
		LatLng lat=new LatLng(data.getLat(),data.getLan());
		DateTimeActivity.selStatus=false;
		if(pos==0||pos==totalCount-1)
		{
			Marker marker = googleMap.addMarker(new MarkerOptions().position(lat)
					.title("Speed is "+data.getSpeed()+" km/h")

					.snippet(Common.getDateCurrentTimeZone(data.getTimestamp())));
			polylineOptions = new PolylineOptions();
			polylineOptions.color(Color.CYAN); 
			polylineOptions.width(5); 
			polylineOptions.geodesic(true);
			arrayPoints.add(lat); polylineOptions.addAll(arrayPoints); 
			googleMap.addPolyline(polylineOptions);
			markers.add(marker);
		}
		else
		{
			Marker marker = googleMap.addMarker(new MarkerOptions().position(lat)
					.title("Speed is "+data.getSpeed()+" km/h")
					.snippet(Common.getDateCurrentTimeZone(data.getTimestamp()))
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_overlay))

					);
			marker.setFlat(true);
			polylineOptions = new PolylineOptions();
			polylineOptions.color(Color.CYAN); 
			polylineOptions.width(5); 
			polylineOptions.geodesic(true);
			arrayPoints.add(lat); polylineOptions.addAll(arrayPoints); 
			googleMap.addPolyline(polylineOptions);
			markers.add(marker);
		}
	}
	/**
	 * Clears all markers from the map.
	 */
	public void clearMarkers() {
		googleMap.clear();
		markers.clear();		
	}
	/**
	 * Remove the currently selected marker.
	 */
	public void removeSelectedMarker() {
		this.markers.remove(this.selectedMarker);
		this.selectedMarker.remove();
	}	

	/**
	 * Highlight the marker by index.
	 */
	private void highLightMarker(int index) {
		highLightMarker(markers.get(index));
	}

	/**
	 * Highlight the marker by marker.
	 */
	private void highLightMarker(Marker marker) {
		//marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
		marker.showInfoWindow();
		this.selectedMarker=marker;
	}		
	//Track Informatiion
	class TrackInfrmation extends AsyncTask<Void, String, String>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}
		@Override
		protected String doInBackground(Void... params) {
			String result="";
			try{
				HttpClient httpclient=new DefaultHttpClient();
				HttpPost httpost=new HttpPost(Common.URL+"ParentAPI.asmx/GetTrackInfo");
				List<NameValuePair> param=new ArrayList<NameValuePair>(1);
				param.add(new BasicNameValuePair("ParentId", Common.userid));
				httpost.setEntity(new UrlEncodedFormEntity(param));
				HttpResponse response = httpclient.execute(httpost);
				result=EntityUtils.toString(response.getEntity());
				Log.e("response", ""+result);
			}catch(Exception e){
				result=e.getMessage();
			}

			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			parsingTrackInfo(result);

		}
	}
	public void parsingTrackInfo(String result) {
		try{
			JSONArray joArray=new JSONArray(result);
			for (int i = 0; i < joArray.length(); i++) {
				JSONObject joObject =joArray.getJSONObject(i);
				GmapDetais dmDetails=new GmapDetais();
				if (i<=0) {
					defaultImage=joObject.getString("Photo").replaceAll("~", "").trim();
				}
				dmDetails.setId(joObject.getString("StudentID"));
				dmDetails.setName(joObject.getString("Name"));
				dmDetails.setPath(joObject.getString("Photo").replaceAll("~", "").trim());
				dmDetails.setType(joObject.getString("Type"));
				arr.add(dmDetails);
			}


		}catch(Exception e){
			Log.e("Exception", ""+e);
		}finally{
			//it work Better but take time to Load
			if (arr.size()>0) {

				gmapList=(ListView)findViewById(R.id.list);
				myAdapter=new HistoryMapAdapter(HistoryActivity.this, R.layout.fragment_mapsidebar, arr,imageLoader);
				gmapList.setAdapter(myAdapter);
				overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
				gmapList.setSelection(0);
				cnt++;
			}else{
				Common.showToast("No User Information",historyContext );
			}

		}
	}
	protected void onDestroy() {
		super.onDestroy();
		finish();
	};

}


