package com.example.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;






import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dto.AddUserDTO;
import com.google.gson.JsonObject;



import com.primesys.mitra.Common;
import com.primesys.mitra.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.TextView;



public class AddFrd_TrackAdpter extends ArrayAdapter<AddUserDTO>{
	Context context;
	int layoutResourceId;
	public static ArrayList<AddUserDTO> data;
	Animation animation = null;
	private RequestQueue reuestQueue;
	private StringRequest stringRequest;
	private String TAG="Reqqq";

	public AddFrd_TrackAdpter(Context context, int layoutResourceId,
			ArrayList<AddUserDTO> data) {
		super(context, layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		AddViewHolder holder;
		try{
			final AddUserDTO s=data.get(position); 
			if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder=new AddViewHolder();
			holder.btnAddUser=(TextView)convertView.findViewById(R.id.studentame);
			holder.city=(TextView)convertView.findViewById(R.id.city);
			convertView.setTag(holder);
			}
			else
                 holder=(AddViewHolder)convertView.getTag();
			
			holder.btnAddUser.setText(s.getUserName());
			holder.city.setText(s.getUserCity());
			holder.btnAddUser.setTag(position); 
			holder.btnAddUser.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try{
						AddUserDTO addusers=data.get((Integer)v.getTag());
						Send_Invitation(addusers);
						//LoginActivity.mClient.sendMessage(AddUserActivity.addFriendForChat(addusers.getUserName(),addusers.getUserId()));	

					}catch(Exception e)
					{
						Log.e("Exception", ""+e);
					}
				}
			});

		}catch(Exception e)
		{
			Log.e("Exception", ""+e);
		}
		return convertView;
	}
	
	
	
	protected void Send_Invitation(final AddUserDTO addusers) {

		
		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setTitle("Please wait.......");
		pDialog.setCancelable(true);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.TrackURL+"UserServiceAPI/SendInvitation",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parsedata(response);
				pDialog.dismiss();
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parsedata(new String(error.networkResponse.data));
				}
			}
		}) {

		@Override
			protected Map<String, String> getParams() {
				

				Map<String, String> params = new HashMap<String, String>();
			
				params.put("SenderId",Common.userid);
				params.put("GettingId",addusers.getUserId());
				//params.put("SenderId",Common.Groupname);

				System.out.println("Reqq- invitation--"+ params);

				return params;

			}

		};
		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	
	
	}
	protected void parsedata(String response) {
		// TODO Auto-generated method stub
		System.out.println("REspo    "+response);

		try {
			JSONObject jo=new JSONObject(response);
			if (jo.get("error").equals("false")) {
				Common.showToast(jo.get("message").toString(), context);
			}else {
				Common.showToast(jo.get("message").toString(), context);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//view holder for add user adapter
	static class AddViewHolder
	{
		TextView btnAddUser;
		TextView city;
	}
}
