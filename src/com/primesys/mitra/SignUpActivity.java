package com.primesys.mitra;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.google.android.gms.common.ConnectionResult;

@SuppressWarnings("deprecation")
public class SignUpActivity extends Activity 
{
	Context contxt_reg=SignUpActivity.this;
	private SharedPreferences mPrefs;
	Button loginfacebook,logingoogle,btnRegister;
	boolean isExist;   
	/*private static String APP_ID = "1083483415000749";  //primesystech Account Rideshare Account Key
	 */	
	private static String APP_ID = "1634968086732703";
	private Facebook facebook = new Facebook(APP_ID);
	private AsyncFacebookRunner mAsyncRunner;

	EditText txtname,txtmessgage,txtcontact,txtemail;
	String strname,strmessage,strcontact,stremail,password,username;
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	Context signupContext=SignUpActivity.this;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG="SignUpActivity"; 
	private String phpurl="http://www.mykiddytracker.com/send_mail";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		FindViewBYID();
		
		btnRegister.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try{
					if(validation())
					{
						//make clear all
						txtname.setError(null);
						txtemail.setError(null);
						txtcontact.setError(null);
						txtmessgage.setError(null);
						txtmessgage.requestFocus();
						signUpRequest();
					}
				}catch(Exception ex){

				}

			}
		});

	}
	Boolean validation() {
		boolean valid=true;
		strname=txtname.getText().toString();
		strmessage=txtcontact.getText().toString();
		stremail=txtemail.getText().toString();
		strcontact=txtcontact.getText().toString();
		if (strname.length()==0) {
			txtname.setError("Enter Name");
			txtname.requestFocus();
			valid=false;
		}else
			if (stremail.length()==0) {
				txtemail.setError("Enter Email");
				txtemail.requestFocus();
				valid=false;
			}else
				if (!stremail.equalsIgnoreCase("")) {
					Pattern pattern = Pattern.compile(EMAIL_PATTERN);
					Matcher matcher = pattern.matcher(stremail);
					valid=matcher.matches();
					if (valid==false) {
						txtemail.setError("Enter valid email ");
						txtemail.requestFocus();
						valid=false;
					}
				}

		return valid;
	}
	//gte Login & receeive  Profile Information
	public void loginToFacebook() {

		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);
		if (access_token != null) {
			facebook.setAccessToken(access_token);
			Log.d("FB Sessions", "" + facebook.isSessionValid());
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}
		/*if (!facebook.isSessionValid()) {*/ //to check Session
		facebook.authorize(this,
				new String[] { "email", "publish_stream" },
				new DialogListener() {

			public void onCancel() {
				// Function to handle cancel event
			}

			public void onComplete(Bundle values) {
				// Function to handle complete event
				// Edit Preferences and update facebook acess_token
				SharedPreferences.Editor editor = mPrefs.edit();
				editor.putString("access_token",
						facebook.getAccessToken());
				editor.putLong("access_expires",
						facebook.getAccessExpires());
				editor.commit();
				Log.d("access_tokenaccess_token", ""+facebook.getAccessToken());
				GetProfileInformation(); //here you get profile information
			}


			public void onError(DialogError error) {
				// Function to handle error

			}


			public void onFacebookError(FacebookError fberror) {
				// Function to handle Facebook errors

			}

		});
		/*}*/
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		facebook.authorizeCallback(requestCode, resultCode, data);
	}
	//to Get facebook Profile Information
	void GetProfileInformation() {
		mAsyncRunner.request("me", new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {

				//Get All data from Facebook
				try{
					String json=response;
					JSONObject profile = new JSONObject(json);
					strname =""+profile.getString("first_name")+"\t"+profile.getString("last_name");
					stremail = profile.getString("email");
					strcontact="";
					strmessage="Registration with facebook";
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							try{
								signUpRequest();
							}catch(Exception e)
							{
								Log.e("Exception", ""+e);
							}

						}
					});


					/*signUpRequest();*/
				}catch(Exception e)
				{
					Log.e("GetProfileInformation", ""+e.getMessage());
				}

			}

			@Override
			public void onIOException(IOException e, Object state) {


			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {


			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {


			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {


			}

		});

	}
	void FindViewBYID() {
		/*loginfacebook=(Button)findViewById(R.id.login_fb);
		logingoogle=(Button)findViewById(R.id.login_gplus);*/
		txtname=(EditText)findViewById(R.id.txtname);
		txtmessgage=(EditText)findViewById(R.id.txtmessage);
		txtemail=(EditText)findViewById(R.id.txtemail);
		txtcontact=(EditText)findViewById(R.id.txtcontact);
		btnRegister=(Button)findViewById(R.id.Submit);
	}
	@Override
	protected void onStart() {

		super.onStart();
	}
	@Override
	protected void onStop() {

		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			Intent loginIntent = new Intent(contxt_reg, LoginActivity.class);
			startActivity(loginIntent);
			finish();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	// volley request for posting the parameter
	public void signUpRequest()
	{
		reuestQueue=Volley.newRequestQueue(signupContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(signupContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.URL+"LoginServiceAPI.asmx/SaveDemoUser",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				try{
					parseJSON(response);
					pDialog.dismiss();
				}catch(Exception ex){

				}

			}

		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.dismiss();
				try{
					if(error.networkResponse != null && error.networkResponse.data != null){
						VolleyError er = new VolleyError(new String(error.networkResponse.data));
						error = er;
						System.out.println(error.toString());
						parseJSON(new String(error.networkResponse.data));
					}
				}catch(Exception ex)
				{

				}

			}
		}) {
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("name",strname);
				params.put("email",stremail);
				params.put("contact",strcontact);
				params.put("message",strmessage);
				params.put("registrationType","App");
				params.put("City",Common.user_regCity);

				return params;
			}

		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	void parseJSON(String result)
	{
		System.err.println(result);
		if(result.contains("error"))
		{
			Common.showToast("Check once again there is Error !", signupContext);
		}
		else{
			try
			{
				JSONObject jo=new JSONObject(result);
				username=jo.getString("username");
				password=jo.getString("password");
				isExist=jo.getBoolean("user_exist");
				if(isExist)
					Common.showToast("You are registered user. For latest password Click on Forgot password!", signupContext);
				else
					senEmailRequest();
			}
			catch(Exception e)
			{

			}
		}
	}
	//send email request
	public void senEmailRequest()
	{
		reuestQueue=Volley.newRequestQueue(signupContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(signupContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,phpurl,new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseEmail(response);
				pDialog.dismiss();
			}

		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.dismiss();
				try{
					if(error.networkResponse != null && error.networkResponse.data != null){
						VolleyError er = new VolleyError(new String(error.networkResponse.data));
						error = er;
						System.out.println(error.toString());
						parseEmail(new String(error.networkResponse.data));
					}
				}catch(Exception e){
					Log.e("Exception", ""+e);
				}

			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("send_mail", "true");
				params.put("name",strname);
				params.put("username", username);
				params.put("email",stremail);
				params.put("password",password);
				return params;
			}

		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	void parseEmail(String result)
	{
		Common.showToast("Check E-mail for Username and Password", signupContext);
		Intent loginIntent = new Intent(contxt_reg, LoginActivity.class);
		startActivity(loginIntent);
		finish();

	}


}
