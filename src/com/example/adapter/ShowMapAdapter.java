package com.example.adapter;

import java.util.ArrayList;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.dto.GmapDetais;
import com.primesys.mitra.CircularNetworkImageView;
import com.primesys.mitra.Common;
import com.primesys.mitra.LruBitmapCache;
import com.primesys.mitra.R;
import com.primesys.mitra.ShowGmapClient;
import com.primesys.mitra.ShowGmapClient;

public class ShowMapAdapter  extends ArrayAdapter<GmapDetais>{

	TextView txtchild,txtcar,txtpet;
	Context context;
	ImageLoader imageLoader;
	int layoutResourceId;
	ArrayList<GmapDetais> datal;
	public static Boolean trackInfo=false;
	LinearLayout laychild,laypet,layCar;
	Bitmap bitmap;
	static RequestQueue RecordSyncQueue;
	public ShowMapAdapter(Context context, int layoutResourceId,
			ArrayList<GmapDetais> data, ImageLoader imageLoader2) {
		super(context, layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.datal = data;
		this.imageLoader=imageLoader2;
		trackInfo=false;
	}

	@Override
	public View getView( int position, View convertView, ViewGroup parent) {

		if (convertView== null) {
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);

		} 

		try{

			GmapDetais gp=datal.get(position);
			final LinearLayout lay=(LinearLayout)convertView.findViewById(R.id.lay_main);
			final CircularNetworkImageView imgchild=(CircularNetworkImageView)convertView.findViewById(R.id.img_child);
			txtchild=(TextView)convertView.findViewById(R.id.txt_child);
			txtchild.setText(gp.getName());
			imgchild.setTag(position);
			txtchild.setTag(position);

			//to show Network Images  here child
			try{
				/*	RecordSyncQueue = Volley.newRequestQueue(context);

					ImageLoader.ImageCache imageCache = new LruBitmapCache();
				imageLoader = new ImageLoader(
						RecordSyncQueue, imageCache);*/

				(imgchild).setImageUrl(Common.relativeurl+gp.getPath().replaceAll(" ","%20"), imageLoader);
				((CircularNetworkImageView) imgchild)
				.setDefaultImageResId(0);
				((CircularNetworkImageView) imgchild)
				.setErrorImageResId(0);



				//To Test That part

			}catch(Exception ex){

			}

			txtchild.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try{


						ShowGmapClient.flag=0;
						GmapDetais user1=datal.get((Integer)v.getTag());
						if (user1.getType().equalsIgnoreCase("Child")) {
							trackInfo=false;
							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();
							ShowGmapClient.bmp1=bmp;

							//Image Loading from URl 
							/*		
							 * 
							URL url = new URL(Common.relativeurl+user1.getPath().replaceAll(" ","%20"));
							HttpURLConnection connection = (HttpURLConnection) url.openConnection();
							connection.setDoInput(true);
							connection.connect();
							InputStream input = connection.getInputStream();
							Bitmap myBitmap = BitmapFactory.decodeStream(input);
							ShowGmapClient.bmp1=getRoundedShape(myBitmap);*/
							com.primesys.mitra.LoginActivity.mClient.sendMessage(StopTracKEvent());
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONChild(user1.getId()));

						}if(user1.getType().equalsIgnoreCase("Car")){
							ShowGmapClient.flag=0;
							trackInfo=true;
							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();
							ShowGmapClient.bmp1=bmp;
							com.primesys.mitra.LoginActivity.mClient.sendMessage(StopTracKEvent());
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONCar(user1.getId()));

						}if(user1.getType().equalsIgnoreCase("Pet")){		
							txtchild.setTextColor(R.color.primary);
							ShowGmapClient.flag=0;
							trackInfo=true;

							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();
							ShowGmapClient.bmp1=bmp;
														com.primesys.mitra.LoginActivity.mClient.sendMessage(StopTracKEvent());
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONPet(user1.getId()));
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});

			//onclick txtxhild
			imgchild.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try{
						//ShowGmapClient.mMap.clear();
						GmapDetais user1=datal.get((Integer)v.getTag());
						ShowGmapClient.flag=0;
						if (user1.getType().equalsIgnoreCase("Child")) {
							txtchild.setTextColor(R.color.primary);
							trackInfo=false;
							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();

							ShowGmapClient.bmp1=bmp;
						
							com.primesys.mitra.LoginActivity.mClient.sendMessage(StopTracKEvent());
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONChild(user1.getId()));
						}else if(user1.getType().equalsIgnoreCase("Car")){

							txtchild.setTextColor(R.color.primary);
							ShowGmapClient.flag=0;
							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();
							trackInfo=true;

							ShowGmapClient.bmp1=bmp;
							com.primesys.mitra.LoginActivity.mClient.sendMessage(StopTracKEvent());
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONCar(user1.getId()));
						}else if(user1.getType().equalsIgnoreCase("Pet"))
						{		

							txtchild.setTextColor(R.color.primary);
							ShowGmapClient.flag=0;
							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();
							trackInfo=true;
						
							ShowGmapClient.bmp1=bmp;
							com.primesys.mitra.LoginActivity.mClient.sendMessage(StopTracKEvent());
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONPet(user1.getId()));
						}



					}catch(Exception ex){

					}
				}
			});



		}catch(Exception ex){
			Log.e("Exception-Adatpte", ""+ex);
		}



		return convertView;
	}

	private Bitmap getRoundedShape(Bitmap bitmap) {
		int targetWidth = 40;
		int targetHeight = 40;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, 
				targetHeight,Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), 
						((float) targetHeight)) / 2),
						Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = bitmap;
		canvas.drawBitmap(sourceBitmap, 
				new Rect(0, 0, sourceBitmap.getWidth(),
						sourceBitmap.getHeight()), 
						new Rect(0, 0, targetWidth, targetHeight), null);
		return targetBitmap;

	}

	protected String makeJSONChild(String Id) {
		String trackSTring="{}";
		try{
			JSONObject jo=new JSONObject();
			jo.put("event","start_track");
			if(Common.roleid.equals("5"))
			{
				jo.put("student_id","demo_student");
				Common.currTrack="demo_student";
			}else{
				jo.put("student_id",Integer.parseInt(Id));
				Common.currTrack=Id;
			}
			trackSTring=jo.toString();
		}
		catch(Exception e)
		{

		}

		return trackSTring;
	}

	protected String makeJSONCar(String Id) {
		String trackSTring="{}";
		try{
			JSONObject jo=new JSONObject();
			jo.put("event","start_track");
			if(Common.roleid.equals("5")){
				jo.put("student_id","demo_car");
				Common.currTrack="demo_car";
			}else{
				jo.put("student_id",Integer.parseInt(Id));
				Common.currTrack=Id;
			}
			trackSTring=jo.toString();
		}
		catch(Exception e)
		{

		}
		return trackSTring;
	}

	protected String makeJSONPet(String Id) {
		String trackSTring="{}";
		try{
			JSONObject jo=new JSONObject();
			jo.put("event","start_track");
			if(Common.roleid.equals("5")){
				jo.put("student_id","demo_pet");
				Common.currTrack="demo_pet";
			}
			else{
				jo.put("student_id",Integer.parseInt(Id));
				Common.currTrack=Id;
			}
			trackSTring=jo.toString();
		}
		catch(Exception e)
		{

		}
		return trackSTring;
	}

	//Stop track
	String StopTracKEvent() {
		String trackSTring="{}";
		try{
			JSONObject jo=new JSONObject();
			jo.put("event","stop_track");
			trackSTring=jo.toString();
		}
		catch(Exception e)
		{

		}
		return trackSTring;
		// TODO Auto-generated method stub

	}

}
