package com.primesys.mitra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.view.View.OnTouchListener;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
public class Res extends Activity {
	
	private GraphicalView mChart;
	HashMap<String,String> answerhasmap=new HashMap<String, String>();
	Context context=Res.this;
	private RequestQueue reuestQueue;
	private StringRequest stringRequest;
	private String TAG="GETResult",Result_req="",Result;
	TextView result;
	ArrayList<Integer> scorelist=new ArrayList<Integer>();
	ArrayList<Integer> attempt=new ArrayList<Integer>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_res);       
        Intent intent = getIntent();    

        scorelist =intent.getIntegerArrayListExtra("Scorelist");
        openChart();	

		/*if(Common.connectionStatus)  
		{
			Calculate_result();
		}*/
        
    }
 // make volley request 
 	void Calculate_result()
 	{
 		String url="http://192.168.1.110:8080/MyKiddyTrackerAPI/MyKiddy/ExamAPI/QuizzTransaction";

 		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
 		final ProgressDialog pDialog = new ProgressDialog(context);
 		pDialog.setTitle("Progress wait.......");
 		pDialog.setCancelable(false);
 		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
 		pDialog.show();
 		//JSon object request for reading the json data
 		stringRequest = new StringRequest(Method.POST,Common.MONGOURL+"ExamAPI/QuizzTransaction",new Response.Listener<String>() {

 			@Override
 			public void onResponse(String response) {


 				parseJSON(response);
 				pDialog.hide();

 				
 			}

 		},
 		new Response.ErrorListener() {


 			@Override
 			public void onErrorResponse(VolleyError error) {
 				pDialog.hide();
 				if(error.networkResponse != null && error.networkResponse.data != null){
 					VolleyError er = new VolleyError(new String(error.networkResponse.data));
 					error = er;
 					System.out.println(error.toString());
 					parseJSON(new String(error.networkResponse.data));
 				}
 			}
 		}) {

 			@Override
 			protected Map<String, String> getParams() {
 				Map<String, String> params = new HashMap<String, String>();

 				params.put("Result",Result_req);

 				System.out.println("REq----------"+Result_req);
 				return params;
 			}

 		};


 		stringRequest.setTag(TAG);
 		// Adding request to request queue
 		reuestQueue.add(stringRequest);
 	}

 	
 	
    @SuppressWarnings("deprecation")
	private void openChart(){
    	int[] x = { 0,1,2,3,4,5,6,7 };
    	int[] income = { 2000,2500,2700,3000,2800,3500,3700,3800};
    	int[] expense = {2200, 2700, 2900, 2800, 2600, 3000, 3300, 3400 };
    	
    	// Creating an  XYSeries for Income
    	XYSeries incomeSeries = new XYSeries("ATTempt");
    	// Creating an  XYSeries for Expense
    	//XYSeries expenseSeries = new XYSeries("Expense");
    	// Adding data to Income and Expense Series
    		for(int i=0;i<scorelist.size();i++){
			incomeSeries.add(i, scorelist.get(i));
			/*expenseSeries.add(x[i],expense[i]);*/
		}

    	
    	// Creating a dataset to hold each series
    	XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
    	// Adding Income Series to the dataset
    	dataset.addSeries(incomeSeries);
    	// Adding Expense Series to dataset
    //	dataset.addSeries(expenseSeries);    	
    	
    	
    	// Creating XYSeriesRenderer to customize incomeSeries
    	XYSeriesRenderer incomeRenderer = new XYSeriesRenderer();
    	incomeRenderer.setColor(Color.WHITE);
    	incomeRenderer.setPointStyle(PointStyle.CIRCLE);
    	incomeRenderer.setFillPoints(true);
    	incomeRenderer.setLineWidth(4);
    	incomeRenderer.setDisplayChartValues(true);
    	
    	// Creating XYSeriesRenderer to customize expenseSeries
    /*	XYSeriesRenderer expenseRenderer = new XYSeriesRenderer();
    	expenseRenderer.setColor(Color.YELLOW);
    	expenseRenderer.setPointStyle(PointStyle.CIRCLE);
    	expenseRenderer.setFillPoints(true);
    	expenseRenderer.setLineWidth(2);
    	expenseRenderer.setDisplayChartValues(true);*/
    	
    	
    	// Creating a XYMultipleSeriesRenderer to customize the whole chart
    	XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
    	multiRenderer.setXLabels(0);
    	multiRenderer.setChartTitle("Attempts vs Score Chart");
		multiRenderer.setXTitle("Attempt");
		multiRenderer.setYTitle("Score");
		multiRenderer.setAxisTitleTextSize(25.0f);
		multiRenderer.setChartValuesTextSize(12);
		multiRenderer.setZoomButtonsVisible(true);   
		multiRenderer.setApplyBackgroundColor(true);
		multiRenderer.setBackgroundColor(Color.parseColor("#CC6698"));
		multiRenderer.setZoomButtonsVisible(true);
		multiRenderer.setPointSize(15.0f);
		multiRenderer.setLabelsTextSize(20.0f);
		multiRenderer.setLabelsColor(R.color.red);
		for(int i=0;i<scorelist.size();i++)
           attempt.add(i);
     	
    	for(int i=0;i<scorelist.size();i++){
    		multiRenderer.addXTextLabel(i, attempt.get(i)+"");   
    		multiRenderer.addYTextLabel(i, scorelist.get(i)+"");
    	}  	
    	
    	// Adding incomeRenderer and expenseRenderer to multipleRenderer
    	// Note: The order of adding dataseries to dataset and renderers to multipleRenderer
    	// should be same
    	multiRenderer.addSeriesRenderer(incomeRenderer);
    //	multiRenderer.addSeriesRenderer(expenseRenderer);
    	
    	// Getting a reference to LinearLayout of the MainActivity Layout
    	LinearLayout chartContainer = (LinearLayout) findViewById(R.id.chart_container);
    	
   		// Creating a Line Chart
   		mChart = (GraphicalView) ChartFactory.getLineChartView(getBaseContext(), dataset, multiRenderer);    	
   		
   		
   		
   		
   	
   		/*SeriesSelection ss=mChartView.getCurrentSeriesAndPoint();
   		double x=ss.getPointIndex()// index of point in chart ex: for first point its 1,for 2nd its 2.
   		double y=ss.getValue(); //value of y axis
   		double[] xy = chart.toScreenPoint(new double[] {x, y });*/
   		// so now xy[0] is yout x location on screen and xy[1] is y location on screen.
   		
   		multiRenderer.setClickEnabled(true);//
     	multiRenderer.setSelectableBuffer(10);
     	
     			
     			
     			mChart.setOnTouchListener(new OnTouchListener(){
     	             public boolean onTouch(View v, MotionEvent event) {

     	             int    pointx = (int)event.getX();
     	             int    pointy = (int)event.getY();

     	     	  	Log.i(this.toString(), "ontouch" +"x="+Integer.toString(pointx)+"y="+Integer.toString(pointy));
     	     	  	
     	     	  	
     	     	  	
     	     	  SeriesSelection seriesSelection = mChart.getCurrentSeriesAndPoint();

	     			if (seriesSelection != null) {     				
	     			
	            	// Getting the clicked Month
         			String attempt_x = attempt.get((int)seriesSelection.getXValue()).toString();

         			// Getting the y value 
         			int score_y = (int) seriesSelection.getValue();
         		//	toast.setGravity(Gravity.TOP|Gravity.LEFT, xy[0], xy[1]);
         		//	Toast.makeText(context, "dhjkhkj", Toast.LENGTH_SHORT).setGravity(Gravity.TOP|Gravity.LEFT,Integer.parseInt(seriesSelection.getPointIndex()+""),Integer.parseInt(seriesSelection.getValue()+""));
         			String msg= "You Got  " +score_y  + " in " + (Integer.parseInt(attempt_x)+1)+"  "+" Attempt.";

     	     	 /* Toast toast=Toast.makeText(context, "x="+Integer.toString(pointx)+"y="+Integer.toString(pointy), Toast.LENGTH_SHORT);
     	                toast .setGravity(Gravity.TOP|Gravity.LEFT,pointx, pointy);
     	                 toast.show();
     	                 */
     	                
         			 
     	              showToast(msg, context, pointx, pointy);
     	                 
     	                
     	     			}
     	                 return false;
     	                 
     	                 
     	             }
     	             
     	     	});

     			
     			
     			/*     			SeriesSelection seriesSelection = mChart.getCurrentSeriesAndPoint();

     			if (seriesSelection != null) {     				
     				int seriesIndex = seriesSelection.getSeriesIndex();
            	  	String selectedSeries="Income";

            	  	double[] xy = mChart.toRealPoint(0); 
            	  	Log.i(this.toString(), "OnClickListener" +seriesSelection.getPointIndex() + "y:" + seriesSelection.getValue());
 			     
            	  	
            	  	// Getting the clicked Month
          			String attempt_x = attempt.get((int)seriesSelection.getXValue()).toString();
          		//	String score_y = attempt.get((int)seriesSelection.getValue()).toString();

          			// Getting the y value 
          			int score_y = (int) seriesSelection.getValue();
          		//	toast.setGravity(Gravity.TOP|Gravity.LEFT, xy[0], xy[1]);
          		//	Toast.makeText(context, "dhjkhkj", Toast.LENGTH_SHORT).setGravity(Gravity.TOP|Gravity.LEFT,Integer.parseInt(seriesSelection.getPointIndex()+""),Integer.parseInt(seriesSelection.getValue()+""));
          			Common.showToast( "You Got  " +score_y  + " in " + (Integer.parseInt(attempt_x)+1)+"  "+" Attempt.",  context);

          			
          			
     			}*/
     		
     	
     

     	
   		// Adding the Line Chart to the LinearLayout
    	chartContainer.addView(mChart);    	
    }
    
    
    
    
    public void showToast(String message,Context context,int x,int y)
	{
		LayoutInflater inflate=((Activity)context).getLayoutInflater();
		View layout=inflate.inflate(R.layout.custum_toast_quiz, (ViewGroup)((Activity)context).findViewById(R.id.layouttoast));
		TextView text = (TextView) layout.findViewById(R.id.textscore);
		text.setText(message);
		Toast toast=Toast.makeText(context, message, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.TOP|Gravity.LEFT, x, y);
		toast.setView(layout);
		toast.show();
	}
    
    public void parseJSON(String result) {

		System.out.println(result);
		try {

			JSONObject jo=new JSONObject(result);
			String point=jo.getString("score");
			JSONArray score=new JSONArray(point);
			for (int i = 0; i < score.length(); i++) {

				scorelist.add(score.getInt(i));
			}
			//Common.showToast("REsult is "+result, context);
			System.out.println("Respo"+result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

    
}