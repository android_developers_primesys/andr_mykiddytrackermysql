package com.example.adapter;

import java.util.ArrayList;
import java.util.List;

import com.example.dto.CategoryDashbord;
import com.example.dto.Student;
import com.primesys.mitra.QuizzLevelActivity;
import com.primesys.mitra.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class QuizGridAdapter extends ArrayAdapter<CategoryDashbord>{

	Context context;
	int layoutResourceId;
	ArrayList<CategoryDashbord> data = new ArrayList<CategoryDashbord>();
	String CATEGORY_KEY="Category";
	String QUIZID_KEY="Quiz_Id";
	
 TextView txtTitle;
	ImageView imageItem;
	public QuizGridAdapter(Context context, int layoutResourceId,
			ArrayList<CategoryDashbord> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			
			row.setTag(position);

		} else {
		//	holder = (RecordHolder) row.getTag();
		}
		
		CategoryDashbord item = data.get(position);
	
		
		final TextView txtTitle=(TextView)row.findViewById(R.id.item_text);
		txtTitle.setText(item.getTitle());
		txtTitle.setTag(position);
		imageItem = (ImageView) row.findViewById(R.id.item_image);
		
		if (item.getImage()!=null) {
			imageItem.setImageBitmap(item.getImage());
		}else{
			imageItem.setBackgroundResource(R.drawable.cat_default);
		}
	

		
	

		row.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	Intent intent=new Intent(context,QuizzLevelActivity.class);
            	CategoryDashbord Cat=data.get((Integer) txtTitle.getTag());
            	System.out.println(Cat.getTitle());
    			intent.putExtra(CATEGORY_KEY,Cat.getTitle());
            	System.out.println("Quiz id in catadp"+Cat.getQuiz_id()+" "+Cat.getTitle());

    			intent.putExtra(QUIZID_KEY,Cat.getQuiz_id());   
    			context.startActivity(intent);
    			((Activity) context).overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
            }
        });

		return row;
		
		
	
		
		
		

	}

	static class RecordHolder {
		

	}
	
	
	
}
