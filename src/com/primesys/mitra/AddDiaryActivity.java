package com.primesys.mitra;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


public class AddDiaryActivity extends Activity {

	LinearLayout mContent;
	signature mSignature;
	public static String tempDir;
	public int count = 1;
	public String current = null,studentId,signPath;
	private Bitmap mBitmap;
	View mView;
	File mypath;
	byte[] imageArray;
	private String uniqueId;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	EditText text_message;
	EditText text_date;
	Button btn_create;
	TextView textNews;
	final String TAG="AddDiaryActivity";
	Context addContext=AddDiaryActivity.this;
	String type;
	ImageView image_attach;
	TextView lbl_attach;
    final static int REUQEST_CODE=100;
    String file_path="",file_name="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_adddiary);
		type=getIntent().getStringExtra("Type");  
		textNews=(TextView)findViewById(R.id.text_news);
		image_attach=(ImageView)findViewById(R.id.image_attach);
		lbl_attach=(TextView)findViewById(R.id.lbl_attach);
		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		textNews.setTypeface(custom_font);
		textNews.setText(APIController.getInstance().getNews());
		studentId=getIntent().getStringExtra("StudentID");
		mContent = (LinearLayout) findViewById(R.id.userSignature);
		mSignature = new signature(this, null);
		mSignature.setBackgroundColor(Color.WHITE);
		mContent.addView(mSignature, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		mView = mContent;
		btn_create=(Button)findViewById(R.id.btn_create);
		text_message=(EditText)findViewById(R.id.text_message);
		text_date=(EditText)findViewById(R.id.text_date);

		tempDir = Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.external_dir) + "/";
		ContextWrapper cw = new ContextWrapper(addContext);
		File directory = cw.getDir(getResources().getString(R.string.external_dir), Context.MODE_PRIVATE);

		prepareDirectory();
		uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + Math.random();
		current = uniqueId + ".png";
		mypath= new File(directory,current);

		getDate();

		mSignature = new signature(this, null);
		mSignature.setBackgroundColor(Color.WHITE);
		mContent.addView(mSignature, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);

		btn_create.setOnClickListener(new OnClickListener() 
		{        
			public void onClick(View v) 
			{
				Log.v("log_tag", "Panel Saved");
				mView.setDrawingCacheEnabled(true);
				mSignature.save(mView);
				signPath=mypath.getAbsolutePath();
				if(Common.connectionStatus)
				{
					postDiaryRequest();
				}

			}
		});
		image_attach.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.setType("file/*");
				startActivityForResult(intent,REUQEST_CODE);
			}
		});
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == REUQEST_CODE && resultCode == RESULT_OK && null != intent) {
			Uri selectedimg = intent.getData();
			file_path=getRealPathFromURI(selectedimg);
			file_name=file_path.substring(file_path.lastIndexOf("/")+1, file_path.length());
			lbl_attach.setText(file_name);
		}
		else{
			lbl_attach.setText("No file selected");
		}
	}
	private String getRealPathFromURI(Uri contentURI) {
	    String result;
	    Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
	    if (cursor == null) { // Source is Dropbox or other similar local file path
	        result = contentURI.getPath();
	    } else { 
	        cursor.moveToFirst(); 
	        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
	        result = cursor.getString(idx);
	        cursor.close();
	    }
	    return result;
	}
	// open the file explorer
	public void openFile(String minmeType) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(minmeType);
        intent.addCategory(Intent.CATEGORY_DEFAULT);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
         // if you want any file type, you can skip next line 
        sIntent.putExtra("CONTENT_TYPE", minmeType); 
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (getPackageManager().resolveActivity(sIntent, 0) != null){
            // it is device with samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent});
        }
        else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
            startActivityForResult(chooserIntent, REUQEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
        	Common.showToast("No suitable File Manager was found.", addContext);
        }
    }

	void getDate()
	{
		SimpleDateFormat sdT = new SimpleDateFormat("a");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		String sDate= sdf.format(date)+" "+sdT.format(date);
		text_date.setText(sDate);
	}
	@Override
	protected void onDestroy() {
		Log.w("GetSignature", "onDestory");
		super.onDestroy();
	}
	// clear all the fields
	void clearAll()
	{
		getDate();
		text_message.setText(" ");
		mSignature.clear();
		mView.refreshDrawableState();
		lbl_attach.setText(getResources().getString(R.string.attachments));
	}
	private String getTodaysDate() { 

		final Calendar c = Calendar.getInstance();
		int todaysDate =     (c.get(Calendar.YEAR) * 10000) + 
				((c.get(Calendar.MONTH) + 1) * 100) + 
				(c.get(Calendar.DAY_OF_MONTH));
		return(String.valueOf(todaysDate));

	}

	private String getCurrentTime() {

		final Calendar c = Calendar.getInstance();
		int currentTime =     (c.get(Calendar.HOUR_OF_DAY) * 10000) + 
				(c.get(Calendar.MINUTE) * 100) + 
				(c.get(Calendar.SECOND));
		Log.w("TIME:",String.valueOf(currentTime));
		return(String.valueOf(currentTime));
	}
	private boolean prepareDirectory() 
	{
		try
		{
			if (makedirs()) 
			{
				return true;
			} else {
				return false;
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
			Common.showToast("Could not initiate File System.. Is Sdcard mounted properly?", addContext);
			return false;
		}
	}

	private boolean makedirs() 
	{
		File tempdir = new File(tempDir);
		if (!tempdir.exists())
			tempdir.mkdirs();

		if (tempdir.isDirectory()) 
		{
			File[] files = tempdir.listFiles();
			for (File file : files) 
			{
				if (!file.delete()) 
				{
					System.out.println("Failed to delete " + file);
				}
			}
		}
		return (tempdir.isDirectory());
	}

	public class signature extends View 
	{
		private static final float STROKE_WIDTH = 5f;
		private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
		private Paint paint = new Paint();
		private Path path = new Path();
		private float lastTouchX;
		private float lastTouchY;
		private final RectF dirtyRect = new RectF();

		public signature(Context context, AttributeSet attrs) 
		{
			super(context, attrs);
			paint.setAntiAlias(true);
			paint.setColor(Color.BLACK);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStrokeWidth(STROKE_WIDTH);
		}

		public void save(View v) 
		{
			if(mBitmap == null)
			{
				mBitmap =  Bitmap.createBitmap (mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
			}
			Canvas canvas = new Canvas(mBitmap);
			try
			{
				FileOutputStream mFileOutStream = new FileOutputStream(mypath);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				v.draw(canvas); 
				mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, mFileOutStream);
				imageArray=stream.toByteArray();

				//save the signature into the DB

				mFileOutStream.flush();
				mFileOutStream.close();
				/*String url = Images.Media.insertImage(getContentResolver(), mBitmap, "title", null);
				Log.v("log_tag","url: " + url);*/
			}
			catch(Exception e) 
			{ 
				Log.v("log_tag", e.toString()); 
			} 
		}

		public void clear() 
		{
			path.reset();
			invalidate();
		}

		@Override
		protected void onDraw(Canvas canvas) 
		{
			canvas.drawPath(path, paint);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) 
		{
			float eventX = event.getX();
			float eventY = event.getY();
			btn_create.setEnabled(true);

			switch (event.getAction()) 
			{
			case MotionEvent.ACTION_DOWN:
				path.moveTo(eventX, eventY);
				lastTouchX = eventX;
				lastTouchY = eventY;
				return true;

			case MotionEvent.ACTION_MOVE:

			case MotionEvent.ACTION_UP:

				resetDirtyRect(eventX, eventY);
				int historySize = event.getHistorySize();
				for (int i = 0; i < historySize; i++) 
				{
					float historicalX = event.getHistoricalX(i);
					float historicalY = event.getHistoricalY(i);
					expandDirtyRect(historicalX, historicalY);
					path.lineTo(historicalX, historicalY);
				}
				path.lineTo(eventX, eventY);
				break;

			default:
				debug("Ignored touch event: " + event.toString());
				return false;
			}

			invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
					(int) (dirtyRect.top - HALF_STROKE_WIDTH),
					(int) (dirtyRect.right + HALF_STROKE_WIDTH),
					(int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

			lastTouchX = eventX;
			lastTouchY = eventY;

			return true;
		}

		private void debug(String string){
		}

		private void expandDirtyRect(float historicalX, float historicalY) 
		{
			if (historicalX < dirtyRect.left) 
			{
				dirtyRect.left = historicalX;
			} 
			else if (historicalX > dirtyRect.right) 
			{
				dirtyRect.right = historicalX;
			}

			if (historicalY < dirtyRect.top) 
			{
				dirtyRect.top = historicalY;
			} 
			else if (historicalY > dirtyRect.bottom) 
			{
				dirtyRect.bottom = historicalY;
			}
		}

		private void resetDirtyRect(float eventX, float eventY) 
		{
			dirtyRect.left = Math.min(lastTouchX, eventX);
			dirtyRect.right = Math.max(lastTouchX, eventX);
			dirtyRect.top = Math.min(lastTouchY, eventY);
			dirtyRect.bottom = Math.max(lastTouchY, eventY);
		}
	}
	// make volley request 
	void postDiaryRequest()
	{
		reuestQueue=Volley.newRequestQueue(addContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(addContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.URL+"ParentAPI.ASMX/SaveDigitalDairy",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseJSON(response);
				pDialog.hide();
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				if (type.contains("T")) 
				{
					params.put("from_id", Common.userid);
					params.put("to_id", studentId);
				}
				else{
					params.put("from_id", studentId);
				    params.put("to_id", "0");
				}
				params.put("message", text_message.getText().toString());
				params.put("documents", fileToBase64(file_path));
				params.put("document_name", file_name);
				if(type.equals("P"))
				{
					params.put("sign_by_parent", fileToBase64(signPath));
					params.put("sign_by_teacher", "");
				}
				else
				{
					params.put("sign_by_parent", "");
					params.put("sign_by_teacher", fileToBase64(signPath));
				}
				params.put("msg_record_dt_time", text_date.getText().toString());
				System.out.println("Diary req"+params);
				return params;
			}

		};
		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	void parseJSON(String result)
	{
		System.out.println(result);
		if(result.contains("false"))
		{
			Common.showToast("Diary created successfully", addContext);
			finish();
		}
		else
			Common.showToast("Check once again there is Error !", addContext);
	}

	String fileToBase64(String docPath)
	{
		String UploadFile="";
		try
		{
			File Uploadfilepath = new File(docPath);
			byte[]  bFileConversion = new byte[(int) Uploadfilepath.length()];
			//convert file into array of bytes
			FileInputStream fileInputStream = new FileInputStream(Uploadfilepath);
			fileInputStream.read(bFileConversion);
			UploadFile=Base64.encodeToString(bFileConversion, Base64.DEFAULT);
		}catch(Exception e)
		{
          e.printStackTrace();
		}
		return UploadFile;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
}
