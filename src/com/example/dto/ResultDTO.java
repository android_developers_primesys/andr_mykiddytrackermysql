package com.example.dto;

import java.util.HashMap;

public class ResultDTO {

	String UserId,QuizId,Category,SubCategory,Level,Score,Time_Taken,Evaluation,Current_Timestamp,mapstring;
	
	public String getTime_Taken() {
		return Time_Taken;
	}
	public void setTime_Taken(String time_Taken) {
		Time_Taken = time_Taken;
	}
	public String getEvaluation() {
		return Evaluation;
	}
	public void setEvaluation(String evaluation) {
		Evaluation = evaluation;
	}
	public String getCurrent_Timestamp() {
		return Current_Timestamp;
	}
	public void setCurrent_Timestamp(String current_Timestamp) {
		Current_Timestamp = current_Timestamp;
	}
	HashMap<String,String> QuestionAnsMap;
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getQuizId() {
		return QuizId;
	}
	public void setQuizId(String quizId) {
		QuizId = quizId;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getSubCategory() {
		return SubCategory;
	}
	public void setSubCategory(String subCategory) {
		SubCategory = subCategory;
	}
	public String getLevel() {
		return Level;
	}
	public void setLevel(String level) {
		Level = level;
	}
	public String getScore() {
		return Score;
	}
	public void setScore(String score) {
		Score = score;
	}
	
	public HashMap<String, String> getQuestionAnsMap() {
		return QuestionAnsMap;
	}
	public void setQuestionAnsMap(HashMap<String, String> questionAnsMap) {
		QuestionAnsMap = questionAnsMap;
	}
	
}
