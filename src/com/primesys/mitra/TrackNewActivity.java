package com.primesys.mitra;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.adapter.TrackPhotoAdapter;
import com.example.dto.Child;

public class TrackNewActivity extends Activity {

	ListView listStudent;
	Context commContext=TrackNewActivity.this;
	ArrayList<Child> childlist = new ArrayList<Child>();
	ProgressDialog pDialog;
	TrackPhotoAdapter padapter;
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//LoginActivity.mClient.sendMessage(makeJSON());
		setContentView(R.layout.activity_track);
		findViewById();
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);
	

		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		listStudent=(ListView)findViewById(R.id.liststudent);
		padapter=new TrackPhotoAdapter(commContext, 0, childlist);

		if (Common.connectionStatus) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				new Parenttask().executeOnExecutor(
						AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
			} else {
				new Parenttask().execute();
			}
		}
	}

	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(commContext)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(commContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(commContext)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(commContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}


	}

	private void findViewById() {
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(commContext)&& APIController.getInstance().getSchool_logo()!=null) {

		imageLoader = new ImageLoader(
				Volley.newRequestQueue(commContext), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}

	// onclick menu item
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	// READ the child information from the service
	class Parenttask extends AsyncTask<Void, Void, String> {
		String url = Common.URL + "ParentAPI.asmx/GetChildInfo";

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(commContext);
			pDialog.setMessage("Progress wait....");
			pDialog.setCancelable(false);
			pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String result = "User is not registered";
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			try {
				List<NameValuePair> postParameter = new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("ParentId",
						Common.userid));
				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {
				result = e.getMessage();
			}
			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			pDialog.hide();
			if (result.contains("error")) {
				//CustomDialog.displayDialog("No Child", commContext);
			} else {
				parseJSON(result);
			}
		}
	}

	// parse the Result which is coming from the JSon
	void parseJSON(String result) {
		try {
			JSONArray array = new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo = array.getJSONObject(i);
				Child child = new Child();
				child.setStudentId(jo.getInt("StudentID"));
				child.setFirstname(jo.getString("FirstName"));
				child.setMiddlename(jo.getString("MiddleName"));
				child.setLastname(jo.getString("LastName"));
				child.setGender(jo.getString("Gender"));
				child.setAdmissiondate(jo.getString("AdmissionDate"));
				child.setClassid(jo.getInt("ClassID"));
				child.setDeviceid(jo.getString("DeviceID"));
				child.setDob(jo.getString("dob"));
				child.setPrim_ContactNo(jo.getString("Prim_ContactNo"));
				child.setPrim_EmailID(jo.getString("Prim_EmailID"));
				child.setAddress(jo.getString("Address"));
				child.setCity(jo.getString("City"));
				child.setState(jo.getString("State"));
				String photo=jo.getString("Photo");
				photo=photo.replace("~", "");
				child.setPhoto(photo);
				child.setActiveStatus(jo.getString("ActiveStatus"));
				child.setCreatedBy(jo.getString("CreatedBy"));
				child.setUpdatedDate(jo.getString("UpdatedDate"));
				child.setLatitude(jo.getString("Latitude"));
				child.setLongitude(jo.getString("Longitude"));
				childlist.add(child);	
			}
			padapter.notifyDataSetChanged();
			listStudent.setAdapter(padapter);
		} catch (JSONException e) {
		}
	}
}
