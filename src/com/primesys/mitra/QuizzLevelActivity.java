package com.primesys.mitra;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adapter.QuizGridAdapter;
import com.example.adapter.QuizLevelAdpter;
import com.example.adapter.level_adpter;
import com.example.dto.CategoryDashbord;
import com.example.dto.QuizLevelDTO;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class QuizzLevelActivity  extends Activity{
	String CATEGORY_KEY="Category",category_label,LEVEL_NO="Levelno",quiz_id;
	Context context=QuizzLevelActivity.this;
	LinearLayout level1,level2,level3,level4,level5,level6,level7,level8,level9,
	level11,level22,level33,level44,level55,level66,level77,level88,level99;
	
	TextView score1,status1,rank1,score2,status2,rank2,score3,status3,rank3,score4,status4,rank4,
			score5,status5,rank5,score6,status6,rank6,score7,status7,rank7,score8,status8,rank8,score9,status9,rank9;
	ArrayList<QuizLevelDTO> Levelmetedata=new ArrayList<QuizLevelDTO>();
	private ArrayList<QuizLevelDTO> gridArray=new ArrayList<QuizLevelDTO>();
	public ArrayList<String> AllLevel=new ArrayList<String>();

	private RequestQueue reuestQueue;
	private StringRequest stringRequest;
	private Object TAG="MetaRequest";
	private GridView gridView;
	String QUIZID_KEY="Quiz_Id";
	public QuizLevelAdpter quizleveladpter;
	public	level_adpter leveladpter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quiz_level_grid);
		FindViewByID();
		gridView = (GridView) findViewById(R.id.gridViewlevel);

		Intent intent=getIntent();
		category_label=intent.getStringExtra(CATEGORY_KEY);
		quiz_id=intent.getStringExtra(QUIZID_KEY);
		
		
    	System.out.println("Quiz id in levelacti"+quiz_id+" "+category_label);

		if (Common.getConnectivityStatus(context)) {
			try {
				GetallLevel();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(AllLevel+"");
		System.out.println("56656566"+Levelmetedata+"");

		
		
	}





	



	




	private void GetallLevel() {
		
		String url="http://192.168.1.110:8080/MyKiddyTrackerAPI/MyKiddy/ExamAPI/GetTotalLevelUsewise";

		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.SQLURL+"SQLQuiz/GetLevelList",new Response.Listener<String>() {
			
			@Override
			public void onResponse(String response) {
			

				parseJSONallLevel(response);
				pDialog.hide();
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSONallLevel(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
			
				params.put("user_id",Common.userid);
			//	params.put("Category",category_label);
				params.put("quizid",quiz_id);

				System.out.println("REq----------"+params);
				return params;
			}

		};


		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}









	protected void parseJSONallLevel(String response) {
		System.out.println("Respoall lecvel "+ response);
		try {
			
			JSONArray respo=new JSONArray(response);
			
			for (int i = 0; i < respo.length(); i++) {
			JSONObject level=respo.getJSONObject(i);
			AllLevel.add(level.getString("Level"));
			
			}
			

			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
			//	Getleveldata();
				
				leveladpter =new level_adpter(context,R.layout.quiz_level_row,AllLevel);
				
				
				gridView.setAdapter(leveladpter);
			

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
	}









	private void FindViewByID() {

	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}



	
	private void Getleveldata() {
		String url="http://192.168.1.110:8080/MyKiddyTrackerAPI/MyKiddy/ExamAPI/GetUserLevel";

		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setTitle("Progress wait.......");
		pDialog.setCancelable(false);
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.SQLURL+"ExamAPI/GetUserLevel",new Response.Listener<String>() {
			
			@Override
			public void onResponse(String response) {
			

				parseJSON(response);
				pDialog.hide();
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
			
				params.put("UserId",Common.userid);
				params.put("Category",category_label);
				params.put("School_id",Common.schoolId);

				System.out.println("REqgetlve meta----------"+params);
				return params;
			}

		};


		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	public void parseJSON(String result) {
		System.out.println("Respogetlve meta "+result);
		try {
			JSONArray rspoarray=new JSONArray(result);

			for(int i=0;i<AllLevel.size();i++)
			{
					
				QuizLevelDTO dto=new QuizLevelDTO();
				/*if(AllLevel.get(i).equals(jo.getString("Level"))){
					
				}*/
				if(i<rspoarray.length()){
					JSONObject jo=rspoarray.getJSONObject(i);
					dto.setRank(jo.getString("Rank"));
					dto.setStatus(jo.getString("Status"));;
					dto.setScore(jo.getString("Score"));

					dto.setQuizId(quiz_id);
					dto.setCategory_label(category_label);
					System.err.println(jo.getString("Score"));
				}else
					dto.setScore("0");
				dto.setQuizId(quiz_id);
				dto.setCategory_label(category_label);
				//dto.setLevel(jo.getString("Level"));
				dto.setLevel(AllLevel.get(i));
				
				Levelmetedata.add(dto);
			}
			quizleveladpter =new QuizLevelAdpter(this,R.layout.quiz_level_row,Levelmetedata);
			
			gridView.setAdapter(quizleveladpter);
		

			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
