package com.example.dto;

import java.io.Serializable;

public class UserContact implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String name,phoneno,email,contactno;



	public UserContact() {
		// TODO Auto-generated constructor stub
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getPhoneno() {
		return phoneno;
	}



	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getContactno() {
		return contactno;
	}



	public void setContactno(String contactno) {
		this.contactno = contactno;
	}


}
