package com.primesys.mitra;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adapter.FriendListAdapter;
import com.example.dto.FrindsDTO;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;

public class DemoUserActivity extends Activity {

	static Context demoContext;
	static ListView listFirends;
	TextView textNews,lbltext;
	TextView textschool;
	Context context=DemoUserActivity.this;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG=DemoUserActivity.class.getSimpleName();
	static ArrayList<FrindsDTO> listFrnd;
	ProgressDialog pDialog;
	static FriendListAdapter adapter;
	public static String sharedMsg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_demo_user);
		findViewById();
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);
		


		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		// handle the intent to share the data with other frnds
		Intent shareIntent=getIntent();
		String action=shareIntent.getAction();
		String type=shareIntent.getType();

		if(Intent.ACTION_SEND.equals(action)&&type!=null)
			if("text/plain".equals(type))
				sharedMsg=shareIntent.getStringExtra(Intent.EXTRA_TEXT);
			else if("image/*".equals(type))
				hanldeIMage(shareIntent);

		demoContext=DemoUserActivity.this;
		textNews=(TextView)findViewById(R.id.text_news);
		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		textNews.setTypeface(custom_font);
		textNews.setText(APIController.getInstance().getNews());
		listFirends=(ListView)findViewById(R.id.list_frnds);
		listFrnd=new ArrayList<FrindsDTO>();
		adapter=new FriendListAdapter(demoContext, listFrnd);
		if (Common.connectionStatus) {
			Common.flag=0;
			LoginActivity.mClient.sendMessage(getFrndList());
			LoginActivity.mClient.sendMessage(getList());
		}
	}
	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(context), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(context), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}


	}
	private void findViewById() {
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {
		imageLoader = new ImageLoader(
				Volley.newRequestQueue(context), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
	}
	//this is for group
	String getList()
	{
		String result="";
		try
		{
			JSONObject jo=new JSONObject();
			jo.put("event", "get_groups_listing");
			jo.put("user_id",Common.userid);
			result=jo.toString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	//this is for friend
	String getFrndList()
	{
		String result="";
		try
		{
			JSONObject jo=new JSONObject();
			jo.put("event", "list_friends");
			jo.put("user_id",Common.userid);
			result=jo.toString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		listFrnd=new ArrayList<FrindsDTO>();
		listFrnd.removeAll(listFrnd);
		listFrnd.clear();
		adapter=new FriendListAdapter(demoContext, listFrnd);
		if (Common.connectionStatus) {
			Common.flag=0;
			LoginActivity.mClient.sendMessage(getFrndList());
			LoginActivity.mClient.sendMessage(getList());
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflate=getMenuInflater();
		inflate.inflate(R.menu.menu_adduser, menu);
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		menu.findItem(R.id.adduser).setIcon(new IconDrawable(this, IconValue.fa_user_plus).colorRes(R.color.primary).actionBarSize());
		menu.findItem(R.id.creategp).setIcon(new IconDrawable(this, IconValue.fa_users).colorRes(R.color.primary).actionBarSize());
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.adduser:
			Intent intent =new Intent(demoContext,AddUserActivity.class);
			startActivity(intent);
			return true;
		case R.id.back:
			onBackPressed();
			return true;
		case R.id.creategp:
			Intent i=new Intent(demoContext,CreateGroup.class);
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	static void  parseJSON(String result)
	{
		try
		{
			JSONObject jmain=new JSONObject(result);
			JSONObject jData=jmain.getJSONObject("data");
			JSONArray array=jData.getJSONArray("groups");
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo=array.getJSONObject(i);
				FrindsDTO frnd=new FrindsDTO();
				frnd.setMemberId(jo.getInt("grp_id"));
				frnd.setMemberName(jo.getString("grp_name"));
				frnd.setGroup(true);
				listFrnd.add(frnd);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			if(listFrnd!=null){
				listFirends.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			}
		}
	}
	//parseJSON for RESTAPI
	static void restParseJSON(String result)
	{
		try
		{
			JSONObject jmain=new JSONObject(result);
			JSONArray jData=jmain.getJSONArray("data");
			for (int i = 0; i < jData.length(); i++) {
				JSONObject jo=jData.getJSONObject(i);
				FrindsDTO frnd=new FrindsDTO();
				frnd.setMemberId(jo.getInt("friend_id"));
				frnd.setMemberName(jo.getString("friend_name"));
				frnd.setGroup(false);
				frnd.setStatus(jo.getString("status"));
				frnd.setSent(jo.getInt("sent"));
				listFrnd.add(frnd);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			listFirends.setAdapter(null);
			if(listFrnd!=null)
			{
				listFirends.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			}
		}
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if(listFrnd!=null)
			outState.putSerializable("friendlist", listFrnd); 
		super.onSaveInstanceState(outState);
	}
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if(savedInstanceState!=null)
		{
			listFrnd.clear();
			listFrnd=(ArrayList<FrindsDTO>) savedInstanceState.getSerializable("friendlist");
			if(listFrnd!=null){
				adapter=new FriendListAdapter(demoContext, listFrnd);
				listFirends.setAdapter(adapter);
			}
			Log.e("size",""+listFrnd.size());
		}
	}
	// share the image
	void hanldeIMage(Intent intent)
	{
		Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
		if (imageUri != null) {
			Common.showToast(imageUri.toString(), demoContext);
		}	
	}
	/*@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}*/
}
