package com.primesys.mitra;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.db.DBHelper;
import com.example.dto.GroupInfo;
import com.example.dto.MessageMain;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;
import com.primesys.emojilibrary.EmojiconEditText;
import com.primesys.emojilibrary.EmojiconGridView.OnEmojiconClickedListener;
import com.primesys.emojilibrary.EmojiconsPopup;
import com.primesys.emojilibrary.EmojiconsPopup.OnEmojiconBackspaceClickedListener;
import com.primesys.emojilibrary.EmojiconsPopup.OnSoftKeyboardOpenCloseListener;
import com.primesys.emojilibrary.emoji.Emojicon;
public class MainActivity extends Activity  {
	ListView mList;
	static ArrayList<MessageMain> arrayList = new ArrayList<MessageMain>();
	static MyCustomAdapter mAdapter ;
	private Client mClient=LoginActivity.mClient;
	private String to;
	String CurrentTime,name; 
	public static MessageMain msg=new MessageMain();
	static Date date=new Date();
	public static String message ;
	static String formattedDate = new SimpleDateFormat("dd MMM yyyy hh:mm:ss").format(date);
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	int selectedPosition;
	String studentId="A";
	boolean isGroup=false;
	Context mainContext=MainActivity.this;
	static ArrayList<GroupInfo> listGrp;
	String SharedMsg; 
	ImageView attachBtn;
	DBHelper helper=DBHelper.getInstance(mainContext);

	final static int RESULT_LOAD_IMAGE=100;
	String picturePath,filename;
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FindeViewBYID();
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);



		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		Common.to=getIntent().getStringExtra("to");
		to=Common.to;
		name=getIntent().getStringExtra("name");
		isGroup=getIntent().getBooleanExtra("isGroup", false);
		if(getIntent().getStringExtra("student")!=null)
			studentId=getIntent().getStringExtra("student");
		listGrp=new ArrayList<GroupInfo>();
		if(Common.connectionStatus&&isGroup)
			LoginActivity.mClient.sendMessage(getGroupMemberListing());
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		final EmojiconEditText editText = (EmojiconEditText) findViewById(R.id.editText);
		ImageView send = (ImageView)findViewById(R.id.send_button);
		final View rootView = findViewById(R.id.root_view);
		final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);
		//attachBtn=(ImageView)findViewById(R.id.attach_button);
		//relate the listView from java to the one created in xml
		mList = (ListView)findViewById(R.id.list);
		setTitle(name);
		APIController.getInstance().setRead(1);
		DBHelper.getInstance(mainContext).updateMessages(to,isGroup);
		getMessageData();
		final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);
		//Will automatically set size according to the soft keyboard size
		popup.setSizeForSoftKeyboard();
		mAdapter = new MyCustomAdapter(mainContext, arrayList,name);
		mList.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
		if(getIntent().getStringExtra(Intent.EXTRA_TEXT)!=null){
			editText.setText(getIntent().getStringExtra(Intent.EXTRA_TEXT));
		}
		final int abTitleId = getResources().getIdentifier("action_bar_title", "id", "android");
		findViewById(abTitleId).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isGroup)
				{
					Intent i=new Intent(MainActivity.this,GroupDetails.class);
					i.putExtra("GroupId", to+"");
					i.putExtra("grp", listGrp);
					startActivity(i);
				}
			}
		});
		send.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String message = editText.getText().toString();
				if(message.trim().length()!=0)
				{
					MessageMain m=new MessageMain();
					m.setMesageText(message);
					m.setDate_time(Common.convertToLong(formattedDate));
					m.setEvent("msg");
					m.setTo(Common.to);
					m.setApp_id(Common.app_id);
					m.setFrom(Common.userid);    
					m.setName(name);
					m.setRef_id(Common.getRefNo());
					arrayList.add(m);
					if (mClient != null) {
						if(isGroup)
						{
							m.setType("g");
							mClient.sendMessage(grpMessage(message,m.getRef_id()));
							helper.insertMessage(m);
						}
						else
						{
							m.setType("s");
							if(studentId==null||studentId==""){
								mClient.sendMessage(m);
								helper.insertMessage(m);
							}
							else
							{
								LoginActivity.mClient.sendMessage(schoolMessage(message,m.getRef_id()));
								helper.insertMessage(m,studentId);
							}
						}
					}
				}else{
					Common.showToast("Enter message",mainContext);
				}
				mAdapter.notifyDataSetChanged();
				editText.setText("");
			}
		});

		mList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectedPosition=position;
				startActionMode(modeCallBack);
				view.setSelected(true);
				return true;
			}
		});
		popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {
			@Override
			public void onEmojiconClicked(Emojicon emojicon) {
				editText.append(emojicon.getEmoji());
			}
		});
		//Set on backspace click listener
		popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {
			@Override
			public void onEmojiconBackspaceClicked(View v) {
				KeyEvent event = new KeyEvent(
						0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
				editText.dispatchKeyEvent(event);
			}
		});
		//If the emoji popup is dismissed, change emojiButton to smiley icon
		popup.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() {
				changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
			}
		});
		//If the text keyboard closes, also dismiss the emoji popup
		popup.setOnSoftKeyboardOpenCloseListener(new OnSoftKeyboardOpenCloseListener() {
			@Override
			public void onKeyboardOpen(int keyBoardHeight) {
			}
			@Override
			public void onKeyboardClose() {
				if(popup.isShowing())
					popup.dismiss();
			}
		});
		//On emoji clicked, add it to edittext
		popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {
			@Override
			public void onEmojiconClicked(Emojicon emojicon) {
				editText.append(emojicon.getEmoji());
			}
		});
		//On backspace clicked, emulate the KEYCODE_DEL key event
		popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {
			@Override
			public void onEmojiconBackspaceClicked(View v) {
				KeyEvent event = new KeyEvent(
						0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
				editText.dispatchKeyEvent(event);
			}
		});
		// To toggle between text keyboard and emoji keyboard keyboard(Popup)
		emojiButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//If popup is not showing => emoji keyboard is not visible, we need to show it
				if(!popup.isShowing()){
					//If keyboard is visible, simply show the emoji popup
					if(popup.isKeyBoardOpen()){
						popup.showAtBottom();
						changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
					}
					//else, open the text keyboard first and immediately after that show the emoji popup
					else{
						editText.setFocusableInTouchMode(true);
						editText.requestFocus();
						popup.showAtBottomPending();
						final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
						changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
					}
				}
				//If popup is showing, simply dismiss it to show the undelying text keyboard
				else{
					popup.dismiss();
				}
			}
		}); 
		//attach the image for sending
		/*	attachBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(i, RESULT_LOAD_IMAGE);
			}
		});*/
	}
	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(mainContext)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(mainContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(mainContext)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(mainContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}


	}
	private void FindeViewBYID() {
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(mainContext)&& APIController.getInstance().getSchool_logo()!=null) {
		imageLoader = new ImageLoader(
				Volley.newRequestQueue(mainContext), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent childlist) {
		try
		{	
			if (requestCode == 100 && resultCode == RESULT_OK && null != childlist) {
				Uri selectedImage = childlist.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				File file=new File(picturePath);
				filename=file.getName();
				cursor.close();
				decodeFile(picturePath);
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public void decodeFile(String filePath) {
		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, o);

		// The new size we want to scale to
		final int REQUIRED_SIZE = 1024;

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
				break;
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		Bitmap bitmap = BitmapFactory.decodeFile(filePath, o2);
		bitmap = Bitmap.createScaledBitmap(bitmap,400, 400, true);
	}

	public void onBackPressed() {
		APIController.getInstance().setRead(0);
		super.onBackPressed();
	};
	String getGroupMemberListing()
	{
		String result="";
		try
		{
			JSONObject jo=new JSONObject();
			jo.put("event", "get_group_members_listing");
			jo.put("group_id",  to+"");
			result=jo.toString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	static void parseJSON(String result)
	{
		try
		{
			String adminID=new JSONObject(result).getJSONObject("data").getJSONArray("members").getJSONObject(0).getString("admin_id");
			JSONArray membrs=new JSONObject(result).getJSONObject("data").getJSONArray("members").getJSONObject(0).getJSONArray("members");
			for (int i = 0; i <membrs.length(); i++) {
				GroupInfo g=new GroupInfo();
				JSONObject jo=membrs.getJSONObject(i);
				g.setParticipantId(jo.getInt("user_id")+"");
				if(jo.isNull("user_name"))
					g.setParticipantName("");
				else
					g.setParticipantName(jo.getString("user_name"));
				if(adminID.equals(g.getParticipantId()))
					g.setType("Admin");
				else
					g.setType("Member");
				listGrp.add(g);
			}
			mAdapter.notifyDataSetChanged();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_dd, menu);
		return super.onCreateOptionsMenu(menu);

	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if(!isGroup)
			menu.removeItem(R.id.add);
		else
			menu.findItem(R.id.add).setIcon(new IconDrawable(this, IconValue.fa_user_plus).colorRes(R.color.primary).actionBarSize()).setTitle("Add participant");
		return super.onPrepareOptionsMenu(menu);
	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
		{
		/*	if (Common.freindflag) {
				Intent friendlist=new Intent(mainContext, DemoUserActivity.class);
				startActivity(friendlist);
			}
			Common.freindflag=false;*/
			onBackPressed();
/*			finish();
*/			return super.onOptionsItemSelected(item);
		}
		case R.id.add:
		{
			Intent i=new Intent(MainActivity.this,AddParticpants.class);
			i.putExtra("GroupId", to);
			startActivity(i);
			return super.onOptionsItemSelected(item);
		}
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("name", name);
		outState.putString("to", to);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if(savedInstanceState!=null)
		{
			name=savedInstanceState.getString("name");
			to=savedInstanceState.getString("to");
		}
	}

	//get the data from the URL
	void getMessageData()
	{
		DBHelper helper=DBHelper.getInstance(MainActivity.this);
		arrayList=helper.getAllMessages(Common.userid, Common.to,isGroup,studentId);
	}

	private ActionMode.Callback modeCallBack = new ActionMode.Callback() {

		public boolean onPrepareActionMode(ActionMode mode, Menu menu){   
			mode.getMenuInflater().inflate(R.menu.cab, menu);
			return true;
		}
		public void onDestroyActionMode(ActionMode mode) {
			mList.setSelection(0);
			mode = null;  
		}

		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			return true;
		}

		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.menu_copy:
				ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE); 
				ClipData clip = ClipData.newPlainText("message", arrayList.get(selectedPosition).getMesageText());
				clipboard.setPrimaryClip(clip);
				mode.finish();
				return true;

			default:
				return false;
			}
		}
	};

	private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId){
		iconToBeChanged.setImageResource(drawableResourceId);
	}
	// send the message to group
	String grpMessage(String msg, long ref)
	{
		String result="";
		try
		{
			JSONObject jo=new JSONObject();
			jo.put("event", "group_msg");
			jo.put("group_id", to);
			jo.put("user_id",Common.userid);
			jo.put("msg", msg);
			jo.put("ref_id",ref);
			result=jo.toString();
			//System.out.println("MainActivity.grpMessage()"+result);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	//school messages
	String schoolMessage(String msg,long refNO)
	{
		JSONObject jmsg = new JSONObject();
		try
		{
			jmsg.put("event", "school_msg");
			jmsg.put("app_id",Common.app_id);
			jmsg.put("from",Common.userid);
			jmsg.put("msg",msg);
			jmsg.put("to", to);
			jmsg.put("student_id",studentId);
			jmsg.put("ref_id",refNO);
			jmsg.put("type", "s");
		}catch(Exception e){}
		return jmsg.toString();
	}


}




