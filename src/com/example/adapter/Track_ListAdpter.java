package com.example.adapter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.dto.Sales_person;
import com.primesys.friendtrack.CircleTransform;
import com.primesys.friendtrack.ShowMap;
import com.primesys.mitra.CircularNetworkImageView;
import com.primesys.mitra.Common;
import com.primesys.mitra.R;
import com.squareup.picasso.Picasso;
public class Track_ListAdpter  extends ArrayAdapter<Sales_person>{
	Context context;
	ArrayList<Sales_person> data=null;
	ArrayList<Sales_person> datacopy;
	ImageLoader imageLoader;
	Bitmap bitmap;
	public Track_ListAdpter(Context context, int resource, ArrayList<Sales_person> tracklist) {
		super(context, resource, tracklist);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data=tracklist;
		this.datacopy = new ArrayList<Sales_person>();
		this.datacopy.addAll(tracklist);
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		try
		{ 
			final Sales_person s=data.get(position); 
			if(convertView==null)
			{
				LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView=inflater.inflate(R.layout.friendlist_row, parent,false);
			}
		//	convertView.setBackgroundResource(R.drawable.table_bg);
			TextView text_name=(TextView)convertView.findViewById(R.id.txtname);
			TextView text_phone=(TextView)convertView.findViewById(R.id.txtphone);
			TextView text_status=(TextView)convertView.findViewById(R.id.tv_status);

			
			Button track=(Button)convertView.findViewById(R.id.track);
			CircularNetworkImageView proimg=(CircularNetworkImageView)convertView.findViewById(R.id.image_pro);
			proimg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_boy));
			text_name.setText(s.getName());
			text_phone.setText(s.getContactNumber());
			
			if (s.getStatus().equals("1")) {
				text_status.setText("Accepted");

			}else if(s.getStatus().equals("2")) {
				text_status.setText("Pending");

			}else if(s.getStatus().equals("3")) {
				text_status.setText("Rejected");

			}
			
			
		
			
			//Set pic of user sales person
			CircularNetworkImageView netCircle=new CircularNetworkImageView(context);

			/*if(s.getPhoto()!=null){
				String photo=s.getPhoto();
				if(photo!=""){
					ImageLoader.ImageCache imageCache = new LruBitmapCache();
					imageLoader = new ImageLoader(
							Volley.newRequestQueue(context), imageCache);
					(proimg).setImageUrl(
							Common.Relative_URL+photo, imageLoader);
					((CircularNetworkImageView) proimg)
					.setDefaultImageResId(R.drawable.ic_profile);
					((CircularNetworkImageView) proimg)
					.setErrorImageResId(R.drawable.ic_profile);
							}else{
					 bitmap = ((BitmapDrawable)proimg.getDrawable()).getBitmap();	
						GraphicsUtil graphicUtil = new GraphicsUtil();
						// picView.setImageBitmap(graphicUtil.getRoundedShape(thePic,(float)1.5,92));
						
						proimg.setImageBitmap(graphicUtil.getCircleBitmap(
								bitmap, 16));				
						}
				}
				
				else{
					 bitmap = ((BitmapDrawable)proimg.getDrawable()).getBitmap();	
					proimg.setImageBitmap(netCircle.getRoundedShape(bitmap));
				}*/

			if(s.getPhoto()!=null){
				String photo=s.getPhoto();
			// Picasso.with(context).load(Common.Relative_URL+photo).into(proimg);
			 Picasso.with(context).load(Common.relativeurl+photo).transform(new CircleTransform()).into(proimg);

			}else{
				 bitmap = ((BitmapDrawable)proimg.getDrawable()).getBitmap();	
				proimg.setImageBitmap(netCircle.getRoundedShape(bitmap));
			}
			track.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
				
					if (s.getStatus().equals("1")) {
						Intent intent=new Intent(context,ShowMap.class);
						System.err.println("Inside onclick"+s.getID());
							intent.putExtra("InvitedId",s.getID());
							intent.putExtra("photo",s.getPhoto());

							((Activity)context).startActivity(intent);
							((Activity)context).overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
						}
					else {
						Common.showToast("Request is Not Accepted",context);
					}
					}
					
			});

		}catch(Exception e)
		{
         e.printStackTrace();
		}

		return convertView;
	}
	/*public void filter(String charText) {
		// TODO Auto-generated method stub
		// Filter Class
	
			charText = charText.toLowerCase(Locale.getDefault());
			data.clear();
			if (charText.length() == 0) {
				data.addAll(datacopy);
			} else {
				for (product p : datacopy) {
					if (//p.getStock_Group().toLowerCase(Locale.getDefault()).contains(charText) ||
						p.getStock_subGroup().toLowerCase(Locale.getDefault()).contains(charText) || 
						p.getProduct_name().toLowerCase(Locale.getDefault()).contains(charText)	
						) {
						data.add(p);
					}
				}
			}
			notifyDataSetChanged();
		
	}
*/
}
