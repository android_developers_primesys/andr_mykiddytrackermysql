package com.primesys.friendtrack;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;



import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adapter.Track_ListAdpterInvitation;
import com.example.dto.Sales_person;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;




import com.primesys.mitra.Common;
import com.primesys.mitra.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

public class Invitationlist extends Activity {
	
	ListView personlist;
	Context context=Invitationlist.this;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG="REquest";
	ArrayList<Sales_person> tracklist=new ArrayList<Sales_person>();
	Track_ListAdpterInvitation padpter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_track__user);
		findviewbyid();
		if(Common.getConnectivityStatus(context)){
			GetAllTrackperson();
		}
		
	}

	private void GetAllTrackperson() {


		reuestQueue = Volley.newRequestQueue(context);
		final ProgressDialog pDialog = new ProgressDialog(context);
		pDialog.setTitle("Please wait.......");
		pDialog.setCancelable(true);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.TrackURL+"UserServiceAPI/GetInvitationList",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseData(response);
				System.err.println("Track Data"+response);
				
				pDialog.hide();
			}
		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseData(new String(error.networkResponse.data));
				}
			}
		})  {
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("userid",Common.userid);
				System.err.println("Track Req--- "+params);
				return params;
			}
		};
		stringRequest.setTag(TAG);
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	
	
	}

	protected void parseData(String result) {
		
		// TODO Auto-generated method stub
		System.out.println("Invitation list ------"+result);
		String response = "";
		try {
			Sales_person p;
			JSONArray array = new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				p=new Sales_person();
				JSONObject jo = array.getJSONObject(i);
				p.setName(jo.getString("Name"));
				p.setContactNumber(jo.getString("ContactNumber"));
				p.setID(jo.getString("id"));
				p.setStatus(jo.getString("status"));
				p.setInvi_Id(jo.getString("Invi_Id"));
				/*if(jo.has("photo"))
				p.setPhoto(jo.getString("photo"));
				else p.setPhoto(null);*/
					tracklist.add(p);

			

			}

			if(tracklist.size()>0){
				System.err.println("personlist"+tracklist);
				padpter=new Track_ListAdpterInvitation(context, 0, tracklist);

				personlist.setAdapter(padpter);
			}else
				Common.showToast("You Don't have a Request", context);

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			
		}
	
		
	
	}

	private void findviewbyid() {
		
	personlist=(ListView) findViewById(R.id.person_list);
	
	}

	
}
