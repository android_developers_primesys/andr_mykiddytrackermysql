package com.example.dto;

import java.io.Serializable;

public class Child implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int studentId,parentid,classid;
	String deviceid;
	String firstname,middlename,lastname,gender,Admissiondate,dob,Prim_ContactNo,Prim_EmailID,Address,City,State;
	String ActiveStatus,CreatedBy,UpdatedDate,photo;
	String userType,school_id;
	String latitude,longitude;
	 String fullname;
	public String getPhoto() {
		return photo;
	}
	public String getFullname() {
		return fullname;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getSchool_id() {
		return school_id;
	}
	public void setSchool_id(String school_id) {
		this.school_id = school_id;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public int getParentid() {
		return parentid;
	}
	public void setParentid(int parentid) {
		this.parentid = parentid;
	}
	public int getClassid() {
		return classid;
	}
	public void setClassid(int classid) {
		this.classid = classid;
	}
	public String getDeviceid() {
		return deviceid;
	}
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAdmissiondate() {
		return Admissiondate;
	}
	public void setAdmissiondate(String admissiondate) {
		Admissiondate = admissiondate;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getPrim_ContactNo() {
		return Prim_ContactNo;
	}
	public void setPrim_ContactNo(String prim_ContactNo) {
		Prim_ContactNo = prim_ContactNo;
	}
	public String getPrim_EmailID() {
		return Prim_EmailID;
	}
	public void setPrim_EmailID(String prim_EmailID) {
		Prim_EmailID = prim_EmailID;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	
	public String getActiveStatus() {
		return ActiveStatus;
	}
	public void setActiveStatus(String activeStatus) {
		ActiveStatus = activeStatus;
	}
	public String getCreatedBy() {
		return CreatedBy;
	}
	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}
	public String getUpdatedDate() {
		return UpdatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		UpdatedDate = updatedDate;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public void setFullname(String name) {
		// TODO Auto-generated method stub
		this.fullname = name;
	}


}
