package com.primesys.mitra;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.Volley;
import com.example.db.DBHelper;
import com.example.dto.Child;

public class CommunicationNew extends Activity {

	public static String StudentD;
	ListView listStudent;
	Context commContext=CommunicationNew.this;
	ArrayList<Child> childlist = new ArrayList<Child>();
	ProgressDialog pDialog;
	PhotoAdapter padapter;
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	final static int RESULT_LOAD_IMAGE=100;
	String picturePath,filename;
	Bitmap bitmap;
	String studenID,studentName;
	int pos;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_comnew);
		findViewbYID();
		listStudent=(ListView)findViewById(R.id.liststudent);

		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);
	


		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		padapter=new PhotoAdapter(0);

		/*	if (Common.connectionStatus) {*/
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			new Parenttask().executeOnExecutor(
					AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);

		} else {
			new Parenttask().execute();
		}
		/*}*/

	}
	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(commContext)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(commContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			if (Common.getConnectivityStatus(commContext)&& APIController.getInstance().getSchool_logo()!=null) {
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(commContext)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(commContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			}
			Updatestatus=0;
			showtime=6000;
		}


	}
	private void findViewbYID() {
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(commContext)&& APIController.getInstance().getSchool_logo()!=null) {
		imageLoader = new ImageLoader(
				Volley.newRequestQueue(commContext), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
		

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}

	// onclick menu item
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	// READ the child information from the service
	class Parenttask extends AsyncTask<Void, Void, String> {
		String url = Common.URL + "ParentAPI.asmx/GetChildInfo";

		@Override
		protected void onPreExecute() {
			pDialog = new ProgressDialog(commContext);
			pDialog.setMessage("Progress wait....");
			pDialog.setCancelable(false);
			pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
			pDialog.show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String result = "User is not registered";

			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			try {
				List<NameValuePair> postParameter = new ArrayList<NameValuePair>(1);
				postParameter.add(new BasicNameValuePair("ParentId",
						Common.userid));

				httpPost.setEntity(new UrlEncodedFormEntity(postParameter));
				HttpResponse response = httpClient.execute(httpPost);
				result = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {
				result = e.getMessage();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			if (result.contains("error")) {
				//CustomDialog.displayDialog("No Child", commContext);
			} else {
				parseJSON(result);
			}
		}
	}

	// parse the Result which is coming from the JSon
	void parseJSON(String result) {
		try {
			JSONArray array = new JSONArray(result);
			for (int i = 0; i < array.length(); i++) {
				JSONObject jo = array.getJSONObject(i);
				Child child = new Child();
				child.setStudentId(jo.getInt("StudentID"));
				StudentD=jo.getInt("StudentID")+"";
				child.setFirstname(jo.getString("FirstName"));
				child.setMiddlename(jo.getString("MiddleName"));
				child.setLastname(jo.getString("LastName"));
				child.setGender(jo.getString("Gender"));
				child.setAdmissiondate(jo.getString("AdmissionDate"));
				child.setClassid(jo.getInt("ClassID"));
				child.setDeviceid(jo.getString("DeviceID"));
				child.setDob(jo.getString("dob"));
				child.setPrim_ContactNo(jo.getString("Prim_ContactNo"));
				child.setPrim_EmailID(jo.getString("Prim_EmailID"));
				child.setAddress(jo.getString("Address"));
				child.setCity(jo.getString("City"));
				child.setState(jo.getString("State"));
				String photo=jo.getString("Photo");
				photo=photo.replace("~", "");
				child.setPhoto(photo);
				child.setActiveStatus(jo.getString("ActiveStatus"));
				child.setCreatedBy(jo.getString("CreatedBy"));
				child.setUpdatedDate(jo.getString("UpdatedDate"));
				childlist.add(child);
			}
			padapter.notifyDataSetChanged();
			listStudent.setAdapter(padapter);
		} catch (JSONException e) {
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent childlist) {
		try
		{	
			if (requestCode == 100 && resultCode == RESULT_OK && null != childlist) {
				Uri selectedImage = childlist.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				File file=new File(picturePath);
				filename=file.getName();
				cursor.close();
				decodeFile(picturePath);
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public class PhotoAdapter  extends ArrayAdapter<Child> {
		int StudentID;
		photoHolder holder;  
		ImageLoader imageLoader; 
		Bitmap bmp;
		String SID;
		public PhotoAdapter(int layoutResourceId) {		
			super(commContext, layoutResourceId, childlist);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			try
			{
				final Child s=childlist.get(position);
				if(convertView==null)
				{
					LayoutInflater inflater=(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					convertView=inflater.inflate(R.layout.list_photo, parent,false);
					holder=new photoHolder();
					holder.text_name=(TextView)convertView.findViewById(R.id.text_name);
					holder.pro_pic=(CircularNetworkImageView)convertView.findViewById(R.id.pro_pic);
					convertView.setTag(holder);
				}
				else
				{
					holder=(photoHolder)convertView.getTag();
				}
				final ImageView imageEdit=(ImageView)convertView.findViewById(R.id.image_edit);
				imageEdit.setTag(position);
				holder.text_name.setText(s.getFirstname()+" "+s.getMiddlename()+" "+s.getLastname());
				holder.pro_pic.setTag(position);
				bmp=DBHelper.getInstance(commContext).getBitMap(s.getStudentId()+"");
				if(bmp!=null)
					holder.pro_pic.setImageBitmap(bmp);
				else{
					try {
						ImageLoader.ImageCache imageCache = new LruBitmapCache();
						
						imageLoader = new ImageLoader(Volley.newRequestQueue(commContext), imageCache);
						(holder.pro_pic).setImageUrl(Common.relativeurl+ s.getPhoto(), imageLoader);
						/*((CircularNetworkImageView) holder.pro_pic)
						.setDefaultImageResId(R.drawable.student);*/
						((CircularNetworkImageView) holder.pro_pic)
						.setErrorImageResId(R.drawable.student);
						imageLoader.get(Common.relativeurl+ s.getPhoto(), new ImageLoader.ImageListener() {
							public void onErrorResponse(VolleyError arg0) {
								(holder.pro_pic).setImageResource(R.drawable.student);
							}
							public void onResponse(ImageContainer response, boolean arg1) {
								if (response.getBitmap() != null) {
									DBHelper.getInstance(commContext).insertPhoto(response.getBitmap(),s.getStudentId()+"");
								} 
							}
						});

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				imageEdit.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						test((Integer)imageEdit.getTag());
					}
				});
				holder.pro_pic.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent=new Intent(commContext,CommunicationParent.class);
						Common.photo=Common.relativeurl+childlist.get((Integer)v.getTag()).getPhoto();
						Log.e("PhotoURL", Common.photo);
						Common.studID=childlist.get((Integer)v.getTag()).getStudentId()+"";
						intent.putExtra("StudentID",childlist.get((Integer)v.getTag()).getStudentId()+"");
						startActivity(intent);
						overridePendingTransition(R.anim.right_in, R.anim.left_out);
					}
				});
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			return convertView;
		}
		void test(int p)
		{
			pos=p;
			Common.studID=childlist.get(pos).getStudentId()+"";
			Common.stuName=childlist.get(pos).getFirstname()+childlist.get(pos).getLastname();
			Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(i, RESULT_LOAD_IMAGE);
		}
	}
	private void update(int id,Bitmap bmp)
	{

		int c = listStudent.getChildCount();
		for (int i = 0; i < c; i++)
		{
			View view = listStudent.getChildAt(i);
			if ((Integer)view.findViewById(R.id.image_edit).getTag() == id)
			{
				CircularNetworkImageView image=(CircularNetworkImageView)view.findViewById(R.id.pro_pic);
				image.setImageBitmap(bitmap);
				DBHelper.getInstance(commContext).insertPhoto(bitmap, childlist.get(pos).getStudentId()+"");
			}
		}
	}
	public void decodeFile(String filePath) {
		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, o);

		// The new size we want to scale to
		final int REQUIRED_SIZE = 1024;

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
				break;
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		bitmap = BitmapFactory.decodeFile(filePath, o2);
		bitmap = Bitmap.createScaledBitmap(bitmap,400, 400, true);
		update(pos, bitmap);
		padapter.notifyDataSetChanged();
		if (picturePath!=null) {
			FileUploadServer fus=new FileUploadServer();
			try {
				fus.FileToByteArrayConversion(Common.studID,picturePath,Common.stuName+".jpg");
				Common.showToast("Photo Uploaded successfully",commContext);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else
		{
			Common.showToast("Select Image",commContext);
		}
	}
	//view holder for photo adapter
	static class photoHolder
	{
		TextView text_name;
		CircularNetworkImageView pro_pic;
	}

}
