package com.primesys.mitra;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.db.DBHelper;
import com.example.dto.MessageMain;
import com.primesys.emojilibrary.EmojiconEditText;
import com.primesys.emojilibrary.EmojiconGridView.OnEmojiconClickedListener;
import com.primesys.emojilibrary.EmojiconsPopup;
import com.primesys.emojilibrary.EmojiconsPopup.OnEmojiconBackspaceClickedListener;
import com.primesys.emojilibrary.EmojiconsPopup.OnSoftKeyboardOpenCloseListener;
import com.primesys.emojilibrary.emoji.Emojicon;
public class MainPrincipal extends Activity {
	ListView mList;
	static MyCustomAdapter mAdapter;//=LoginActivity.mAdapter;
	private ClientPrincipal mClient=LoginActivity.mClientPri;
	static String to,name;
	String CurrentTime,studentId="A";
	static Date date=new Date();
	static ArrayList<MessageMain> arrayList = new ArrayList<MessageMain>();
	static String formattedDate = new SimpleDateFormat("dd MMM yyyy hh:mm:ss").format(date);
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();

	int selectedPosition;
	Context mainContext=MainPrincipal.this;
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViewById();
		
		textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);
		
		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		final EmojiconEditText editText = (EmojiconEditText) findViewById(R.id.editText);
		ImageView send = (ImageView)findViewById(R.id.send_button);
		final View rootView = findViewById(R.id.root_view);
		final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);
		//relate the listView from java to the one created in xml
		mList = (ListView)findViewById(R.id.list);
		Common.to=getIntent().getStringExtra("to");
		name=getIntent().getStringExtra("name");
		studentId=getIntent().getStringExtra("student");
		setTitle(name);
		APIController.getInstance().setRead(1);
		getMessageData();
		final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);
		//Will automatically set size according to the soft keyboard size
		popup.setSizeForSoftKeyboard();
		mAdapter = new MyCustomAdapter(mainContext, arrayList,name);
		mList.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
		send.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String message = editText.getText().toString();
				if(message.trim().length()!=0)
				{
					//added by amit 17.02.2015
					MessageMain m=new MessageMain();
					m.setMesageText(message);
					m.setEvent("msg");
					m.setDate_time(Common.convertToLong(formattedDate));
					m.setApp_id(Common.app_id);
					m.setTo(Common.to);
					m.setFrom(Common.userid);
					m.setName(name);
					m.setType("s");
					m.setRef_id(Common.getRefNo());
					DBHelper helper=DBHelper.getInstance(mainContext);
					helper.insertMessage(m,studentId);
					arrayList.add(m);
					if (mClient != null) {
						LoginActivity.mClientPri.messageSend(schoolMessage(message,m.getRef_id()));
					}				
				}
				else{
					Common.showToast("Enter message", mainContext);
				}
				//refresh the list
				mAdapter.notifyDataSetChanged();
				editText.setText("");
			}
		});
		mList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				selectedPosition=position;
				startActionMode(modeCallBack);
				view.setSelected(true);
				return true;
			}
		});
		popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {
			@Override
			public void onEmojiconClicked(Emojicon emojicon) {
				editText.append(emojicon.getEmoji());
			}
		});
		//Set on backspace click listener
		popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {
			@Override
			public void onEmojiconBackspaceClicked(View v) {
				KeyEvent event = new KeyEvent(
						0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
				editText.dispatchKeyEvent(event);
			}
		});
		//If the emoji popup is dismissed, change emojiButton to smiley icon
		popup.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() {
				changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
			}
		});
		//If the text keyboard closes, also dismiss the emoji popup
		popup.setOnSoftKeyboardOpenCloseListener(new OnSoftKeyboardOpenCloseListener() {
			@Override
			public void onKeyboardOpen(int keyBoardHeight) {
			}
			@Override
			public void onKeyboardClose() {
				if(popup.isShowing())
					popup.dismiss();
			}
		});
		//On emoji clicked, add it to edittext
		popup.setOnEmojiconClickedListener(new OnEmojiconClickedListener() {
			@Override
			public void onEmojiconClicked(Emojicon emojicon) {
				editText.append(emojicon.getEmoji());
			}
		});
		//On backspace clicked, emulate the KEYCODE_DEL key event
		popup.setOnEmojiconBackspaceClickedListener(new OnEmojiconBackspaceClickedListener() {
			@Override
			public void onEmojiconBackspaceClicked(View v) {
				KeyEvent event = new KeyEvent(
						0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
				editText.dispatchKeyEvent(event);
			}
		});
		// To toggle between text keyboard and emoji keyboard keyboard(Popup)
		emojiButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//If popup is not showing => emoji keyboard is not visible, we need to show it
				if(!popup.isShowing()){
					//If keyboard is visible, simply show the emoji popup
					if(popup.isKeyBoardOpen()){
						popup.showAtBottom();
						changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
					}
					//else, open the text keyboard first and immediately after that show the emoji popup
					else{
						editText.setFocusableInTouchMode(true);
						editText.requestFocus();
						popup.showAtBottomPending();
						final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
						changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
					}
				}
				//If popup is showing, simply dismiss it to show the undelying text keyboard
				else{
					popup.dismiss();
				}
			}
		}); 

	}
	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(mainContext)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(mainContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(mainContext)&& APIController.getInstance().getSchool_logo()!=null) {

			imageLoader = new ImageLoader(
					Volley.newRequestQueue(mainContext), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}
		
	}
	private void findViewById() {
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		imageLoader = new ImageLoader(
				Volley.newRequestQueue(mainContext), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
	}
	public void onBackPressed() {
		APIController.getInstance().setRead(0);
		 super.onBackPressed();
	};
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("name", name);
		outState.putString("to", to);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if(savedInstanceState!=null)
		{
			name=savedInstanceState.getString("name");
			to=savedInstanceState.getString("to");
		}
	}
	//get the data from the URL
	void getMessageData()
	{
		DBHelper helper=DBHelper.getInstance(MainPrincipal.this);
		arrayList=helper.getAllMessages(Common.userid, Common.to,false,studentId);
	}

	private ActionMode.Callback modeCallBack = new ActionMode.Callback() {

		public boolean onPrepareActionMode(ActionMode mode, Menu menu){   
			mode.getMenuInflater().inflate(R.menu.cab, menu);
			return true;
		}
		public void onDestroyActionMode(ActionMode mode) {
			mList.setSelection(0);
			mode = null;  
		}

		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			return true;
		}

		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.menu_copy:
				ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE); 
				ClipData clip = ClipData.newPlainText("message", arrayList.get(selectedPosition).getMesageText());
				clipboard.setPrimaryClip(clip);
				mode.finish();
				return true;

			default:
				return false;
			}
		}
	};
	private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId){
		iconToBeChanged.setImageResource(drawableResourceId);
	}
	//school messages
		String schoolMessage(String msg,long refid)
		{
			JSONObject jmsg = new JSONObject();
			try
			{
				jmsg.put("event", "school_msg");
				jmsg.put("app_id",Common.app_id);
				jmsg.put("from",Common.userid);
				jmsg.put("msg",msg);
				jmsg.put("to",Common.to);
				jmsg.put("student_id",studentId);
				jmsg.put("ref_id",refid);
				jmsg.put("type", "s");
			}catch(Exception e){}
			return jmsg.toString();
		}
}




