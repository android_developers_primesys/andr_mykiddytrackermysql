package com.example.adapter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dto.Sales_person;
import com.primesys.friendtrack.CircleTransform;
import com.primesys.mitra.CircularNetworkImageView;
import com.primesys.mitra.Common;
import com.primesys.mitra.R;
import com.squareup.picasso.Picasso;
public class Track_ListAdpterInvitation  extends ArrayAdapter<Sales_person>{
	Context context;
	ArrayList<Sales_person> data=null;
	ArrayList<Sales_person> datacopy;
	ImageLoader imageLoader;
	Bitmap bitmap;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG="REquest";
	public Track_ListAdpterInvitation(Context context, int resource, ArrayList<Sales_person> tracklist) {
		super(context, resource, tracklist);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data=tracklist;
		this.datacopy = new ArrayList<Sales_person>();
		this.datacopy.addAll(tracklist);
	}

	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		try
		{ 
			final Sales_person s=data.get(position); 
			if(convertView==null)
			{
				LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView=inflater.inflate(R.layout.invitation_row, parent,false);
			}
		//	convertView.setBackgroundResource(R.drawable.table_bg);
			TextView text_name=(TextView)convertView.findViewById(R.id.txtname);
			TextView text_phone=(TextView)convertView.findViewById(R.id.txtphone);
			TextView text_status=(TextView)convertView.findViewById(R.id.tv_status);

			
			Button accapt=(Button)convertView.findViewById(R.id.accept);
			Button reject=(Button)convertView.findViewById(R.id.reject);
			CircularNetworkImageView proimg=(CircularNetworkImageView)convertView.findViewById(R.id.image_pro);
			proimg.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_boy));
			text_name.setText(s.getName());
			text_phone.setText(s.getContactNumber());
			
			if (s.getStatus().equals("1")) {
				text_status.setText("Accepted");

			}else {
				text_status.setText("Pending");

			}
			
		
			
			//Set pic of user sales person
			CircularNetworkImageView netCircle=new CircularNetworkImageView(context);

			/*if(s.getPhoto()!=null){
				String photo=s.getPhoto();
				if(photo!=""){
					ImageLoader.ImageCache imageCache = new LruBitmapCache();
					imageLoader = new ImageLoader(
							Volley.newRequestQueue(context), imageCache);
					(proimg).setImageUrl(
							Common.Relative_URL+photo, imageLoader);
					((CircularNetworkImageView) proimg)
					.setDefaultImageResId(R.drawable.ic_profile);
					((CircularNetworkImageView) proimg)
					.setErrorImageResId(R.drawable.ic_profile);
							}else{
					 bitmap = ((BitmapDrawable)proimg.getDrawable()).getBitmap();	
						GraphicsUtil graphicUtil = new GraphicsUtil();
						// picView.setImageBitmap(graphicUtil.getRoundedShape(thePic,(float)1.5,92));
						
						proimg.setImageBitmap(graphicUtil.getCircleBitmap(
								bitmap, 16));				
						}
				}
				
				else{
					 bitmap = ((BitmapDrawable)proimg.getDrawable()).getBitmap();	
					proimg.setImageBitmap(netCircle.getRoundedShape(bitmap));
				}*/

			if(s.getPhoto()!=null){
				String photo=s.getPhoto();
			// Picasso.with(context).load(Common.Relative_URL+photo).into(proimg);
			 Picasso.with(context).load(Common.relativeurl+photo).transform(new CircleTransform()).into(proimg);

			}else{
				 bitmap = ((BitmapDrawable)proimg.getDrawable()).getBitmap();	
				proimg.setImageBitmap(netCircle.getRoundedShape(bitmap));
			}
			
			
			
			accapt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
				
						AcceptInvitation();
					}

				private void AcceptInvitation() {



					reuestQueue = Volley.newRequestQueue(context);
					final ProgressDialog pDialog = new ProgressDialog(context);
					pDialog.setTitle("Please wait.......");
					pDialog.setCancelable(true);
					pDialog.show();
					//JSon object request for reading the json data
					stringRequest = new StringRequest(Method.POST,Common.TrackURL+"UserServiceAPI/AcceptInvitation",new Response.Listener<String>() {

						@Override
						public void onResponse(String response) {
							try {
								pDialog.hide();

								JSONObject jo=new JSONObject(response);
								System.err.println("Track Data"+response);
								if (response.contains("false")) {
									Common.showToast("Request is Accepted",context);
									
									data.remove(position);
									notifyDataSetChanged();
								}else {
									Common.showToast("Request is Not Accepted",context);

								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
						}
					},
					new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							pDialog.hide();
							if(error.networkResponse != null && error.networkResponse.data != null){
								VolleyError er = new VolleyError(new String(error.networkResponse.data));
								error = er;
								System.out.println(error.toString());
							}
						}
					})  {
						@Override
						protected Map<String, String> getParams() {
							Map<String, String> params = new HashMap<String, String>();
							params.put("Invited_PId",s.getInvi_Id());
							System.err.println("Track Req--- "+params);
							return params;
						}
					};
					stringRequest.setTag(TAG);
					stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
					// Adding request to request queue
					reuestQueue.add(stringRequest);
				
				
				
					
				}
					
			});

			
			
			
			
			///Reject Invitation
			
			reject.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
				
					RejectInvitation();
					}

				private void RejectInvitation() {



					reuestQueue = Volley.newRequestQueue(context);
					final ProgressDialog pDialog = new ProgressDialog(context);
					pDialog.setTitle("Please wait.......");
					pDialog.setCancelable(true);
					pDialog.show();
					//JSon object request for reading the json data
					stringRequest = new StringRequest(Method.POST,Common.TrackURL+"UserServiceAPI/RejectInvitation",new Response.Listener<String>() {

						@Override
						public void onResponse(String response) {
							try {
								pDialog.hide();

								JSONObject jo=new JSONObject(response);
								System.err.println("Track Data"+response);
								if (response.contains("false")) {
									Common.showToast("Request is Rejected",context);
									
									data.remove(position);
									notifyDataSetChanged();
								}else {
									Common.showToast("Request is Not Rejected",context);

								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
						}
					},
					new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							pDialog.hide();
							if(error.networkResponse != null && error.networkResponse.data != null){
								VolleyError er = new VolleyError(new String(error.networkResponse.data));
								error = er;
								System.out.println(error.toString());
							}
						}
					})  {
						@Override
						protected Map<String, String> getParams() {
							Map<String, String> params = new HashMap<String, String>();
							params.put("Invited_PId",s.getInvi_Id());
							System.err.println("Track Req--- "+params);
							return params;
						}
					};
					stringRequest.setTag(TAG);
					stringRequest.setRetryPolicy(new DefaultRetryPolicy(300000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
					// Adding request to request queue
					reuestQueue.add(stringRequest);
				
				
				
					
				}
					
			});
		}catch(Exception e)
		{
         e.printStackTrace();
		}

		


		
		
		
		return convertView;
	}
	/*public void filter(String charText) {
		// TODO Auto-generated method stub
		// Filter Class
	
			charText = charText.toLowerCase(Locale.getDefault());
			data.clear();
			if (charText.length() == 0) {
				data.addAll(datacopy);
			} else {
				for (product p : datacopy) {
					if (//p.getStock_Group().toLowerCase(Locale.getDefault()).contains(charText) ||
						p.getStock_subGroup().toLowerCase(Locale.getDefault()).contains(charText) || 
						p.getProduct_name().toLowerCase(Locale.getDefault()).contains(charText)	
						) {
						data.add(p);
					}
				}
			}
			notifyDataSetChanged();
		
	}
*/


	
}
