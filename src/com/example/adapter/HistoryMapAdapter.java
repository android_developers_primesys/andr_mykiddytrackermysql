package com.example.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.db.DBHelper;
import com.example.dto.GmapDetais;
import com.primesys.mitra.CircularNetworkImageView;
import com.primesys.mitra.Common;
import com.primesys.mitra.DateTimeActivity;
import com.primesys.mitra.HistoryActivity;
import com.primesys.mitra.LruBitmapCache;
import com.primesys.mitra.R;
import com.primesys.mitra.ShowGMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HistoryMapAdapter  extends ArrayAdapter<GmapDetais>{

	TextView txtchild,txtcar,txtpet;
	Context context;
	ImageLoader imageLoader;
	int layoutResourceId;
	ArrayList<GmapDetais> datal;
	String timestamp;
	LinearLayout laychild,laypet,layCar;
	Bitmap bitmap;
	HistoryActivity history;
	static RequestQueue RecordsSyncQueue;

	public HistoryMapAdapter(Context context, int layoutResourceId,
			ArrayList<GmapDetais> data, ImageLoader imageLoader2) {
		super(context, layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.datal = data;
		this.imageLoader=imageLoader2;


	}

	@Override
	public View getView( int position, View convertView, ViewGroup parent) {
		if (convertView== null) {
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);

		} 

		try{
			//final SimpleDateFormat mFormatter = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
			final String formattedDate = new SimpleDateFormat("dd MMM yyyy hh:mm:ss").format(new Date());
			history=new HistoryActivity();
			GmapDetais gp=datal.get(position);
			final LinearLayout lay=(LinearLayout)convertView.findViewById(R.id.lay_main);
			final CircularNetworkImageView imgchild=(CircularNetworkImageView)convertView.findViewById(R.id.img_child);
			txtchild=(TextView)convertView.findViewById(R.id.txt_child);
			txtchild.setText(gp.getName());
			imgchild.setTag(position);
			txtchild.setTag(position);

			try{
				(imgchild).setImageUrl(Common.relativeurl+gp.getPath().replaceAll(" ","%20"), imageLoader);
				((CircularNetworkImageView) imgchild)
				.setDefaultImageResId(0);
				((CircularNetworkImageView) imgchild)
				.setErrorImageResId(0);

			}catch(Exception e){

			}


			txtchild.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try{
						ShowGMap gmap=new ShowGMap();
						gmap.flag=0;
						DateTimeActivity.selStatus=true;
						GmapDetais user1=datal.get((Integer)v.getTag());
						if (user1.getType().equalsIgnoreCase("Child")) {
							ShowGMap.Updatestatus=true;
							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();
							ShowGMap.bmp1=bmp;
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONHistoryChild(formattedDate,user1.getId()));

						}if(user1.getType().equalsIgnoreCase("Car")){
							ShowGMap.Updatestatus=true;

							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();
							ShowGMap.bmp1=bmp;
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONHistoryCar(formattedDate, user1.getId()));

						}if(user1.getType().equalsIgnoreCase("Pet")){		

							ShowGMap.Updatestatus=true;

							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();
							ShowGMap.bmp1=bmp;
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONHistoryPet(formattedDate, user1.getId()));
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});

			//onclick txtxhild
			imgchild.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try{
						DateTimeActivity.selStatus=true;
						GmapDetais user1=datal.get((Integer)v.getTag());
						ShowGMap gmap=new ShowGMap();
						gmap.flag=0;
						if (user1.getType().equalsIgnoreCase("Child")) {


							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();
							ShowGMap.bmp1=bmp;
							com.primesys.mitra.LoginActivity.mClient.sendMessage((makeJSONHistoryChild(formattedDate,user1.getId())));
						}else if(user1.getType().equalsIgnoreCase("Car")){



							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();

							ShowGMap.bmp1=bmp;
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONHistoryCar(formattedDate, user1.getId()));
						}else if(user1.getType().equalsIgnoreCase("Pet"))
						{		



							BitmapDrawable bitmap_draw=(BitmapDrawable)imgchild.getDrawable();
							Bitmap  bmp=bitmap_draw.getBitmap();

							ShowGMap.bmp1=bmp;
							com.primesys.mitra.LoginActivity.mClient.sendMessage(makeJSONHistoryPet(formattedDate, user1.getId()));
						}



					}catch(Exception ex){

					}
				}
			});



		}catch(Exception ex){
			Log.e("Exception-Adatpte", ""+ex);
		}


		return convertView;
	}

	private Bitmap getRoundedShape(Bitmap bitmap) {
		int targetWidth = 40;
		int targetHeight = 40;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, 
				targetHeight,Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), 
						((float) targetHeight)) / 2),
						Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = bitmap;
		canvas.drawBitmap(sourceBitmap, 
				new Rect(0, 0, sourceBitmap.getWidth(),
						sourceBitmap.getHeight()), 
						new Rect(0, 0, targetWidth, targetHeight), null);
		return targetBitmap;

	}


	//History Child
	String makeJSONHistoryChild(String date,String Id)
	{
		DBHelper helper=DBHelper.getInstance(context);
		helper.truncateTables("db_history");
		String trackSTring="{}";
		try{
			JSONObject jo=new JSONObject();
			jo.put("event","get_tracking_history");
			if(Common.roleid.equals("5"))
				jo.put("student_id","demo_student");
			else
				jo.put("student_id",Integer.parseInt(Id));
			jo.put("timestamp",Common.convertToLong(date));
			trackSTring=jo.toString();
		}
		catch(Exception e)
		{

		}
		return trackSTring;
	}
	//History pet
	String makeJSONHistoryPet(String date,String Id)
	{
		DBHelper helper=DBHelper.getInstance(context);
		helper.truncateTables("db_history");
		String trackSTring="{}";
		try{
			JSONObject jo=new JSONObject();
			jo.put("event","get_tracking_history");
			if(Common.roleid.equals("5"))
				jo.put("student_id","demo_pet");
			else
				jo.put("student_id",Integer.parseInt(Id));
			jo.put("timestamp",Common.convertToLong(date));
			trackSTring=jo.toString();
		}
		catch(Exception e)
		{

		}
		return trackSTring;
	}
	//History Car
	String makeJSONHistoryCar(String date,String Id)
	{
		DBHelper helper=DBHelper.getInstance(context);
		helper.truncateTables("db_history");
		String trackSTring="{}";
		try{
			JSONObject jo=new JSONObject();
			jo.put("event","get_tracking_history");
			if(Common.roleid.equals("5"))
				jo.put("student_id","demo_car");
			else
				jo.put("student_id",Integer.parseInt(Id));
			jo.put("timestamp",Common.convertToLong(date));
			trackSTring=jo.toString();
		}
		catch(Exception e)
		{

		}
		return trackSTring;
	}

}
