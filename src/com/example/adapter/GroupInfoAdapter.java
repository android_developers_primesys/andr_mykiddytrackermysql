package com.example.adapter;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.IconTextView;
import android.widget.TextView;

import com.example.dto.GroupInfo;
import com.primesys.mitra.R;

public class GroupInfoAdapter extends BaseAdapter {


	Context currentContext;
	ArrayList<GroupInfo> listFrnd;
    String grpid;
	// constructor
	public GroupInfoAdapter(Context context,ArrayList<GroupInfo> list,String grpid) {
		this.currentContext=context;
		this.listFrnd=list;
		this.grpid=grpid;
	}
	@Override
	public int getCount() {
		return listFrnd.size();
	}

	@Override
	public GroupInfo getItem(int position) {
		return listFrnd.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		  GroupInfo frnd=getItem(position);
		 if(convertView==null)
		 {
			 LayoutInflater inflater = ((Activity) currentContext).getLayoutInflater();
				convertView=inflater.inflate(R.layout.list_friends, null);
		 }
			final TextView member_name=(TextView)convertView.findViewById(R.id.member_name);
			member_name.setText(frnd.getParticipantName());
			member_name.setTag(position);
			IconTextView userIcon=(IconTextView)convertView.findViewById(R.id.icon_user);
			IconTextView iconDelete=(IconTextView)convertView.findViewById(R.id.icon_delete);
			convertView.findViewById(R.id.text_count).setVisibility(View.GONE);
			if(frnd.getType().equals("Admin"))
			{
				userIcon.setText("Admin");
				iconDelete.setVisibility(View.GONE);
			}
			else
			{
				userIcon.setText("Member");
				iconDelete.setVisibility(View.VISIBLE);
			}
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					/*LoginActivity.mClient.sendMessage(removeMember(1));*/
				}
			});
			iconDelete.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {	
				}
			});
		
		return convertView;
	}
	String removeMember(final int pid)
	{
		String result="";
		try
		{
			JSONObject jo=new JSONObject();
			jo.put("event", "remove_group_member");
			jo.put("group_id", grpid);
			jo.put("user_id",pid);
			result=jo.toString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	String addMember(final int pid)
	{
		String result="";
		try
		{
			JSONObject jo=new JSONObject();
			jo.put("event", "add_group_member");
			jo.put("group_id",grpid);
			jo.put("user_id",pid);
			result=jo.toString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
}
