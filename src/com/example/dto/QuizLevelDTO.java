package com.example.dto;

import com.primesys.mitra.Common;

public class QuizLevelDTO {
	String Rank,Status,Score,Level,QuizId,category_label;

	public String getQuizId() {
		return QuizId;
	}

	public void setQuizId(String quizId) {
		QuizId = quizId;
	}

	public String getRank() {
		return Rank;
	}

	public void setRank(String rank) {
		Rank = rank;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getScore() {
		return Score;
	}

	public void setScore(String score) {
		Score = score;
	}

	public String getLevel() {
		return Level;
	}

	public void setLevel(String level) {
		Level = level;
	}

	public String getCategory_label() {
		return category_label;
	}

	public void setCategory_label(String category_label) {
		this.category_label = category_label;
	}

	
	

}
