package com.primesys.mitra;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class CreateGroup extends Activity {

	static Context createContext;
	EditText text_username;
	TextView textNews,lbltext;
	TextView textschool;
	CircularNetworkImageView img_school;
	ImageLoader imageLoader;
	int Updatestatus=0;
	int showtime=15000;
	final Handler handler = new Handler();
	Button btnAdd;
	StringRequest stringRequest;
	Context context=CreateGroup.this;
	RequestQueue reuestQueue;
	final String TAG=CreateGroup.class.getSimpleName();
	private String userName;
	static long groupId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_group);
		FINDvIEWbYiD();
			textNews.setVisibility(View.INVISIBLE);
		lbltext.setVisibility(View.INVISIBLE);

		 

		
		handler.postDelayed(new Runnable() {
			public void run() {
				try{

					NewsHeaderUpdate();


				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}
				handler.postDelayed(this, showtime); //now is every 1 minutes
			}
		}, showtime);
		createContext=CreateGroup.this;
		textNews=(TextView)findViewById(R.id.text_news);

		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		/*textNews.setTypeface(custom_font);
		textNews.setText(APIController.getInstance().getNews());*/
		btnAdd=(Button)findViewById(R.id.btn_add);
		text_username=(EditText)findViewById(R.id.text_username);
		text_username.setHint("Enter Group Name! ");

		btnAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				userName=text_username.getText().toString().trim();
				if(userName.length()==0||userName==null)
					Common.showToast("Please Enter the Group Name ", createContext);
				else
					if(Common.connectionStatus)
						LoginActivity.mClient.sendMessage(createGroup());
				text_username.setText("");
			}
		});
	}

	protected void NewsHeaderUpdate() {
		if (Updatestatus<=0) {
			textNews.setVisibility(View.VISIBLE);
			lbltext.setVisibility(View.VISIBLE);
			textschool.setVisibility(View.INVISIBLE);

			img_school.setVisibility(View.VISIBLE);
			img_school.setVisibility(View.INVISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(
					Volley.newRequestQueue(context), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus++;
			showtime=60000;
		}else{
			textNews.setVisibility(View.INVISIBLE);
			lbltext.setVisibility(View.INVISIBLE);
			textschool.setVisibility(View.VISIBLE);
			textschool.setText(APIController.getInstance().getSchool_name());
			img_school.setVisibility(View.VISIBLE);
			ImageLoader.ImageCache imageCache = new LruBitmapCache();
			if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(Volley.newRequestQueue(context), imageCache);
			(img_school).setImageUrl(
					Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
			((CircularNetworkImageView) img_school)
			.setDefaultImageResId(R.drawable.student);
			((CircularNetworkImageView) img_school)
			.setErrorImageResId(R.drawable.student);
			}
			Updatestatus=0;
			showtime=6000;
		}
	}

	private void FINDvIEWbYiD() {
		textNews=(TextView)findViewById(R.id.text_news);
		textNews.setText(APIController.getInstance().getNews());
		lbltext=(TextView)findViewById(R.id.lbltext);
		textschool=(TextView)findViewById(R.id.txt_school);
		textschool.setText(APIController.getInstance().getSchool_name());
		img_school=(CircularNetworkImageView)findViewById(R.id.img_school);
		
		ImageLoader.ImageCache imageCache = new LruBitmapCache();
		if (Common.getConnectivityStatus(context)&& APIController.getInstance().getSchool_logo()!=null) {
			imageLoader = new ImageLoader(Volley.newRequestQueue(context), imageCache);
		(img_school).setImageUrl(
				Common.relativeurl+APIController.getInstance().getSchool_logo().replaceAll(" ","%20"), imageLoader);
		}
	}

	//make JSON for creating the add group request
	String createGroup()
	{
		String result="";
		try
		{
			JSONObject jo=new JSONObject();
			jo.put("event", "add_group");
			jo.put("group_name", userName);
			jo.put("admin_id", Common.userid);
			result=jo.toString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}

	static void parseJSON(String result)
	{
		try
		{
			JSONObject jo=new JSONObject(result);
			JSONObject jData=jo.getJSONObject("data");
			groupId=jData.getLong("group_id");
			Common.showToast("Group created successfully.", createContext); 
			((Activity) createContext).finish();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{

		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflate=getMenuInflater();
		inflate.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.back:
		/*	Intent friendlist=new Intent(createContext, DemoUserActivity.class);
			startActivity(friendlist);*/
			onBackPressed();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}
