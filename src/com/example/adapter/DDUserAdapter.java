package com.example.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.example.db.DBHelper;
import com.example.dto.Child;
import com.primesys.mitra.CircularNetworkImageView;
import com.primesys.mitra.Common;
import com.primesys.mitra.LruBitmapCache;
import com.primesys.mitra.R;
import com.primesys.mitra.TeacherDiaryActivity;

public class DDUserAdapter  extends ArrayAdapter<Child> {

	Context context;
	ArrayList<Child> data;
	ImageLoader imageLoader;
	final static int RESULT_LOAD_IMAGE=100;
	String picturePath,filename;
	Bitmap bitmap;
	int StudentID;
	int pos;
	Bitmap bmp;
    String SID;
	public DDUserAdapter(Context context, int layoutResourceId,
			ArrayList<Child> data) {		
		super(context, layoutResourceId, data);
		this.context = context;
		this.data=data;
	Log.e("IN Side Constructor", "hgfhdfhdf");
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		try
		{
			final Child s=data.get(position); 
			if(convertView==null)
			{
				LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView=inflater.inflate(R.layout.list_photo, parent,false);
			}
			ImageView imageEdit=(ImageView)convertView.findViewById(R.id.image_edit);
			imageEdit.setVisibility(View.GONE);
			TextView text_name=(TextView)convertView.findViewById(R.id.text_name);
			text_name.setText(s.getFullname());
            
			final CircularNetworkImageView pro_pic=(CircularNetworkImageView)convertView.findViewById(R.id.pro_pic);
			pro_pic.setTag(position);
			/////////////////Added by RUpesh
		
			

			
	
		bmp=DBHelper.getInstance(context).getBitMap(s.getStudentId()+"");
			if(bmp!=null){
				//Common.showToast("Image in Offline",context);
				pro_pic.setImageBitmap(bmp);}
		else{
			
			//Common.showToast("Image in Online",context);

			try {
				ImageLoader.ImageCache imageCache = new LruBitmapCache();
				imageLoader = new ImageLoader(
						Volley.newRequestQueue(context), imageCache);
				(pro_pic).setImageUrl(
						Common.relativeurl+ s.getPhoto(), imageLoader);
				((CircularNetworkImageView) pro_pic)
				.setDefaultImageResId(R.drawable.student);
				((CircularNetworkImageView) pro_pic)
				.setErrorImageResId(R.drawable.student);
				imageLoader.get(Common.relativeurl+ s.getPhoto(), new ImageLoader.ImageListener() {
			        public void onErrorResponse(VolleyError arg0) {
			        pro_pic.setImageResource(R.drawable.student);
			        }
			        public void onResponse(ImageContainer response, boolean arg1) {
			            if (response.getBitmap() != null) {
							DBHelper.getInstance(context).insertPhoto(response.getBitmap(),s.getStudentId()+"");
			            } 
			        }
			    });
				
			} catch (Exception e) {
				Log.e("setdata", "" + e);
			}
//		
//				
			}
		
			
			
			pro_pic.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent=new Intent(context,TeacherDiaryActivity.class);
					Common.photo=Common.relativeurl+data.get((Integer)v.getTag()).getPhoto();
					intent.putExtra("StudentID",data.get((Integer)v.getTag()).getStudentId()+"");
					intent.putExtra("Type", "P");
					((Activity)context).startActivity(intent);
					((Activity)context).overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
				}
			});

		}catch(Exception e)
		{
         e.printStackTrace();
		}

		return convertView;
	}
	void test(int p)
	{
		pos=p;
		Intent i = new Intent(
				Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		((Activity) context).startActivityForResult(i, RESULT_LOAD_IMAGE);
	}
}
