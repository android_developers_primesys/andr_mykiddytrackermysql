
package com.primesys.friendtrack;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.commonsware.cwac.locpoll.LocationPollerResult;
import com.google.gson.JsonObject;
import com.primesys.mitra.Common;

public class LocationReceiver extends BroadcastReceiver {
	private final String key_User_id="User_Id";
	private final String key_Location_url = "Url";
	public SharedPreferences sharedPreferences;
	String UserId,lat,lang,time;
	SharedPreferences.Editor editor ;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	public static final String TAG="Postloc";
Context context;
  @Override
  public void onReceive(Context context, Intent intent) {
	  
	  this.context=context;
    File log=
        new File(Environment.getExternalStorageDirectory(),
                 "LocationLog.txt");

    try {
      BufferedWriter out=
          new BufferedWriter(new FileWriter(log.getAbsolutePath(),
                                            log.exists()));

      out.write(new Date().toString());
      out.write(" : ");
      
      Bundle b=intent.getExtras();
		//bundle.putCharSequence(key_id,);

      LocationPollerResult locationResult = new LocationPollerResult(b);
		sharedPreferences = context.getSharedPreferences("userInfo",Context.MODE_PRIVATE);

      Location loc = null;
	try {
		loc = locationResult.getLocation();
		  System.out.println("Location --------------"+loc.toString());

		  UserId=sharedPreferences.getString(key_User_id,"");
		  lat=loc.getLatitude()+"";
		  lang=loc.getLongitude()+"";
		  time=loc.getTime()+"";
		  /*Vibrator v = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);
		  // Vibrate for 500 milliseconds
		  v.vibrate(500);*/
		  try {
			postLocation_toserver();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      
   
      String msg;

      if (loc==null) {
        loc=locationResult.getLastKnownLocation();

        if (loc==null) {
          msg=locationResult.getError();
        }
        else {
          msg="TIMEOUT, lastKnown="+loc.toString();
        }
      }
      else {
        msg=loc.toString();
      }

      if (msg==null) {
        msg="Invalid broadcast received!";
      }

      out.write(msg);
      out.write("\n");
      out.close();
    }
    catch (IOException e) {
      Log.e(getClass().getName(), "Exception appending to log file", e);
    }
  }
private void postLocation_toserver() {

	try {
		reuestQueue=Volley.newRequestQueue(context); //getting Request object from it
		/*final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
		pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
		pDialog.setTitleText("Login wait.......");
		pDialog.setCancelable(true);
		pDialog.show();*/
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.TrackURL+"UserServiceAPI/PushLocation",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
			
					parseJSON(response);
				}
				
				
		},
		new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				
			      params.put("Lat", lat);
			      params.put("id", UserId);
			      params.put("Lang", lang);
			      params.put("Time", time);
			     // System.out.println("Req Location"+params.toString());
			      

				return params;
			
			}
		};
		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	}
protected void parseJSON(String response) {
   // System.out.println("respo Location"+response);
	try {
		JSONObject jo=new JSONObject(response);
		if (jo.getString("error").equals("true")&&com.primesys.mitra.Common.getConnectivityStatus(context)) {
			postLocation_toserver();
		}
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
}