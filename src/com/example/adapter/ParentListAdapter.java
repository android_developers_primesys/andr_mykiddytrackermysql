package com.example.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.example.dto.Parent;
import com.primesys.mitra.Common;
import com.primesys.mitra.MainTeacher;
import com.primesys.mitra.ParentInformation;
import com.primesys.mitra.R;
//Added by Amit
public class ParentListAdapter extends ArrayAdapter<Parent> {
	Context context;
	int layoutResourceId;
	public static ArrayList<Parent> data;
	Intent Studeninfo;
	CheckBox selection;
	Animation animation = null;
	public static String selectedparent;
	public ParentListAdapter(Context context, int layoutResourceId,
			ArrayList<Parent> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		ParentListAdapter.data=new ArrayList<Parent>();
		ParentListAdapter.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);
		}
			Parent s = data.get(position);
			// Log.e("student",s.getFirstName());
			final CheckBox selection=(CheckBox)convertView.findViewById(R.id.selection);

			selection.setChecked(s.getSelectionStatus());
			if(selection.isChecked())

				Common.to=s.getParentID();
			selection.setTag(position);


			Button btnEdit = (Button) convertView.findViewById(R.id.studentame);
			btnEdit.setText("\t\t"+""+"\t" + s.getParentName());
			animation = AnimationUtils.loadAnimation(context, R.anim.animlistview);
			selection.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					Parent user1=data.get((Integer)selection.getTag());
					if(selection.isChecked()){
						Common.to=user1.getParentID();
						user1.setSelectionStatus(true);
					}
					else{
						user1.setSelectionStatus(false);
						ParentInformation.selection.setChecked(false);
					}
					boolean valid=true;
					for(Parent su : data){
						if(!su.getSelectionStatus()){
							valid=false;
							break;
						}
					}
					if(valid)
						ParentInformation.selection.setChecked(true);
				}
			});

			//to initiate one to one chat..
			btnEdit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Parent user1=data.get((Integer)selection.getTag());
					String photo=user1.getPhoto();
					photo=photo.replace("~", "");
					Common.to=photo;
					Intent initiat_chat=new Intent(context,MainTeacher.class);
					initiat_chat.putExtra("to", ""+user1.getParentID());
					initiat_chat.putExtra("name", ""+user1.getParentName());
					context.startActivity(initiat_chat);
					((Activity)context).overridePendingTransition(R.anim.right_in, R.anim.left_out);
				}
			});

	



		animation.setDuration(500);
		convertView.startAnimation(animation);
		/*  animation = null;*/
		return convertView;

	}
}
