package com.primesys.friendtrack;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.dto.LocationDTO;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.primesys.mitra.CircularNetworkImageView;
import com.primesys.mitra.Common;
import com.primesys.mitra.R;
import com.squareup.picasso.Picasso;



public class ShowMap  extends Activity implements ConnectionCallbacks,LocationListener, OnConnectionFailedListener{
	public   GoogleMap mMap;
	public static  Bitmap bmp1 ;
	private  LatLng prev ;
	public   static int flag=0;
	static String  InvitedId;
	String path;
	ImageLoader imageLoader;
	static Context trackContext;
	private static int speed;
	private static String date;
	MarkerOptions mp;
	String latval = null,lanval;
	Marker mark;
	static RequestQueue RecordSyncQueue;
	ListView gmapList;
	String defaultImage;
	String TAG="ShowGMap";
	ShowMap contextMap=ShowMap.this;
	long freeSize = 0L;
	long totalSize = 0L;
	long usedSize = -1L;
	private static final long K = 1024;
	private static final long M = K * K;
	private static final long G = M * K;
	private static final long T = G * K;
	public static Boolean Updatestatus=false,menuSelct;
	int cnt=0;
	int cntMap=0;
	public Bitmap bitmap=null;
	private String photo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		trackContext = ShowMap.this;
	
		if (!isGooglePlayServicesAvailable()) {
			Common.showToast("Google Play services not available !",this);
			
			finish();
		}

		try{

			getUsedMemorySize();

			if (usedSize>totalSize-1000) {
				Common.showToast("Insufficient Memory in Loading Map !",this);


			}else{
				
				InvitedId=getIntent().getStringExtra("InvitedId");
				photo=getIntent().getStringExtra("photo");		
				setContentView(R.layout.activity_currentlocation);

				StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());

	

				// Call Api to get track information
				try{
					if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
						new TrackInfrmation().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
					}
					else{
						new TrackInfrmation().execute();
					}
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}

				if (mMap == null) {

					mMap = ((MapFragment) getFragmentManager().findFragmentById(
							R.id.gmap)).getMap();
					mMap.getUiSettings().setZoomControlsEnabled(true);
					mMap.getUiSettings().setMyLocationButtonEnabled(true);
					showCurrentLocation();
					// check if map is created successfully or not
					if (mMap == null) {
						Common.showToast("Sorry! unable to create maps !",this);
					}
				}

				mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


			}

		}catch(Exception e)
		{
			Log.e("ShowGMap", ""+e);
		}



	}

	@Override
	protected void onStop() {
		super.onStop();
		flag=0;
		String trackSTring="{}";
		try{
			JSONObject jo=new JSONObject();
			jo.put("event","stop_track");
			trackSTring=jo.toString();
		}
		catch(Exception e)
		{

		}
		//LoginActivity.mClient.sendMessage(trackSTring);
	}
	@Override
	protected void onRestart() {
		super.onRestart();
		/*LoginActivity.mClient.sendMessage(makeJSON());*/
		//LoginActivity.mClient.sendMessage(restartTrackEvent());
	}
	/*public static void changeLocation(String message)
	{

		System.out.print("Event Received");
		try
		{


			JSONObject jo = new JSONObject(message);
			JSONObject jData = jo.getJSONObject("data");
			String lat=jData.getString("lat");
			String lan=jData.getString("lan");
			speed=jData.getInt("speed");
			date=Common.getDateCurrentTimeZone(jData.getLong("timestamp"));
			((ShowMap) trackContext).updateGoogleMapLocation(lat,lan);
			System.err.print(message);
		}
		catch(Exception e)
		{

		}
	}*/
	//show currentLocation
	void showCurrentLocation()
	{
		try{
			/*
			 * This Is One Is Default 
			 */
			mMap.setMyLocationEnabled(true);
			LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
			// Getting LocationManager object from System Service LOCATION_SERVICE

			// Creating a criteria object to retrieve provider
			Criteria criteria = new Criteria();

			// Getting the name of the best provider
			String provider = lm.getBestProvider(criteria, true);

			// Getting Current Location
			Location location = lm.getLastKnownLocation(provider);

			/*if(location!=null){
				onLocationChanged(location);
			}*/
			lm.requestLocationUpdates(provider, 20000, 0, this);

		}catch(Exception e){

		}
	}
	@Override
	public void onLocationChanged(Location location) {

		// Getting latitude of the current location
		double latitude = location.getLatitude();

		// Getting longitude of the current location
		double longitude = location.getLongitude();

		// Creating a LatLng object for the current location
		LatLng latLng = new LatLng(latitude, longitude);

		// Showing the current location in Google Map
		mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

		// Zoom in the Google Map
		mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	/*void updateGoogleMapLocation(String lat,String lan)
	{
		try{

			Bitmap bmpDefaulr=BitmapFactory.decodeResource(trackContext.getResources(), R.drawable.default_marker1
					);

			LatLng current = new LatLng(Double.parseDouble(lat), Double.parseDouble(lan));
			if(flag==0)  //when the first update comes, we have no previous points,hence this 
			{ 

				mMap.clear();
				prev=current;
				flag=1;
				cntMap=1;
				mp = new MarkerOptions();
				Bitmap bmp=BitmapFactory.decodeResource(trackContext.getResources(), R.drawable.custom_marker);
				mp.position(current).icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(trackContext, customMarker())));
				mp.snippet("Latitude : "+String.format("%.6f",current.latitude)+"\t"+"Longitude : "+String.format("%.6f",current.longitude));
				mp.title("Speed : "+speed+" km/h"+System.getProperty("line.separator")+"\t"+"Date : "+date+"\n"+"");
				mark=mMap.addMarker(mp);
				mark.showInfoWindow();
			}
			else
			{
				if (cntMap==1) {
					mark=mMap.addMarker(mp);
					cntMap++;
				}else{
					mark.setIcon(BitmapDescriptorFactory.fromBitmap(bmpDefaulr));

					mMap.addPolyline((new PolylineOptions())
							.add(prev, current).width(6).color(Color.CYAN)
							.visible(true));
					prev=current;
					mp = new MarkerOptions();
					Bitmap bmp=BitmapFactory.decodeResource(trackContext.getResources(), R.drawable.custom_marker);
					mp.position(current).icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(trackContext, customMarker())));
					mp.snippet("Latitude : "+String.format("%.6f",current.latitude)+"\t"+"Longitude : "+String.format("%.6f",current.longitude));
					mp.title("Speed : "+speed+" km/h"+System.getProperty("line.separator")+"\t"+"Date : "+date+"\n"+"");
					mark=mMap.addMarker(mp);
					mark.showInfoWindow();
					current = null;

				}



			}
			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(lat),Double.parseDouble(lan)), 15));

		}catch(Exception ex){

		}

	}*/
	//creating custom marker
	@SuppressLint("InflateParams")
	View customMarker()
	{			CircularNetworkImageView netCircle=new CircularNetworkImageView(contextMap);

		View marker = ((LayoutInflater)trackContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
		ImageView childImage=(ImageView)marker.findViewById(R.id.child_icon);
			if(getIntent().getStringExtra("photo")!=null){
			// Picasso.with(context).load(Common.Relative_URL+photo).into(proimg);
			 Picasso.with(contextMap).load(Common.relativeurl+getIntent().getStringExtra("photo")).transform(new CircleTransform()).into(childImage);

			}else{
				 bitmap = ((BitmapDrawable)childImage.getDrawable()).getBitmap();	
				 childImage.setImageBitmap(netCircle.getRoundedShape(bitmap));
			}
		return marker;
	}

	private boolean isGooglePlayServicesAvailable() {
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (ConnectionResult.SUCCESS == status) {
			return true;
		} else {
			GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
			return false;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
			finish();
			return super.onOptionsItemSelected(item);
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
	//	menu.findItem(R.id.history).setIcon(new IconDrawable(this, IconValue.fa_history).colorRes(R.color.primary).actionBarSize());
		return super.onPrepareOptionsMenu(menu);
	}
	public static Bitmap createDrawableFromView(Context context, View view) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.buildDrawingCache();
		Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);

		return bitmap;
	}
	
	
	//Track Informatiion
	class TrackInfrmation extends AsyncTask<Void, String, String>{
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}
		@Override
		protected String doInBackground(Void... params) {
			String result="";
			try{
				HttpClient httpclient=new DefaultHttpClient();
				HttpPost httpost=new HttpPost(Common.TrackURL+"UserServiceAPI/Getlocation");
				List<NameValuePair> param=new ArrayList<NameValuePair>(1);
				param.add(new BasicNameValuePair("UserId", InvitedId));
				Log.e("Loc Req ", ""+InvitedId);

				httpost.setEntity(new UrlEncodedFormEntity(param));
				HttpResponse response = httpclient.execute(httpost);
				result=EntityUtils.toString(response.getEntity());
				Log.e("response", ""+result);
			}catch(Exception e){
				result=e.getMessage();
			}

			return result;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			parsingTrackInfo(result);

		}
	}
	
	
	
	public void parsingTrackInfo(String result) {
		try{
			  
				JSONObject joObject =new JSONObject(result);
				LocationDTO dmDetails=new LocationDTO();
				if(joObject.has("Lat")&&joObject.has("Lang")&&joObject.has("Time"))
				{	dmDetails.setLat(joObject.getString("Lat"));
				dmDetails.setLang(joObject.getString("Lang"));

				dmDetails.setTime(joObject.getString("Time"));
				dmDetails.setUserid(joObject.getString("Userid"));
				SetSales_person_location(dmDetails);
				}else {
					Common.showToast("Location Data Not Found!",this);

					
					/*pDialog.setCancelable(true);
					pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
				        @Override
				        public void onClick(SweetAlertDialog sDialog) {
                         contextMap.finish();
				        }
				    });
					pDialog.show();*/
				}


		}catch(Exception e){
			Log.e("Exception", ""+e);
		}
		
	}
	

	
	private void SetSales_person_location(LocationDTO dmDetails) {
		// TOD
		
		try{

			Bitmap bmpDefaulr=BitmapFactory.decodeResource(trackContext.getResources(), R.drawable.default_marker1
					);

			LatLng current = new LatLng(Double.parseDouble(dmDetails.getLat()), Double.parseDouble(dmDetails.getLang()));
			if(flag==0)  //when the first update comes, we have no previous points,hence this 
			{ 

				mMap.clear();
				prev=current;
				flag=1;
				cntMap=1;
				mp = new MarkerOptions();
				//Bitmap bmp=BitmapFactory.decodeResource(trackContext.getResources(), R.drawable.custom_marker);
				mp.position(current).icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(trackContext, customMarker())));
				ArrayList<String> address=getAddress(current.latitude,current.longitude);
				mp.title(address.get(0));
				mp.snippet(address.get(1));
				mark=mMap.addMarker(mp);
				mark.showInfoWindow();
			}
			else
			{
				if (cntMap==1) {
					mark=mMap.addMarker(mp);
					cntMap++;
				}else{
					mark.setIcon(BitmapDescriptorFactory.fromBitmap(bmpDefaulr));

					mMap.addPolyline((new PolylineOptions())
							.add(prev, current).width(6).color(Color.CYAN)
							.visible(true));
					prev=current;
					mp = new MarkerOptions();
				//	Bitmap bmp=BitmapFactory.decodeResource(trackContext.getResources(), R.drawable.custom_marker);
					mp.position(current).icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(trackContext, customMarker())));
					ArrayList<String> address=getAddress(current.latitude,current.longitude);
					mp.title(address.get(0));
					mp.snippet(address.get(1));
					mark=mMap.addMarker(mp);
					mark.showInfoWindow();
					current = null;

				}



			}
			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(dmDetails.getLat()),Double.parseDouble(dmDetails.getLang())), 15));

		}catch(Exception ex){

		}

	
		
	}

	private Bitmap getRoundedShape(Bitmap bitmap) {
		int targetWidth = 100;
		int targetHeight = 100;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, 
				targetHeight,Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), 
						((float) targetHeight)) / 2),
						Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = bitmap;
		canvas.drawBitmap(sourceBitmap, 
				new Rect(0, 0, sourceBitmap.getWidth(),
						sourceBitmap.getHeight()), 
						new Rect(0, 0, targetWidth, targetHeight), null);
		return targetBitmap;

	}

	public  long getUsedMemorySize() {
		try {
			Runtime info = Runtime.getRuntime();
			freeSize = info.freeMemory();
			totalSize = info.totalMemory();
			usedSize = totalSize - freeSize;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return usedSize;

	}
	public  String convertToStringRepresentation(final long value){
		final long[] dividers = new long[] { T, G, M, K, 1 };
		final String[] units = new String[] { "TB", "GB", "MB", "KB", "B" };
		if(value < 1)
			throw new IllegalArgumentException("Invalid file size: " + value);
		String result = null;
		for(int i = 0; i < dividers.length; i++){
			final long divider = dividers[i];
			if(value >= divider){
				result = format(value, divider, units[i]);
				break;
			}
		}
		return result;
	}
	private static String format(final long value,
			final long divider,
			final String unit){
		final double result =
				divider > 1 ? (double) value / (double) divider : (double) value;
				return new DecimalFormat("#,##0.#").format(result) + " " + unit;
	}



	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}


	public ArrayList<String> getAddress(double lat, double lng) {
		String   add = null;String postal=null;
		ArrayList<String>   addlist = new ArrayList<String>();

        Geocoder geocoder = new Geocoder(ShowMap.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
               add = obj.getAddressLine(0);
            String  currentAddress = obj.getSubAdminArea() + ","
                    + obj.getAdminArea();
            double   latitude = obj.getLatitude();
            double longitude = obj.getLongitude();
            String currentCity= obj.getSubAdminArea();
            String currentState= obj.getAdminArea();
            add = add + "\n" + obj.getLocality()+",";
            add = add + "\n" + obj.getSubAdminArea();
           
            addlist.add(add.replace("null", ""));
            postal="\n"+ obj.getAdminArea()+",";
            postal=postal+"\n"+ obj.getCountryName()+",";
            postal=postal+"\n"+ obj.getPostalCode()+"";
            addlist.add(postal.replace("null",""));



            Log.v("IGA", "Address" + add);
            // Toast.makeText(this, "Address=>" + add,
            // Toast.LENGTH_SHORT).show();

            // TennisAppActivity.showDialog(add);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        //    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
		return addlist;
    }

}


