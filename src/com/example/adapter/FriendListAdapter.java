package com.example.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.IconTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.db.DBHelper;
import com.example.dto.FrindsDTO;
import com.primesys.mitra.Common;
import com.primesys.mitra.DemoUserActivity;
import com.primesys.mitra.MainActivity;
import com.primesys.mitra.R;

public class FriendListAdapter extends BaseAdapter{

	Context currentContext;
	ArrayList<FrindsDTO> listFrnd;

	// constructor
	public FriendListAdapter(Context context,ArrayList<FrindsDTO> list) {
		this.currentContext=context;
		this.listFrnd=list;
	}

	@Override
	public int getCount() {
		return listFrnd.size();
	}

	@Override
	public FrindsDTO getItem(int position) {
		return listFrnd.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FrindsDTO frnd=getItem(position);
		UserViewHolder holder=null;

		if(convertView==null)
		{
			holder=new UserViewHolder();
			LayoutInflater inflater = ((Activity) currentContext).getLayoutInflater();
			convertView=inflater.inflate(R.layout.list_friends, null);
			holder.text_count=(TextView)convertView.findViewById(R.id.text_count);
			holder.userIcon=(IconTextView)convertView.findViewById(R.id.icon_user);
			holder.profile_pic=(ImageView)convertView.findViewById(R.id.profile_pic);
			convertView.setTag(holder);
		}
		else
		{
			holder=(UserViewHolder)convertView.getTag();
		}
		final TextView member_name=(TextView)convertView.findViewById(R.id.member_name);
		member_name.setText(frnd.getMemberName());
		member_name.setTag(position);
		if(frnd.isGroup())
			holder.userIcon.setText("{fa-users}");
		else
			holder.userIcon.setText("{fa-user}");
		int cnt=DBHelper.getInstance(currentContext).getCount(frnd.getMemberId()+"",frnd.isGroup());


		if(cnt!=0)
		{
			holder.text_count.setVisibility(View.VISIBLE);
			holder.text_count.setText(cnt+"");
		}
		else
			holder.text_count.setVisibility(View.GONE);
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i=new Intent(currentContext,MainActivity.class);
				Common.freindflag=true;
				Common.studID=null;
				i.putExtra("to",(listFrnd.get((Integer) member_name.getTag()).getMemberId())+"");
				i.putExtra("name", listFrnd.get((Integer) member_name.getTag()).getMemberName());
				i.putExtra("isGroup", listFrnd.get((Integer) member_name.getTag()).isGroup());
				i.putExtra(Intent.EXTRA_TEXT, DemoUserActivity.sharedMsg);
				((Activity)currentContext).startActivity(i);
				((Activity)currentContext).overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
		return convertView;
	}
	// the View holder for the above user list
	static class UserViewHolder
	{
		ImageView profile_pic;
		IconTextView userIcon;
		TextView text_count;
	}

}
