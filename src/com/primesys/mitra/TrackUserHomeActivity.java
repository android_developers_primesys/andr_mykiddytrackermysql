package com.primesys.mitra;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


public class TrackUserHomeActivity extends Activity{
	Button Btntrack,BtnComplaint;
	Context context=TrackUserHomeActivity.this;
	Bitmap G_image;
	MenuItem username;
	ByteArrayOutputStream stream;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_track_user);
		findViewById();


		if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
			new BitMapAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void)null);
		}
		else{
			new BitMapAsync().execute();
		}
		GPSEnableSetting gps=new GPSEnableSetting();
		gps.GPSDialog(context);

		Btntrack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try{


					Intent intent=new Intent(context,ShowGmapClient.class);
					//Toast.makeText(getApplicationContext(), ""+Common.schoolId, Toast.LENGTH_LONG).show();
					System.out.println("Student ID-------"+Common.schoolId);
					intent.putExtra("StudentId",Integer.parseInt(Common.schoolId));
					byte[] byteArray = stream.toByteArray();
					intent.putExtra("G_image", byteArray);
					startActivity(intent);


				}catch(Exception e){
					System.err.print("-hjjjjjjjjjj"+e);
				}

			}
		});
		BtnComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try{
					Intent intentemail=new Intent(context,TrackUserReportComplaint.class);
					startActivity(intentemail);
					overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
				}catch(Exception ex){
					System.err.print(ex);
				}


			}
		});
	}
	private void findViewById() {
		Btntrack=(Button)findViewById(R.id.img_track);
		BtnComplaint=(Button)findViewById(R.id.img_mail);
	}
	private Bitmap getRoundedShape(Bitmap bitmap) {
		int targetWidth = 100;
		int targetHeight = 100;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, 
				targetHeight,Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), 
						((float) targetHeight)) / 2),
						Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = bitmap;
		canvas.drawBitmap(sourceBitmap, 
				new Rect(0, 0, sourceBitmap.getWidth(),
						sourceBitmap.getHeight()), 
						new Rect(0, 0, targetWidth, targetHeight), null);
		return targetBitmap;

	}

	class BitMapAsync extends AsyncTask<Void, String, String>{

		@Override
		protected String doInBackground(Void... params) {
			String result="Success";
			try{
				G_image=getRoundedShape(BitmapFactory.decodeStream((InputStream)new URL(Common.relativeurl+Common.markerImg.replaceAll("~", "").trim()).getContent()));
			System.out.println("----------------------"+Common.relativeurl+G_image);
				stream = new ByteArrayOutputStream();
				G_image.compress(Bitmap.CompressFormat.PNG, 100, stream);
			}catch(Exception e){
				G_image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_boy);
			}
			return result;
		}

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
		startActivity(intent);
		finish();
		System.exit(0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_logout, menu);
		return super.onCreateOptionsMenu(menu);

	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		try{
			username=menu.findItem(R.id.username);
			username.setTitle(Common.username);
			username.setIcon(new IconDrawable(this, IconValue.fa_user).colorRes(R.color.primary).actionBarSize());
			menu.findItem(R.id.share).setIcon(new IconDrawable(this, IconValue.fa_share_alt).colorRes(R.color.primary).actionBarSize());
			menu.findItem(R.id.add).setIcon(new IconDrawable(this, IconValue.fa_map_marker).colorRes(R.color.primary).actionBarSize());
			menu.findItem(R.id.renew).setIcon(new IconDrawable(this, IconValue.fa_refresh).colorRes(R.color.primary).actionBarSize());
			menu.findItem(R.id.changepass).setIcon(new IconDrawable(this, IconValue.fa_pencil).colorRes(R.color.primary).actionBarSize());

		}catch(Exception ex){

		}
		return super.onPrepareOptionsMenu(menu);
	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.username:
			Intent intentProfile=new Intent(context,UserProfileActivity.class);
			startActivity(intentProfile);
			/*Common.showToast(getResources().getString(R.string.welcome)+Common.username, dHomeContext);*/
			return true;
		case R.id.add:
			Common.showToast(getResources().getString(R.string.demo_msg), context);
			return true;
		case R.id.share:
			shareIt();
			return true;
		case R.id.renew:
			Common.showToast(getResources().getString(R.string.demo_msg), context);
			return true;
		case R.id.changepass:

			if (Common.user_regType.equalsIgnoreCase("Google")||Common.user_regType.equalsIgnoreCase("Facebook")) {


			}else{
				Intent chIntent=new Intent(TrackUserHomeActivity.this,ChanagePassword.class);
				startActivity(chIntent);
			}


			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	// share the application
	private void shareIt() {
		//sharing implementation here
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "MyKiddyTrack");
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.sharemessaage));
		startActivity(sharingIntent);
	}



}
