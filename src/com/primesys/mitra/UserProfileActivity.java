package com.primesys.mitra;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.IconTextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.db.DBHelper;

public class UserProfileActivity  extends Activity implements TextWatcher{
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	ImageLoader imageLoader;
	IconTextView edit;
	final static int REQUEST_CAMERA=100;
	final static int SELECT_FILE=200;
	ProgressDialog progres;
	Context proContext=UserProfileActivity.this;
	EditText text_user,txt_address,txt_contactno,txt_email,text_gender,text_child1,text_child2;
	CircularNetworkImageView profile_pic;
	String docPath="",filename="";
	String strname,straddress,strcontactNo,strGender,stremail;
	public static final String TAG="ProfileTag";
	//For Camera Image 7/9/2015
		final int CAMERA_CAPTURE = 1;
		// keep track of cropping intent
		final int PIC_CROP = 2;
		// captured picture uri
		private Uri picUri;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

		FindViewById();
		Log.e(TAG, ""+Common.userid);


		text_user.addTextChangedListener(this);
		txt_address.addTextChangedListener(this);
		txt_contactno.addTextChangedListener(this);
		text_gender.addTextChangedListener(this);



		//Gte Image Form Gallery & Uplaod it
		edit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try{
					selectImage();
				}catch(Exception e)
				{
					Log.e(TAG, ""+e);
				}

			}
		});
		//Call APi Service
		try{
			if (Common.connectionStatus)  {
				//get Profile Information
				postprofileRequest();
				//get Childre Information
				postprofileGetChildName();
			}

		}catch(Exception e)
		{
			Log.e(TAG, e.getMessage());
		}

	}
		//GetchildInformation
	void postprofileGetChildName() {
		reuestQueue=Volley.newRequestQueue(proContext); //getting Request object from it
		/*	progres=new ProgressDialog(proContext);
		progres.setMessage("Please Wait...");
		progres.show();*/
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.URL + "ParentAPI.asmx/GetChildInfo",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				Log.e(TAG,""+response);
				parseChildJSON(response);
				progres.dismiss();
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				/*progres.dismiss();*/
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());

					parseChildJSON(new String(error.networkResponse.data));

				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("ParentId",Common.userid+"");
				return params;
			}

		};
		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);

	}

	//parsinog child infornmation 
	void parseChildJSON(String response) {
		try{
			JSONArray joarray=new JSONArray(response);
			for (int i = 0; i < joarray.length(); i++) {
				JSONObject jobject=joarray.getJSONObject(i);
				if(i==0) {
					text_child1.setVisibility(View.VISIBLE);
					text_child1.setText(jobject.getString("FirstName")+"\t"+jobject.getString("LastName"));

				}else{
					text_child2.setVisibility(View.VISIBLE);
					text_child2.setText(jobject.getString("FirstName")+"\t"+jobject.getString("LastName"));
				}
			}

		}catch(Exception e)
		{
			Log.e(TAG, "getchild"+e);
		}

	}

	void FindViewById() {
		text_user=(EditText)findViewById(R.id.text_user);
		text_gender=(EditText)findViewById(R.id.txt_gender);
		txt_email=(EditText)findViewById(R.id.txt_email);
		txt_address=(EditText)findViewById(R.id.txt_address);
		txt_contactno=(EditText)findViewById(R.id.txt_contactno);
		profile_pic=(CircularNetworkImageView)findViewById(R.id.profile_pic);
		text_child1=(EditText)findViewById(R.id.text_child1);
		text_child2=(EditText)findViewById(R.id.text_child2);
		edit=(IconTextView)findViewById(R.id.edit);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}





	//get Profile Information  Here
	void postprofileRequest()
	{
		reuestQueue=Volley.newRequestQueue(proContext); //getting Request object from it
		progres=new ProgressDialog(proContext);
		progres.setMessage("Please Wait...");
		progres.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		progres.setCancelable(false);
		progres.show();

		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,Common.URL+"ParentAPI.asmx/GetProfileInformation",new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				Log.e(TAG,""+response);
				try {
					progres.dismiss();
					parseProfileJSON(response);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				progres.dismiss();
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				progres.dismiss();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					try {
						parseProfileJSON(new String(error.networkResponse.data));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("UserId",Common.userid+"");
				return params;
			}

		};
		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}

	//Parsinlg Profile Information
	void parseProfileJSON(String result) throws JSONException
	{
		try
		{
				System.out.println("Profile Respo----"+result);
			JSONArray joarray=new JSONArray(result);
			for (int i = 0; i < joarray.length(); i++) {
				JSONObject jo= joarray.getJSONObject(i);
				text_user.setText(jo.getString("Name"));
				txt_address.setText(jo.getString("Address"));
				txt_contactno.setText(jo.getString("MobileNo"));
				txt_email.setText(jo.getString("EmailID"));
				if (jo.getString("Gender").contains("F")) {
					text_gender.setText("Female");	
				}else if(jo.getString("Gender").contains("M")) {
					text_gender.setText("Male");	
				}
				/*String Photo=jo.getString("Photo").trim();
				String Temp=Photo.replaceAll("~", " ").trim();
				ImageLoader.ImageCache imageCache = new LruBitmapCache();
				imageLoader = new ImageLoader(
						Volley.newRequestQueue(proContext), imageCache);
				(profile_pic).setImageUrl(
						Common.relativeurl+Temp.replaceAll(" ","%20"), imageLoader);
				((CircularNetworkImageView) profile_pic)
				.setDefaultImageResId(R.drawable.student);
				((CircularNetworkImageView) profile_pic)
				.setErrorImageResId(R.drawable.student);

				try{
					Bitmap bitmap = ((BitmapDrawable)profile_pic.getDrawable()).getBitmap();
					profile_pic.setImageBitmap(getRoundedShape(bitmap));
				}catch(Exception e){
					Log.e(TAG,""+e.getMessage());
				}*/

				String Photo=jo.getString("Photo").trim();

				Bitmap bitmap=null;
				bitmap=DBHelper.getInstance(proContext).getBitMap(Common.userid+"");
				if (bitmap != null) {
					profile_pic.setImageBitmap(getRoundedShape(bitmap));	
				}else{

					if(Photo.equalsIgnoreCase("No Photo")){

						Drawable myDrawable = proContext.getResources().getDrawable(R.drawable.student);
						Bitmap anImage      = ((BitmapDrawable) myDrawable).getBitmap();
						profile_pic.setImageBitmap(anImage);	
					}else{
					 Bitmap bmp = Common.bytebitmapconversion(Photo);
					profile_pic.setImageBitmap(getRoundedShape(bmp));}
				}
			}


		}	catch(Exception e)
		{
			Log.e(TAG, ""+e);
		}
	}


	// show the rounded Image 
	public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
		int targetWidth = 300;
		int targetHeight = 300;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, 
				targetHeight,Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), 
						((float) targetHeight)) / 2),
						Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, 
				new Rect(0, 0, sourceBitmap.getWidth(),
						sourceBitmap.getHeight()), 
						new Rect(0, 0, targetWidth, targetHeight), null);
		return targetBitmap;
	}

	// select image dialog that shows two option camera & select from gallery
	private void selectImage() {
		final CharSequence[] items = { "Take Photo", "Choose from Library",
		"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(proContext);
		builder.setTitle("Add Photo!");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (items[item].equals("Take Photo")) {
					/*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					File f = new File(android.os.Environment
							.getExternalStorageDirectory(), "temp.jpg");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
					startActivityForResult(intent, REQUEST_CAMERA);*/
					 
					//	14/10/2015	For Camera Image
					Intent captureIntent = new Intent(
							MediaStore.ACTION_IMAGE_CAPTURE);
					// we will handle the returned data in onActivityResult
					startActivityForResult(captureIntent, REQUEST_CAMERA);
					
				} else if (items[item].equals("Choose from Library")) {
					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					startActivityForResult(
							Intent.createChooser(intent, "Select File"),
							SELECT_FILE);
				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}
	// get the on Activity result back
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		//System.err.println("Data value :"+data.getData());
		if (resultCode == RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {
				/*File f = new File(Environment.getExternalStorageDirectory()
						.toString());
				for (File temp : f.listFiles()) {
					
				System.err.println("Data :"+temp);

					if (temp.getName().equals("temp.jpg")) {
						f = temp;
						break;
					}
				}
				try {
					Bitmap bm;
					BitmapFactory.Options btmapOptions = new BitmapFactory.Options();

					bm = BitmapFactory.decodeFile(f.getAbsolutePath(),
							btmapOptions);

					// bm = Bitmap.createScaledBitmap(bm, 70, 70, true);
					profile_pic.setImageBitmap(bm);

					String path = android.os.Environment
							.getExternalStorageDirectory()
							+ File.separator
							+ "Phoenix" + File.separator + "default";
					f.delete();
					OutputStream fOut = null;
					File file = new File(path, String.valueOf(System
							.currentTimeMillis()) + ".jpg");
					try {
						fOut = new FileOutputStream(file);
						bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
						fOut.flush();
						fOut.close();
						
						
						String tempPath = getPath(data.getData(), UserProfileActivity.this);

						try{
							FileUploadServer upload=new FileUploadServer();

							upload.FileToByteArrayConversionForParent(""+Common.userid, tempPath, Common.userid+".jpg");

							Common.showToast("photo Uploaded Successfully", proContext);
						}catch(Exception ex)
						{
							Log.e(TAG, ""+ex);
						}
						
						
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}*/
				
				
				// 07/09/2015 FOr Camera Image 
				picUri = data.getData();
				System.out.println("--------------"+picUri);
			//	Common.showToast(data.getData()+"", proContext);
				// carry out the crop operation
				performCrop();
			} 
			else if (requestCode == PIC_CROP) {
				// get the returned data
				Bundle extras = data.getExtras();
				// get the cropped bitmap
							Bitmap thePic = extras.getParcelable("data");
							
							
							//Upload Images On server 07/09/2015
							Common.showToast(data.getData()+"", proContext);
							String tempPath = getPath(picUri, UserProfileActivity.this);
							Bitmap bm;
							BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
							bm = BitmapFactory.decodeFile(tempPath, btmapOptions);
							//profile_pic.setImageBitmap(getRoundedShape(bm));
							// retrieve a reference to the ImageView
						//	ImageView picView = (ImageView) findViewById(R.id.profile_pic);
							// display the returned cropped image
							GraphicsUtil graphicUtil = new GraphicsUtil();
							// picView.setImageBitmap(graphicUtil.getRoundedShape(thePic,(float)1.5,92));
							
							profile_pic.setImageBitmap(graphicUtil.getCircleBitmap(
									thePic, 16));

							try{
								FileUploadServer upload=new FileUploadServer();

								upload.FileToByteArrayConversionForParent(""+Common.userid, tempPath, Common.userid+".jpg");

								Common.showToast(tempPath+"photo Uploaded Successfully",proContext);
							}catch(Exception ex)
							{
								Common.showToast(ex.getMessage(), proContext);
							}
							//For IMAge upload
							
	
			}
			
			//07/09/2015 For Image Crop
			
			 else if (requestCode == SELECT_FILE) {
				Uri selectedImageUri = data.getData();

				String tempPath = getPath(selectedImageUri, UserProfileActivity.this);
				Bitmap bm;
				BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
				bm = BitmapFactory.decodeFile(tempPath, btmapOptions);
				profile_pic.setImageBitmap(getRoundedShape(bm));

				try{
					FileUploadServer upload=new FileUploadServer();

					upload.FileToByteArrayConversionForParent(""+Common.userid, tempPath, Common.userid+".jpg");

					Common.showToast("photo Uploaded Successfully", proContext);
				}catch(Exception ex)
				{
					Log.e(TAG, ""+ex);
				}
			}
		}
	}
	// read Path of saved image from the sd card
	public String getPath(Uri uri, Activity activity) {
		String[] projection = { MediaColumns.DATA };
		@SuppressWarnings("deprecation")
		Cursor cursor = activity
		.managedQuery(uri, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {


	}
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		strname=text_user.getText().toString();
		strGender=text_gender.getText().toString();
		strcontactNo=txt_contactno.getText().toString();
		straddress=txt_address.getText().toString();
		stremail=txt_email.getText().toString();


		//send 'F ' as female & 'Male' as M
		if (strGender.equalsIgnoreCase("Female")) {
			strGender="F";
		}else if (strGender.contains("Male")) {
			strGender="M";
		}
		try{
			//Call Update API
			if (Validation()) {


				if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
					if (Common.connectionStatus) 
						new UpdateProfileAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
				}
				else{
					if (Common.connectionStatus) 
						new UpdateProfileAsync().execute();

				}}
		}catch(Exception ex)
		{
			Log.e(TAG, ""+ex);
		}


	}
	Boolean Validation() {
		Boolean Valid=true;
		 String 	strgender=text_gender.getText().toString().trim();
		if (strname.length()==0) {
			Valid=false;
		}
		if (strGender.length()==0) {
			Valid=false;
		}else if (!(strgender.equalsIgnoreCase("male")||strgender.equalsIgnoreCase("female"))) {
			text_gender.setError("Enter Proper gender");
			text_gender.requestFocus();
			Valid=false;
		}
		if (strcontactNo.length()==0) {
			Valid=false;
		}
		if (straddress.length()==0) {
			Valid=false;
		}

		return Valid;
	}

	@Override
	public void afterTextChanged(Editable s) {


	}

	public class UpdateProfileAsync extends AsyncTask< Void, String,String>{
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			/*	progres=new ProgressDialog(proContext);
			progres.setMessage("Profile Updating");
			progres.setTitle("Please Wait...!");

			progres.show();*/
		}
		@Override
		protected String doInBackground(Void... params) {
			String result="";
			try{

				HttpClient httpclient= new DefaultHttpClient();
				HttpPost httppost=new HttpPost(Common.URL+"ParentAPI.asmx/UpdteProfileInformation");              
				List<NameValuePair> param=new ArrayList<NameValuePair>(6);
				param.add(new BasicNameValuePair("UserId", Common.userid));
				param.add(new BasicNameValuePair("Name", strname));
				param.add(new BasicNameValuePair("Gender", strGender));
				param.add(new BasicNameValuePair("MobileNumber", strcontactNo));
				param.add(new BasicNameValuePair("EmailId", stremail));
				param.add(new BasicNameValuePair("Address", straddress));
				httppost.setEntity(new UrlEncodedFormEntity(param));
				HttpResponse response = httpclient.execute(httppost);
				result=EntityUtils.toString(response.getEntity());

			}catch(Exception ex)

			{
				Log.e(TAG, ""+ex);
			}

			return result;	
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			/*progres.dismiss();*/
			ParseReponse(result);

		}


	}
	//Parse Profile Uploaded JSON Response
	public void ParseReponse(String result) {
		try{
			/*	JSONArray joarray=new JSONArray(result);
			for (int i = 0; i < joarray.length(); i++) {
				JSONObject jobject=joarray.getJSONObject(i);

				if (jobject.getString("Message").contains("Information Updated Successfully")) {
					Common.showToast("Your profile updated Successfully..!", proContext);
				}

			}

			 */
		}catch(Exception ex)
		{
			Log.e(TAG, ""+ex);
		}
	}
	
	
	
	private void performCrop() {
		// take care of exceptions
		try {
			// call the standard crop action intent (the user device may not
			// support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			// indicate image type and Uri
			cropIntent.setDataAndType(picUri, "image/*");
			// set crop properties
			cropIntent.putExtra("crop", "true");
			// indicate aspect of desired crop
			cropIntent.putExtra("aspectX", 1);
			cropIntent.putExtra("aspectY", 1);
			// indicate output X and Y
			cropIntent.putExtra("outputX", 220);
			cropIntent.putExtra("outputY", 220);
			// retrieve data on return
			cropIntent.putExtra("return-data", true);
			// start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, PIC_CROP);
		}
		// respond to users whose devices do not support the crop action
		catch (ActivityNotFoundException anfe) {
			// display an error message
			String errorMessage = "Whoops - your device doesn't support the crop action!";
			Toast toast = Toast
					.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.show();
		}
	}

}
