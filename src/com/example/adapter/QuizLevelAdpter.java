package com.example.adapter;

import java.util.ArrayList;

import com.example.dto.CategoryDashbord;
import com.example.dto.QuizLevelDTO;
import com.primesys.mitra.ExamActivity;
import com.primesys.mitra.QuizzLevelActivity;
import com.primesys.mitra.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.CalendarContract.Colors;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class QuizLevelAdpter extends ArrayAdapter<QuizLevelDTO>{

	Context context;
	int layoutResourceId;
	ArrayList<QuizLevelDTO> data = new ArrayList<QuizLevelDTO>();
	ArrayList<QuizLevelDTO> datacopy = new ArrayList<QuizLevelDTO>();

	String CATEGORY_KEY="Category",LEVEL_NO="Levelno",QUIZID_KEY="Quiz_Id";
	public QuizLevelAdpter(Context context, int layoutResourceId,
			ArrayList<QuizLevelDTO> levelmetedata) {
		super(context, layoutResourceId,levelmetedata);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = levelmetedata;
		this.datacopy = levelmetedata;

	}

	
	
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;
		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			
			row.setTag(position);

		} else {
			row = convertView;
		//	holder = (RecordHolder) row.getTag();
		}
		
	//	CategoryDashbord item = data.get(position);
	
		
		final TextView level_label=(TextView)row.findViewById(R.id.lbl_level);
		level_label.setTag(position);

		LinearLayout level11=(LinearLayout)row.findViewById(R.id.level11);
		final	LinearLayout level=(LinearLayout)row.findViewById(R.id.level1);
		level.setTag(position);

		
		TextView score=(TextView)row.findViewById(R.id.txt_score);
		TextView status=(TextView)row.findViewById(R.id.txt_status);
		TextView rank=(TextView)row.findViewById(R.id.txt_rank);
    	QuizLevelDTO Cat=datacopy.get((Integer) level_label.getTag());

		level_label.setText(data.get(position).getLevel());
		if(data.get(position).getScore().equals("0")){
			level11.setVisibility(View.INVISIBLE);
		}else{
			score.setText(data.get(position).getScore());
			status.setText(data.get(position).getStatus());
			rank.setText(data.get(position).getRank());
			if (data.get(position).getStatus().equals("P")) {
				status.setTextColor(Color.parseColor("#00FF00"));
			}else {
				status.setTextColor(R.color.red);

			}
		
		}
	/*	level_label = (ImageView) row.findViewById(R.id.lbl_level);
		imageItem.setImageBitmap(item.getImage());*/

		level.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	
            	System.out.println("Inside Lvel onclick");

            	Intent intent=new Intent(context,ExamActivity.class);
            	QuizLevelDTO Cat=datacopy.get((Integer) level_label.getTag());
            	System.out.println("piositio))))))))))"+(Integer) level_label.getTag());

            	intent.putExtra(QUIZID_KEY,Cat.getQuizId());  
            	intent.putExtra(LEVEL_NO,Cat.getLevel());
            	intent.putExtra(CATEGORY_KEY,Cat.getCategory_label());
    			context.startActivity(intent);
    			((Activity) context).overridePendingTransition(R.anim.slide_in_up, R.anim.slide_in_out);
            }
        });

		return row;
		
		

	}

	static class RecordHolder {
		
	}
	
}
