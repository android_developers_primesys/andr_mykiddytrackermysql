package com.example.dto;

import java.io.Serializable;

public class MyKiddyTrackerContact implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String emailId,contactno,name,ID;

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	} 




}
