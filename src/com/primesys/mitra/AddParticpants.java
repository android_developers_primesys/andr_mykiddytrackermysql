package com.primesys.mitra;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.example.adapter.ParticipantAdapter;
import com.example.dto.FrindsDTO;
import com.example.dto.GroupInfo;

public class AddParticpants extends Activity {

	static ListView list_user;
	TextView textNews;
	Context addContext=AddParticpants.this;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	final String TAG=GroupDetails.class.getSimpleName();
	static ArrayList<FrindsDTO> listGrp;
	static ParticipantAdapter adapter;
	String grpId="0";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_diary_details);
		FindViewByID();
		grpId=getIntent().getStringExtra("GroupId");
		textNews=(TextView)findViewById(R.id.text_news);
		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		textNews.setTypeface(custom_font);
		textNews.setText(APIController.getInstance().getNews());
		list_user=(ListView)findViewById(R.id.list_diary);
		listGrp=new ArrayList<FrindsDTO>();
		adapter=new ParticipantAdapter(addContext, listGrp,grpId);
		if(Common.connectionStatus)
		{
			Common.flag=1;
			LoginActivity.mClient.sendMessage(getFrndList());
		}

	}
	private void FindViewByID() {
		// TODO Auto-generated method stub

	}
	String getFrndList()
	{
		String result="";
		try
		{
			JSONObject jo=new JSONObject();
			jo.put("event", "list_friends");
			jo.put("user_id",Common.userid);
			result=jo.toString();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	static void parseJSON(String result)
	{
		try
		{
			int flag=0;
			JSONObject jmain=new JSONObject(result);
			JSONArray jData=jmain.getJSONArray("data");
			for (int i = 0; i < jData.length(); i++) {
				flag=0;
				JSONObject jo=jData.getJSONObject(i);
				FrindsDTO frnd=new FrindsDTO();
				frnd.setMemberId(jo.getInt("friend_id"));
				frnd.setMemberName(jo.getString("friend_name"));
				frnd.setGroup(false);
				frnd.setStatus(jo.getString("status"));
				frnd.setSent(jo.getInt("sent"));
				for (GroupInfo g:MainActivity.listGrp)
				{
					if(Integer.parseInt(g.getParticipantId())==frnd.getMemberId())
					{
						flag=1;
						System.err.println(g.getParticipantId()+"  "+frnd.getMemberId());
					}
				}
				if(flag==0)
					listGrp.add(frnd);
			}
			adapter.notifyDataSetChanged();
			list_user.setAdapter(adapter);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}

	// onclick menu item
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
}
