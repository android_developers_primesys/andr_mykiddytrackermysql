package com.primesys.mitra;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class ChanagePassword extends Activity {

	EditText textCurrent,textNew,textConfirm; 
	ImageView backimage;
	Context contextpasss=ChanagePassword.this;
	TextView textheader;
	String curretPassword,newPassword,confirmPasssword;
	ProgressDialog dialog;
	Button btnSave,btnCancel;
	String strCurrent,strNew,strConfirm;
	TextView textNews;
	StringRequest stringRequest;
	RequestQueue reuestQueue;
	String TAG="ChangePassword";
	Context changeContext=ChanagePassword.this;
	private String url=Common.URL+"LoginServiceAPI.asmx/ChangePassword";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chanage_password);
		findViewByID();
		//show News & Events

		Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
		textNews.setTypeface(custom_font);
		textNews.setText(APIController.getInstance().getNews());


		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try{
					if(Validation()){
						textConfirm.setError(null);
						textCurrent.setError(null);
						textNew.setError(null);
						if(Common.connectionStatus)  
						{ 	
							postForgetPasswordREQ();
							setClear();
						}
					}
				}catch(Exception e)
				{
					Log.e("Exception", ""+e);
				}

			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setClear();
			}
		});

	}
	//VALIDATION ON SEVICE
	boolean Validation() {
		Boolean valid=true;
		strConfirm=textConfirm.getText().toString().trim();
		strCurrent=textCurrent.getText().toString().trim();
		strNew=textNew.getText().toString().trim();
		if (strCurrent.length()==0) {
			textCurrent.setError("Enter Current Password");
			textCurrent.requestFocus();
			valid=false;
		}else if (strNew.length()==0) {
			textNew.setError("Enter New Passoword");
			textNew.requestFocus();
			valid=false;
		}else if (strNew.length()<6) {
				textNew.setError("Passoword shouled Min 6 digit");
				textNew.requestFocus();
				valid=false;
		}else if (strConfirm.length()==0) {
			textConfirm.setError("Enter Confirm Password");
			textConfirm.requestFocus();
			valid=false;
		}else if (!strNew.equalsIgnoreCase(strConfirm)) {
			textConfirm.setError("Password Not Matched");
			textConfirm.requestFocus();
			valid=false;
		}
		return valid;
	}
	//findView by id
	void findViewByID()
	{
		textCurrent=(EditText)findViewById(R.id.text_current);
		textNew=(EditText)findViewById(R.id.text_new);
		textConfirm=(EditText)findViewById(R.id.text_confirm);
		btnSave=(Button)findViewById(R.id.btn_submit);
		btnCancel=(Button)findViewById(R.id.btn_Cancel);
		textNews=(TextView)findViewById(R.id.text_news);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_back, menu);
		return super.onCreateOptionsMenu(menu);

	}
	// onclick menu item 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.back:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	void postForgetPasswordREQ()
	{
		reuestQueue=Volley.newRequestQueue(changeContext); //getting Request object from it
		final ProgressDialog pDialog = new ProgressDialog(changeContext);
		pDialog.setTitle("Progress wait.......");
		pDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.animated_circle));
		pDialog.setCancelable(false);
		pDialog.show();
		//JSon object request for reading the json data
		stringRequest = new StringRequest(Method.POST,url,new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				parseJSON(response);
				pDialog.dismiss();
			}

		},
		new Response.ErrorListener() {


			@Override
			public void onErrorResponse(VolleyError error) {
				pDialog.hide();
				if(error.networkResponse != null && error.networkResponse.data != null){
					VolleyError er = new VolleyError(new String(error.networkResponse.data));
					error = er;
					System.out.println(error.toString());
					parseJSON(new String(error.networkResponse.data));
				}
			}
		}) {

			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<String, String>();
				params.put("UserId", Common.userid);
				params.put("CurrentPassword", strCurrent);
				params.put("NewPassword", strNew);
				return params;
			}

		};
		stringRequest.setTag(TAG);
		// Adding request to request queue
		reuestQueue.add(stringRequest);
	}
	//Parsing Response Coming From  server
	void parseJSON(String response) {
		try
		{

			JSONObject jo=new JSONObject(response);
			Log.e("Pasword Does't Exist", ""+response);
			if (jo.getString("msg").contains("Password Does Not Exist!")) {
				Common.showToast(jo.getString("msg"), changeContext);
				/*finish();*/
			}else{
				Intent i=new Intent(changeContext,LoginActivity.class);
				Common.showToast(jo.getString("msg"), changeContext);
				startActivity(i);
				finish();
			}



		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	//clear text fields
	public void setClear() {
		textNew.setText("");
		textNew.setError(null);
		textCurrent.setText("");
		textCurrent.setError(null);
		textConfirm.setText("");
		textConfirm.setError(null);
		textCurrent.requestFocus();
	}

}
