package com.example.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.example.dto.ParentStudent;
import com.primesys.mitra.ParentStudentInformation;
import com.primesys.mitra.R;

public class ParentStudentListAdapter extends ArrayAdapter<ParentStudent> {
	Context context;
	int layoutResourceId;
	public static ArrayList<ParentStudent> data;
	Intent Studeninfo;
	CheckBox selection;

	public ParentStudentListAdapter(Context context, int layoutResourceId,
			ArrayList<ParentStudent> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		ParentStudentListAdapter.data=new ArrayList<ParentStudent>();
		ParentStudentListAdapter.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layoutResourceId, parent, false);
			ParentStudent s = data.get(position);
			selection = (CheckBox) convertView.findViewById(R.id.selection);
			selection.setTag(position);
			selection.setChecked(ParentStudentInformation.selected);
			Log.d("status-get", "" + s.getSelectionStatus());
			Button btnEdit = (Button) convertView.findViewById(R.id.parentname);
			btnEdit.setText("\t\t"+" Name:" + "\t" + s.getStudentNmae());
			selection.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					ParentStudent user1=data.get((Integer)selection.getTag());
					if(selection.isChecked()){
						data.get((Integer)selection.getTag()).setSelectionStatus(true);
					}
					else{
						user1.setSelectionStatus(false);
						ParentStudentInformation.selection.setChecked(false);
					}
					boolean valid=true;
					for(ParentStudent su : data){
						if(!su.getSelectionStatus()){
							valid=false;
							break;
						}
					}
					if(valid)
						ParentStudentInformation.selection.setChecked(true);

				}
			});


		}
		return convertView;

	}
}
